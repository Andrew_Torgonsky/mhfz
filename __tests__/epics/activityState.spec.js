import { from } from 'rxjs';
import { reduce } from 'rxjs/operators';
import { Types, ActionCreators } from '../../src/redux/actions';
import { activityStateWatcher } from '../../src/redux/epics/activityState';
import * as activityStateApi from '../../src/api/activityState';

activityStateApi.activityState$ = jest.fn();

describe('activityStateWatcher saga', () => {
  it('turns observable of app states to obervable of actions', done => {
    activityStateApi.activityState$.mockReturnValue(from([
      'background',
      'active'
    ]));
    activityStateWatcher().pipe(
      reduce((acc, cur) => [...acc, cur], [])
    ).subscribe(data => {
      expect(data).toEqual([
        ActionCreators.appMinimized(),
        ActionCreators.appMaximized()
      ]);
      done();
    })
  });
});