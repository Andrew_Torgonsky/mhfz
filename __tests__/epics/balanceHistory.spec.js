import { Subject } from 'rxjs';
import { Types, ActionCreators } from '../../src/redux/actions';
import { balanceHistoryUpdateWsWatcher } from '../../src/redux/epics/balanceHistory';

describe('Balance history update watcher', () => {
  it('dispatches actions for triggering updates for corresponding currencies', () => {
    const mockActions$ = new Subject();
    const outputs = [];
    balanceHistoryUpdateWsWatcher(mockActions$).subscribe(data => outputs.push(data));
    mockActions$.next(ActionCreators.wsEvent({ type: 'balance', data: { updated: ['BTC', 'ETH'] } }));
    mockActions$.next(ActionCreators.wsEvent({ type: 'balance', data: { updated: ['ETH'] } }));
    mockActions$.next(ActionCreators.wsEvent({ type: 'balance', data: { updated: ['BTC', 'XRP'] } }));
    expect(outputs).toEqual([
      ActionCreators.updateBalanceHistory('bitcoin'),
      ActionCreators.updateBalanceHistory('ethereum'),
      ActionCreators.updateBalanceHistory('ethereum'),
      ActionCreators.updateBalanceHistory('bitcoin'),
      ActionCreators.updateBalanceHistory('ripple'),
    ]);
  });
});