import { from } from 'rxjs';
import { reduce } from 'rxjs/operators';
import { Types, ActionCreators } from '../../src/redux/actions';
import { rateUpdate } from '../../src/redux/epics/rates';

describe('rateUpdate epic', () => {
  it('listens to ws actions of exchange rate type and dispatches corresponding actions', done => {
    const mockActions$ = from([
      { type: 'random action which should be ignored', payload: 'random payload' },
      ActionCreators.wsEvent({ type: 'exchange_rate', data: { BTC: 3, ETH: 2, XRP: 1, currency: 'USD' } }),
      { type: 'one more random action which should be ignored', payload: 'random payload' },
      ActionCreators.wsEvent({ type: 'exchange_rate', data: { BTC: 4, XRP: 2, currency: 'EUR' } }),
    ]);
    rateUpdate(mockActions$).pipe(
      reduce((acc, cur) => [...acc, cur], [])
    ).subscribe(data => {
      expect(data).toEqual([
        ActionCreators.changeWalletsStat('bitcoin', 'rate', 3),
        ActionCreators.changeWalletsStat('ethereum', 'rate', 2),
        ActionCreators.changeWalletsStat('ripple', 'rate', 1),
        ActionCreators.changeSession('default_currency', 'usd'),
        ActionCreators.changeWalletsStat('bitcoin', 'rate', 4),
        ActionCreators.changeWalletsStat('ripple', 'rate', 2),
        ActionCreators.changeSession('default_currency', 'eur'),
      ]);
      done();
    });
  });
});