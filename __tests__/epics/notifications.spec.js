import { from } from 'rxjs';
import { reduce } from 'rxjs/operators';
import { Types, ActionCreators } from '../../src/redux/actions';
import { notifications } from '../../src/redux/epics/notifications';
import * as notificationsApi from '../../src/api/notifications';

notificationsApi.notifications$ = jest.fn();

describe('notifications epic', () => {
  it('turns observable of notifications into observable of actions which trigger updates of balances of corresponding currencies', done => {
    // mocks
    notificationsApi.notifications$.mockReturnValue(from([
      { payload: { additionalData: { currency: 'BTC' } } },
      {},
      { payload: { additionalData: { currency: 'ETH' } } },
      { payload: { additionalData: { currency: 'XRP' } } },
    ]));
    notifications().pipe(
      reduce((acc, cur) => [...acc, cur], [])
    ).subscribe(data => {
      expect(data).toEqual([
        ActionCreators.updateHistory('bitcoin', false),
        ActionCreators.requestBalances(),
        ActionCreators.updateHistory('ethereum', false),
        ActionCreators.requestBalances(),
        ActionCreators.updateHistory('ripple', false),
        ActionCreators.requestBalances(),
      ]);
      done();
    })
  });
});