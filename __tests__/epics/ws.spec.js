import { from, of, Subject, BehaviorSubject } from 'rxjs';
import { reduce } from 'rxjs/operators';
import { Types, ActionCreators } from '../../src/redux/actions';
import { INITIAL_STATE } from '../../src/redux/reducers';
import { wsConnection } from '../../src/redux/epics/ws';
import * as wsApi from '../../src/api/ws';

wsApi.ws$ = jest.fn();

describe('ws epic', () => {
  it('connects and disconnects from ws on corresponding actions with current session token', () => {
    const mockState$ = new BehaviorSubject({
      ...INITIAL_STATE,
      session: {
        ...INITIAL_STATE.session,
        token: 'asdfgh123'
      }
    });
    const mockActions$ = new Subject();
    const mockWSEvents$ = new Subject();
    wsApi.ws$.mockImplementation(() => {
      return mockWSEvents$;
    });
    const outputs = [];
    wsConnection(mockActions$, mockState$).subscribe(data => outputs.push(data));

    mockActions$.next(ActionCreators.startWs());
    mockWSEvents$.next('1');
    mockWSEvents$.next('2');
    mockWSEvents$.next('3');
    mockActions$.next(ActionCreators.stopWs());
    mockWSEvents$.next('4');
    mockWSEvents$.next('5');
    mockWSEvents$.next('6');
    mockState$.next({
      ...INITIAL_STATE,
      session: {
        ...INITIAL_STATE.session,
        token: 'qwe123'
      }
    });
    mockActions$.next(ActionCreators.startWs());
    mockWSEvents$.next('7');
    mockWSEvents$.next('8');
    mockActions$.next(ActionCreators.stopWs());
    mockWSEvents$.next('9');
    mockWSEvents$.next('10');
    // checks
    expect(outputs).toEqual([
      ActionCreators.wsEvent('1'),
      ActionCreators.wsEvent('2'),
      ActionCreators.wsEvent('3'),
      ActionCreators.wsEvent('7'),
      ActionCreators.wsEvent('8'),
    ]);
    expect(wsApi.ws$.mock.calls).toEqual([
      ['asdfgh123'],
      ['qwe123']
    ]);
  });
});