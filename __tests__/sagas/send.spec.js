import { cloneableGenerator, createMockTask } from 'redux-saga/utils';
import { runSaga, delay } from 'redux-saga';
import { Keyboard } from 'react-native';
import { Types, ActionCreators } from '../../src/redux/actions';
import { send } from '../../src/redux/sagas/blockchain';
import { INITIAL_STATE } from '../../src/redux/reducers';
import * as navigation from '../../src/navigation';
import translations from '../../src/translations';
import * as blockchainAPI from '../../src/api/blockchain';
import * as transactionsAPI from '../../src/api/transactions';
import * as config from '../../src/config';

navigation.goBack = jest.fn();
Keyboard.dismiss = jest.fn();
transactionsAPI.addTransaction = jest.fn();
blockchainAPI.bitcoin.send = jest.fn();
blockchainAPI.ethereum.send = jest.fn();
blockchainAPI.ripple.send = jest.fn();
config.USE_BLOCKCHAIN_TESTNET = false;


describe('send saga', () => {
  it('sends transaction if everything is ok', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => { };
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    }
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        addressToSend: '1DJs1kx2pDWBjsP26Kfreiugdg86HYdese',
        amountToSend: '2',
        currencyToSend: 'bitcoin',
        feeOptions: [{ name: 'regular', fee: 0.0001 }, { name: 'fast', fee: 0.0002 }],
        chosenFeeOption: 1,
        contactToSend: {
          firstName: 'John',
          lastName: 'Doe',
          photo: 'johndoe.png',
          emails: [],
          phoneNumbers: [],
          wallets: []
        }
      },
      session: {
        ...INITIAL_STATE.session,
        token: 'asdfgh'
      },
      wallets: {
        ...INITIAL_STATE.wallets,
        bitcoin: {
          walletAddress: 'randombtcaddress',
          privateKey: 'randomprivatekey'
        },
        ethereum: {
          walletAddress: 'randomethaddress',
          privateKey: 'randomprivatekey'
        },
        ripple: {
          walletAddress: 'randomxrpaddress',
          privateKey: 'randomprivatekey'
        }
      }
    };
    sagaParams.getState.mockReturnValue(initialState);
    transactionsAPI.addTransaction.mockResolvedValue();
    blockchainAPI.bitcoin.send.mockResolvedValue({ txid: 'randomtxid' });
    // starting saga
    const isSagaDone = runSaga(sagaParams, send, { toContact: false }).done;
    // finishing
    await isSagaDone;
    // checks
    // expect(sagaParams.dispatch.mock.calls).toEqual([

    // ])
    // reset mocks for next test
    navigation.goBack.mockReset();
    Keyboard.dismiss.mockReset();
    transactionsAPI.addTransaction.mockReset();
    blockchainAPI.bitcoin.send.mockReset();
    blockchainAPI.ethereum.send.mockReset();
    blockchainAPI.ripple.send.mockReset();
  });
});