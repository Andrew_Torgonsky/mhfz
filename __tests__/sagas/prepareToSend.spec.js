import { cloneableGenerator, createMockTask } from 'redux-saga/utils';
import { runSaga, delay } from 'redux-saga';
import { put, call, fork, select, all, take, cancel, takeLatest, takeEvery } from 'redux-saga/effects';
import { Types, ActionCreators } from '../../src/redux/actions';
import { getContacts } from '../../src/redux/sagas/contacts';
import { prepareToSend } from '../../src/redux/sagas/blockchain';
import { INITIAL_STATE } from '../../src/redux/reducers';
import * as blockchainAPI from '../../src/api/blockchain';

blockchainAPI.bitcoin.getFeeOptions = jest.fn();
blockchainAPI.ethereum.getFeeOptions = jest.fn();
blockchainAPI.ripple.getFeeOptions = jest.fn();

describe('Prepare to send saga', () => {
  it('works correctly if no contact to send or contact, address and currency are in sync', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        addressToSend: 'addr123',
        amountToSend: '12',
        currencyToSend: 'bitcoin',
      },
      wallets: {
        ...INITIAL_STATE.wallets,
        bitcoin: {
          walletAddress: 'addr456',
          privateKey: ''
        }
      }
    };
    const feeOptions = [
      { name: 'regular', priority: 6, fee: 0.001 },
      { name: 'fast', priority: 2, fee: 0.003 },
    ];
    sagaParams.getState.mockReturnValue(initialState);
    blockchainAPI.bitcoin.getFeeOptions.mockResolvedValue(feeOptions);
    // starting saga
    const isSagaDone = runSaga(sagaParams, prepareToSend).done;
    // finishing
    await isSagaDone;
    // check if it updates fee options and switches senPreparing flag to true while receiving fee options
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changeForm('sendPreparing', true)],
      [ActionCreators.changeForm('feeOptions', [])],
      [ActionCreators.changeForm('feeOptions', feeOptions)],
      [ActionCreators.changeForm('chosenFeeOption', 0)],
      [ActionCreators.changeForm('sendPreparing', false)],
    ]);
    // check if it requests feeoptions from api with addresses and amout taken from the store
    expect(blockchainAPI.bitcoin.getFeeOptions.mock.calls).toEqual([
      ['addr456', 'addr123', '12']
    ]);
    // resetting mocked api for the next test
    blockchainAPI.bitcoin.getFeeOptions.mockReset();
    blockchainAPI.ethereum.getFeeOptions.mockReset();
    blockchainAPI.ripple.getFeeOptions.mockReset();
  });
  // ============================================================================================================



  it(`syncronizes the differences between chosen contact address and currency to send
  and dispatches PREPARE_TO_SEND to be invoked again (CASE: address to send is not found in any of the contact's wallets)`, async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        addressToSend: 'addr123',
        amountToSend: '12',
        currencyToSend: 'bitcoin',
        contactToSend: {
          firstName: 'John',
          lastName: 'Doe',
          photo: 'john.jpg',
          emails: [],
          phoneNumbers: ['123456789'],
          wallets: [
            {
              walletAddress: 'address1',
              currency: 'bitcoin'
            },
            {
              walletAddress: 'address2',
              currency: 'ethereum'
            },
            {
              walletAddress: 'address3',
              currency: 'ripple'
            },
          ]
        }
      }
    };
    sagaParams.getState.mockReturnValue(initialState);
    // starting saga
    const isSagaDone = runSaga(sagaParams, prepareToSend).done;
    // finishing
    await isSagaDone;
    // check if it resets contact to send if address to send is not found among contact's wallets
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changeForm('sendPreparing', true)],
      [ActionCreators.changeForm('feeOptions', [])],
      [ActionCreators.changeForm('contactToSend', undefined)],
      [ActionCreators.prepareToSend()],
    ]);
    // resetting mocked api for the next test
    blockchainAPI.bitcoin.getFeeOptions.mockReset();
    blockchainAPI.ethereum.getFeeOptions.mockReset();
    blockchainAPI.ripple.getFeeOptions.mockReset();
  });
  // ============================================================================================================



  it(`syncronizes the differences between chosen contact address and currency to send
  and dispatches PREPARE_TO_SEND to be invoked again
  (CASE: address to send is found in the contact's wallets but currency to send doesn't correspond to currency of this wallet)`, async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        addressToSend: 'address2',
        amountToSend: '12',
        currencyToSend: 'bitcoin',
        contactToSend: {
          firstName: 'John',
          lastName: 'Doe',
          photo: 'john.jpg',
          emails: [],
          phoneNumbers: ['123456789'],
          wallets: [
            {
              walletAddress: 'address1',
              currency: 'bitcoin'
            },
            {
              walletAddress: 'address2',
              currency: 'ethereum'
            },
            {
              walletAddress: 'address3',
              currency: 'ripple'
            },
          ]
        }
      }
    };
    sagaParams.getState.mockReturnValue(initialState);
    // starting saga
    const isSagaDone = runSaga(sagaParams, prepareToSend).done;
    // finishing
    await isSagaDone;
    // check if it updates address to send if previous address to send was found among contact's wallets but currency to send was changed
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changeForm('sendPreparing', true)],
      [ActionCreators.changeForm('feeOptions', [])],
      [ActionCreators.changeForm('addressToSend', 'address1')],
      [ActionCreators.prepareToSend()],
    ]);
    // resetting mocked api for the next test
    blockchainAPI.bitcoin.getFeeOptions.mockReset();
    blockchainAPI.ethereum.getFeeOptions.mockReset();
    blockchainAPI.ripple.getFeeOptions.mockReset();
  });
});