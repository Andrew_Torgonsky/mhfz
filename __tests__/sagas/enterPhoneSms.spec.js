import { cloneableGenerator, createMockTask } from 'redux-saga/utils';
import { runSaga, delay } from 'redux-saga';
import { Keyboard } from 'react-native';
import { Types, ActionCreators } from '../../src/redux/actions';
import { enterPhoneSms } from '../../src/redux/sagas/settings';
import { INITIAL_STATE } from '../../src/redux/reducers';
import translations from '../../src/translations';
import * as settingsApi from '../../src/api/settings';
import * as navigation from '../../src/navigation';


navigation.navigate = jest.fn();
settingsApi.checkSmsCode = jest.fn();

describe('enterPhoneSms saga', () => {
  it('works as expected if everything is ok', async () => {
    // preparations
  let dispatchToSaga;
  const sagaParams = {
    subscribe: (sub) => {
      dispatchToSaga = sub;
      return () => {};
    },
    dispatch: jest.fn(),
    getState: jest.fn(),
  };
  // mocking values
  const initialState = {
    ...INITIAL_STATE,
    form: {
      ...INITIAL_STATE.form,
      smsCode: '1234',
      phonePrefix: '7',
      phoneNumber: '9513811456'
    },
    session: {
      ...INITIAL_STATE.session,
      token: 'token123'
    }
  };
  sagaParams.getState.mockReturnValue(initialState);
  settingsApi.checkSmsCode.mockResolvedValue({});
  // starting saga
  const isSagaDone = runSaga(sagaParams, enterPhoneSms).done;
  // finishing
  await isSagaDone;
  // checks
  expect(sagaParams.dispatch.mock.calls).toEqual([
    [ActionCreators.changePending('account', true)],
    [ActionCreators.changePending('account', false)],
    [ActionCreators.changeSession('phone_number', '79513811456')],
    [ActionCreators.changeFormMultiple({
      smsCode: '',
      phoneNumber: '',
    })],
    [ActionCreators.changeAlert(translations.t('PHONE_CHANGED'), 'green')]
  ]);
  expect(navigation.navigate.mock.calls).toEqual([
    [navigation.Scenes.Settings]
  ]);
  expect(settingsApi.checkSmsCode.mock.calls).toEqual([
    ['1234', 'token123']
  ]);
  // resetting mocks
  navigation.navigate.mockReset();
  settingsApi.checkSmsCode.mockReset();
  });
  it('stops and dispatches logout action if backend responds with unauthorized', async () => {
    // preparations
  let dispatchToSaga;
  const sagaParams = {
    subscribe: (sub) => {
      dispatchToSaga = sub;
      return () => {};
    },
    dispatch: jest.fn(),
    getState: jest.fn(),
  };
  // mocking values
  const initialState = {
    ...INITIAL_STATE,
    form: {
      ...INITIAL_STATE.form,
      smsCode: '1234',
      phonePrefix: '7',
      phoneNumber: '9513811456'
    },
    session: {
      ...INITIAL_STATE.session,
      token: 'token123'
    }
  };
  sagaParams.getState.mockReturnValue(initialState);
  settingsApi.checkSmsCode.mockResolvedValue({ error: 'UNAUTHORIZED' });
  // starting saga
  const isSagaDone = runSaga(sagaParams, enterPhoneSms).done;
  // finishing
  await isSagaDone;
  // checks
  expect(sagaParams.dispatch.mock.calls).toEqual([
    [ActionCreators.changePending('account', true)],
    [ActionCreators.changePending('account', false)],
    [ActionCreators.logOut()],
    [ActionCreators.changeForm('smsCode', '')]
  ]);
  expect(navigation.navigate.mock.calls).toEqual([
  ]);
  expect(settingsApi.checkSmsCode.mock.calls).toEqual([
    ['1234', 'token123']
  ]);
  // resetting mocks
  navigation.navigate.mockReset();
  settingsApi.checkSmsCode.mockReset();
  });
  it('navigates back to entering phone if backend responds with NO_TEMP_SESSION', async () => {
    // preparations
  let dispatchToSaga;
  const sagaParams = {
    subscribe: (sub) => {
      dispatchToSaga = sub;
      return () => {};
    },
    dispatch: jest.fn(),
    getState: jest.fn(),
  };
  // mocking values
  const initialState = {
    ...INITIAL_STATE,
    form: {
      ...INITIAL_STATE.form,
      smsCode: '1234',
      phonePrefix: '7',
      phoneNumber: '9513811456'
    },
    session: {
      ...INITIAL_STATE.session,
      token: 'token123'
    }
  };
  sagaParams.getState.mockReturnValue(initialState);
  settingsApi.checkSmsCode.mockResolvedValue({ error: 'NO_TEMP_SESSION' });
  // starting saga
  const isSagaDone = runSaga(sagaParams, enterPhoneSms).done;
  // finishing
  await isSagaDone;
  // checks
  expect(sagaParams.dispatch.mock.calls).toEqual([
    [ActionCreators.changePending('account', true)],
    [ActionCreators.changePending('account', false)],
    [ActionCreators.changeAlert(translations.t('CODE_EXPIRED'), 'red')],
    [ActionCreators.changeForm('smsCode', '')]
  ]);
  expect(navigation.navigate.mock.calls).toEqual([
    [navigation.Scenes.SettingsPhone]
  ]);
  expect(settingsApi.checkSmsCode.mock.calls).toEqual([
    ['1234', 'token123']
  ]);
  // resetting mocks
  navigation.navigate.mockReset();
  settingsApi.checkSmsCode.mockReset();
  });
  it('stops if code is wrong', async () => {
    // preparations
  let dispatchToSaga;
  const sagaParams = {
    subscribe: (sub) => {
      dispatchToSaga = sub;
      return () => {};
    },
    dispatch: jest.fn(),
    getState: jest.fn(),
  };
  // mocking values
  const initialState = {
    ...INITIAL_STATE,
    form: {
      ...INITIAL_STATE.form,
      smsCode: '1234',
      phonePrefix: '7',
      phoneNumber: '9513811456'
    },
    session: {
      ...INITIAL_STATE.session,
      token: 'token123'
    }
  };
  sagaParams.getState.mockReturnValue(initialState);
  settingsApi.checkSmsCode.mockResolvedValue({ error: 'BAD_INPUT' });
  // starting saga
  const isSagaDone = runSaga(sagaParams, enterPhoneSms).done;
  // finishing
  await isSagaDone;
  // checks
  expect(sagaParams.dispatch.mock.calls).toEqual([
    [ActionCreators.changePending('account', true)],
    [ActionCreators.changePending('account', false)],
    [ActionCreators.changeForm('smsCodeError', true)],
    [ActionCreators.changeAlert(translations.t('WRONG_CODE'), 'red')],
    [ActionCreators.changeForm('smsCode', '')],
  ]);
  expect(navigation.navigate.mock.calls).toEqual([
  ]);
  expect(settingsApi.checkSmsCode.mock.calls).toEqual([
    ['1234', 'token123']
  ]);
  // resetting mocks
  navigation.navigate.mockReset();
  settingsApi.checkSmsCode.mockReset();
  });
});