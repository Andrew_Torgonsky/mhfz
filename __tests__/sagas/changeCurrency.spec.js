import { cloneableGenerator, createMockTask } from 'redux-saga/utils';
import { runSaga, delay } from 'redux-saga';
import { Types, ActionCreators } from '../../src/redux/actions';
import { changeCurrency } from '../../src/redux/sagas/settings';
import { INITIAL_STATE } from '../../src/redux/reducers';
import translations from '../../src/translations';
import * as settingsApi from '../../src/api/settings';
import * as navigation from '../../src/navigation';

settingsApi.changeCurrency = jest.fn();


describe('changeCurrency saga', () => {
  it('changes currency and updates rates if everything is ok', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => { };
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      session: {
        ...INITIAL_STATE.session,
        token: 'token123',
        default_currency: 'usd'
      }
    };
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.changeCurrency.mockResolvedValue({ bitcoin: 3, ethereum: 2, ripple: 1, currency: 'eur' });
    // starting saga
    const isSagaDone = runSaga(sagaParams, changeCurrency, { currency: 'eur' }).done;
    // finishing
    await isSagaDone;
    // checks
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changePending('account', true)],
      [ActionCreators.changeWalletsStat('bitcoin', 'rate', 3)],
      [ActionCreators.changeWalletsStat('ethereum', 'rate', 2)],
      [ActionCreators.changeWalletsStat('ripple', 'rate', 1)],
      [ActionCreators.changeSession('default_currency', 'eur')],
      [ActionCreators.changePending('account', false)],
    ]);
    expect(settingsApi.changeCurrency).toHaveBeenCalledWith('eur', 'token123');
    // reset mocks
    settingsApi.changeCurrency.mockReset();
  });
  it('stops and dispatches error message if something went wrong', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => { };
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      session: {
        ...INITIAL_STATE.session,
        token: 'token123',
        default_currency: 'usd'
      }
    };
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.changeCurrency.mockResolvedValue({ error: 'SOMETHING_ELSE' });
    // starting saga
    const isSagaDone = runSaga(sagaParams, changeCurrency, { currency: 'eur' }).done;
    // finishing
    await isSagaDone;
    // checks
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changePending('account', true)],
      [ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red')],
      [ActionCreators.changePending('account', false)],
    ]);
    expect(settingsApi.changeCurrency).toHaveBeenCalledWith('eur', 'token123');
    // reset mocks
    settingsApi.changeCurrency.mockReset();
  });
})