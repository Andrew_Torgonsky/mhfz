import { cloneableGenerator, createMockTask } from 'redux-saga/utils';
import { runSaga, delay } from 'redux-saga';
import { Types, ActionCreators } from '../../src/redux/actions';
import { setNewPinCode } from '../../src/redux/sagas/settings';
import { INITIAL_STATE } from '../../src/redux/reducers';
import * as navigation from '../../src/navigation';
import translations from '../../src/translations';
import * as Keychain from '../../src/utils/keychain';

navigation.navigate = jest.fn();
Keychain.set = jest.fn();

describe('setNewPinCode saga', () => {
  it('sets new pin if entered codes match', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => { };
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    }
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        pinCode: '1234',
        pinCodeConfirm: '1234'
      },
      session: {
        ...INITIAL_STATE.session,
        token: 'asdfg1234'
      },
      wallets: {
        bitcoin: {
          walletAddress: '1234asd',
          privateKey: '545y4dfg'
        },
        ethereum: {
          walletAddress: '123dsf',
          privateKey: '545y4asadfg'
        },
        ripple: {
          walletAddress: '1234123asd',
          privateKey: '545y4asddfg'
        }
      }
    }
    sagaParams.getState.mockReturnValue(initialState);
    // starting saga
    const isSagaDone = runSaga(sagaParams, setNewPinCode).done;
    // finishing
    await isSagaDone;
    // checks
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changeFormMultiple({
        pinCode: '',
        pinCodeConfirm: ''
      })],
      [ActionCreators.changeAlert(translations.t('PIN_CHANGED'), 'green')]
    ]);
    expect(navigation.navigate.mock.calls).toEqual([
      [navigation.Scenes.Settings]
    ]);
    expect(Keychain.set.mock.calls).toEqual([
      ['mahfazahToken', '1234', 'asdfg1234'],
      ['mahfazahWallets', '1234', JSON.stringify({
        bitcoin: {
          walletAddress: '1234asd',
          privateKey: '545y4dfg'
        },
        ethereum: {
          walletAddress: '123dsf',
          privateKey: '545y4asadfg'
        },
        ripple: {
          walletAddress: '1234123asd',
          privateKey: '545y4asddfg'
        }
      })]
    ]);
    // reset mocks for next test
    Keychain.set.mockReset();
    navigation.navigate.mockReset();
  });
  it('dispatches error actions if entered codes dont match', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => { };
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    }
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        pinCode: '1234',
        pinCodeConfirm: '5678'
      },
      session: {
        ...INITIAL_STATE.session,
        token: 'asdfg1234'
      },
      wallets: {
        bitcoin: {
          walletAddress: '1234asd',
          privateKey: '545y4dfg'
        },
        ethereum: {
          walletAddress: '123dsf',
          privateKey: '545y4asadfg'
        },
        ripple: {
          walletAddress: '1234123asd',
          privateKey: '545y4asddfg'
        }
      }
    }
    sagaParams.getState.mockReturnValue(initialState);
    // starting saga
    const isSagaDone = runSaga(sagaParams, setNewPinCode).done;
    // finishing
    await isSagaDone;
    // checks
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changeForm('pinCodeError', true)],
      [ActionCreators.changeAlert(translations.t('PIN_NOT_MATCH'), 'red')],
      [ActionCreators.changeFormMultiple({
        pinCodeError: false,
        pinCode: '',
        pinCodeConfirm: ''
      })],
      [ActionCreators.changeAlert('', '')]
    ]);
    expect(navigation.navigate.mock.calls).toEqual([
      [navigation.Scenes.SettingsPinEnter]
    ]);
    expect(Keychain.set.mock.calls).toEqual([]);
    // reset mocks for next test
    Keychain.set.mockReset();
    navigation.navigate.mockReset();
  });
});