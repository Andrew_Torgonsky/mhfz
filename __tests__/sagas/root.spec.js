
import { cloneableGenerator, createMockTask } from 'redux-saga/utils';
import { put, call, fork, select, all, take, cancel, takeLatest, takeEvery } from 'redux-saga/effects';
import { Types, ActionCreators } from '../../src/redux/actions';
import rootSaga, { startUp, logOut, minimize } from '../../src/redux/sagas';
import { changeAlert } from '../../src/redux/sagas/other';
import { INITIAL_STATE } from '../../src/redux/reducers';
import { languageInit } from '../../src/translations';

describe('Root saga', () => {
  const gen = cloneableGenerator(rootSaga)();
  const mocktask = createMockTask();
  let gen1; // for logout case
  let gen2; // for minimize case
  let gen3; // for minimize case when prevent minimize is true
  it('starts with setting language', () => {
    expect(gen.next().value).toEqual(call(languageInit));
  });
  it('starts to listen all CHANGE_ALERT actions in the beginning', () => {
    expect(gen.next().value).toEqual(takeEvery(Types.CHANGE_ALERT, changeAlert));
  });
  it('runs startUp saga', () => {
    expect(gen.next().value).toEqual(fork(startUp));
  });
  it('waits for logout or minimization', () => {
    expect(gen.next(mocktask).value).toEqual(take([Types.LOG_OUT, Types.APP_MINIMIZED]));
  });
  // log out case
  it('asks for state when LOG_OUT action was dispatched', () => {
    gen1 = gen.clone();
    expect(gen1.next(ActionCreators.logOut()).value).toEqual(select());
  });
  it('(in log out case) proceeds to close websocket', () => {
    expect(gen1.next(INITIAL_STATE).value).toEqual(put(ActionCreators.stopWs()));
  });
  it('(in log out case) proceeds to cancel startUp saga', () => {
    expect(gen1.next().value).toEqual(cancel(mocktask));
  });
  it('after cancellation it calls logOut saga', () => {
    expect(gen1.next().value).toEqual(call(logOut));
  });
  it('after logOut it forks startUp saga again', () => {
    expect(gen1.next().value).toEqual(fork(startUp));
  });
  // minimize case
  it('asks for state when APP_MINIMIZED action was dispatched', () => {
    gen2 = gen.clone();
    expect(gen2.next(ActionCreators.appMinimized()).value).toEqual(select());
  });
    // if preventMinimize is true
  it('does nothing and waits for minimize or log out actions if preventMinimize is true in APP_MINIMIZED case', () => {
    gen3 = gen2.clone();
    expect(gen3.next({
      ...INITIAL_STATE,
      preventMinimize: true
    }).value).toEqual(take([Types.LOG_OUT, Types.APP_MINIMIZED]));
  });
    // if preventMinimize is false
  it('closes websocket if APP_MINIMIZED dispatched and preventMinimize is false', () => {
    expect(gen2.next({
      ...INITIAL_STATE,
      preventMinimize: false
    }).value).toEqual(put(ActionCreators.stopWs()));
  });
  it('cancells startUpTasks after closing ws', () => {
    expect(gen2.next().value).toEqual(cancel(mocktask));
  });
  it('calls minimize saga after cancelation of startUpTasks', () => {
    expect(gen2.next().value).toEqual(call(minimize));
  });
  it('runs startUp saga again after minimize saga', () => {
    expect(gen2.next().value).toEqual(fork(startUp));
  });
});