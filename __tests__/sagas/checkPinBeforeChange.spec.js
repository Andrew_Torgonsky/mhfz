import { cloneableGenerator, createMockTask } from 'redux-saga/utils';
import { runSaga, delay } from 'redux-saga';
import { Types, ActionCreators } from '../../src/redux/actions';
import { checkPinBeforeChange } from '../../src/redux/sagas/settings';
import { INITIAL_STATE } from '../../src/redux/reducers';
import * as navigation from '../../src/navigation';
import translations from '../../src/translations';
import * as Keychain from '../../src/utils/keychain';

navigation.navigate = jest.fn();
Keychain.get = jest.fn();

describe('checkPinBeforeChange saga', () => {
  it('works as expected when everything is ok', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    }
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        pinCode: '1234'
      },
    }
    sagaParams.getState.mockReturnValue(initialState);
    Keychain.get.mockImplementation((key) => {
      if (key === 'mahfazahToken') return Promise.resolve('asdf1234')
      if (key === 'mahfazahWallets') return Promise.resolve('{"bitcoin":{"privateKey":"123","walletAddress":"456"},"ethereum":{"privateKey":"123","walletAddress":"456"},"ripple":{"privateKey":"123","walletAddress":"456"}}');
      return Promise.resolve(false);
    })
    // starting saga
    const isSagaDone = runSaga(sagaParams, checkPinBeforeChange).done;
    // finishing
    await isSagaDone;
    // checks
    expect(Keychain.get.mock.calls).toEqual([['mahfazahToken', '1234'], ['mahfazahWallets', '1234']]);
    expect(navigation.navigate.mock.calls).toEqual([[navigation.Scenes.SettingsPinEnter]]);
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changeForm('pinCode', '')]
    ]);
    // reset mocks for next test
    navigation.navigate.mockReset();
    Keychain.get.mockReset();
  });
  it('dispatches corresponding actions if keychain doesnt return wallets or token', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    }
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        pinCode: '1234'
      },
    }
    sagaParams.getState.mockReturnValue(initialState);
    Keychain.get.mockImplementation((key) => {
      if (key === 'mahfazahToken') return Promise.resolve('asdf1234')
      if (key === 'mahfazahWallets') return Promise.resolve(false);
      return Promise.resolve(false);
    })
    // starting saga
    const isSagaDone = runSaga(sagaParams, checkPinBeforeChange).done;
    // finishing
    await isSagaDone;
    // checks
    expect(Keychain.get.mock.calls).toEqual([['mahfazahToken', '1234'], ['mahfazahWallets', '1234']]);
    expect(navigation.navigate.mock.calls).toEqual([]);
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changeForm('pinCodeError', true)],
      [ActionCreators.changeAlert(translations.t('WRONG_PIN'), 'red')],
      [ActionCreators.changeFormMultiple({
        pinCodeError: false,
        pinCode: '',
        pinCodeConfirm: ''
      })],
      [ActionCreators.changeAlert('', '')]
    ]);
    // reset mocks for next test
    navigation.navigate.mockReset();
    Keychain.get.mockReset();
  });
});