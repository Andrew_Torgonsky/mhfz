import { cloneableGenerator, createMockTask } from 'redux-saga/utils';
import { runSaga, delay } from 'redux-saga';
import { Keyboard } from 'react-native';
import { Types, ActionCreators } from '../../src/redux/actions';
import { editAccount } from '../../src/redux/sagas/settings';
import { INITIAL_STATE } from '../../src/redux/reducers';
import * as settingsApi from '../../src/api/settings';
import * as navigation from '../../src/navigation';
import translations from '../../src/translations';

Keyboard.dismiss = jest.fn();
settingsApi.changeNameAndEmail = jest.fn();
navigation.navigate = jest.fn();

describe('Edit account saga', () => {
  it('works as expected when everything is ok', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    }
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        name: 'vasya pupkin',
        email: 'vasya@pupkin.ru'
      },
      session: {
        ...INITIAL_STATE.session,
        name: 'ivanov ivan',
        email: 'ivanov@ivan.ivanovich',
        token: '123asd'
      }
    }
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.changeNameAndEmail.mockResolvedValue({});
    // starting saga
    const isSagaDone = runSaga(sagaParams, editAccount).done;
    // finishing
    await isSagaDone;
    // checks
    expect(Keyboard.dismiss.mock.calls).toEqual([[]]);
    expect(settingsApi.changeNameAndEmail.mock.calls).toEqual([['vasya pupkin', 'vasya@pupkin.ru', '123asd']]);
    expect(navigation.navigate.mock.calls).toEqual([[navigation.Scenes.Account]]);
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changePending('account', true)],
      [ActionCreators.changeSessionMultiple({
        name: 'vasya pupkin',
        email: 'vasya@pupkin.ru'
      })],
      [ActionCreators.changeAlert(translations.t('CHANGES_SAVED'), 'green')],
      [ActionCreators.changeFormMultiple({ 'name': '', 'email': '' })],
      [ActionCreators.changePending('account', false)]
    ]);
    // reset mocks for next test
    Keyboard.dismiss.mockReset();
    settingsApi.changeNameAndEmail.mockReset();
    navigation.navigate.mockReset();
  });
  it('stops if new email is invalid', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    }
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        name: 'vasya pupkin',
        email: 'invalidemail'
      },
      session: {
        ...INITIAL_STATE.session,
        name: 'ivanov ivan',
        email: 'ivanov@ivan.ivanovich',
        token: '123asd'
      }
    }
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.changeNameAndEmail.mockResolvedValue({});
    // starting saga
    const isSagaDone = runSaga(sagaParams, editAccount).done;
    // finishing
    await isSagaDone;
    // checks
    expect(Keyboard.dismiss.mock.calls).toEqual([]);
    expect(settingsApi.changeNameAndEmail.mock.calls).toEqual([]);
    expect(navigation.navigate.mock.calls).toEqual([]);
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changeForm('emailError', true)],
      [ActionCreators.changeAlert(translations.t('INVALID_EMAIL'), 'red')]
    ]);
    // reset mocks for next test
    Keyboard.dismiss.mockReset();
    settingsApi.changeNameAndEmail.mockReset();
    navigation.navigate.mockReset();
  });
  it('dispatches logout action if backend responses with unauthirized error', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    }
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        name: 'vasya pupkin',
        email: 'vasya@pupkin.ru'
      },
      session: {
        ...INITIAL_STATE.session,
        name: 'ivanov ivan',
        email: 'ivanov@ivan.ivanovich',
        token: '123asd'
      }
    }
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.changeNameAndEmail.mockResolvedValue({ error: 'UNAUTHORIZED' });
    // starting saga
    const isSagaDone = runSaga(sagaParams, editAccount).done;
    // finishing
    await isSagaDone;
    // checks
    expect(Keyboard.dismiss.mock.calls).toEqual([[]]);
    expect(settingsApi.changeNameAndEmail.mock.calls).toEqual([['vasya pupkin', 'vasya@pupkin.ru', '123asd']]);
    expect(navigation.navigate.mock.calls).toEqual([]);
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changePending('account', true)],
      [ActionCreators.logOut()],
      [ActionCreators.changePending('account', false)]
    ]);
    // reset mocks for next test
    Keyboard.dismiss.mockReset();
    settingsApi.changeNameAndEmail.mockReset();
    navigation.navigate.mockReset();
  });
  it('it stops on any error from backend', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    }
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        name: 'vasya pupkin',
        email: 'vasya@pupkin.ru'
      },
      session: {
        ...INITIAL_STATE.session,
        name: 'ivanov ivan',
        email: 'ivanov@ivan.ivanovich',
        token: '123asd'
      }
    }
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.changeNameAndEmail.mockResolvedValue({ error: 'ANY_ERROR' });
    // starting saga
    const isSagaDone = runSaga(sagaParams, editAccount).done;
    // finishing
    await isSagaDone;
    // checks
    expect(Keyboard.dismiss.mock.calls).toEqual([[]]);
    expect(settingsApi.changeNameAndEmail.mock.calls).toEqual([['vasya pupkin', 'vasya@pupkin.ru', '123asd']]);
    expect(navigation.navigate.mock.calls).toEqual([]);
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changePending('account', true)],
      [ActionCreators.changePending('account', false)]
    ]);
    // reset mocks for next test
    Keyboard.dismiss.mockReset();
    settingsApi.changeNameAndEmail.mockReset();
    navigation.navigate.mockReset();
  });
});
