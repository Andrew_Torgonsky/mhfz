import { cloneableGenerator, createMockTask } from 'redux-saga/utils';
import { runSaga, delay } from 'redux-saga';
import { Keyboard } from 'react-native';
import { Types, ActionCreators } from '../../src/redux/actions';
import { changePhone } from '../../src/redux/sagas/settings';
import { INITIAL_STATE } from '../../src/redux/reducers';
import * as settingsApi from '../../src/api/settings';
import * as navigation from '../../src/navigation';
import translations from '../../src/translations';


navigation.navigate = jest.fn();
Keyboard.dismiss = jest.fn();
settingsApi.enterNewPhone = jest.fn();
Date.now = jest.fn();
Date.now.mockReturnValue(123456789);

describe('change phone saga', () => {
  it('works as expected if everithing is ok', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        phonePrefix: '966',
        phoneNumber: '123456789'
      },
      session: {
        ...INITIAL_STATE.session,
        token: 'token123',
        phone_number: '79513811456'
      }
    };
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.enterNewPhone.mockResolvedValue({});
    
    // starting saga
    const isSagaDone = runSaga(sagaParams, changePhone).done;
    // finishing
    await isSagaDone;
    // checks
    expect(Keyboard.dismiss.mock.calls).toEqual([[]]);
    expect(settingsApi.enterNewPhone.mock.calls).toEqual([['966123456789', 'token123']]);
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changePending('account', true)],
      [ActionCreators.changePending('account', false)],
      [ActionCreators.changeForm('smsCodeTimestamp', 123456789)]
    ]);
    expect(navigation.navigate.mock.calls).toEqual([
      [navigation.Scenes.SettingsCode]
    ]);
    // reset mocks for next test
    navigation.navigate.mockReset();
    Keyboard.dismiss.mockReset();
    settingsApi.enterNewPhone.mockReset();
  });
  it('stops as expected if new number is invalid', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        phonePrefix: '966',
        phoneNumber: '123'
      },
      session: {
        ...INITIAL_STATE.session,
        token: 'token123',
        phone_number: '79513811456'
      }
    };
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.enterNewPhone.mockResolvedValue({});
    
    // starting saga
    const isSagaDone = runSaga(sagaParams, changePhone).done;
    // finishing
    await isSagaDone;
    // checks
    expect(Keyboard.dismiss.mock.calls).toEqual([]);
    expect(settingsApi.enterNewPhone.mock.calls).toEqual([]);
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changeForm('phoneError', true)],
      [ActionCreators.changeAlert(translations.t('WRONG_PHONE'), 'red')],
    ]);
    expect(navigation.navigate.mock.calls).toEqual([]);
    // reset mocks for next test
    navigation.navigate.mockReset();
    Keyboard.dismiss.mockReset();
    settingsApi.enterNewPhone.mockReset();
  });
  it('stops as expected if new number matches the old one', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        phonePrefix: '7',
        phoneNumber: '9513811456'
      },
      session: {
        ...INITIAL_STATE.session,
        token: 'token123',
        phone_number: '79513811456'
      }
    };
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.enterNewPhone.mockResolvedValue({});
    
    // starting saga
    const isSagaDone = runSaga(sagaParams, changePhone).done;
    // finishing
    await isSagaDone;
    // checks
    expect(Keyboard.dismiss.mock.calls).toEqual([]);
    expect(settingsApi.enterNewPhone.mock.calls).toEqual([]);
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changeForm('phoneError', true)],
      [ActionCreators.changeAlert(translations.t('PHONE_MATCH'), 'red')],
    ]);
    expect(navigation.navigate.mock.calls).toEqual([]);
    // reset mocks for next test
    navigation.navigate.mockReset();
    Keyboard.dismiss.mockReset();
    settingsApi.enterNewPhone.mockReset();
  });
  it('dispatches log out action if gets unauthorized error from backend', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        phonePrefix: '966',
        phoneNumber: '123456789'
      },
      session: {
        ...INITIAL_STATE.session,
        token: 'token123',
        phone_number: '79513811456'
      }
    };
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.enterNewPhone.mockResolvedValue({ error: 'UNAUTHORIZED' });
    
    // starting saga
    const isSagaDone = runSaga(sagaParams, changePhone).done;
    // finishing
    await isSagaDone;
    // checks
    expect(Keyboard.dismiss.mock.calls).toEqual([[]]);
    expect(settingsApi.enterNewPhone.mock.calls).toEqual([['966123456789', 'token123']]);
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changePending('account', true)],
      [ActionCreators.changePending('account', false)],
      [ActionCreators.logOut()]
    ]);
    expect(navigation.navigate.mock.calls).toEqual([]);
    // reset mocks for next test
    navigation.navigate.mockReset();
    Keyboard.dismiss.mockReset();
    settingsApi.enterNewPhone.mockReset();
  });
});