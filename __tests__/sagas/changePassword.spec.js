import { cloneableGenerator, createMockTask } from 'redux-saga/utils';
import { runSaga, delay } from 'redux-saga';
import { Keyboard } from 'react-native';
import { Types, ActionCreators } from '../../src/redux/actions';
import { changePassword } from '../../src/redux/sagas/settings';
import { INITIAL_STATE } from '../../src/redux/reducers';
import * as settingsApi from '../../src/api/settings';
import * as navigation from '../../src/navigation';
import translations from '../../src/translations';


navigation.navigate = jest.fn();
Keyboard.dismiss = jest.fn();
settingsApi.changePassword = jest.fn();

describe('change password saga', () => {
  it('works properly if everything is ok', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        password: 'Qwe123!!!',
        password2nd: 'Asd123!!!'
      },
      session: {
        ...INITIAL_STATE.session,
        token: 'qwe123'
      }
    };
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.changePassword.mockResolvedValue({});
    // starting saga
    const isSagaDone = runSaga(sagaParams, changePassword).done;
    // finishing
    await isSagaDone;
    // checks
    expect(Keyboard.dismiss.mock.calls).toEqual([[]]);
    expect(settingsApi.changePassword.mock.calls).toEqual([['Asd123!!!', 'Qwe123!!!', 'qwe123']]);
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changePending('account', true)],
      [ActionCreators.changePending('account', false)],
      [ActionCreators.changeAlert(translations.t('PWD_CHANGED'), 'green')],
      [ActionCreators.changeFormMultiple({
        password: '',
        password2nd: ''
      })]
    ]);
    expect(navigation.navigate.mock.calls).toEqual([
      [navigation.Scenes.Settings]
    ]);
    // reset mocks for next test
    navigation.navigate.mockReset();
    Keyboard.dismiss.mockReset();
    settingsApi.changePassword.mockReset();
  });
  it('stops as expected if old password is invalid', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        password: 'Qwe',
        password2nd: 'Asd123!!!'
      },
      session: {
        ...INITIAL_STATE.session,
        token: 'qwe123'
      }
    };
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.changePassword.mockResolvedValue({});
    // starting saga
    const isSagaDone = runSaga(sagaParams, changePassword).done;
    // finishing
    await isSagaDone;
    // checks
    expect(Keyboard.dismiss.mock.calls).toEqual([]);
    expect(settingsApi.changePassword.mock.calls).toEqual([]);
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changeForm('passwordError', true)],
      [ActionCreators.changeAlert(translations.t('INVALID_PASSWORD'), 'red')]
    ]);
    expect(navigation.navigate.mock.calls).toEqual([]);
    // reset mocks for next test
    navigation.navigate.mockReset();
    Keyboard.dismiss.mockReset();
    settingsApi.changePassword.mockReset();
  });
  it('stops as expected if new password is invalid', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        password: 'Qwe123!!!',
        password2nd: 'Asd'
      },
      session: {
        ...INITIAL_STATE.session,
        token: 'qwe123'
      }
    };
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.changePassword.mockResolvedValue({});
    // starting saga
    const isSagaDone = runSaga(sagaParams, changePassword).done;
    // finishing
    await isSagaDone;
    // checks
    expect(Keyboard.dismiss.mock.calls).toEqual([]);
    expect(settingsApi.changePassword.mock.calls).toEqual([]);
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changeForm('passwordError2nd', true)],
      [ActionCreators.changeAlert(translations.t('INVALID_PASSWORD'), 'red')]
    ]);
    expect(navigation.navigate.mock.calls).toEqual([]);
    // reset mocks for next test
    navigation.navigate.mockReset();
    Keyboard.dismiss.mockReset();
    settingsApi.changePassword.mockReset();
  });
  it('dispatches logout action if backend responds with unauthorized error', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        password: 'Qwe123!!!',
        password2nd: 'Asd123!!!'
      },
      session: {
        ...INITIAL_STATE.session,
        token: 'qwe123'
      }
    };
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.changePassword.mockResolvedValue({ error: 'UNAUTHORIZED' });
    // starting saga
    const isSagaDone = runSaga(sagaParams, changePassword).done;
    // finishing
    await isSagaDone;
    // checks
    expect(Keyboard.dismiss.mock.calls).toEqual([[]]);
    expect(settingsApi.changePassword.mock.calls).toEqual([['Asd123!!!', 'Qwe123!!!', 'qwe123']]);
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changePending('account', true)],
      [ActionCreators.changePending('account', false)],
      [ActionCreators.logOut()]
    ]);
    expect(navigation.navigate.mock.calls).toEqual([]);
    // reset mocks for next test
    navigation.navigate.mockReset();
    Keyboard.dismiss.mockReset();
    settingsApi.changePassword.mockReset();
  });
  it('dispatches bad input error if receives bad input from backend', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        password: 'Qwe123!!!',
        password2nd: 'Asd123!!!'
      },
      session: {
        ...INITIAL_STATE.session,
        token: 'qwe123'
      }
    };
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.changePassword.mockResolvedValue({ error: 'BAD_INPUT' });
    // starting saga
    const isSagaDone = runSaga(sagaParams, changePassword).done;
    // finishing
    await isSagaDone;
    // checks
    expect(Keyboard.dismiss.mock.calls).toEqual([[]]);
    expect(settingsApi.changePassword.mock.calls).toEqual([['Asd123!!!', 'Qwe123!!!', 'qwe123']]);
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changePending('account', true)],
      [ActionCreators.changePending('account', false)],
      [ActionCreators.changeForm('passwordError', true)],
      [ActionCreators.changeAlert(translations.t('INVALID_PASSWORD'), 'red')]
    ]);
    expect(navigation.navigate.mock.calls).toEqual([]);
    // reset mocks for next test
    navigation.navigate.mockReset();
    Keyboard.dismiss.mockReset();
    settingsApi.changePassword.mockReset();
  });
});