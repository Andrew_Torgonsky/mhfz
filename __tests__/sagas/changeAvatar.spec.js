import { cloneableGenerator, createMockTask } from 'redux-saga/utils';
import { runSaga, delay } from 'redux-saga';
import ImagePicker from 'react-native-image-crop-picker';
import { Types, ActionCreators } from '../../src/redux/actions';
import { changeAvatar } from '../../src/redux/sagas/settings';
import { INITIAL_STATE } from '../../src/redux/reducers';
import translations from '../../src/translations';
import * as settingsApi from '../../src/api/settings';
import * as navigation from '../../src/navigation';

// ImagePicker.openPicker = jest.fn();
// ImagePicker.openCamera = jest.fn();
settingsApi.changeAvatar = jest.fn();
navigation.navigate = jest.fn();

describe('Change avatar saga', () => {
  it('changes avatar taken from camera if everything is ok', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => { };
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      session: {
        ...INITIAL_STATE.session,
        token: 'token123'
      }
    };
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.changeAvatar.mockResolvedValue({ image: 'ololo.png' });
    ImagePicker.openPicker.mockResolvedValue({ path: 'somedirectory/asdasd.png', mime: 'image/png', filename: 'asdasd.png' });
    ImagePicker.openCamera.mockResolvedValue({ path: 'somedirectory/asdasd.png', mime: 'image/png', filename: 'asdasd.png' });
    // starting saga
    const isSagaDone = runSaga(sagaParams, changeAvatar, false).done;
    // finishing
    await isSagaDone;
    // checks
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changeAppState('preventMinimize', true)],
      [ActionCreators.changeAppState('preventMinimize', false)],
      [ActionCreators.changePending('account', true)],
      [ActionCreators.changePending('account', false)],
      [ActionCreators.changeSession('image', 'ololo.png')]
    ]);
    expect(settingsApi.changeAvatar.mock.calls).toEqual([
      [{ path: 'somedirectory/asdasd.png', mime: 'image/png', filename: 'asdasd.png' }, 'token123']
    ]);
    expect(ImagePicker.openPicker).not.toBeCalled();
    expect(ImagePicker.openCamera).toBeCalled();
    expect(navigation.navigate).toHaveBeenCalledWith(navigation.Scenes.Account);
    // reset mocks
    navigation.navigate.mockReset();
    settingsApi.changeAvatar.mockReset();
    ImagePicker.openPicker.mockReset();
    ImagePicker.openCamera.mockReset();
  });
  it('stops if picking from gallery was cancelled or another error related to taking image appeared', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => { };
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      session: {
        ...INITIAL_STATE.session,
        token: 'token123'
      }
    };
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.changeAvatar.mockResolvedValue({ image: 'ololo.png' });
    ImagePicker.openPicker.mockRejectedValue(new Error('cancelled'));
    ImagePicker.openCamera.mockResolvedValue({ path: 'somedirectory/asdasd.png', mime: 'image/png', filename: 'asdasd.png' });
    // starting saga
    const isSagaDone = runSaga(sagaParams, changeAvatar, true).done;
    // finishing
    await isSagaDone;
    // checks
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changeAppState('preventMinimize', true)],
      [ActionCreators.changeAppState('preventMinimize', false)],
    ]);
    expect(settingsApi.changeAvatar.mock.calls).toEqual([
    ]);
    expect(ImagePicker.openPicker).toBeCalled();
    expect(ImagePicker.openCamera).not.toBeCalled();
    expect(navigation.navigate).not.toBeCalled();
    // reset mocks
    navigation.navigate.mockReset();
    settingsApi.changeAvatar.mockReset();
    ImagePicker.openPicker.mockReset();
    ImagePicker.openCamera.mockReset();
  });
  it('stops if backend responds with error', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => { };
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    };
    // mocking values
    const initialState = {
      ...INITIAL_STATE,
      session: {
        ...INITIAL_STATE.session,
        token: 'token123'
      }
    };
    sagaParams.getState.mockReturnValue(initialState);
    settingsApi.changeAvatar.mockResolvedValue({ error: 'SOMETHING_ELSE' });
    ImagePicker.openPicker.mockResolvedValue({ path: 'somedirectory/asdasd.png', mime: 'image/png', filename: 'asdasd.png' });
    ImagePicker.openCamera.mockResolvedValue({ path: 'somedirectory/asdasd.png', mime: 'image/png', filename: 'asdasd.png' });
    // starting saga
    const isSagaDone = runSaga(sagaParams, changeAvatar, false).done;
    // finishing
    await isSagaDone;
    // checks
    expect(sagaParams.dispatch.mock.calls).toEqual([
      [ActionCreators.changeAppState('preventMinimize', true)],
      [ActionCreators.changeAppState('preventMinimize', false)],
      [ActionCreators.changePending('account', true)],
      [ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red')],
      [ActionCreators.changePending('account', false)],
    ]);
    expect(settingsApi.changeAvatar.mock.calls).toEqual([
      [{ path: 'somedirectory/asdasd.png', mime: 'image/png', filename: 'asdasd.png' }, 'token123']
    ]);
    expect(ImagePicker.openPicker).not.toBeCalled();
    expect(ImagePicker.openCamera).toBeCalled();
    expect(navigation.navigate).not.toBeCalled();
    // reset mocks
    navigation.navigate.mockReset();
    settingsApi.changeAvatar.mockReset();
    ImagePicker.openPicker.mockReset();
    ImagePicker.openCamera.mockReset();
  });
});