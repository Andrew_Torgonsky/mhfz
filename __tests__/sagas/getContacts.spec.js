import { cloneableGenerator, createMockTask } from 'redux-saga/utils';
import { runSaga, delay } from 'redux-saga';
import { put, call, fork, select, all, take, cancel, takeLatest, takeEvery } from 'redux-saga/effects';
import { Types, ActionCreators } from '../../src/redux/actions';
import { getContacts } from '../../src/redux/sagas/contacts';
import { INITIAL_STATE } from '../../src/redux/reducers';
import * as contactsApi from '../../src/api/contacts';


contactsApi.getAllContacts = jest.fn();
contactsApi.getWalletsForContacts = jest.fn();
// make timeouts 10 times faster, because jest.useFakeTimers() doesn't seem to work
const oldTimeout = global.setTimeout;
global.setTimeout = function(callback, timeout) {
  oldTimeout(callback, timeout/10);
}

describe('Get contacts saga', () => {
  it('works properly when apis respond successfully', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    }
    // mocking
    const contactsFromDevice = [
      {
        firstName: 'John',
        lastName: 'Doe',
        photo: 'john.jpg',
        emails: [],
        phoneNumbers: ['123456789'],
        wallets: []
      }
    ];
    const contactsWithWalletsFromBackend = {
      contacts: [
        {
          firstName: 'John',
          lastName: 'Doe',
          photo: 'john.jpg',
          emails: [],
          phoneNumbers: ['123456789'],
          wallets: [
            {
              walletAddress: '123456qwerty',
              currency: 'bitcoin'
            },
            {
              walletAddress: '654321qwerty',
              currency: 'ethereum'
            },
            {
              walletAddress: '123456ytrewq',
              currency: 'ripple'
            },
          ]
        }
      ]
    };
    const initialState = {
      ...INITIAL_STATE,
      session: {
        ...INITIAL_STATE.session,
        token: 'qwerty123'
      }
    };
    contactsApi.getAllContacts.mockResolvedValueOnce(contactsFromDevice)
    contactsApi.getWalletsForContacts.mockResolvedValueOnce(contactsWithWalletsFromBackend);
    sagaParams.getState.mockReturnValueOnce(initialState);
    // starting saga
    const isSagaDone = runSaga(sagaParams, getContacts).done;
    // finishing
    await isSagaDone;
    // checking
      // that it takes contacts from device and sends them to backend with session token
    expect(contactsApi.getWalletsForContacts.mock.calls).toEqual([[contactsFromDevice, 'qwerty123']]);
      // that it puts contacts with wallets from backend to store
    expect(sagaParams.dispatch.mock.calls).toEqual([[ActionCreators.changeAppState('contacts', contactsWithWalletsFromBackend.contacts)]]);
    // resetting mocked api for the next test
    contactsApi.getAllContacts.mockReset();
    contactsApi.getWalletsForContacts.mockReset();
  });

  it('if no contacts from device it dispatches GET_CONTACTS action after 20s', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    }
    // mocking
    const contactsFromDevice = [];
    const initialState = INITIAL_STATE;
    contactsApi.getAllContacts.mockResolvedValueOnce(contactsFromDevice);
    sagaParams.getState.mockReturnValueOnce(initialState);
    // starting saga
    const isSagaDone = runSaga(sagaParams, getContacts).done;
    expect(sagaParams.dispatch.mock.calls).toEqual([]);
    await delay(21000);
    expect(sagaParams.dispatch.mock.calls).toEqual([[ActionCreators.getContacts()]]);
    // finishing
    await isSagaDone;
    // resetting mocked api for the next test
    contactsApi.getAllContacts.mockReset();
    contactsApi.getWalletsForContacts.mockReset();
  });

  it('puts contacts without wallets and schedules another contact request in 20s if any error from backend except UNAUTHORIZED occured', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    }
    // mocking
    const contactsFromDevice = [
      {
        firstName: 'John',
        lastName: 'Doe',
        photo: 'john.jpg',
        emails: [],
        phoneNumbers: ['123456789'],
        wallets: []
      }
    ];
    const contactsWithWalletsFromBackend = {
      error: 'LITERALLY_ANY_ERROR_WHICH_IS_NOT_UNAUTHORIZED'
    };
    const initialState = {
      ...INITIAL_STATE,
      session: {
        ...INITIAL_STATE.session,
        token: 'qwerty123'
      }
    };
    contactsApi.getAllContacts.mockResolvedValueOnce(contactsFromDevice)
    contactsApi.getWalletsForContacts.mockResolvedValueOnce(contactsWithWalletsFromBackend);
    sagaParams.getState.mockReturnValueOnce(initialState);
    // starting saga
    const isSagaDone = runSaga(sagaParams, getContacts).done;
    await delay(1000);
    // checking
      // that it takes contacts from device and sends them to backend with session token
    expect(contactsApi.getWalletsForContacts.mock.calls).toEqual([[contactsFromDevice, 'qwerty123']]);
      // that it puts contacts from device without wallets to store on backend fail
    expect(sagaParams.dispatch.mock.calls).toEqual([[ActionCreators.changeAppState('contacts', contactsFromDevice)]]);
      // that it dispatches GET_CONTACTS action in 20s on backend fail
    await delay(20000);
    expect(sagaParams.dispatch.mock.calls).toEqual([[ActionCreators.changeAppState('contacts', contactsFromDevice)], [ActionCreators.getContacts()]]);
    // finishing
    await isSagaDone;
    
    // resetting mocked api for the next test
    contactsApi.getAllContacts.mockReset();
    contactsApi.getWalletsForContacts.mockReset();
  });

  it('dispatches LOG_OUT action if backend responded with UNAUTHORIZED', async () => {
    // preparations
    let dispatchToSaga;
    const sagaParams = {
      subscribe: (sub) => {
        dispatchToSaga = sub;
        return () => {};
      },
      dispatch: jest.fn(),
      getState: jest.fn(),
    }
    // mocking
    const contactsFromDevice = [
      {
        firstName: 'John',
        lastName: 'Doe',
        photo: 'john.jpg',
        emails: [],
        phoneNumbers: ['123456789'],
        wallets: []
      }
    ];
    const contactsWithWalletsFromBackend = {
      error: 'UNAUTHORIZED'
    };
    const initialState = {
      ...INITIAL_STATE,
      session: {
        ...INITIAL_STATE.session,
        token: 'qwerty123'
      }
    };
    contactsApi.getAllContacts.mockResolvedValueOnce(contactsFromDevice)
    contactsApi.getWalletsForContacts.mockResolvedValueOnce(contactsWithWalletsFromBackend);
    sagaParams.getState.mockReturnValueOnce(initialState);
    // starting saga
    const isSagaDone = runSaga(sagaParams, getContacts).done;
    await delay(1000);
    // checking that it takes contacts from device and sends them to backend with session token
    expect(contactsApi.getWalletsForContacts.mock.calls).toEqual([[contactsFromDevice, 'qwerty123']]);
    // finishing
    await isSagaDone;
    // checking that it dispatches LOG_OUT on UNAUTHORIZED from backend
    expect(sagaParams.dispatch.mock.calls).toEqual([[ActionCreators.logOut()]]);
    // resetting mocked api for the next test
    contactsApi.getAllContacts.mockReset();
    contactsApi.getWalletsForContacts.mockReset();
  });
});