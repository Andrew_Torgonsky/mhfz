import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import reducers from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import { PendingAnimationConnected } from '../../src/components/PendingAnimation';

describe('PendingAnimation', () => {
  const getStore = () => createStore(reducers);
  const getWrapper = currentStore => shallow(<PendingAnimationConnected />, {
    context: {
      store: currentStore
    }
  });

  it('matches snapshot when all pendings are false', () => {
    const store = getStore();
    const wrapper = getWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('matches snapshot when some pendings are true', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changePending('auth', true));
    const wrapper = getWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('returns nothing when all pendings are false', () => {
    const store = getStore();
    const wrapper = getWrapper(store);
    expect(wrapper.dive().children().length).toBe(0);
  });

  it('renders something when some pendings are true', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changePending('auth', true));
    const wrapper = getWrapper(store);
    expect(wrapper.dive().children().length).toBeGreaterThan(0);
  });
});
