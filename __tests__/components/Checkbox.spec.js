import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { TouchableOpacity } from 'react-native';
import Checkbox from '../../src/components/Checkbox';

describe('Checkbox', () => {
  const props = {
    checked: false,
    onPress: jest.fn()
  };

  it('matches snapshot with checked true', () => {
    props.checked = true;
    const wrapper = shallow(<Checkbox {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('matches snapshot with checked false', () => {
    props.checked = false;
    const wrapper = shallow(<Checkbox {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('triggers onPress when pressed', () => {
    const wrapper = shallow(<Checkbox {...props} />);
    wrapper.find(TouchableOpacity).simulate('press');
    expect(props.onPress).toBeCalled();
  });
});
