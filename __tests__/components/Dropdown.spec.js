import React from 'react';
import { shallow } from 'enzyme';
import { TouchableOpacity } from 'react-native';
import Dropdown from '../../src/components/Dropdown';


const getPropsWrapperAndButton = () => {
  const props = {
    options: ['one', 'two', 'three'],
    value: 0,
    onChange: jest.fn(),
    dropdownShift: 0
  };
  const wrapper = shallow(<Dropdown {...props} />);
  const mainButton = wrapper.findWhere(el => el.is(TouchableOpacity) && el.key() === undefined);
  return { props, wrapper, mainButton };
};

describe('DropdownIOS', () => {
  // it('changes state when clicked', () => {
  //   const { props, wrapper, mainButton } = getPropsWrapperAndButton();
  //   expect(wrapper.state().dropdownOpen).toBe(false);
  //   expect(wrapper.find(TouchableOpacity).length).toBe(1);
  //   mainButton.simulate('press');
  //   expect(wrapper.state().dropdownOpen).toBe(true);
  //   expect(wrapper.find(TouchableOpacity).length).toBe(4);
  //   mainButton.simulate('press');
  //   expect(wrapper.state().dropdownOpen).toBe(false);
  //   expect(wrapper.find(TouchableOpacity).length).toBe(1);
  //   mainButton.simulate('press');
  //   expect(wrapper.state().dropdownOpen).toBe(true);
  //   expect(wrapper.find(TouchableOpacity).length).toBe(4);
  // });

  it('matches snapshot when closed', () => {
    const { props, wrapper, mainButton } = getPropsWrapperAndButton();
    expect(wrapper).toMatchSnapshot();
  });

  // it('matches snapshot when open', () => {
  //   const { props, wrapper, mainButton } = getPropsWrapperAndButton();
  //   mainButton.simulate('press');
  //   expect(wrapper).toMatchSnapshot();
  // });

  // it('changes state and triggers onChange when options pressed', () => {
  //   const { props, wrapper, mainButton } = getPropsWrapperAndButton();
  //   mainButton.simulate('press');
  //   wrapper.findWhere(el => el.is(TouchableOpacity) && el.key() === '2').simulate('press');
  //   expect(wrapper.state().dropdownOpen).toBe(false);
  //   expect(wrapper.find(TouchableOpacity).length).toBe(1);
  //   expect(props.onChange).toBeCalledWith(2);
  // });
});

