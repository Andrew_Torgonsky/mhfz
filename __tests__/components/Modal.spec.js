import { View } from 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import Modal from '../../src/components/Modal';

describe('Modal', () => {
  it('matches snapshot', () => {
    const props = {
      children: <View />
    };
    const wrapper = shallow(<Modal {...props} />);
    expect(wrapper.dive()).toMatchSnapshot();
  });
});
