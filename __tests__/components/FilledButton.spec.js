import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import FilledButton from '../../src/components/FilledButton';

describe('FilledButton', () => {
  it('matches snapshot when blue', () => {
    expect(shallow(<FilledButton title="blue button" />)).toMatchSnapshot();
  });
  it('matches snapshot when red', () => {
    expect(shallow(<FilledButton title="red button" red />)).toMatchSnapshot();
  });
});
