import React from 'react';
import { shallow } from 'enzyme';
import { TouchableOpacity } from 'react-native';
import CountryCodeInput from '../../src/components/CountryCodeInput';
import InputField from '../../src/components/InputField';

describe('CountryCodeInput', () => {
  it('matches snapshot and renders properly with ru +7 and no error props', () => {
    const props = {
      onPress: jest.fn(),
      countryISO: 'ru',
      phonePrefix: '7',
      error: false
    };
    const wrapper = shallow(<CountryCodeInput {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('matches snapshot and renders properly with odd country iso', () => {
    const props = {
      onPress: jest.fn(),
      countryISO: 'ololo',
      phonePrefix: '10',
      error: false
    };
    const wrapper = shallow(<CountryCodeInput {...props} />);
    expect(wrapper).toMatchSnapshot();
  });


  it('triggers onPress when pressed', () => {
    const props = {
      onPress: jest.fn(),
      countryISO: '7',
      phonePrefix: 'ru',
      error: false
    };
    const wrapper = shallow(<CountryCodeInput {...props} />);
    wrapper.find(TouchableOpacity).simulate('press');
    expect(props.onPress).toBeCalled();
  });
});
