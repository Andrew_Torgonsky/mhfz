import React from 'react';
import { shallow } from 'enzyme';
import ClearButton from '../../src/components/ClearButton';

describe('ClearButton', () => {
  it('matches snapshot', () => {
    expect(shallow(<ClearButton />)).toMatchSnapshot();
  });
});
