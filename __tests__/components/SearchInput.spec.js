import React from 'react';
import { shallow } from 'enzyme';
import SearchInput from '../../src/components/SearchInput';

describe('SearchInput', () => {
  it('matches snapshot', () => {
    const wrapper = shallow(<SearchInput />);
    expect(wrapper).toMatchSnapshot();
  });
});
