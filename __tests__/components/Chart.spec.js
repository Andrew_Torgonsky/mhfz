import React from 'react';
import { shallow } from 'enzyme';
import Chart from '../../src/components/Chart';

describe('Chart', () => {
  it('matches snapshot with some data passed', () => {
    const props = {
      data: [1, 2, 3, 4, 5]
    };
    const wrapper = shallow(<Chart {...props} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('matches snapshot with no data passed', () => {
    const props = {
      data: []
    };
    const wrapper = shallow(<Chart {...props} />);
    expect(wrapper).toMatchSnapshot();
  });
});
