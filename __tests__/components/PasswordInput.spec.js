import { TouchableOpacity } from 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import PasswordInput from '../../src/components/PasswordInput';

describe('PasswordInput', () => {
  it('matches snapshot when visible false', () => {
    const props = {
      visible: false,
      onEyePress: jest.fn()
    };
    const wrapper = shallow(<PasswordInput {...props} />);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('matches snapshot when visible true', () => {
    const props = {
      visible: true,
      onEyePress: jest.fn()
    };
    const wrapper = shallow(<PasswordInput {...props} />);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('triggers onEyePress when eye is pressed', () => {
    const props = {
      visible: false,
      onEyePress: jest.fn()
    };
    const wrapper = shallow(<PasswordInput {...props} />).dive();
    wrapper.find(TouchableOpacity).simulate('press');
    expect(props.onEyePress).toBeCalled();
  });
});
