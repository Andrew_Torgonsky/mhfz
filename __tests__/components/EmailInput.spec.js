import React from 'react';
import { shallow } from 'enzyme';
import EmailInput from '../../src/components/EmailInput';

describe('EmailInput', () => {
  it('matches snapshot when nothing passed as props', () => {
    const props = {};
    expect(shallow(<EmailInput {...props} />)).toMatchSnapshot();
  });

  it('matches snapshot when props are passed', () => {
    const props = {
      style: {},
      color: 'red'
    };
    expect(shallow(<EmailInput {...props} />)).toMatchSnapshot();
  });
});
