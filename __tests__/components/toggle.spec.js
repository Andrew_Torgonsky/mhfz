import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import { TouchableOpacity } from 'react-native';
import Toggle from '../../src/components/Toggle';

describe('Toggle', () => {
	const props = {
		active: false
	};

  it('matches snapshot with inactive status', () => {
		expect(shallow(<Toggle {...props} />)).toMatchSnapshot();
	});

	it('matches snapshot with active status', () => {
		props.active = true;
		expect(shallow(<Toggle {...props} />)).toMatchSnapshot();
	})
});
