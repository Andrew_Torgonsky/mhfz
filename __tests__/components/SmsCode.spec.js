import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import SmsCode from '../../src/components/SmsCode';

const getPropsAndWrapper = ({ value = '', error = false } = {}) => {
  const props = {
    style: {},
    error,
    value,
    onChangeText: jest.fn(),
    onFinish: jest.fn()
  };
  const wrapper = shallow(<SmsCode {...props} />);
  return wrapper;
};

describe('SmsCode', () => {
  it('matches snapshot when value length is 0', () => {
    expect(getPropsAndWrapper()).toMatchSnapshot();
  });
  it('matches snapshot when value length is 1', () => {
    expect(getPropsAndWrapper({ value: '1' })).toMatchSnapshot();
  });
  it('matches snapshot when value length is 2', () => {
    expect(getPropsAndWrapper({ value: '12' })).toMatchSnapshot();
  });
  it('matches snapshot when value length is 3', () => {
    expect(getPropsAndWrapper({ value: '123' })).toMatchSnapshot();
  });
  it('matches snapshot when value length is 4', () => {
    expect(getPropsAndWrapper({ value: '1234' })).toMatchSnapshot();
  });
  it('matches snapshot when error true', () => {
    expect(getPropsAndWrapper({ error: true })).toMatchSnapshot();
  });
});
