import React from 'react';
import { shallow } from 'enzyme';
import { TouchableOpacity } from 'react-native';
import CurrencyBadge from '../../src/components/CurrencyBadge';
import { INITIAL_STATE } from '../../src/redux/reducers';


describe('CurrencyBadge', () => {
  it('renders correctly and matches snapshot', () => {
    const props = {
      icon: 'bitcoin.png',
      title: 'bitcoin',
      defaultCurrency: 'usd',
      onPress: jest.fn(),
      walletStat: INITIAL_STATE.walletsStat.bitcoin
    };
    expect(shallow(<CurrencyBadge {...props} />)).toMatchSnapshot();
  });

  it('renders correctly and matches snapshot with zero values', () => {
    const props = {
      icon: 'bitcoin.png',
      title: 'bitcoin',
      defaultCurrency: 'usd',
      onPress: jest.fn(),
      walletStat: INITIAL_STATE.walletsStat.bitcoin
    };
    expect(shallow(<CurrencyBadge {...props} />)).toMatchSnapshot();
  });

  it('renders correctly and matches snapshot with some errors', () => {
    const props = {
      icon: 'bitcoin.png',
      title: 'bitcoin',
      defaultCurrency: 'usd',
      onPress: jest.fn(),
      walletStat: INITIAL_STATE.walletsStat.bitcoin
    };
    expect(shallow(<CurrencyBadge {...props} />)).toMatchSnapshot();
  });

  it('triggers onPress when pressed', () => {
    const props = {
      icon: 'bitcoin.png',
      title: 'bitcoin',
      defaultCurrency: 'usd',
      onPress: jest.fn(),
      walletStat: INITIAL_STATE.walletsStat.bitcoin
    };
    shallow(<CurrencyBadge {...props} />).find(TouchableOpacity).simulate('press');
    expect(props.onPress).toBeCalled();
  });
});
