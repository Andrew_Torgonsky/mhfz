import React from 'react';
import { TouchableOpacity } from 'react-native';
import { shallow } from 'enzyme';
import TextButton from '../../src/components/TextButton';

describe('TextButton', () => {
  const props = {
    onPress: jest.fn(),
    title: 'Text Button'
  };
  const wrapper = shallow(<TextButton {...props} />);
  it('matches snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('triggers onPress when pressed', () => {
    wrapper.find(TouchableOpacity).simulate('press');
    expect(props.onPress).toBeCalled();
  });
});
