import React from 'react';
import { shallow } from 'enzyme';
import { TouchableOpacity } from 'react-native';
import WordButton from '../../src/components/WordButton';

describe('WordButton', () => {
  it('triggers onPress when visible and pressed', () => {
    const props = {
      word: {
        text: 'some text',
        visible: true
      },
      onPress: jest.fn()
    };
    shallow(<WordButton {...props} />).find(TouchableOpacity).simulate('press');
    expect(props.onPress).toBeCalled();
  });

  it('doesn\'t trigger onPress when invisible and pressed', () => {
    const props = {
      word: {
        text: 'some text',
        visible: false
      },
      onPress: jest.fn()
    };
    shallow(<WordButton {...props} />).find(TouchableOpacity).simulate('press');
    expect(props.onPress).toHaveBeenCalledTimes(0);
  });

  it('matches snapshot when visible', () => {
    const props = {
      word: {
        text: 'visible',
        visible: true
      },
      onPress: jest.fn()
    };
    expect(shallow(<WordButton {...props} />)).toMatchSnapshot();
  });

  it('matches snapshot when invisible', () => {
    const props = {
      word: {
        text: 'invisible',
        visible: false
      },
      onPress: jest.fn()
    };
    shallow(<WordButton {...props} />).find(TouchableOpacity).simulate('press');
    expect(shallow(<WordButton {...props} />)).toMatchSnapshot();
  });
});
