import React from 'react';
import { shallow } from 'enzyme';
import * as translations from '../../src/translations';
import BackButton from '../../src/components/Images/BackButton';

translations.isRTL = jest.fn();

describe('BackButtonImage', () => {
  it('matches snapshot when UI is left to right', () => {
    translations.isRTL.mockReturnValue(false);
    expect(shallow(<BackButton />)).toMatchSnapshot();
  });

  it('matches snapshot when UI is right to left', () => {
    translations.isRTL.mockReturnValue(true);
    expect(shallow(<BackButton />)).toMatchSnapshot();
  });

  it('doesn\'t rotate image when UI is left to right', () => {
    translations.isRTL.mockReturnValue(false);
    expect(shallow(<BackButton />).is({ style: { transform: [] } })).toBe(true);
  });

  it('rotates image UI is right to left', () => {
    translations.isRTL.mockReturnValue(true);
    expect(shallow(<BackButton />).is({ style: { transform: [{ rotate: '180deg' }] } })).toBe(true);
  });
});
