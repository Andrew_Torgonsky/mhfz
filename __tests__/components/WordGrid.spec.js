import React from 'react';
import { shallow } from 'enzyme';
import WordGrid from '../../src/components/WordGrid';

const getPropsAndWrapper = (words = []) => {
  const props = {
    words,
    onPress: jest.fn()
  };
  const wrapper = shallow(<WordGrid {...props} />);
  return { props, wrapper };
};

describe('WordGrid', () => {
  it('triggers onPress with corresponding word being pressed', () => {
    const words = 'zero one two three four five six seven eight nine ten eleven'.split(' ').map(el => ({ text: el, visible: true }));
    const { props, wrapper } = getPropsAndWrapper(words);
    for (let i = 0; i < 12; i++) {
      wrapper.findWhere(el => el.key() === `${i}`).simulate('press');
      expect(props.onPress).toHaveBeenLastCalledWith(words[i].text);
    }
  });
  it('matches snapshot', () => {
    const words = 'zero one two three four five six seven eight nine ten eleven'.split(' ').map(el => ({ text: el, visible: true }));
    const { props, wrapper } = getPropsAndWrapper(words);
    expect(wrapper).toMatchSnapshot();
  });
});
