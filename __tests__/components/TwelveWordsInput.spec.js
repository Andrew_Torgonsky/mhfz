import React from 'react';
import { shallow } from 'enzyme';
import TwelveWordsInput from '../../src/components/TwelveWordsInput';
import InputField from '../../src/components/InputField';
import ClearButton from '../../src/components/ClearButton';

const getPropsAndWrapper = ({ value = [], editable = false, clearButton = false } = {}) => {
  const props = {
    value,
    editable,
    clearButton,
    onChange: jest.fn(),
    onClear: jest.fn(),
    onFocus: jest.fn()
  };
  const wrapper = shallow(<TwelveWordsInput {...props} />);
  return { props, wrapper };
};

describe('TwelveWordsInput', () => {
  it('triggers onChange when each of inputs changes', () => {
    const { props, wrapper } = getPropsAndWrapper({ editable: true });
    for (let i = 0; i < 12; i++) {
      wrapper.findWhere(el => el.key() === `${i}`).children().find(InputField).simulate('changeText', `text ${i}`);
      expect(props.onChange).toHaveBeenLastCalledWith(`text ${i}`, i);
    }
  });

  it('triggers onFocus when each of inputs focuses', () => {
    const { props, wrapper } = getPropsAndWrapper({ editable: true });
    for (let i = 0; i < 12; i++) {
      wrapper.findWhere(el => el.key() === `${i}`).children().find(InputField).simulate('focus');
    }
    expect(props.onFocus).toHaveBeenCalledTimes(12);
  });

  it('triggers onClear when clear button is pressed', () => {
    const { props, wrapper } = getPropsAndWrapper({ clearButton: true });
    wrapper.find(ClearButton).simulate('press');
    expect(props.onClear).toBeCalled();
  });

  it('matches snapshot', () => {
    const { wrapper } = getPropsAndWrapper();
    expect(wrapper).toMatchSnapshot();
  });

  it('matches snapshot when some data provided', () => {
    const { wrapper } = getPropsAndWrapper({
      clearButton: true,
      editable: true,
      value: [
        'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve'
      ]
    });
    expect(wrapper).toMatchSnapshot();
  });
});
