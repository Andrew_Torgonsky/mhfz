import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { Text, TouchableOpacity, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { AlertPanelConnected } from '../../src/components/AlertPanel';
import reducers from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import { type ReduxStateType } from '../../src/types';


describe('AlertPanel', () => {
  const getStore = () => createStore(reducers);
  const getWrapper = currentStore => shallow(<AlertPanelConnected />, {
    context: {
      store: currentStore
    }
  });

  it('matches snapshot if no alert state provided', () => {
    const store = getStore();
    const wrapper = getWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('matches snapshot with error text', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeAppState('alertState', {
      alertText: 'error text',
      alertType: 'red',
      alertTimestamp: 0
    }));
    const wrapper = getWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('matches snapshot with notification', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeAppState('alertState', {
      alertText: 'notification',
      alertType: 'green',
      alertTimestamp: 0
    }));
    const wrapper = getWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('contains no child elements if no alert state provided (initial state)', () => {
    const store = getStore();
    const wrapper = getWrapper(store);
    expect(wrapper.dive().children().length).toBe(0);
  });

  it('contains all elements when alert state provided', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeAppState('alertState', {
      alertText: 'notification',
      alertType: 'green',
      alertTimestamp: 0
    }));
    const wrapper = getWrapper(store);
    expect(wrapper.dive().find(Image).length).toBe(2);
    expect(wrapper.dive().find(TouchableOpacity).length).toBe(1);
    expect(wrapper.dive().find(Text).length).toBe(1);
    expect(wrapper.dive().find(LinearGradient).length).toBe(1);
  });

  // it('dismisses notification on press', () => {
  //   const store = getStore();
  //   store.dispatch(ActionCreators.putAlert('notification', 'green', 0));

  //   const { alertState: { alertText, alertType } }: ReduxStateType = store.getState();
  //   expect(alertText).toBe('notification');
  //   expect(alertType).toBe('green');

  //   const wrapper = getWrapper(store);
  //   wrapper.dive().find('TouchableOpacity').simulate('press');

  //   const { alertState: { alertText: alertText2, alertType: alertType2 } }: ReduxStateType = store.getState();
  //   expect(alertText2).toBe('');
  //   expect(alertType2).toBe('');
  // });
});

