import React from 'react';
import { Image } from 'react-native';
import { shallow } from 'enzyme';
import Avatar from '../../src/components/Avatar';

describe('Avatar', () => {
  it('matches snapshot when nothing passed as props', () => {
    const props = {};
    expect(shallow(<Avatar {...props} />)).toMatchSnapshot();
  });

  it('matches a snapshot when passing a URI', () => {
    expect(shallow(
      <Avatar
        uri='https://www.google.ru/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwiO-vrEmIXdAhXoQJoKHcMiBiQQjRx6BAgBEAU&url=http%3A%2F%2Fwww.radionetplus.ru%2Fizobrazhenija%2Fzabavnye_foto%2F39190-krasivye-kartinki-na-rabochiy-stol-pandy-pandochki-pandyata-1920h1080-30-foto.html&psig=AOvVaw3NTkE_MVwx-cnvueoVCGOK&ust=1535183011713866'
      />
    )).toMatchSnapshot();
  });

  it('associates a snapshot with an image', () => {
    expect(shallow(
      <Avatar
        source='/disk/photo.png'
      />
    )).toMatchSnapshot();
  });

  it('associates a snapshot with a text', () => {
    expect(shallow(
      <Avatar
        text="Image"
      />
    )).toMatchSnapshot();
  });
});