import React from 'react';
import { shallow } from 'enzyme';
import Account from '../../src/components/Images/Account';

describe('AccountImage', () => {
  it('matches snapshot', () => {
    expect(shallow(<Account />)).toMatchSnapshot();
  });

  it('matches snapshot when active', () => {
    expect(shallow(<Account active />)).toMatchSnapshot();
  });
});
