import React from 'react';
import { shallow } from 'enzyme';
import BackupCreatedModal from '../../src/components/BackupCreatedModal';
import FilledButton from '../../src/components/FilledButton';

describe('BackupCreatedModal', () => {
  // translations.default.t = jest.fn();
  const props = {
    type: 1,
    onPress: jest.fn()
  };

  it('matches snapshot with 1st type chosen', () => {
    props.type = 1;
    const wrapper = shallow(<BackupCreatedModal {...props} />).dive();
    expect(wrapper).toMatchSnapshot();
  });

  it('matches snapshot with 2nd type chosen', () => {
    props.type = 2;
    const wrapper = shallow(<BackupCreatedModal {...props} />).dive();
    expect(wrapper).toMatchSnapshot();
  });

  it('triggers onPress prop when the button is pressed', () => {
    props.onPress.mockReset();
    const wrapper = shallow(<BackupCreatedModal {...props} />).dive();
    wrapper.find(FilledButton).simulate('press');
    expect(props.onPress).toBeCalled();
  });

  // it('uses this translations if 1st type chosen', () => {
  //   props.type = 1;
  //   translations.default.t.mockReset();
  //   const wrapper = shallow(<BackupCreatedModal {...props} />).dive();
  //   expect(translations.default.t).toBeCalledWith('WAL_BAC');
  //   expect(translations.default.t).toBeCalledWith('SAVE_Y_COMB');
  //   expect(translations.default.t).toBeCalledWith('CONTINUE');
  //   expect(translations.default.t).toHaveBeenCalledTimes(3);
  // });

  // it('uses this translations if 2nd type chosen', () => {
  //   props.type = 2;
  //   translations.default.t.mockReset();
  //   const wrapper = shallow(<BackupCreatedModal {...props} />).dive();
  //   expect(translations.default.t).toBeCalledWith('WAL_BAC_CR');
  //   expect(translations.default.t).toBeCalledWith('SAVE_Y_COMB');
  //   expect(translations.default.t).toBeCalledWith('UNDERSTOOD');
  //   expect(translations.default.t).toHaveBeenCalledTimes(3);
  // });
});
