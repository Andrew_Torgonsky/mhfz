import React from 'react';
import { shallow } from 'enzyme';
import Portfolio from '../../src/components/Images/Portfolio';

describe('PortfolioImage', () => {
  it('matches snapshot', () => {
    expect(shallow(<Portfolio />)).toMatchSnapshot();
  });

  it('matches snapshot when active', () => {
    expect(shallow(<Portfolio active />)).toMatchSnapshot();
  });
});
