import React from 'react';
import { createStore } from 'redux';
import { TouchableOpacity } from 'react-native';
import { shallow } from 'enzyme';
import LoadPhoto, { LoadPhotoConnect } from '../../src/components/LoadPhoto';
import { ActionCreators } from '../../src/redux/actions';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import type { ReduxStateType } from '../../src/types';

describe('LoadPhoto', () => {
	it('matches snapshot', () => {
		const props = {
			onGalleryPress: jest.fn(),
			onCameraPress: jest.fn(),
			onClose: jest.fn()
		};
		expect(shallow(<LoadPhoto {...props} />)).toMatchSnapshot();
	});

	it('сlosing when you press on the shadow', () => {
		const props = {
			onGalleryPress: jest.fn(),
			onCameraPress: jest.fn(),
			onClose: jest.fn()
		};
		const wrapper = shallow(<LoadPhoto {...props} />);
		wrapper
			.dive()
			.find(TouchableOpacity)
			.at(0)
			.simulate('press');
		expect(props.onClose).toBeCalled();
	});

	it('closing when you press on the close button', () => {
		const props = {
			onGalleryPress: jest.fn(),
			onCameraPress: jest.fn(),
			onClose: jest.fn()
		};
		const wrapper = shallow(<LoadPhoto {...props} />);
		wrapper
			.dive()
			.find(TouchableOpacity)
			.at(1)
			.simulate('press');
		expect(props.onClose).toBeCalled();
	});

	it('press on the "Take a photo"', () => {
		const props = {
			onGalleryPress: jest.fn(),
			onCameraPress: jest.fn(),
			onClose: jest.fn()
		};
		const wrapper = shallow(<LoadPhoto {...props} />);
		wrapper
			.find(TouchableOpacity)
			.at(2)
			.simulate('press');
		expect(props.onCameraPress).toBeCalled();
	});

	it('press on the "Choose from gallery"', () => {
		const props = {
			onGalleryPress: jest.fn(),
			onCameraPress: jest.fn(),
			onClose: jest.fn()
		};
		const wrapper = shallow(<LoadPhoto {...props} />);
		wrapper
			.find(TouchableOpacity)
			.at(3)
			.simulate('press');
		
		expect(props.onGalleryPress).toBeCalled();
	});
});
