import React from 'react';
import { TouchableOpacity } from 'react-native';
import { shallow } from 'enzyme';
import GhostButton from '../../src/components/GhostButton';

describe('GhostButton', () => {
	const props = {
			style: {},
			title: 'Ghost Button',
			onPress: jest.fn()
	};

  it('matches snapshot when nothing passed as props', () => {
    const props = {};
    expect(shallow(<GhostButton {...props} />)).toMatchSnapshot();
  });

  it('matches snapshot when props are passed', () => {
    const props = {
      style: {},
      title: 'Ghost Button',
      onPress: jest.fn()
    };
    expect(shallow(<GhostButton {...props} />)).toMatchSnapshot();
	});
	
	it('triggers onPress when pressed', () => {
		shallow(<GhostButton {...props} />).find(TouchableOpacity).simulate('press');
    expect(props.onPress).toBeCalled();
	});
});
