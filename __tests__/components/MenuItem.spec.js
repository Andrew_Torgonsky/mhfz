import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { shallow } from 'enzyme';
import MenuItem from '../../src/components/MenuItem';

describe('MenuItem', () => {
  it('matches snapshot when nothing passed as props', () => {
    const props = {};
    expect(shallow(<MenuItem {...props} />)).toMatchSnapshot();
  });

  it('matches snapshot when props are passed', () => {
    const props = {
      title: 'title',
      right: <View />,
      rightArrow: true,
      onPress: jest.fn()
    };
    expect(shallow(<MenuItem {...props} />)).toMatchSnapshot();
  });

  it('triggers onPress when pressed', () => {
    const props = {
      title: 'title',
      right: <View />,
      rightArrow: true,
      onPress: jest.fn()
    };
    shallow(<MenuItem {...props} />).find(TouchableOpacity).simulate('press');
    expect(props.onPress).toBeCalled();
  });
});
