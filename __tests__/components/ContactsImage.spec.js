import React from 'react';
import { shallow } from 'enzyme';
import Contacts from '../../src/components/Images/Contacts';

describe('ContactsImage', () => {
  it('matches snapshot', () => {
    expect(shallow(<Contacts />)).toMatchSnapshot();
  });

  it('matches snapshot when active', () => {
    expect(shallow(<Contacts active />)).toMatchSnapshot();
  });
});
