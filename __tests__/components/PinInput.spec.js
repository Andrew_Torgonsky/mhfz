import React from 'react';
import { shallow } from 'enzyme';
import { TextInput } from 'react-native';
import PinInput from '../../src/components/PinInput';

const getPropsAndWrapper = ({ value = '', error = false } = {}) => {
  const props = {
    style: {},
    error,
    value,
    onChangeText: jest.fn(),
    onFinish: jest.fn(),
    autoFocus: true
  };
  const wrapper = shallow(<PinInput {...props} />);
  return { props, wrapper };
};

describe('PinInput', () => {
  // it('triggers onChangeText when text is changed', () => {
  //   const { props, wrapper } = getPropsAndWrapper();
  //   wrapper.find(TextInput).simulate('changeText', '1');
  //   expect(props.onChangeText).toBeCalledWith('1');
  //   expect(props.onFinish).toHaveBeenCalledTimes(0);
  // });

  // it('triggers onFinish when text length is 4 characters', () => {
  //   const { props, wrapper } = getPropsAndWrapper();
  //   wrapper.find(TextInput).simulate('changeText', '1234');
  //   expect(props.onFinish).toBeCalled();
  // });

  it('matches snapshot when value length is 0', () => {
    const { props, wrapper } = getPropsAndWrapper();
    expect(wrapper).toMatchSnapshot();
  });
  it('matches snapshot when value length is 1', () => {
    const { props, wrapper } = getPropsAndWrapper({ value: '1' });
    expect(wrapper).toMatchSnapshot();
  });
  it('matches snapshot when value length is 2', () => {
    const { props, wrapper } = getPropsAndWrapper({ value: '12' });
    expect(wrapper).toMatchSnapshot();
  });
  it('matches snapshot when value length is 3', () => {
    const { props, wrapper } = getPropsAndWrapper({ value: '123' });
    expect(wrapper).toMatchSnapshot();
  });
  it('matches snapshot when value length is 4', () => {
    const { props, wrapper } = getPropsAndWrapper({ value: '1234' });
    expect(wrapper).toMatchSnapshot();
  });
  it('matches snapshot when error true', () => {
    const { props, wrapper } = getPropsAndWrapper({ error: true });
    expect(wrapper).toMatchSnapshot();
  });
});
