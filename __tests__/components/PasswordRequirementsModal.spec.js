import React from 'react';
import { shallow } from 'enzyme';
import PasswordRequirementsModal from '../../src/components/PasswordRequirementsModal';
import FilledButton from '../../src/components/FilledButton';

describe('PasswordRequirementsModal', () => {
  it('matches snapshot', () => {
    const props = {
      onPress: jest.fn()
    };
    const wrapper = shallow(<PasswordRequirementsModal {...props} />);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('triggers onPress when pressed', () => {
    const props = {
      onPress: jest.fn()
    };
    const wrapper = shallow(<PasswordRequirementsModal {...props} />).dive();
    wrapper.find(FilledButton).simulate('press');
    expect(props.onPress).toBeCalled();
  });
});
