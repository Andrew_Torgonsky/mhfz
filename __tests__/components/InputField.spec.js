import React from 'react';
import { View } from 'react-native';
import { shallow } from 'enzyme';
import InputField from '../../src/components/InputField';

describe('InputField', () => {
  it('matches snapshot when nothing passed as props', () => {
    const props = {};
    expect(shallow(<InputField {...props} />)).toMatchSnapshot();
  });
  it('matches snapshot when props are passed', () => {
    const props = {
      style: {},
      error: true,
      before: <View />,
      after: <View />,
      inputIndentaion: 10
    };
    expect(shallow(<InputField {...props} />)).toMatchSnapshot();
  });
});
