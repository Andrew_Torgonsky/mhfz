import { truncateString } from '../../src/utils/other';

describe('truncate string', () => {
  it('returns the string as is if it is shorter than provided length', () => {
    expect(truncateString('cat', 5)).toBe('cat');
  });
  it('slices string of provided length from the beginning of provided string and adds three dots to it if provided string is longer than provided length', () => {
    expect(truncateString('catwoman', 5)).toBe('catwo...');
  });
})