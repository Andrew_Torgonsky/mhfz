import Moment from 'moment';
import { getCombinedHistoryForChart } from '../../src/utils/chart';

describe('getCombinedHistoryForChart', () => {
  it('combines all histories into array of x and y coordinates', () => {
    const currentTimeStamp = Moment('2018-09-09 1:00').valueOf();
    const weekAgoTimeStamp = Moment(currentTimeStamp).subtract(7, 'days').valueOf();
    const mockWalletsStat = {
      bitcoin: {
        balance: 2,
        rate: 10,
        history: [
          {
            timestamp: Moment('2018-09-03').valueOf(),
            average: 2
          },
          {
            timestamp: Moment('2018-09-05').valueOf(),
            average: 3
          },
          {
            timestamp: Moment('2018-09-07').valueOf(),
            average: 6
          }
        ]
      },
      ethereum: {
        balance: 5,
        rate: 5,
        history: [
          {
            timestamp: Moment('2018-08-31').valueOf(),
            average: 3
          },
          {
            timestamp: Moment('2018-09-02').valueOf(),
            average: 2
          },
          {
            timestamp: Moment('2018-09-04').valueOf(),
            average: 4
          },
          {
            timestamp: Moment('2018-09-06').valueOf(),
            average: 3
          },
        ]
      },
      ripple: {
        balance: 5,
        rate: 3,
        history: [
          {
            timestamp: Moment('2018-09-04').valueOf(),
            average: 3
          },
          {
            timestamp: Moment('2018-09-05').valueOf(),
            average: 6
          },
          {
            timestamp: Moment('2018-09-07').valueOf(),
            average: 3
          },
        ]
      }
    };
    expect(getCombinedHistoryForChart(mockWalletsStat, 0, currentTimeStamp)).toEqual([
      {
        x: 0,
        y: 10
      },
      {
        x: Moment('2018-09-03').valueOf() - weekAgoTimeStamp,
        y: 30
      },
      {
        x: Moment('2018-09-04').valueOf() - weekAgoTimeStamp,
        y: 49
      },
      {
        x: Moment('2018-09-05').valueOf() - weekAgoTimeStamp,
        y: 68
      },
      {
        x: Moment('2018-09-06').valueOf() - weekAgoTimeStamp,
        y: 63
      },
      {
        x: Moment('2018-09-07').valueOf() - weekAgoTimeStamp,
        y: 84
      },
      {
        x: currentTimeStamp - weekAgoTimeStamp,
        y: 60
      },
    ]);
  });
  it('ignores currency with empty history and combines all other histories into array of x and y coordinates', () => {
    const currentTimeStamp = Moment('2018-09-09 1:00').valueOf();
    const weekAgoTimeStamp = Moment(currentTimeStamp).subtract(7, 'days').valueOf();
    const mockWalletsStat = {
      bitcoin: {
        balance: 2,
        rate: 10,
        history: [
          {
            timestamp: Moment('2018-09-03').valueOf(),
            average: 2
          },
          {
            timestamp: Moment('2018-09-05').valueOf(),
            average: 3
          },
          {
            timestamp: Moment('2018-09-07').valueOf(),
            average: 6
          }
        ]
      },
      ethereum: {
        balance: 5,
        rate: 5,
        history: [
          {
            timestamp: Moment('2018-08-31').valueOf(),
            average: 3
          },
          {
            timestamp: Moment('2018-09-02').valueOf(),
            average: 2
          },
          {
            timestamp: Moment('2018-09-04').valueOf(),
            average: 4
          },
          {
            timestamp: Moment('2018-09-06').valueOf(),
            average: 3
          },
        ]
      },
      ripple: {
        balance: 5,
        rate: 3,
        history: [
          {
            timestamp: Moment('2018-09-04').valueOf(),
            average: 3
          },
          {
            timestamp: Moment('2018-09-05').valueOf(),
            average: 6
          },
          {
            timestamp: Moment('2018-09-07').valueOf(),
            average: 3
          },
        ]
      },
      dogecoin: {
        balance: 100500,
        rate: 1,
        history: []
      }
    };
    expect(getCombinedHistoryForChart(mockWalletsStat, 0, currentTimeStamp)).toEqual([
      {
        x: 0,
        y: 10
      },
      {
        x: Moment('2018-09-03').valueOf() - weekAgoTimeStamp,
        y: 30
      },
      {
        x: Moment('2018-09-04').valueOf() - weekAgoTimeStamp,
        y: 49
      },
      {
        x: Moment('2018-09-05').valueOf() - weekAgoTimeStamp,
        y: 68
      },
      {
        x: Moment('2018-09-06').valueOf() - weekAgoTimeStamp,
        y: 63
      },
      {
        x: Moment('2018-09-07').valueOf() - weekAgoTimeStamp,
        y: 84
      },
      {
        x: currentTimeStamp - weekAgoTimeStamp,
        y: 60
      },
    ]);
  });
  it('returs empty array if all histories are empty', () => {
    const currentTimeStamp = Moment('2018-09-09 1:00').valueOf();
    const weekAgoTimeStamp = Moment(currentTimeStamp).subtract(7, 'days').valueOf();
    const mockWalletsStat = {
      bitcoin: {
        balance: 2,
        rate: 10,
        history: []
      },
      ethereum: {
        balance: 5,
        rate: 5,
        history: []
      },
      ripple: {
        balance: 5,
        rate: 3,
        history: []
      },
      dogecoin: {
        balance: 100500,
        rate: 1,
        history: []
      }
    };
    expect(getCombinedHistoryForChart(mockWalletsStat, 0, currentTimeStamp)).toEqual([]);
  });
})