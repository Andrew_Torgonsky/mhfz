import { combineBalancesToFiat } from '../../src/utils/currencies';

describe('combineBalancesToFiat', () => {
  it('works', () => {
    const mockWalletsStat = {
      bitcoin: {
        balance: 12,
        rate: 10,
      },
      ethereum: {
        balance: 23.123,
        rate: 5,
      },
      ripple: {
        balance: 32,
        rate: 2,
      }
    };
    expect(combineBalancesToFiat(mockWalletsStat)).toBe(299.61);
  });
  
});