import { inFiat } from '../../src/utils/currencies';

describe('inFiat', () => {
  it('returns string with currency sign(if currency name provided) and amount with two signs after dot', () => {
    expect(inFiat(20, 5, 'usd')).toBe('$100.00');
    expect(inFiat(20, 5, 'eur')).toBe('€100.00');
    expect(inFiat(20, 5, 'sar')).toBe('SR100.00');
    expect(inFiat(20, 5)).toBe('100.00');
    expect(inFiat('20', '5')).toBe('100.00');
  });
});