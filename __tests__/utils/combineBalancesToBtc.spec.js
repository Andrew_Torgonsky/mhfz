import { combineBalancesToBtc } from '../../src/utils/currencies';

describe('combineBalancesToBtc', () => {
  it('works', () => {
    const mockWalletsStat = {
      bitcoin: {
        balance: 8,
        rate: 10
      }, // 8 in btc
      ethereum: {
        balance: 15,
        rate: 5
      }, // 15*5/10 = 7.5 in btc
      ripple: {
        balance: 20,
        rate: 2
      }, // 20*2/10 = 4 in btc
    };
    expect(combineBalancesToBtc(mockWalletsStat)).toBe(19.5)
  });
  it('returns 0 if btc rate is 0', () => {
    const mockWalletsStat = {
      bitcoin: {
        balance: 8,
        rate: 0
      },
      ethereum: {
        balance: 15,
        rate: 5
      },
      ripple: {
        balance: 20,
        rate: 2
      },
    };
    expect(combineBalancesToBtc(mockWalletsStat)).toBe(0)
  });
});