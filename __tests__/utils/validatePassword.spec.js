import { validatePassword } from '../../src/utils/other';

describe('validate password', () => {
  it('returns false if password doesnt include capital letter, small letter or number, shorter than 8 or longer than 14', () => {
    expect(validatePassword('Addddddd')).toBeFalsy();
    expect(validatePassword('A99999999')).toBeFalsy();
    expect(validatePassword('9ddddddd')).toBeFalsy();
    expect(validatePassword('Aa111')).toBeFalsy();
    expect(validatePassword('Aa1111111111111111111111111')).toBeFalsy();
  });
  it('returns true otherwise', () => {
    expect(validatePassword('Aa111111111')).toBeTruthy();
  })
})