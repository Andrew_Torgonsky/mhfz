import { amountAndFee } from '../../src/utils/currencies';

describe('amount and fee', () => {
  it('works with normal input (numbers and strings as amount to send)', () => {
    const mockFeeOptions = [
      { fee: 0.01 },
      { fee: 0.02 }
    ];
    expect(amountAndFee('30', mockFeeOptions, 0)).toBe(30.01);
    expect(amountAndFee(30, mockFeeOptions, 0)).toBe(30.01)
  });
  it('returns 0 if no feeOptions or choosen fee option is not present in fee options array', () => {
    const mockFeeOptions = [
    ];
    expect(amountAndFee('30', mockFeeOptions, 0)).toBe(0);
  });
  it('returns 0 if amount is empty string', () => {
    const mockFeeOptions = [
      { fee: 0.01 },
      { fee: 0.02 }
    ];
    expect(amountAndFee('', mockFeeOptions, 0)).toBe(0);
  });
});