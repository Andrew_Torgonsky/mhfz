import Moment from 'moment';
import { getHistoryForChart } from '../../src/utils/chart';

describe('getHistoryForChart', () => {
  it('returns array of points with x and y coordinates from walletStat provided when history begins before choosen period', () => {
    const currentTimestamp = Moment('2018-09-12 15:00').valueOf();
    const mockWalletStat = {
      balance: 3,
      history: [
        {
          timestamp: Moment('2018-08-30 15:00').valueOf(),
          average: 1,
        },
        {
          timestamp: Moment('2018-09-02 15:00').valueOf(),
          average: 5,
        },
        {
          timestamp: Moment('2018-09-07 15:00').valueOf(),
          average: 8,
        },
        {
          timestamp: Moment('2018-09-09 15:00').valueOf(),
          average: 3,
        },
        {
          timestamp: Moment('2018-09-10 15:00').valueOf(),
          average: 6,
        }
      ]
    };
    const weekAgoTimeStamp = Moment(currentTimestamp).subtract(7, 'days').valueOf();
    expect(getHistoryForChart(mockWalletStat, 0, currentTimestamp)).toEqual([
      {
        x: 0,
        y: 5
      },
      {
        x: Moment('2018-09-07 15:00').valueOf() - weekAgoTimeStamp,
        y: 8
      },
      {
        x: Moment('2018-09-09 15:00').valueOf() - weekAgoTimeStamp,
        y: 3
      },
      {
        x: Moment('2018-09-10 15:00').valueOf() - weekAgoTimeStamp,
        y: 6
      },
      {
        x: currentTimestamp - weekAgoTimeStamp,
        y: 3
      }
    ]);
  });
  it(`returns array of points with x and y coordinates from walletStat provided when history begins after beginning of choosen period
  and prepends point with y = 0 a day before the first entry in array of history records`, () => {
    const currentTimestamp = Moment('2018-09-12 15:00').valueOf();
    const mockWalletStat = {
      balance: 3,
      history: [
        {
          timestamp: Moment('2018-08-30 15:00').valueOf(),
          average: 1,
        },
        {
          timestamp: Moment('2018-09-02 15:00').valueOf(),
          average: 5,
        },
        {
          timestamp: Moment('2018-09-07 15:00').valueOf(),
          average: 8,
        },
        {
          timestamp: Moment('2018-09-09 15:00').valueOf(),
          average: 3,
        },
        {
          timestamp: Moment('2018-09-10 15:00').valueOf(),
          average: 6,
        }
      ]
    };
    const monthAgoTimeStamp = Moment(currentTimestamp).subtract(30, 'days').valueOf();
    expect(getHistoryForChart(mockWalletStat, 1, currentTimestamp)).toEqual([
      {
        x: 0,
        y: 0
      },
      {
        x: Moment('2018-08-29 15:00').valueOf() - monthAgoTimeStamp,
        y: 0,
      },
      {
        x: Moment('2018-08-30 15:00').valueOf() - monthAgoTimeStamp,
        y: 1,
      },
      {
        x: Moment('2018-09-02 15:00').valueOf() - monthAgoTimeStamp,
        y: 5,
      },
      {
        x: Moment('2018-09-07 15:00').valueOf() - monthAgoTimeStamp,
        y: 8,
      },
      {
        x: Moment('2018-09-09 15:00').valueOf() - monthAgoTimeStamp,
        y: 3,
      },
      {
        x: Moment('2018-09-10 15:00').valueOf() - monthAgoTimeStamp,
        y: 6,
      },
      {
        x: currentTimestamp - monthAgoTimeStamp,
        y: 3
      }
    ]);
  });
  it('returns empty array if history is empty', () => {
    const currentTimestamp = Moment('2018-09-12 15:00').valueOf();
    const mockWalletStat = {
      balance: 3,
      history: []
    };
    const weekAgoTimeStamp = Moment(currentTimestamp).subtract(7, 'days').valueOf();
    expect(getHistoryForChart(mockWalletStat, 0, currentTimestamp)).toEqual([]);
  });
});