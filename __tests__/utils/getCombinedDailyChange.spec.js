import { getCombinedDailyChange } from '../../src/utils/currencies';
const now = 1537333744000;
const dayAgo = now - 24 * 60 * 60 * 1000; // 1537247344000
const mockDate = require('mockdate');
mockDate.set(now);

describe('getCombinedDailyChange', () => {
  it('returns balance difference between now and 24h ago if all currencies have history record older or the same as 24 ago', () => {
    const mockWalletsStat = {
      bitcoin: {
        balance: 5,
        rate: 10,
        history: [
          { timestamp: dayAgo - 100500, value: 3 }
        ]
      }, // old 30, new 50
      ethereum: {
        balance: 10,
        rate: 5,
        history: [
          { timestamp: dayAgo - 100500, value: 6 }
        ]
      }, // old 30, new 50
      ripple: {
        balance: 8,
        rate: 2,
        history: [
          { timestamp: dayAgo - 100500, value: 3 }
        ]
      } // old 6, new 16
    }; // all old 66, new 116 // (116 - 66) / 66 * 100 = 75.75...
    expect(getCombinedDailyChange(mockWalletsStat)).toBe(75.75);
  });
  it('returns balance difference between now and 24h ago and ignores currency with no history', () => {
    const mockWalletsStat = {
      bitcoin: {
        balance: 5,
        rate: 10,
        history: [
          { timestamp: dayAgo - 100500, value: 3 }
        ]
      }, // old 30, new 50
      ethereum: {
        balance: 10,
        rate: 5,
        history: [
        ]
      }, // ignored
      ripple: {
        balance: 8,
        rate: 2,
        history: [
          { timestamp: dayAgo - 100500, value: 3 }
        ]
      } // old 6, new 16
    }; // all old 36, new 66 // (66 - 36) / 36 * 100 = 83.33333...
    expect(getCombinedDailyChange(mockWalletsStat)).toBe(83.33);
  });
});