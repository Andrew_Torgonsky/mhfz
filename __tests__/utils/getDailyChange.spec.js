import { getDailyChange } from '../../src/utils/currencies';

const now = 1537333744000;
const dayAgo = now - 24 * 60 * 60 * 1000; // 1537247344000
const mockDate = require('mockdate');
mockDate.set(now);


describe('getDailyChange function', () => {
  it('outputs difference between current time and a day ago in percentage (if there\'s history record(s) older than day ago) (positive change)', () => {
    const mockWalletStat = {
      balance: 100,
      history: [
        {
          timestamp: dayAgo - 100500,
          value: 50
        }
      ]
    };
    expect(getDailyChange(mockWalletStat)).toBe(100);
  });
  it('outputs difference between current time and a day ago in percentage (if there\'s history record(s) older than day ago) (negative change)', () => {
    const mockWalletStat = {
      balance: 150,
      history: [
        {
          timestamp: dayAgo - 100500,
          value: 200
        }
      ]
    };
    expect(getDailyChange(mockWalletStat)).toBe(-25);
  });
  it('outputs infinity if no history record are present', () => {
    const mockWalletStat = {
      balance: 100,
      history: []
    };
    expect(getDailyChange(mockWalletStat)).toBe(Infinity);
  });
  it('outputs infinity if there are only history records which are newer than day ago', () => {
    const mockWalletStat = {
      balance: 100,
      history: [
        {
          timestamp: dayAgo + 100500,
          value: 50
        }
      ]
    };
    expect(getDailyChange(mockWalletStat)).toBe(Infinity);
  });
});