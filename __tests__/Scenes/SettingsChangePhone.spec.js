import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import type { ReduxStateType } from '../../src/types';
import { SettingsChangePhoneConnected } from '../../src/scenes/SettingsChangePhone';
import InputField from '../../src/components/InputField';
import CountryCodeInput from '../../src/components/CountryCodeInput';
import { Scenes } from '../../src/navigation/shared';

describe('SettimgsChangePhone', () => {
  const getStore = () => createStore(reducers);
  const getConnectedWrapper = (currentStore) => {
    const navigation = {
      navigate: jest.fn()
    };
    const wrapper = shallow(<SettingsChangePhoneConnected navigation={navigation} />, {
      context: {
        store: currentStore,
      }
    });
    return { wrapper, navigation };
  };

  it('matches snapshot in initial state', () => {
    const store = getStore();
    const { wrapper } = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('press Country Code input', () => {
    const store = getStore();
    const { navigation, wrapper } = getConnectedWrapper(store);

    wrapper
      .dive()
      .find(CountryCodeInput)
      .simulate('press');

    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.CountrySelect);
  });

  it('changes redux state on input', () => {
    const store = getStore();
    const { wrapper } = getConnectedWrapper(store);

    wrapper
      .dive()
      .find(InputField)
      .simulate('changeText', '1234567890');

    const { form: { phoneNumber } }: ReduxStateType = store.getState();
    expect(phoneNumber).toBe('1234567890');
  });

  it('dismisses errors on countrycode press', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('phoneError', true));
    store.dispatch(ActionCreators.changeAlert('some error', 'red'));

    const { wrapper } = getConnectedWrapper(store);

    wrapper
      .dive()
      .find(CountryCodeInput)
      .simulate('press');

    const {
      form: {
        phoneError
      },
      alertState: {
        alertText,
        alertType
      }
    }: ReduxStateType = store.getState();
    
    expect(phoneError).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
  });

  it('dismisses errors on phone input focus', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('phoneError', true));
    store.dispatch(ActionCreators.changeAlert('some error', 'red'));

    const { wrapper } = getConnectedWrapper(store);

    wrapper
      .dive()
      .find(InputField)
      .simulate('focus');

    const {
      form: {
        phoneError
      },
      alertState: {
        alertText,
        alertType
      }
    }: ReduxStateType = store.getState();

    expect(phoneError).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
  });

  it('dismisses errors on phone input text', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('phone', '123456'));
    store.dispatch(ActionCreators.changeForm('phoneError', true));
    store.dispatch(ActionCreators.changeAlert('some error', 'red'));

    const { wrapper } = getConnectedWrapper(store);

    wrapper
      .dive()
      .find(InputField)
      .simulate('changeText', '1234567890');

    const {
      form: {
        phoneError
      },
      alertState: {
        alertText,
        alertType
      }
    }: ReduxStateType = store.getState();

    expect(phoneError).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
  });
});
