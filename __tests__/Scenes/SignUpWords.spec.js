import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { SignUpWordsConnected } from '../../src/scenes/SignUpWords';
import FilledButton from '../../src/components/FilledButton';
import { Scenes } from '../../src/navigation/shared';

describe('SignUpWords', () => {
  const getStore = () => createStore(reducers);
  const getConnectedWrapper = (currentStore) => {
    const navigation = {
      navigate: jest.fn()
    };
    const wrapper = shallow(<SignUpWordsConnected navigation={navigation} />, {
      context: {
        store: currentStore,
      }
    });
    return { wrapper, navigation };
  };

  it('matches snapshot with empty props', () => {
    const store = getStore();
    const { wrapper } = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('press button', () => {
    const store = getStore();
    const { wrapper, navigation } = getConnectedWrapper(store);

    wrapper
      .dive()
      .find(FilledButton)
      .simulate('press');

    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.SignUpWordsConfirm);
  });
});
