import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import type { ReduxStateType } from '../../src/types';
import { ContactConnected } from '../../src/scenes/Contact';

const props = {
  emails: ['test@test.com'],
  phoneNumbers: ['1234567890'],
  firstName: 'John',
  lastName: 'Doe',
  wallets: [],
  photo: null
};
const getStore = () => createStore(reducers);
const getConnectedWrapper = currentStore =>
  shallow(<ContactConnected navigation={{ state: { params: props } }} />, {
    context: {
      store: currentStore
    }
  });

describe('Contact', () => {
  it('matches snapshots', () => {
    const store = getStore();
    const wrapper = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });
});
