import React from 'react';
import { View, Text, TouchableOpacity, SectionList } from 'react-native';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import { ActionCreators } from '../../src/redux/actions';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import type { ReduxStateType } from '../../src/types';
import Contacts, { ContactsConnected } from '../../src/scenes/Contacts';
import * as Permissions from 'react-native-permissions';
import SearchInput from '../../src/components/SearchInput';

Permissions.default.check = jest.fn();

// jest.fn().

Permissions.default.check.mockResolvedValue('authorized');

describe('Contacts', () => {
  const props = {
    type: 'contacts',
    contacts: [
      {
        emails: [],
        firstName: 'Sylvester',
        lastName: 'Stallone',
        phoneNumbers: ['79139131414'],
        photo: '',
        wallets: []
      },
      {
        emails: [],
        firstName: 'Arnold',
        lastName: 'Schwarzenegger',
        phoneNumbers: ['79139131414'],
        photo: '',
        wallets: []
      },
      {
        emails: [],
        firstName: 'Dwayne',
        lastName: 'Johnson',
        phoneNumbers: ['79139131414'],
        photo: '',
        wallets: []
      }
    ],
    navigation: {
      navigate: jest.fn()
    }
  };

  const getStore = () => createStore(reducers);
  const getWrapper = props => shallow(<Contacts {...props} />).dive();
  const getConnectedWrapper = currentStore => shallow(<ContactsConnected {...props} />, {
    context: {
      store: currentStore
    }
  });

  it('matches snapshots', () => {
    const store = getStore();
    const wrapper = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  /* it('changing data in state when typing text in a search string', () => {
    const store = getStore();
    const wrapper = getConnectedWrapper(store);
    expect(wrapper.state().search).toBe('');
    wrapper.dive().find(SearchInput).simulate('changeText', 'J');
    expect(wrapper.state().search).toBe('J');
  }); */

  /* it('sorting contacts', () => {
    const wrapper = getWrapper(props);
    wrapper.dive().find(SearchInput).simulate('changeText', 'Sylvester');
    expect(wrapper.dive().find(SectionList).props('sections')).toEqual([{"data": [{"emails": [], "firstName": "Dwayne", "lastName": "Johnson", "phoneNumbers": ["79139131414"], "photo": "", "wallets": []}], "title": "J"}, {"data": [{"emails": [], "firstName": "Arnold", "lastName": "Schwarzenegger", "phoneNumbers": ["79139131414"], "photo": "", "wallets": []}, {"emails": [], "firstName": "Sylvester", "lastName": "Stallone", "phoneNumbers": ["79139131414"], "photo": "", "wallets": []}], "title": "S"}]);
    // wrapper.dive().find(SearchInput).simulate('changeText', 'Dwayne');
    // expect(wrapper.dive().find(SectionList).prop('sections')).toEqual('');
    // expect(wrapper.state().search).toEqual('');
    // expect(wrapper.dive().find(SectionList).prop('sections')).toEqual('');
  }); */
});
