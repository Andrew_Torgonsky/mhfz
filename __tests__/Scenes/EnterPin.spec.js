import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import { EnterPinConnected } from '../../src/scenes/EnterPin';
import TextButton from '../../src/components/TextButton';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import PinInput from '../../src/components/PinInput';
import { ActionCreators } from '../../src/redux/actions';

describe('EnterPin', () => {
  const getStore = () => createStore(reducers);
  const getConnectedWrapper = currentStore => shallow(<EnterPinConnected />, {
    context: {
      store: currentStore
    }
  });

  it('it matches snapshot in initial state', () => {
    const store = getStore();
    const wrapper = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('passes pin code value to pininput component and matches snapshot', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('pinCode', '123'));
    const wrapper = getConnectedWrapper(store);
    expect(wrapper.dive().find(PinInput).prop('value')).toBe('123');
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('passes pin code value to pininput component and matches snapshot', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('pinCodeError', true));
    const wrapper = getConnectedWrapper(store);
    expect(wrapper.dive().find(PinInput).prop('error')).toBe(true);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('changes state when pin is entered', () => {
    const store = getStore();
    const wrapper = getConnectedWrapper(store);
    wrapper.dive().find(PinInput).simulate('changeText', '123');
    const { form: { pinCode } } = store.getState();
    expect(pinCode).toBe('123');
  });

  it('dispatches check pin code when pin input emits onfinish', () => {
    const store = createMockStore(INITIAL_STATE);
    const wrapper = getConnectedWrapper(store);
    expect(store.isActionDispatched(ActionCreators.checkPinCode())).toBe(false);
    wrapper.dive().find(PinInput).simulate('finish');
    expect(store.isActionDispatched(ActionCreators.checkPinCode())).toBe(true);
  });

  it('dispatches logout when corresponding button is pressed', () => {
    const store = createMockStore(INITIAL_STATE);
    const wrapper = getConnectedWrapper(store);
    expect(store.isActionDispatched(ActionCreators.logOut())).toBe(false);
    wrapper.dive().find(TextButton).simulate('press');
    expect(store.isActionDispatched(ActionCreators.logOut())).toBe(true);
  });
});

