import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import type { ReduxStateType } from '../../src/types';
import { LanguageConnected } from '../../src/scenes/Language';
import MenuItem from '../../src/components/MenuItem';

describe('Language', () => {
  const getStore = () => createStore(reducers);
  const getConnectedWrapper = currentStore => shallow(
    <LanguageConnected />,
    {
      context: {
        store: currentStore
      }
    }
  );

  it('matches snapshot', () => {
    const store = getStore();
    const wrapper = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('switch language to english', () => {
    const store = createMockStore(INITIAL_STATE);
    const wrapper = getConnectedWrapper(store);

    wrapper
      .dive()
      .find(MenuItem)
      .at(0)
      .simulate('press');

    expect(store.isActionDispatched(ActionCreators.switchToLanguage('en'))).toBe(true);
  });

  // it('switch language to arabic', () => {
  //   const store = createMockStore(INITIAL_STATE);
  //   const wrapper = getConnectedWrapper(store);

  //   wrapper
  //     .dive()
  //     .find(MenuItem)
  //     .at(1)
  //     .simulate('press');

  //   expect(store.isActionDispatched(ActionCreators.switchToLanguage('ar'))).toBe(true);
  // });
});
