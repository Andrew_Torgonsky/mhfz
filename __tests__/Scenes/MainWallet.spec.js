import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import type { ReduxStateType } from '../../src/types';
import { MainWalletConnected } from '../../src/scenes/MainWallet';
import * as translations from '../../src/translations';
import Dropdown from '../../src/components/Dropdown';
import CurrencyBadge from '../../src/components/CurrencyBadge';
import { Scenes } from '../../src/navigation/shared';

translations.isRTL = jest.fn(() => false);

const getStore = () => createStore(reducers);
const getConnectedWrapper = (currentStore) => {
  const navigation = {
    navigate: jest.fn()
  };
  const wrapper = shallow(<MainWalletConnected navigation={navigation} />, {
    context: {
      store: currentStore,
    }
  });
  return { wrapper, navigation };
};

describe('MainWallet', () => {
  it('changes balance period', () => {
    const store = getStore();
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(Dropdown).simulate('change', 1);
    const { balancePeriod }: ReduxStateType = store.getState();
    expect(balancePeriod).toBe(1);
  });

  it('navigates to corresponding screens when currencies are pressed', () => {
    const store = getStore();
    const { wrapper, navigation } = getConnectedWrapper(store);
    wrapper.dive().findWhere(item => item.is(CurrencyBadge) && item.is({ title: 'Bitcoin - BTC' })).simulate('press');
    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.BtcWallet);
    wrapper.dive().findWhere(item => item.is(CurrencyBadge) && item.is({ title: 'Ethereum - ETH' })).simulate('press');
    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.EthWallet);
    wrapper.dive().findWhere(item => item.is(CurrencyBadge) && item.is({ title: 'Ripple - XRP' })).simulate('press');
    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.XrpWallet);
  });

  // need more tests after we decide how to store history and when to request balance
});
