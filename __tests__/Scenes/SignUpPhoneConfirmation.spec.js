import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import { SignUpPhoneConfirmation } from '../../src/scenes/SignUpPhoneConfirmation';

describe('SignUpPhoneConfirmation', () => {
  const wrapper = shallow(<SignUpPhoneConfirmation />);

  it('SignUpPhoneConfirmation', () => {
    expect(wrapper.dive()).toMatchSnapshot();
  });
});
