import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import SignInWords from '../../src/scenes/SignInWords';

describe('SignInWords', () => {

  const wrapper = shallow(<SignInWords />);

  it('SignInWords', () => {
    expect(wrapper.dive()).toMatchSnapshot();
  });
});
