import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { SingUpWordsConfirmConnected } from '../../src/scenes/SignUpWordsConfirm';
import FilledButton from '../../src/components/FilledButton';
import { Scenes } from '../../src/navigation/shared';

describe('SignUpWords', () => {
  const getStore = () => createStore(reducers);
  const getConnectedWrapper = (currentStore) => {
    const wrapper = shallow(<SingUpWordsConfirmConnected />, {
      context: {
        store: currentStore,
      }
    });
    return wrapper;
  };

  it('matches snapshot', () => {
    const store = getStore();
    const wrapper = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });
});
