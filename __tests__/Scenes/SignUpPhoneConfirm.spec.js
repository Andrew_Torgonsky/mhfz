import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { SignUpPhoneConfirmationConnected } from '../../src/scenes/SignUpPhoneConfirmation';
import SmsCode from '../../src/components/SmsCode';
import { ActionCreators } from '../../src/redux/actions';

describe('SignUpPhoneConfirm', () => {
  const getStore = () => createStore(reducers);
  const getConnectedWrapper = currentStore => shallow(<SignUpPhoneConfirmationConnected />, {
    context: {
      store: currentStore
    }
  });

  it('it matches snapshot in initial state', () => {
    const store = getStore();
    const wrapper = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('passes sms code value to smsinput component and matches snapshot', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('smsCode', '123'));
    const wrapper = getConnectedWrapper(store);
    expect(wrapper.dive().find(SmsCode).prop('value')).toBe('123');
    expect(wrapper.dive()).toMatchSnapshot();
  });



  it('changes state when code is entered', () => {
    const store = getStore();
    const wrapper = getConnectedWrapper(store);
    wrapper
      .dive()
      .find(SmsCode)
      .simulate('changeText', '123');
    const { form: { smsCode } } = store.getState();
    expect(smsCode).toBe('123');
  });

  it('dispatche sms code when input emits onfinish', () => {
    const store = createMockStore(INITIAL_STATE);
    const wrapper = getConnectedWrapper(store);

    expect(store.isActionDispatched(ActionCreators.checkSignUpSmsCode())).toBe(false);

    wrapper
      .dive()
      .find(SmsCode)
      .simulate('finish');

    expect(store.isActionDispatched(ActionCreators.checkSignUpSmsCode())).toBe(true);
  });
});
