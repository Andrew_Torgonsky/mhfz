import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import type { ReduxStateType } from '../../src/types';
import { SignUpConnected } from '../../src/scenes/SignUp';
import { Scenes } from '../../src/navigation/shared';
import CountryCodeInput from '../../src/components/CountryCodeInput';
import InputField from '../../src/components/InputField';
import PasswordInput from '../../src/components/PasswordInput';
import FilledButton from '../../src/components/FilledButton';
import TextButton from '../../src/components/TextButton';
import EmailInput from '../../src/components/EmailInput';
import Checkbox from '../../src/components/Checkbox';
import translations from '../../src/translations';

describe('SignUp', () => {
  const getStore = () => createStore(reducers);
  const getConnectedWrapper = (currentStore) => {
    const navigation = {
      navigate: jest.fn()
    };
    const wrapper = shallow(<SignUpConnected navigation={navigation} />, {
      context: {
        store: currentStore,
      }
    });
    return { wrapper, navigation };
  };

  it('matches snapshot in initial state', () => {
    const store = getStore();
    const { wrapper } = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('changes redux state on input', () => {
    const store = getStore();
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(EmailInput).simulate('changeText', 'user@email.com');
    wrapper.dive().find(InputField).simulate('changeText', '54321');
    wrapper.dive().find(PasswordInput).simulate('changeText', 'pa$$word');
    wrapper.dive().find(PasswordInput).simulate('eyePress');
    wrapper.dive().find(Checkbox).simulate('press');
    const { form: { email, phoneNumber, password, passwordVisibility, agree } }: ReduxStateType = store.getState();
    expect(email).toBe('user@email.com');
    expect(phoneNumber).toBe('54321');
    expect(password).toBe('pa$$word');
    expect(passwordVisibility).toBe(true);
    expect(agree).toBe(false);
  });

  it('passes values from redux to corresponding components', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('email', 'user@email.com'));
    store.dispatch(ActionCreators.changeForm('emailError', true))
    store.dispatch(ActionCreators.changeForm('phonePrefix', '7'));
    store.dispatch(ActionCreators.changeForm('countryIso', 'ru'));
    store.dispatch(ActionCreators.changeForm('phoneNumber', '123456789'));
    store.dispatch(ActionCreators.changeForm('password', 'swordfish'));
    store.dispatch(ActionCreators.changeForm('passwordVisibility', true));
    store.dispatch(ActionCreators.changeForm('phoneError', true));
    store.dispatch(ActionCreators.changeForm('passwordError', true));
    store.dispatch(ActionCreators.changeForm('agree', true));
    const { wrapper } = getConnectedWrapper(store);
    expect(wrapper.dive().find(EmailInput).prop('value')).toBe('user@email.com');
    expect(wrapper.dive().find(EmailInput).prop('error')).toBe(true);
    expect(wrapper.dive().find(CountryCodeInput).prop('countryISO')).toBe('ru');
    expect(wrapper.dive().find(CountryCodeInput).prop('error')).toBe(true);
    expect(wrapper.dive().find(CountryCodeInput).prop('phonePrefix')).toBe('7');
    expect(wrapper.dive().find(InputField).prop('value')).toBe('123456789');
    expect(wrapper.dive().find(InputField).prop('error')).toBe(true);
    expect(wrapper.dive().find(PasswordInput).prop('error')).toBe(true);
    expect(wrapper.dive().find(PasswordInput).prop('visible')).toBe(true);
    expect(wrapper.dive().find(PasswordInput).prop('value')).toBe('swordfish');
    expect(wrapper.dive().find(Checkbox).prop('checked')).toBe(true);
  });

  it('dismisses errors on email input focus', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('emailError', true));
    store.dispatch(ActionCreators.changeForm('phoneError', true));
    store.dispatch(ActionCreators.changeForm('passwordError', true));
    store.dispatch(ActionCreators.changeAlert('some error', 'red'));
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(EmailInput).simulate('focus');
    const { form: { phoneError, passwordError, emailError }, alertState: { alertText, alertType } }: ReduxStateType = store.getState();
    expect(emailError).toBe(false);
    expect(phoneError).toBe(false);
    expect(passwordError).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
  });

  it('dismisses errors on countrycode press', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('emailError', true));
    store.dispatch(ActionCreators.changeForm('phoneError', true));
    store.dispatch(ActionCreators.changeForm('passwordError', true));
    store.dispatch(ActionCreators.changeAlert('some error', 'red'));
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(CountryCodeInput).simulate('press');
    const { form: { phoneError, passwordError, emailError }, alertState: { alertText, alertType } }: ReduxStateType = store.getState();
    expect(emailError).toBe(false);
    expect(phoneError).toBe(false);
    expect(passwordError).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
  });

  it('dismisses errors on phone input focus', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('emailError', true));
    store.dispatch(ActionCreators.changeForm('phoneError', true));
    store.dispatch(ActionCreators.changeForm('passwordError', true));
    store.dispatch(ActionCreators.changeAlert('some error', 'red'));
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(InputField).simulate('focus');
    const { form: { phoneError, passwordError, emailError }, alertState: { alertText, alertType } }: ReduxStateType = store.getState();
    expect(emailError).toBe(false);
    expect(phoneError).toBe(false);
    expect(passwordError).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
  });

  it('dismisses errors on password input focus', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('emailError', true));
    store.dispatch(ActionCreators.changeForm('phoneError', true));
    store.dispatch(ActionCreators.changeForm('passwordError', true));
    store.dispatch(ActionCreators.changeAlert('some error', 'red'));
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(PasswordInput).simulate('focus');
    const { form: { phoneError, passwordError, emailError }, alertState: { alertText, alertType } }: ReduxStateType = store.getState();
    expect(emailError).toBe(false);
    expect(phoneError).toBe(false);
    expect(passwordError).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
  });

  it('dispatches submit signUn on press', () => {
    const store = createMockStore(INITIAL_STATE);
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(FilledButton).simulate('press');
    expect(store.isActionDispatched(ActionCreators.submitSignUp())).toBe(true);
  });

  it('navigates to other screens', () => {
    const store = getStore();
    const { wrapper, navigation } = getConnectedWrapper(store);
    wrapper.dive().find(CountryCodeInput).simulate('press');
    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.CountrySelect);
    wrapper.dive().findWhere(item => item.is(TextButton) && item.is({ title: translations.t('T_A_C') })).simulate('press');
    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.TermsOfService);
    wrapper.dive().findWhere(item => item.is(TextButton) && item.is({ title: translations.t('P_A') })).simulate('press');
    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.PrivacyPolicy);
    wrapper.dive().findWhere(item => item.is(TextButton) && item.is({ title: translations.t('SIGN_IN') })).simulate('press');
    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.SignIn);
  });
});
