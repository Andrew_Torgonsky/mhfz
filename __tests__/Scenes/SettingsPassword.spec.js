import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import type { ReduxStateType } from '../../src/types';
import SettingsPassword, { SettingsPasswordConntected } from '../../src/scenes/SettingsPassword';
import PasswordInput from '../../src/components/PasswordInput';
import FilledButton from '../../src/components/FilledButton';

describe('SettingsPassword', () => {
	const getStore = () => createStore(reducers);
	const getConnectedWrapper = (currentStore) => {
		const wrapper = shallow(<SettingsPasswordConntected />, {
			context: {
				store: currentStore,
			}
		});
		return wrapper;
  };
  
  it('matches snapshot', () => {
		const props = {};
		expect(shallow(<SettingsPassword {...props} />)).toMatchSnapshot();
  });

  it('matches snapshot with props', () => {
		const props = {
      password: 'Qwe123!!!',
      password2nd: 'Asd123!!!'
    };
		expect(shallow(<SettingsPassword {...props} />)).toMatchSnapshot();
  });
  
  it('changes redux state on input', () => {
		const store = getStore();
    const wrapper = getConnectedWrapper(store);
    
    wrapper
      .dive()
      .find(PasswordInput)
      .at(0)
      .simulate('changeText', 'Qwe123!!!');

    wrapper
      .dive()
      .find(PasswordInput)
      .at(1)
      .simulate('changeText', 'Asd123!!!');
    
    const {
      form: {
        password,
        password2nd
      }
    }: ReduxStateType = store.getState();

		expect(password).toBe('Qwe123!!!');
		expect(password2nd).toBe('Asd123!!!');
  });

  it('dismisses errors on enter text', () => {
		const store = getStore();
    store.dispatch(ActionCreators.changeForm('password', 'Qwe123'));
    store.dispatch(ActionCreators.changeForm('passwordError', true));
    store.dispatch(ActionCreators.changeForm('passwordError2nd', true));
		store.dispatch(ActionCreators.changeAlert('some error', 'red'));
		
    const wrapper = getConnectedWrapper(store);
    
    wrapper
      .dive()
      .find(PasswordInput)
      .at(0)
      .simulate('changeText', 'Qwe123!!!');

    const {
      form: {
        passwordError,
        passwordError2nd
      },
      alertState: {
        alertText,
        alertType
      }
    }: ReduxStateType = store.getState();

		expect(passwordError).toBe(false);
    expect(passwordError2nd).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
  });

  it('dismisses errors on input focus', () => {
		const store = getStore();
    store.dispatch(ActionCreators.changeForm('password', 'Qwe123'));
    store.dispatch(ActionCreators.changeForm('passwordError', true));
    store.dispatch(ActionCreators.changeForm('passwordError2nd', true));
		store.dispatch(ActionCreators.changeAlert('some error', 'red'));
		
    const wrapper = getConnectedWrapper(store);
    
    wrapper
      .dive()
      .find(PasswordInput)
      .at(1)
      .simulate('focus');

    const {
      form: {
        passwordError,
        passwordError2nd
      },
      alertState: {
        alertText,
        alertType
      }
    }: ReduxStateType = store.getState();

		expect(passwordError).toBe(false);
    expect(passwordError2nd).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
  });

  it('press button', () => {
    const store = createMockStore(INITIAL_STATE);
    const wrapper = getConnectedWrapper(store);
    wrapper
      .dive()
      .find(FilledButton)
      .simulate('press');

    expect(store.isActionDispatched(ActionCreators.changePassword())).toBe(true);
  });

});
