import 'react-native';
import React from 'react';
import { shallow } from 'enzyme';
import { TermsOfService } from '../../src/scenes/TermsOfService';

describe('TermsOfService', () => {
  const wrapper = shallow(<TermsOfService />);

  it('TermsOfService', () => {
    expect(wrapper.dive()).toMatchSnapshot();
  });
});
