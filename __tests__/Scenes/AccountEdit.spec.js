import React from 'react';
import { TouchableOpacity } from 'react-native';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import reducers from '../../src/redux/reducers';
import { AccountEditConnected, AccountEdit } from '../../src/scenes/AccountEdit';
import { ActionCreators } from '../../src/redux/actions';
import type { ReduxStateType } from '../../src/types';
import EmailInput from '../../src/components/EmailInput';
import InputField from '../../src/components/InputField';
import GhostButton from '../../src/components/GhostButton';
import { Scenes } from '../../src/navigation/shared';

describe('AccountEdit', () => {
	const getStore = () => createStore(reducers);
	const getConnectedWrapper = (currentStore) => {
		const navigation = {
			navigate: jest.fn(),
			setParams: jest.fn()
		};
		const wrapper = shallow(<AccountEditConnected navigation={navigation} />, {
			context: {
				store: currentStore,
			}
		});
		return { wrapper, navigation };
	};

  it('matches snapshot', () => {
		const props = {
			navigation: {
				setParams: jest.fn(),
				name: 'John',
				email: 'john.doe@gmail.com',
				defaultPhoneNumber: '1234567890'
			}
		};
		expect(shallow(<AccountEdit {...props} />)).toMatchSnapshot();
	});
	
	// it('press on the load photo', () => {
	// 	const store = getStore();
	// 	const { wrapper } = getConnectedWrapper(store);
	// 	wrapper
	// 		.dive()
	// 		.find(TouchableOpacity)
	// 		.at(0)
	// 		.simulate('press');
	// 	const { loadPhoto }: ReduxStateType = store.getState();
	// 	expect(loadPhoto).toBe(true);
	// });

	it('dismisses errors on enter text', () => {
		const store = getStore();
    store.dispatch(ActionCreators.changeForm('email', 'developer'));
    store.dispatch(ActionCreators.changeForm('nameError', true));
    store.dispatch(ActionCreators.changeForm('emailError', true));
		store.dispatch(ActionCreators.changeAlert('some error', 'red'));
		
		const { wrapper } = getConnectedWrapper(store);
		
		wrapper
			.dive()
			.find(EmailInput)
			.simulate('changeText', 'developer@allmax.team');

		
		const {
			form: {
				nameError,
				emailError
			},
			alertState: {
				alertText,
				alertType
			}
		}: ReduxStateType = store.getState();

		expect(nameError).toBe(false);
    expect(emailError).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
	});

	it('dismisses errors on input focus', () => {
		const store = getStore();
    store.dispatch(ActionCreators.changeForm('email', 'developer'));
    store.dispatch(ActionCreators.changeForm('nameError', true));
    store.dispatch(ActionCreators.changeForm('emailError', true));
		store.dispatch(ActionCreators.changeAlert('some error', 'red'));
		
		const { wrapper } = getConnectedWrapper(store);
		
		wrapper
			.dive()
			.find(EmailInput)
			.simulate('focus');

		const {
			form: {
				nameError,
				emailError
			},
			alertState: {
				alertText,
				alertType
			}
		}: ReduxStateType = store.getState();

		expect(nameError).toBe(false);
    expect(emailError).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
	});

	it('changes redux state on input', () => {
		const store = getStore();
		const { wrapper } = getConnectedWrapper(store);
		
		wrapper
			.dive()
			.find(InputField)
			.at(0)
			.simulate('changeText', 'Ivan');
		
		wrapper
			.dive()
			.find(EmailInput)
			.simulate('changeText', 'ivan.ivanovich@gmail.com');

		const {
			form: {
				name,
				email
			}
		}: ReduxStateType = store.getState();

		expect(name).toBe('Ivan');
		expect(email).toBe('ivan.ivanovich@gmail.com');
	});
});
