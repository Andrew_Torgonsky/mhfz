import React from 'react';
import { shallow } from 'enzyme';
import { createMockStore } from 'redux-test-utils';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { AccountConnected } from '../../src/scenes/Account';

describe('Account', () => {
  const getConnectedWrapper = currentStore => shallow(
    <AccountConnected />,
    {
      context: {
        store: currentStore
      }
    }
	);
  const store = createMockStore(INITIAL_STATE);
  const wrapper = getConnectedWrapper(store);
	
	it('it matches snapshot in initial state', () => expect(wrapper.dive()).toMatchSnapshot());
});
