import React from 'react';
import { shallow } from 'enzyme';
import { PrivacyPolicy } from '../../src/scenes/PrivacyPolicy';

describe('PrivacyPolicy', () => {
  const wrapper = shallow(<PrivacyPolicy />);

  it('PrivacyPolicy', () => {
    expect(wrapper.dive()).toMatchSnapshot();
  });
});
