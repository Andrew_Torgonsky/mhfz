import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import DefaultCurrency, { DefaultCurrencyConnected } from '../../src/scenes/DefaultCurrency';
import MenuItem from '../../src/components/MenuItem';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import type { ReduxStateType } from '../../src/types';

describe('DefaultCurrency', () => {
	const getStore = () => createStore(reducers);
	const getWrapper = () => shallow(<DefaultCurrency />);
  const getConnectedWrapper = currentStore => shallow(
    <DefaultCurrencyConnected />,
    {
      context: {
        store: currentStore
      }
    }
  );

  it('it matches snapshot in initial state', () => {
    const store = getStore();
    const wrapper = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('simulation of pressing, selection: usd', () => {
    const store = createMockStore(INITIAL_STATE);
    const wrapper = getConnectedWrapper(store);
    wrapper
      .dive()
      .find(MenuItem)
      .at(0)
      .simulate('press');
    expect(store.isActionDispatched(ActionCreators.changeCurrency('usd'))).toBe(true);
  });

  it('simulation of pressing, selection: eur', () => {
    const store = createMockStore(INITIAL_STATE);
    const wrapper = getConnectedWrapper(store);
    wrapper
      .dive()
      .find(MenuItem)
      .at(1)
      .simulate('press');
    expect(store.isActionDispatched(ActionCreators.changeCurrency('eur'))).toBe(true);
  });

  it('simulation of pressing, selection: sar', () => {
    const store = createMockStore(INITIAL_STATE);
    const wrapper = getConnectedWrapper(store);
    wrapper
      .dive()
      .find(MenuItem)
      .at(2)
      .simulate('press');
    expect(store.isActionDispatched(ActionCreators.changeCurrency('sar'))).toBe(true);
  });
});
