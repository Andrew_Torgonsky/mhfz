import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import { type ReduxStateType } from '../../src/types';
import { SignInPinConfirmConnected } from '../../src/scenes/SignInPinConfirm';
import PinInput from '../../src/components/PinInput';


const getStore = () => createStore(reducers);
const getConnectedWrapper = (currentStore) => {
  const navigation = {
    navigate: jest.fn()
  };
  const wrapper = shallow(<SignInPinConfirmConnected navigation={navigation} />, {
    context: {
      store: currentStore,
    }
  });
  return { wrapper, navigation };
};

describe('SignInPinConfirm', () => {
  it('triggers comparepincodes action on input finish', () => {
    const store = createMockStore(INITIAL_STATE);
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(PinInput).simulate('finish');
    expect(store.isActionDispatched(ActionCreators.compareSignInPinCodes())).toBe(true);
  });
  it('changes redux state on input', () => {
    const store = getStore();
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(PinInput).simulate('changeText', '1456');
    const { form: { pinCodeConfirm } }: ReduxStateType = store.getState();
    expect(pinCodeConfirm).toBe('1456');
  });
  it('passes values from redux state to components', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('pinCodeConfirm', '1234'));
    store.dispatch(ActionCreators.changeForm('pinCodeError', true));
    const { wrapper } = getConnectedWrapper(store);
    expect(wrapper.dive().find(PinInput).prop('value')).toBe('1234');
    expect(wrapper.dive().find(PinInput).prop('error')).toBe(true);
  });
  it('matches snapshot', () => {
    const store = getStore();
    const { wrapper } = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });
});
