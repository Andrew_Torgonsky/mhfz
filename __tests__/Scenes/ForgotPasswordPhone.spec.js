import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import FilledButton from '../../src/components/FilledButton';
import InputField from '../../src/components/InputField';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import { type ReduxStateType } from '../../src/types';
import { ForgotPasswordPhoneConnected } from '../../src/scenes/ForgotPasswordPhone';
import CountryCodeInput from '../../src/components/CountryCodeInput';
import { Scenes } from '../../src/navigation/shared';

describe('ForgotPasswordPhone', () => {
  const getStore = () => createStore(reducers);
  const getConnectedWrapper = (currentStore) => {
    const navigation = {
      navigate: jest.fn()
    };
    const wrapper = shallow(<ForgotPasswordPhoneConnected navigation={navigation} />, {
      context: {
        store: currentStore,
      }
    });
    return { wrapper, navigation };
  };

  it('dissmisses errors when country input is pressed and navigates to countryselect', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('phoneError', true));
    store.dispatch(ActionCreators.changeAlert('some error', 'red'));
    const { wrapper, navigation } = getConnectedWrapper(store);
    wrapper.dive().find(CountryCodeInput).simulate('press');
    const { form: { phoneError }, alertState: { alertText, alertType } }: ReduxStateType = store.getState();
    expect(phoneError).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
    expect(navigation.navigate).toBeCalledWith(Scenes.CountrySelect);
  });

  it('dissmisses errors when phone input is focused', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('phoneError', true));
    store.dispatch(ActionCreators.changeAlert('some error', 'red'));
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(InputField).simulate('focus');
    const { form: { phoneError }, alertState: { alertText, alertType } }: ReduxStateType = store.getState();
    expect(phoneError).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
  });

  it('changes redux state when phone number is entered', () => {
    const store = getStore();
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(InputField).simulate('changeText', '12345');
    const { form: { phoneNumber } }: ReduxStateType = store.getState();
    expect(phoneNumber).toBe('12345');
  });

  it('triggers request password recovery when clicked continue', () => {
    const store = createMockStore(INITIAL_STATE);
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(FilledButton).simulate('press');
    expect(store.isActionDispatched(ActionCreators.requestPasswordRecovery())).toBe(true);
  });

  it('passes values from redux to corresponding components', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('phonePrefix', '7'));
    store.dispatch(ActionCreators.changeForm('countryIso', 'ru'));
    store.dispatch(ActionCreators.changeForm('phoneNumber', '123456789'));
    store.dispatch(ActionCreators.changeForm('phoneError', true));
    const { wrapper } = getConnectedWrapper(store);
    expect(wrapper.dive().find(CountryCodeInput).prop('countryISO')).toBe('ru');
    expect(wrapper.dive().find(CountryCodeInput).prop('error')).toBe(true);
    expect(wrapper.dive().find(CountryCodeInput).prop('phonePrefix')).toBe('7');
    expect(wrapper.dive().find(InputField).prop('value')).toBe('123456789');
    expect(wrapper.dive().find(InputField).prop('error')).toBe(true);
  });

  it('matches snapshot', () => {
    const store = getStore();
    const { wrapper } = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });
});
