import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import { type ReduxStateType } from '../../src/types';
import PasswordInput from '../../src/components/PasswordInput';
import { ForgotPasswordResetConnected, ForgotPasswordReset } from '../../src/scenes/ForgotPasswordReset';
import PasswordRequirementsModal from '../../src/components/PasswordRequirementsModal';
import TextButton from '../../src/components/TextButton';
import FilledButton from '../../src/components/FilledButton';

const getStore = () => createStore(reducers);
const getWrapper = () => shallow(<ForgotPasswordReset />);
const getConnectedWrapper = (currentStore) => {
  const wrapper = shallow(<ForgotPasswordResetConnected />, {
    context: {
      store: currentStore,
    }
  });
  return wrapper;
};

describe('ForgotPasswordReset', () => {
  it('passes values from redux to corresponding components', () => {
    const store = getStore();
    const wrapper = getConnectedWrapper(store);
    expect(wrapper.dive().find(PasswordInput).prop('value')).toBe('');
    expect(wrapper.dive().find(PasswordInput).prop('visible')).toBe(false);
    expect(wrapper.dive().find(PasswordInput).prop('error')).toBe(false);
    store.dispatch(ActionCreators.changeForm('password', 'swordfish'));
    store.dispatch(ActionCreators.changeForm('passwordVisibility', true));
    store.dispatch(ActionCreators.changeForm('passwordError', true));
    const wrapper2 = getConnectedWrapper(store);
    expect(wrapper2.dive().find(PasswordInput).prop('value')).toBe('swordfish');
    expect(wrapper2.dive().find(PasswordInput).prop('visible')).toBe(true);
    expect(wrapper2.dive().find(PasswordInput).prop('error')).toBe(true);
  });

  it('opens and closes modal with password requirements', () => {
    const wrapper = getWrapper();
    expect(wrapper.find(PasswordRequirementsModal).length).toBe(0);
    wrapper.find(TextButton).simulate('press');
    expect(wrapper.find(PasswordRequirementsModal).length).toBe(1);
    wrapper.find(TextButton).simulate('press');
    expect(wrapper.find(PasswordRequirementsModal).length).toBe(0);
  });

  it('dismisses errors when password input is focused', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeAlert('some error', 'red'));
    store.dispatch(ActionCreators.changeForm('passwordError', true));
    const wrapper = getConnectedWrapper(store);
    wrapper.dive().find(PasswordInput).simulate('focus');
    const { form: { passwordError }, alertState: { alertText, alertType } }: ReduxStateType = store.getState();
    expect(passwordError).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
  });

  it('changes redux state when password is entered or visibility button is pressed', () => {
    const store = getStore();
    const wrapper = getConnectedWrapper(store);
    wrapper.dive().find(PasswordInput).simulate('changeText', 'swordfish');
    wrapper.dive().find(PasswordInput).simulate('eyePress');
    const { form: { password, passwordVisibility } }: ReduxStateType = store.getState();
    expect(password).toBe('swordfish');
    expect(passwordVisibility).toBe(true);
  });

  it('dispatches update password on corresponding button press', () => {
    const store = createMockStore(INITIAL_STATE);
    const wrapper = getConnectedWrapper(store);
    wrapper.dive().find(FilledButton).simulate('press');
    expect(store.isActionDispatched(ActionCreators.updatePassword())).toBe(true);
  });

  it('matches snapshot in initial state', () => {
    const store = getStore();
    const wrapper = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });
});
