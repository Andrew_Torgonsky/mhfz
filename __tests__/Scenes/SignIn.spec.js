import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import { type ReduxStateType } from '../../src/types';
import { SignInConnected } from '../../src/scenes/SignIn';
import CountryCodeInput from '../../src/components/CountryCodeInput';
import InputField from '../../src/components/InputField';
import PasswordInput from '../../src/components/PasswordInput';
import FilledButton from '../../src/components/FilledButton';
import TextButton from '../../src/components/TextButton';
import { Scenes } from '../../src/navigation/shared';
import translations from '../../src/translations';

const getStore = () => createStore(reducers);
const getConnectedWrapper = (currentStore) => {
  const navigation = {
    navigate: jest.fn()
  };
  const wrapper = shallow(<SignInConnected navigation={navigation} />, {
    context: {
      store: currentStore,
    }
  });
  return { wrapper, navigation };
};

describe('SignIn', () => {
  it('dismisses errors on countrycode press', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('phoneError', true));
    store.dispatch(ActionCreators.changeForm('passwordError', true));
    store.dispatch(ActionCreators.changeAlert('some error', 'red'));
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(CountryCodeInput).simulate('press');
    const { form: { phoneError, passwordError }, alertState: { alertText, alertType } }: ReduxStateType = store.getState();
    expect(phoneError).toBe(false);
    expect(passwordError).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
  });
  it('dismisses errors on phone input focus', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('phoneError', true));
    store.dispatch(ActionCreators.changeForm('passwordError', true));
    store.dispatch(ActionCreators.changeAlert('some error', 'red'));
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(InputField).simulate('focus');
    const { form: { phoneError, passwordError }, alertState: { alertText, alertType } }: ReduxStateType = store.getState();
    expect(phoneError).toBe(false);
    expect(passwordError).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
  });
  it('dismisses errors on password input focus', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('phoneError', true));
    store.dispatch(ActionCreators.changeForm('passwordError', true));
    store.dispatch(ActionCreators.changeAlert('some error', 'red'));
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(PasswordInput).simulate('focus');
    const { form: { phoneError, passwordError }, alertState: { alertText, alertType } }: ReduxStateType = store.getState();
    expect(phoneError).toBe(false);
    expect(passwordError).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
  });
  it('passes values from redux to corresponding components', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('phonePrefix', '7'));
    store.dispatch(ActionCreators.changeForm('countryIso', 'ru'));
    store.dispatch(ActionCreators.changeForm('phoneNumber', '123456789'));
    store.dispatch(ActionCreators.changeForm('password', 'swordfish'));
    store.dispatch(ActionCreators.changeForm('passwordVisibility', true));
    store.dispatch(ActionCreators.changeForm('phoneError', true));
    store.dispatch(ActionCreators.changeForm('passwordError', true));
    const { wrapper } = getConnectedWrapper(store);
    expect(wrapper.dive().find(CountryCodeInput).prop('countryISO')).toBe('ru');
    expect(wrapper.dive().find(CountryCodeInput).prop('error')).toBe(true);
    expect(wrapper.dive().find(CountryCodeInput).prop('phonePrefix')).toBe('7');
    expect(wrapper.dive().find(InputField).prop('value')).toBe('123456789');
    expect(wrapper.dive().find(InputField).prop('error')).toBe(true);
    expect(wrapper.dive().find(PasswordInput).prop('error')).toBe(true);
    expect(wrapper.dive().find(PasswordInput).prop('visible')).toBe(true);
    expect(wrapper.dive().find(PasswordInput).prop('value')).toBe('swordfish');
  });
  it('changes redux state on input', () => {
    const store = getStore();
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(InputField).simulate('changeText', '54321');
    wrapper.dive().find(PasswordInput).simulate('changeText', 'pa$$word');
    wrapper.dive().find(PasswordInput).simulate('eyePress');
    const { form: { phoneNumber, password, passwordVisibility } }: ReduxStateType = store.getState();
    expect(phoneNumber).toBe('54321');
    expect(password).toBe('pa$$word');
    expect(passwordVisibility).toBe(true);
  });
  it('dispatches submit signIn on press', () => {
    const store = createMockStore(INITIAL_STATE);
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(FilledButton).simulate('press');
    expect(store.isActionDispatched(ActionCreators.submitSignInFields())).toBe(true);
  });
  it('navigates to other screens', () => {
    const store = getStore();
    const { wrapper, navigation } = getConnectedWrapper(store);
    wrapper.dive().find(CountryCodeInput).simulate('press');
    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.CountrySelect);
    wrapper.dive().findWhere(item => item.is(TextButton) && item.is({ title: translations.t('FORGOT_PWD') })).simulate('press');
    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.ForgotPasswordPhone);
    wrapper.dive().findWhere(item => item.is(TextButton) && item.is({ title: translations.t('SIGN_UP') })).simulate('press');
    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.SignUp);
  });
  it('matches snapshot in initial state', () => {
    const store = getStore();
    const { wrapper } = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });
});
