import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import type { ReduxStateType } from '../../src/types';
import PinInput from '../../src/components/PinInput';
import { Scenes } from '../../src/navigation/shared';
import { SignUpPinConnected } from '../../src/scenes/SignUpPin';

describe('SignUpPin', () => {
  const getStore = () => createStore(reducers);
  const getConnectedWrapper = (currentStore) => {
    const navigation = {
      navigate: jest.fn(),
      getParam: jest.fn()
    };
    const wrapper = shallow(<SignUpPinConnected navigation={navigation} />, {
      context: {
        store: currentStore,
      }
    });
    return { wrapper, navigation };
  };

  it('matches snapshot in initial state', () => {
    const store = getStore();
    const { wrapper } = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('changes redux state on input', () => {
    const store = getStore();
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(PinInput).simulate('changeText', '4444');
    const { form: { pinCode } }: ReduxStateType = store.getState();
    expect(pinCode).toBe('4444');
  });

  it('passes values from redux to corresponding components', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('pinCode', '321'));
    store.dispatch(ActionCreators.changeForm('pinCodeError', true));
    const { wrapper } = getConnectedWrapper(store);
    expect(wrapper.dive().find(PinInput).prop('value')).toBe('321');
    expect(wrapper.dive().find(PinInput).prop('error')).toBe(true);
  });

  it('navigates to signinpinconfirm on input finish', () => {
    const store = getStore();
    const { wrapper, navigation } = getConnectedWrapper(store);
    wrapper.dive().find(PinInput).simulate('finish');
    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.SignUpPinConfirm);
  });
});
