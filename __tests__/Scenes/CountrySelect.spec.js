import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { FlatList } from 'react-native';
import { CountrySelectConnected, CountrySelect } from '../../src/scenes/CountrySelect';
import SearchInput from '../../src/components/SearchInput';
import reducers from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import countries from '../../src/global/countries';

describe('CountrySelect', () => {
  const getStore = () => createStore(reducers);
  const getWrapper = () => shallow(<CountrySelect />);
  const getConnectedWrapper = currentStore => shallow(<CountrySelectConnected />, {
    context: {
      store: currentStore
    }
  });
  it('it matches snapshot in initial state', () => {
    const store = getStore();
    const wrapper = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  // it('dispatches proper actions', () => {
  //   const store = getStore();
  //   const wrapper = getConnectedWrapper(store);
  //   wrapper.dive().simulate('changeForm', 'countryIso', 'ru');
  //   wrapper.dive().simulate('changeForm', 'phonePrefix', '7');
  //   const { form: { countryIso, phonePrefix } } = store.getState();
  //   expect(countryIso).toBe('ru');
  //   expect(phonePrefix).toBe('7');
  // });
  // no idea how to trigger onpress of render list item due to its absence in shallow render

  // it('filters conuntry list on input regardless of choosen case and presence of + sign', () => {
  //   const wrapper = getWrapper();

  //   expect(JSON.stringify(wrapper.find(FlatList).prop('data'))).toBe(JSON.stringify(countries));

  //   wrapper.find(SearchInput).simulate('ChangeText', 'sa');
  //   const filteredCountries = countries.filter(el => el.name.toLowerCase().startsWith('sa'));
  //   expect(JSON.stringify(wrapper.find(FlatList).prop('data'))).toBe(JSON.stringify(filteredCountries));

  //   wrapper.find(SearchInput).simulate('ChangeText', 'SA');
  //   const filteredCountries2 = countries.filter(el => el.name.toUpperCase().startsWith('SA'));
  //   expect(JSON.stringify(wrapper.find(FlatList).prop('data'))).toBe(JSON.stringify(filteredCountries2));

  //   wrapper.find(SearchInput).simulate('ChangeText', '7');
  //   const filteredCountries3 = countries.filter(el => el.dialCode.startsWith('7'));
  //   expect(JSON.stringify(wrapper.find(FlatList).prop('data'))).toBe(JSON.stringify(filteredCountries3));

  //   wrapper.find(SearchInput).simulate('ChangeText', '+7');
  //   const filteredCountries4 = countries.filter(el => el.dialCode.startsWith('7'));
  //   expect(JSON.stringify(wrapper.find(FlatList).prop('data'))).toBe(JSON.stringify(filteredCountries4));
  // });
  // need to rewrite this test

  it('changes value of search input on input', () => {
    const wrapper = getWrapper();
    expect(wrapper.find(SearchInput).prop('value')).toBe('');
    wrapper.find(SearchInput).simulate('ChangeText', 'sa');
    expect(wrapper.find(SearchInput).prop('value')).toBe('sa');
  });
});
