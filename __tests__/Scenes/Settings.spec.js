import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import type { ReduxStateType } from '../../src/types';
import { SettingsConnected } from '../../src/scenes/Settings';
import MenuItem from '../../src/components/MenuItem';
import { Scenes } from '../../src/navigation/shared';

describe('Settings', () => {
  const getStore = () => createStore(reducers);
  const getConnectedWrapper = (currentStore) => {
    const navigation = {
      navigate: jest.fn()
    };
    const wrapper = shallow(<SettingsConnected navigation={navigation} />, {
      context: {
        store: currentStore,
      }
    });
    return { wrapper, navigation };
  };

  it('matches snapshot', () => {
    const store = getStore();
    const { wrapper } = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });

  it('press on the button "Default Currency"', () => {
    const store = getStore();
    const { wrapper, navigation } = getConnectedWrapper(store);

    wrapper
      .dive()
      .find(MenuItem)
      .at(0)
      .simulate('press');

    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.DefaultCurrency);
  });

  it('press on the button "Language"', () => {
    const store = getStore();
    const { wrapper, navigation } = getConnectedWrapper(store);

    wrapper
      .dive()
      .find(MenuItem)
      .at(1)
      .simulate('press');

    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.Language);
  });

  it('press on the button "Notifications"', () => {
    const store = createMockStore(INITIAL_STATE);
    const { wrapper } = getConnectedWrapper(store);

    wrapper
      .dive()
      .find(MenuItem)
      .at(2)
      .simulate('press');

    expect(store.isActionDispatched(ActionCreators.switchNotifications())).toBe(true);
  });

  it('press on the button "Change PIN"', () => {
    const store = getStore();
    const { wrapper, navigation } = getConnectedWrapper(store);

    wrapper
      .dive()
      .find(MenuItem)
      .at(3)
      .simulate('press');

    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.SettingsPin);
  });

  it('press on the button "Change password"', () => {
    const store = getStore();
    const { wrapper, navigation } = getConnectedWrapper(store);

    wrapper
      .dive()
      .find(MenuItem)
      .at(4)
      .simulate('press');

    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.SettingsPassword);
  });

  it('press on the button "Change phone number"', () => {
    const store = getStore();
    const { wrapper, navigation } = getConnectedWrapper(store);

    wrapper
      .dive()
      .find(MenuItem)
      .at(5)
      .simulate('press');

    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.SettingsPhone);
  });

  it('press on the button "Privacy policy"', () => {
    const store = getStore();
    const { wrapper, navigation } = getConnectedWrapper(store);

    wrapper
      .dive()
      .find(MenuItem)
      .at(7)
      .simulate('press');

    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.PrivacyPolicy);
  });

  it('press on the button "Terms of service"', () => {
    const store = getStore();
    const { wrapper, navigation } = getConnectedWrapper(store);

    wrapper
      .dive()
      .find(MenuItem)
      .at(8)
      .simulate('press');

    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.TermsOfService);
  });

  it('press on the button "Log Out"', () => {
    const store = createMockStore(INITIAL_STATE);
    const { wrapper } = getConnectedWrapper(store);

    wrapper
      .dive()
      .find(MenuItem)
      .at(9)
      .simulate('press');

    expect(store.isActionDispatched(ActionCreators.logOut())).toBe(true);
  });
});
