import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import { type ReduxStateType } from '../../src/types';
import { SignInPinConnected } from '../../src/scenes/SignInPin';
import PinInput from '../../src/components/PinInput';
import { Scenes } from '../../src/navigation/shared';


const getStore = () => createStore(reducers);
const getConnectedWrapper = (currentStore) => {
  const navigation = {
    navigate: jest.fn()
  };
  const wrapper = shallow(<SignInPinConnected navigation={navigation} />, {
    context: {
      store: currentStore,
    }
  });
  return { wrapper, navigation };
};

describe('SignInPin', () => {
  it('navigates to signinpinconfirm on input finish', () => {
    const store = getStore();
    const { wrapper, navigation } = getConnectedWrapper(store);
    wrapper.dive().find(PinInput).simulate('finish');
    expect(navigation.navigate).toHaveBeenLastCalledWith(Scenes.SignInPinConfirm);
  });
  it('changes redux state on input', () => {
    const store = getStore();
    const { wrapper } = getConnectedWrapper(store);
    wrapper.dive().find(PinInput).simulate('changeText', '4444');
    const { form: { pinCode } }: ReduxStateType = store.getState();
    expect(pinCode).toBe('4444');
  });

  it('passes values from redux to corresponding components', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('pinCode', '321'));
    store.dispatch(ActionCreators.changeForm('pinCodeError', true));
    const { wrapper } = getConnectedWrapper(store);
    expect(wrapper.dive().find(PinInput).prop('value')).toBe('321');
    expect(wrapper.dive().find(PinInput).prop('error')).toBe(true);
  });
  it('matches snapshot in initial state', () => {
    const store = getStore();
    const { wrapper } = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });
});

