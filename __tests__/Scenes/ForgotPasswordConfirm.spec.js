import React from 'react';
import { shallow } from 'enzyme';
import { createStore } from 'redux';
import { createMockStore } from 'redux-test-utils';
import { ForgotPasswordConfirmConnected } from '../../src/scenes/ForgotPasswordConfirm';
import SmsCode from '../../src/components/SmsCode';
import TextButton from '../../src/components/TextButton';
import reducers, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import { type ReduxStateType } from '../../src/types';

describe('ForgotPasswordConfirm', () => {
  const getStore = () => createStore(reducers);
  const getConnectedWrapper = currentStore => shallow(<ForgotPasswordConfirmConnected />, {
    context: {
      store: currentStore
    }
  });

  it('changes smscode in redux state and dismisses errors when code is entered', () => {
    const store = getStore();
    store.dispatch(ActionCreators.changeForm('smsCodeError', true));
    store.dispatch(ActionCreators.changeAlert('some error', 'red'));
    const wrapper = getConnectedWrapper(store);
    wrapper.dive().find(SmsCode).simulate('changeText', '123');
    const { form: { smsCode, smsCodeError }, alertState: { alertText, alertType } }: ReduxStateType = store.getState();
    expect(smsCode).toBe('123');
    expect(smsCodeError).toBe(false);
    expect(alertText).toBe('');
    expect(alertType).toBe('');
  });

  // it('dismisses errors when focused', () => {
  //   const store = getStore();
  //   store.dispatch(ActionCreators.changeForm('smsCodeError', true));
  //   store.dispatch(ActionCreators.changeAlert('some error', 'red'));
  //   const wrapper = getConnectedWrapper(store);
  //   wrapper.dive().find(SmsCode).simulate('focus');
  //   const { form: { smsCodeError }, alertState: { alertText, alertType } }: ReduxStateType = store.getState();
  //   expect(smsCodeError).toBe(false);
  //   expect(alertText).toBe('');
  //   expect(alertType).toBe('');
  // });

  // it('dispatches password recovery request when corresponding button is pressed', () => {
  //   const store = createMockStore(INITIAL_STATE);
  //   const wrapper = getConnectedWrapper(store);
  //   wrapper.dive().find(TextButton).simulate('press');
  //   expect(store.isActionDispatched(ActionCreators.requestPasswordRecovery())).toBe(true);
  // });

  it('dispatches check password recovery code when input is finished', () => {
    const store = createMockStore(INITIAL_STATE);
    const wrapper = getConnectedWrapper(store);
    wrapper.dive().find(SmsCode).simulate('finish');
    expect(store.isActionDispatched(ActionCreators.checkRecoverySecret())).toBe(true);
  });

  it('passes value and error to smsCode component', () => {
    const store = getStore();

    const wrapper = getConnectedWrapper(store);
    expect(wrapper.dive().find(SmsCode).prop('value')).toBe('');
    expect(wrapper.dive().find(SmsCode).prop('error')).toBe(false);

    store.dispatch(ActionCreators.changeForm('smsCodeError', true));
    store.dispatch(ActionCreators.changeForm('smsCode', '123'));

    const wrapper2 = getConnectedWrapper(store);
    expect(wrapper2.dive().find(SmsCode).prop('value')).toBe('123');
    expect(wrapper2.dive().find(SmsCode).prop('error')).toBe(true);
  });

  it('matches snapshot', () => {
    const store = getStore();
    const wrapper = getConnectedWrapper(store);
    expect(wrapper.dive()).toMatchSnapshot();
  });
});

