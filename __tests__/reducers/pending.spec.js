// @flow

import rootReducer, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';

describe('pending reducer', () => {
  it('changes state according to provided action', () => {
    expect(rootReducer(INITIAL_STATE, ActionCreators.changePending('passwordRecovery', true))).toEqual({
      ...INITIAL_STATE,
      pending: {
        ...INITIAL_STATE.pending,
        passwordRecovery: true
      }
    });
  });
});