// @flow

import rootReducer, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';

describe('form reducer', () => {
  it('changes values of the form on changeForm action', () => {
    expect(rootReducer(INITIAL_STATE, ActionCreators.changeForm('name', 'Alexandr'))).toEqual({
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        name: 'Alexandr'
      }
    });
    expect(rootReducer(INITIAL_STATE, ActionCreators.changeForm('phoneNumber', '123'))).toEqual({
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        phoneNumber: '123'
      }
    });
  });
  it('changes multiple values of the form on changeFormMultiple action', () => {
    expect(rootReducer(INITIAL_STATE, ActionCreators.changeFormMultiple({
      password: 'swordfish',
      passwordError: true
    }))).toEqual({
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        password: 'swordfish',
        passwordError: true
      }
    });
  });
  it('changes one of twelve words on changeFormTwelveWords action', () => {
    expect(rootReducer(INITIAL_STATE, ActionCreators.changeFormTwelveWords('abc', 0))).toEqual({
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        twelveWords: ['abc']
      }
    });
    expect(rootReducer(INITIAL_STATE, ActionCreators.changeFormTwelveWords('abc', 1))).toEqual({
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        twelveWords: [undefined, 'abc']
      }
    });
    expect(rootReducer({
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        twelveWords: ['cabin', 'carpet', 'rude']
      }
    }, ActionCreators.changeFormTwelveWords('abc', 1))).toEqual({
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        twelveWords: ['cabin', 'abc', 'rude']
      }
    });
  });
  it('removes twelve words and resets visibility status of randomized words on clearTwelveWords action', () => {
    expect(rootReducer({
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        twelveWords: ['cabin', 'carpet', 'rude'], // for test it doesn't matter whether there are 12 words of less
        twelveWordsRandom: [{ text: 'cabin', visible: false }, { text: 'carpet', visible: false }, { text: 'rude', visible: false }, { text: 'pony', visible: true }]
      }
    }, ActionCreators.clearTwelveWords())).toEqual({
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        twelveWords: [],
        twelveWordsRandom: [{ text: 'cabin', visible: true }, { text: 'carpet', visible: true }, { text: 'rude', visible: true }, { text: 'pony', visible: true }]
      }
    });
  });
  it('inserts chosen word to twelveWords array and set visibility of corresponding element of twelveWordsRandom to false on pickRandomWord action', () => {
    expect(rootReducer({
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        twelveWords: ['cabin'],
        twelveWordsRandom: [{ text: 'cabin', visible: false }, { text: 'spoil', visible: true }, { text: 'pony', visible: true }],
      }
    }, ActionCreators.pickRandomWord('pony'))).toEqual({
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        twelveWords: ['cabin', 'pony'],
        twelveWordsRandom: [{ text: 'cabin', visible: false }, { text: 'spoil', visible: true }, { text: 'pony', visible: false }],
      }
    })
  });
  it('(if one word is present two times) inserts chosen word to twelveWords array and set visibility of only one corresponding element of twelveWordsRandom to false on pickRandomWord action', () => {
    expect(rootReducer({
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        twelveWords: ['cabin'],
        twelveWordsRandom: [{ text: 'cabin', visible: false }, { text: 'spoil', visible: true }, { text: 'pony', visible: true }, { text: 'spoil', visible: true }],
      }
    }, ActionCreators.pickRandomWord('spoil'))).toEqual({
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        twelveWords: ['cabin', 'spoil'],
        twelveWordsRandom: [{ text: 'cabin', visible: false }, { text: 'spoil', visible: false }, { text: 'pony', visible: true }, { text: 'spoil', visible: true }],
      }
    })
  });
  it('does nothing on pickRandomWord action if choosen word was not found among visible randomized words', () => {
    expect(rootReducer({
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        twelveWords: ['cabin'],
        twelveWordsRandom: [{ text: 'cabin', visible: false }, { text: 'spoil', visible: true }, { text: 'pony', visible: true }],
      }
    }, ActionCreators.pickRandomWord('cabin'))).toEqual({
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        twelveWords: ['cabin'],
        twelveWordsRandom: [{ text: 'cabin', visible: false }, { text: 'spoil', visible: true }, { text: 'pony', visible: true }],
      }
    })
  });
});