// @flow

import rootReducer, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';
import moment from 'moment';

describe('walletsStat reducer', () => {
  // -----------------------------------------------------------------------------------------------------------------------------------------------
  it('changes state on changeWalletsStat', () => {
    expect(rootReducer(INITIAL_STATE, ActionCreators.changeWalletsStat('bitcoin', 'rate', 6000))).toEqual({
      ...INITIAL_STATE,
      walletsStat: {
        ...INITIAL_STATE.walletsStat,
        bitcoin: {
          ...INITIAL_STATE.walletsStat.bitcoin,
          rate: 6000,
        }
      }
    });
  });
  // -----------------------------------------------------------------------------------------------------------------------------------------------
  it('changes multiple values on changeWalletsStatMultiple', () => {
    expect(rootReducer(INITIAL_STATE, ActionCreators.changeWalletsStatMultiple('ethereum', {
      balance: 10,
      rate: 400
    }))).toEqual({
      ...INITIAL_STATE,
      walletsStat: {
        ...INITIAL_STATE.walletsStat,
        ethereum: {
          ...INITIAL_STATE.walletsStat.ethereum,
          rate: 400,
          balance: 10
        }
      }
    });
  });
  // -----------------------------------------------------------------------------------------------------------------------------------------------
  it('puts transactions into state ordered by timestamp (and doesn\'t switch newTx to true because lastChecked is newer than transactions)', () => {
    expect(rootReducer({
      ...INITIAL_STATE,
      walletsStat: {
        ...INITIAL_STATE.walletsStat,
        ripple: {
          ...INITIAL_STATE.walletsStat.ripple,
          lastChecked: moment('2018-08-25 12:00').valueOf()
        }
      }
    }, ActionCreators.putTransactions('ripple', [
      {
        id: 'asdasd1',
        address: 'sadasdas',
        amount: 4,
        status: 'success',
        timestamp: moment('2018-08-25 10:00').valueOf(),
        confirmations: 5,
        block: 12345,
        isSeen: false
      },
      {
        id: 'asdasd3',
        address: 'sadasdas',
        amount: 4,
        status: 'success',
        timestamp: moment('2018-08-25 9:00').valueOf(),
        confirmations: 5,
        block: 12345,
        isSeen: false
      },
      {
        id: 'asdasd2',
        address: 'sadasdas',
        amount: 4,
        status: 'success',
        timestamp: moment('2018-08-25 11:00').valueOf(),
        confirmations: 5,
        block: 12345,
        isSeen: false
      },
    ]))).toEqual({
      ...INITIAL_STATE,
      walletsStat: {
        ...INITIAL_STATE.walletsStat,
        ripple: {
          ...INITIAL_STATE.walletsStat.ripple,
          lastChecked: moment('2018-08-25 12:00').valueOf(),
          transactions: [
            {
              id: 'asdasd2',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 11:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
            {
              id: 'asdasd1',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 10:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
            {
              id: 'asdasd3',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 9:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
          ]
        }
      }
    })
  });
  // -----------------------------------------------------------------------------------------------------------------------------------------------
  it('puts transactions into state and switches newTx to true if lastChecked is older than transactions', () => {
    expect(rootReducer({
      ...INITIAL_STATE,
      walletsStat: {
        ...INITIAL_STATE.walletsStat,
        ripple: {
          ...INITIAL_STATE.walletsStat.ripple,
          lastChecked: moment('2018-08-25 9:00').valueOf()
        }
      }
    }, ActionCreators.putTransactions('ripple', [
      {
        id: 'asdasd1',
        address: 'sadasdas',
        amount: 4,
        status: 'success',
        timestamp: moment('2018-08-25 10:00').valueOf(),
        confirmations: 5,
        block: 12345,
        isSeen: false
      },
    ]))).toEqual({
      ...INITIAL_STATE,
      walletsStat: {
        ...INITIAL_STATE.walletsStat,
        ripple: {
          ...INITIAL_STATE.walletsStat.ripple,
          lastChecked: moment('2018-08-25 9:00').valueOf(),
          newTx: true,
          transactions: [
            {
              id: 'asdasd1',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 10:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
          ]
        }
      }
    })
  });
  // -----------------------------------------------------------------------------------------------------------------------------------------------
  it('merge old and new transactions (with the same id) keeping old isSeen status (and sorting by time)', () => {
    expect(rootReducer({
      ...INITIAL_STATE,
      walletsStat: {
        ...INITIAL_STATE.walletsStat,
        ripple: {
          ...INITIAL_STATE.walletsStat.ripple,
          transactions: [
            {
              id: 'asdasd1',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 9:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
            {
              id: 'asdasd2',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 10:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: true
            },
          ]
        }
      }
    }, ActionCreators.putTransactions('ripple', [
      {
        id: 'asdasd2',
        address: 'sadasdas',
        amount: 4,
        status: 'success',
        timestamp: moment('2018-08-25 10:00').valueOf(),
        confirmations: 10,
        block: 12345,
        isSeen: false
      },
      {
        id: 'asdasd3',
        address: 'sadasdas',
        amount: 4,
        status: 'success',
        timestamp: moment('2018-08-25 10:10').valueOf(),
        confirmations: 5,
        block: 12345,
        isSeen: false
      },
    ]))).toEqual({
      ...INITIAL_STATE,
      walletsStat: {
        ...INITIAL_STATE.walletsStat,
        ripple: {
          ...INITIAL_STATE.walletsStat.ripple,
          newTx: true,
          transactions: [
            {
              id: 'asdasd3',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 10:10').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
            {
              id: 'asdasd2',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 10:00').valueOf(),
              confirmations: 10, // confirmations from new
              block: 12345,
              isSeen: true // isSeen from old state
            },
            {
              id: 'asdasd1',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 9:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
          ]
        }
      }
    })
  });
  // -----------------------------------------------------------------------------------------------------------------------------------------------
  it('checks tx as seen', () => {
    expect(rootReducer({
      ...INITIAL_STATE,
      walletsStat: {
        ...INITIAL_STATE.walletsStat,
        bitcoin: {
          ...INITIAL_STATE.walletsStat.bitcoin,
          transactions: [
            {
              id: 'bit1',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 9:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
            {
              id: 'bit2',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 10:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
          ]
        },
        ethereum: {
          ...INITIAL_STATE.walletsStat.ethereum,
          transactions: [
            {
              id: 'eth1',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 9:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
            {
              id: 'eth2',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 10:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
          ]
        },
        ripple: {
          ...INITIAL_STATE.walletsStat.ripple,
          transactions: [
            {
              id: 'rip1',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 9:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
            {
              id: 'rip2',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 10:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
          ]
        }
      }
    }, ActionCreators.checkTxAsSeen('bit2'))).toEqual({
      ...INITIAL_STATE,
      walletsStat: {
        ...INITIAL_STATE.walletsStat,
        bitcoin: {
          ...INITIAL_STATE.walletsStat.bitcoin,
          transactions: [
            {
              id: 'bit1',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 9:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
            {
              id: 'bit2',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 10:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: true
            },
          ]
        },
        ethereum: {
          ...INITIAL_STATE.walletsStat.ethereum,
          transactions: [
            {
              id: 'eth1',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 9:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
            {
              id: 'eth2',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 10:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
          ]
        },
        ripple: {
          ...INITIAL_STATE.walletsStat.ripple,
          transactions: [
            {
              id: 'rip1',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 9:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
            {
              id: 'rip2',
              address: 'sadasdas',
              amount: 4,
              status: 'success',
              timestamp: moment('2018-08-25 10:00').valueOf(),
              confirmations: 5,
              block: 12345,
              isSeen: false
            },
          ]
        }
      }
    })
  });
  // -----------------------------------------------------------------------------------------------------------------------------------------------
});