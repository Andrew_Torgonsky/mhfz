// @flow

import rootReducer, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';

describe('session reducer', () => {
  it('changes one value on changeSession action', () => {
    expect(rootReducer(INITIAL_STATE, ActionCreators.changeSession('token', '123asd'))).toEqual({
      ...INITIAL_STATE,
      session: {
        ...INITIAL_STATE.session,
        token: '123asd'
      }
    });
  });
  it('changes multiple values on changeSessionMultiple action', () => {
    expect(rootReducer(INITIAL_STATE, ActionCreators.changeSessionMultiple({
      token: 'blablabla',
      name: 'John',
      phone_number: '123'
    }))).toEqual({
      ...INITIAL_STATE,
      session: {
        ...INITIAL_STATE.session,
        token: 'blablabla',
        name: 'John',
        phone_number: '123'
      }
    });
  });
});