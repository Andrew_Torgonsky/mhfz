// @flow

import rootReducer, { INITIAL_STATE } from '../../src/redux/reducers';
import { ActionCreators } from '../../src/redux/actions';

describe('Root reducer', () => {
  it('returns initial state when no state or action provided', () => {
    expect(rootReducer()).toEqual(INITIAL_STATE);
  });
  it('returns the same state if no action provided', () => {
    expect(rootReducer(INITIAL_STATE)).toEqual(INITIAL_STATE);
  });
  it('returns the same state if no corresponding action provided', () => {
    expect(rootReducer(INITIAL_STATE, { type: 'SOME_RANDOM_ACTION' })).toEqual(INITIAL_STATE);
  });
  it('resets everything except form and appMinimized on RESET action', () => {
    const state = {
      form: {
        ...INITIAL_STATE.form,
        name: 'Vasya'
      },
      appMinimized: true,
      alertState: 'some state which is not initial',
      pending: 'some state which is not initial',
      wallets: 'some state which is not initial',
      session: 'some state which is not initial',
      walletsStat: 'some state which is not initial',
      contacts: 'some state which is not initial',
      preventMinimize: 'some state which is not initial',
      loadPhoto: 'some state which is not initial',
      pushNotifications: 'some state which is not initial',
      balancePeriod: 'some state which is not initial',
    };
    expect(rootReducer(state, ActionCreators.reset())).toEqual({
      ...INITIAL_STATE,
      form: {
        ...INITIAL_STATE.form,
        name: 'Vasya'
      },
      appMinimized: true,
    })
  });
  it('resets form on RESET_FORM action', () => {
    const state = {
      form: 'some state which is not initial',
      appState: 'some state which is not initial',
      alertState: 'some state which is not initial',
      pending: 'some state which is not initial',
      wallets: 'some state which is not initial',
      session: 'some state which is not initial',
      walletsStat: 'some state which is not initial',
      contacts: 'some state which is not initial',
      loadPhoto: 'some state which is not initial',
    };
    expect(rootReducer(state, ActionCreators.resetForm())).toEqual({
      ...state,
      form: INITIAL_STATE.form
    });
  });
});