import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingHorizontal: scale(16),
    alignItems: 'center'
  },
  textContainer1: {
    marginTop: scaleByVertical(10)
  },
  textContainer2: {
    marginTop: scaleByVertical(54)
  },
  pinInputContainer: {
    marginTop: scaleByVertical(32),
  },

});
