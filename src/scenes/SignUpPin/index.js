import React from 'react';
import { View, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './styles';
import globalStyles from '../../global/styles';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import InputField from '../../components/InputField';
import CountryCodeInput from '../../components/CountryCodeInput';
import PasswordInput from '../../components/PasswordInput';
import { scale, scaleByVertical } from '../../utils/scale';
import translations from '../../translations';
import { ActionCreators } from '../../redux/actions';
import TextButton from '../../components/TextButton';
import FilledButton from '../../components/FilledButton';
import PinInput from '../../components/PinInput';
import { Scenes } from '../../navigation/shared';
import BackupCreatedModal from '../../components/BackupCreatedModal';

type Props = {
  navigation: Object,
  pinCode: string,
  changeForm(value: string, key: string): void,
  pinCodeError: boolean
}

class SignUpPin extends React.PureComponent<Props> {
  constructor(props) {
    super(props);
    const show = !props.navigation.getParam('noModal', false);
    this.state = {
      showModal: show
    };
  }
  hideModal = () => {
    this.setState({
      showModal: false
    });
  }
  render() {
    const {
      navigation, pinCode, changeForm, pinCodeError
    } = this.props;
    return (
      <BackgroundWrapper>
        <View style={styles.container}>
          <View style={styles.textContainer1}>
            <Text style={globalStyles.normalText}>{translations.t('SET_PIN_TO_SEC')}</Text>
          </View>
          <View style={styles.textContainer2}>
            <Text style={globalStyles.normalText}>{translations.t('ENTER_PIN')}</Text>
          </View>
          <View style={styles.pinInputContainer}>
            <PinInput
              value={pinCode}
              onChangeText={val => changeForm('pinCode', val)}
              onFinish={() => navigation.navigate(Scenes.SignUpPinConfirm)}
              error={pinCodeError}
            />
          </View>
        </View>
        {this.state.showModal ? <BackupCreatedModal onPress={this.hideModal} type={2} /> : null}
      </BackgroundWrapper>
    );
  }
}

SignUpPin.navigationOptions = () => ({
  title: translations.t('SET_PIN'),
  headerLeft: null
});

const mapStateToProps = ({
  form: {
    pinCode,
    pinCodeError
  }
}) => (
  {
    pinCode,
    pinCodeError
  }
);

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
}, dispatch);

export const SignUpPinConnected = connect(mapStateToProps, mapDispatchToProps)(SignUpPin);
