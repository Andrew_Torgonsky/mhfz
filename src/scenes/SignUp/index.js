import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { View, Text, Image } from 'react-native';
import translations from '../../translations';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import CountryCodeInput from '../../components/CountryCodeInput';
import InputField from '../../components/InputField';
import PasswordInput from '../../components/PasswordInput';
import EmailInput from '../../components/EmailInput';
import TextButton from '../../components/TextButton';
import FilledButton from '../../components/FilledButton';
import Checkbox from '../../components/Checkbox';
import styles from './styles';
import globalStyles from '../../global/styles';
import { ActionCreators } from '../../redux/actions';
import { scale, scaleByVertical } from '../../utils/scale';
import { Scenes } from '../../navigation/shared';
import PasswordRequirementsModal from '../../components/PasswordRequirementsModal';
import { isRTL } from '../../translations';

type Props = {
  navigation: Object,
  email: string,
  phonePrefix: string,
  countryIso: string,
  phoneNumber: string,
  password: string,
  agree: boolean,
  passwordVisibility: boolean,
  phoneError: boolean,
  passwordError: boolean,
  emailError: boolean,
  changeForm(key: string, value: string | boolean): void,
  submitSignUp(): void,
  changeAlert: (text: string, type: string) => void
}

export default class SignUp extends React.PureComponent<Props> {
  state = {
    isOpen: false
  };

  dismissErrors = () => {
    this.props.changeForm('emailError', false);
    this.props.changeForm('phoneError', false);
    this.props.changeForm('passwordError', false);
    this.props.changeAlert('', '');
  };

  toggleModal = () => this.setState({ isOpen: !this.state.isOpen });

  render() {
    return (
      <BackgroundWrapper>
        <View style={styles.container}>
          <View style={styles.titleContainer}>
            <Text style={globalStyles.title1}>
              {translations.t('SIGN_UP')}
            </Text>
          </View>

          <View style={styles.inputContainerEmail}>
            <EmailInput
              value={this.props.email}
              onChangeText={val => this.props.changeForm('email', val)}
              error={this.props.emailError}
              onFocus={this.dismissErrors}
            />
          </View>

          <View style={isRTL() ? styles.inputContainerArabic : styles.inputContainer}>
            <View style={isRTL() ? styles.inputSubContainer1Arabic : styles.inputSubContainer1}>
              <CountryCodeInput
                onPress={() => {
                  this.props.navigation.navigate(Scenes.CountrySelect);
                  this.dismissErrors();
                }}
                phonePrefix={this.props.phonePrefix}
                countryISO={this.props.countryIso}
                error={this.props.phoneError}
              />
            </View>
            <View style={styles.inputSubContainer2}>
              <InputField
                inputIndentaion={scale(16)}
                keyboardType="phone-pad"
                value={this.props.phoneNumber}
                onChangeText={val => this.props.changeForm('phoneNumber', val)}
                onFocus={this.dismissErrors}
                placeholder={translations.t('P_NUM')}
                error={this.props.phoneError}
              />
            </View>
          </View>

          <View style={[styles.inputContainer, styles.inputContainer2]}>
            <PasswordInput
              visible={this.props.passwordVisibility}
              value={this.props.password}
              onChangeText={val => this.props.changeForm('password', val)}
              onEyePress={() => this.props.changeForm('passwordVisibility', !this.props.passwordVisibility)}
              onFocus={this.dismissErrors}
              error={this.props.passwordError}
            />
          </View>

          <View style={styles.forgotContainer}>
            <TextButton
              title={translations.t('PWD_REQ')}
              onPress={this.toggleModal}
            />
          </View>

          <View style={styles.agreement}>
            <Checkbox
              checked={this.props.agree}
              onPress={() => this.props.changeForm('agree', !this.props.agree)}
            />
            <View style={styles.agreementTextContainer}>
              <Text style={styles.agreementText}>
                {translations.t('I_AGREE_WITH')}
              </Text>
              <TextButton
                style={styles.agreementTextButton}
                title={translations.t('T_A_C')}
                onPress={() => this.props.navigation.navigate(Scenes.TermsOfService)}
              />
              <Text style={styles.agreementText}>{translations.t('AND')}</Text>
              <TextButton
                style={styles.agreementTextButton}
                title={translations.t('P_A')}
                onPress={() => this.props.navigation.navigate(Scenes.PrivacyPolicy)}
              />
            </View>
          </View>

          <View style={styles.containerButton1}>
            <View style={styles.containerButton2}>
              <FilledButton
                title={translations.t('SIGN_UP')}
                onPress={() => this.props.submitSignUp()}
              />
            </View>
          </View>

          <View style={isRTL() ? styles.signInContainerArabic : styles.signInContainer}>
            <Text style={[
              globalStyles.greyText,
              styles.signInContainerText
            ]}>
              {translations.t('HAVE_ACC')}
            </Text>
            <TextButton
              title={translations.t('SIGN_IN')}
              onPress={() => this.props.navigation.navigate(Scenes.SignIn)}
            />
          </View>
        </View>
        {
          this.state.isOpen ? <PasswordRequirementsModal onPress={this.toggleModal} /> : null
        }
      </BackgroundWrapper>
    );
  }
}

SignUp.navigationOptions = {
  headerStyle: globalStyles.transparentHeader
};


const mapStateToProps = ({
  form: {
    phonePrefix, countryIso, phoneNumber, password, agree, passwordVisibility, email, phoneError, passwordError, emailError
  }
}) => (
  {
    email,
    phonePrefix,
    countryIso,
    phoneNumber,
    password,
    agree,
    passwordVisibility,
    phoneError,
    passwordError,
    emailError
  }
);

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  changeAlert: ActionCreators.changeAlert,
  submitSignUp: ActionCreators.submitSignUp
}, dispatch);

export const SignUpConnected = connect(mapStateToProps, mapDispatchToProps)(SignUp);
