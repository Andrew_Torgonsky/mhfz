import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingHorizontal: scale(16)
  },
  titleContainer: {
    marginTop: scaleByVertical(30),
    alignItems: 'center',
  },
  inputContainer: {
    marginTop: scaleByVertical(24),
    flexDirection: 'row',
    height: scaleByVertical(44)
  },
  inputContainerArabic: {
    marginTop: scaleByVertical(24),
    flexDirection: 'row-reverse',
    height: scaleByVertical(44)
  },
  inputContainer2: {
    marginTop: scaleByVertical(24),
  },
  inputContainerEmail: {
    marginTop: scaleByVertical(40),
    height: scaleByVertical(44)
  },
  inputSubContainer1: {
    flex: 1,
    paddingRight: scale(4)
  },
  inputSubContainer1Arabic: {
    flex: 1,
    paddingLeft: scale(4)
  },
  inputSubContainer2: {
    flex: 1
  },
  forgotContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: scaleByVertical(8)
  },
  agreement: {
    flexDirection: 'row',
    marginTop: scaleByVertical(24)
  },
  agreementTextContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginLeft: scale(9),
  },
  agreementText: {
    fontSize: scaleByVertical(14),
    lineHeight: scaleByVertical(20),
    color: 'white',
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  agreementTextButton: {
    transform: [
      { translateY: scaleByVertical(1) }
    ],
  },
  containerButton1: {
    marginTop: scaleByVertical(53),
    alignItems: 'center'
  },
  containerButton2: {
    width: scale(180),
    height: scaleByVertical(40)
  },
  signInContainer: {
    alignItems: 'center',
    marginTop: scaleByVertical(40)
  },
  signInContainerArabic: {
    marginTop: scaleByVertical(40),
    flexDirection: 'row-reverse',
    justifyContent: 'center'
  },
  signInContainerText: {
    marginLeft: scale(10)
  }
});
