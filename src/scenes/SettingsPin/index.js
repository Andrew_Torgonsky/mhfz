import React from 'react';
import { View, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import PinInput from '../../components/PinInput';
import translations from '../../translations';
import globalStyles from '../../global/styles';
import { ActionCreators } from '../../redux/actions';
import { Scenes } from '../../navigation/shared';
import styles from './styles';

type Props = {
  navigation: Object,
  pinCode: string,
  pinCodeConfirm: string,
  changeForm(value: string, key: string): void,
  checkPinBeforeChange(): void,
  setNewPinCode(): void,
  pinCodeError: boolean,
  enter: boolean,
  confirm: boolean,
}

export default function SettingsPin(props: Props) {
  const {
    navigation,
    pinCode,
    pinCodeConfirm,
    changeForm,
    pinCodeError,
    checkPinBeforeChange,
    setNewPinCode,
    enter,
    confirm
  } = props;

  function getTopText() {
    if (!enter && !confirm) return translations.t('SET_PIN_TO_SEC');
  }

  function getText() {
    if (enter) return translations.t('ENTER_NEW_PIN');
    else if (confirm) return translations.t('REPEAT_NEW_PIN');
    return translations.t('ENTER_CUR_PIN');
  }

  function getFinish() {
    if (enter) return () => navigation.navigate(Scenes.SettingsPinConfirm);
    else if (confirm) return setNewPinCode;
    return checkPinBeforeChange;
  }

  function getValue() {
    if (confirm) return pinCodeConfirm;
    return pinCode;
  }

  function getChangeText() {
    if (confirm) return val => changeForm('pinCodeConfirm', val);
    return val => changeForm('pinCode', val);
  }

  return (
    <BackgroundWrapper>
      <View style={styles.container}>
        <View style={styles.textContainer1}>
          <Text style={globalStyles.normalText}>{getTopText()}</Text>
        </View>
        <View style={styles.textContainer2}>
          <Text style={globalStyles.normalText}>{getText()}</Text>
        </View>
        <View style={styles.pinInputContainer}>
          <PinInput
            value={getValue()}
            onChangeText={getChangeText()}
            error={pinCodeError}
            onFinish={getFinish()}
          />
        </View>
      </View>
    </BackgroundWrapper>
  );
}

SettingsPin.navigationOptions = () => ({
  title: translations.t('SET_PIN')
});

const mapStateToProps = ({
  form: {
    pinCode,
    pinCodeConfirm,
    pinCodeError
  }
}) => (
  {
    pinCode,
    pinCodeConfirm,
    pinCodeError
  }
);

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  checkPinBeforeChange: ActionCreators.checkPinBeforeChange,
  setNewPinCode: ActionCreators.setNewPinCode
}, dispatch);

export const SettingsPinConnected = connect(mapStateToProps, mapDispatchToProps)(SettingsPin);
