// @flow

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { View, Text } from 'react-native';
import translations, { isRTL } from '../../translations';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import Dropdown from '../../components/Dropdown';
import styles from './styles';
import globalStyles from '../../global/styles';
import images from '../../global/images';
import { ActionCreators } from '../../redux/actions';
import { scale, width } from '../../utils/scale';
import { Scenes } from '../../navigation/shared';
import Chart from '../../components/Chart';
import CurrencyBadge from '../../components/CurrencyBadge';
import { combineBalancesToBtc, combineBalancesToFiat, getCombinedDailyChange } from '../../utils/currencies';
import { getCombinedHistoryForChart } from '../../utils/chart';
import type { ReduxStateType, FiatCurrencyType, WalletsStatType } from '../../types';

type Props = {
  navigation: Object,
  balancePeriod: 0 | 1 | 2,
  walletsStat: WalletsStatType,
  changeAppState: (key: string, value: string) => void,
  default_currency: FiatCurrencyType,
}


const MainWallet = (props: Props) => {
  const {
    changeAppState, balancePeriod, default_currency, walletsStat, navigation
  } = props;
  const combinedHistory = getCombinedHistoryForChart(walletsStat, balancePeriod);
  const dailyChange = getCombinedDailyChange(walletsStat);
  const options = [translations.t('WEEK'), translations.t('MONTH'), translations.t('YEAR')];
  return (
    <BackgroundWrapper>
      <View style={styles.container1}>
        <Text style={globalStyles.walletTitle1}>{translations.t('TOTAL_BALANCE')}</Text>
        <Dropdown
          options={options}
          value={balancePeriod}
          onChange={val => changeAppState('balancePeriod', val)}
          dropdownShift={isRTL() && -width + scale(125)}
        />
      </View>
      <View style={styles.horizontalContainers}>
        <View style={styles.horizontalContainer1}>
          <View style={styles.container2}>
            <Text style={globalStyles.walletTitle2}>{combineBalancesToBtc(walletsStat)}</Text>
            <Text style={[globalStyles.walletTitle4, styles.currencyLabel]}>BTC</Text>
          </View>
          <View style={styles.container3}>
            { !walletsStat.bitcoin.rate || !walletsStat.ethereum.rate || !walletsStat.ripple.rate ?
              <Text style={styles.noInfo}>{translations.t('NO_RATE')}</Text> :
              <React.Fragment>
                <Text style={globalStyles.walletTitle3}>{combineBalancesToFiat(walletsStat)}</Text>
                <Text style={[globalStyles.walletTitle5, styles.currencyLabel]}>{default_currency.toUpperCase()}</Text>
              </React.Fragment>
            }
            
          </View>
        </View>
        <View style={styles.horizontalContainer2}>
          <Text style={styles.changeLabel}>{translations.t('24_CHANGE')}</Text>
          {
            dailyChange === Infinity ?
              <Text style={styles.noInfo}>{translations.t('NO_INFO')}</Text> :
              <Text style={[styles.change, dailyChange < 0 && styles.changeNegative]}>{dailyChange}%</Text>
          }
        </View>
      </View>
      <View style={styles.container4}>
        <Chart data={combinedHistory} />
      </View>
      <View style={styles.container5}>
        <Text style={globalStyles.walletTitle1}>{translations.t('WALLETS')}</Text>
      </View>
      <View style={styles.currencyContainer}>
        <CurrencyBadge
          icon={images.bitcoin}
          title={`${translations.t('BITCOIN')} - BTC`}
          defaultCurrency={default_currency}
          walletStat={walletsStat.bitcoin}
          onPress={() => navigation.navigate(Scenes.BtcWallet)}
        />
      </View>
      <View style={styles.currencyContainer}>
        <CurrencyBadge
          icon={images.ethereum}
          title={`${translations.t('ETHEREUM')} - ETH`}
          defaultCurrency={default_currency}
          walletStat={walletsStat.ethereum}
          onPress={() => navigation.navigate(Scenes.EthWallet)}
        />
      </View>
      <View style={styles.currencyContainer}>
        <CurrencyBadge
          icon={images.ripple}
          title={`${translations.t('RIPPLE')} - XRP`}
          defaultCurrency={default_currency}
          walletStat={walletsStat.ripple}
          onPress={() => navigation.navigate(Scenes.XrpWallet)}
        />
      </View>
    </BackgroundWrapper>
  );
};

MainWallet.navigationOptions = () => ({
  title: translations.t('PORTFOLIO'),
});

const mapStateToProps = ({
  session:
  { default_currency },
  balancePeriod,
  walletsStat
}: ReduxStateType) => ({
  balancePeriod, default_currency, walletsStat
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changeAppState: ActionCreators.changeAppState
}, dispatch);

export const MainWalletConnected = connect(mapStateToProps, mapDispatchToProps)(MainWallet);
