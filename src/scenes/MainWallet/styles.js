import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  container1: {
    height: scaleByVertical(44),
    flexDirection: 'row',
    paddingHorizontal: scale(16),
    justifyContent: 'space-between',
    alignItems: 'center',
    zIndex: 100,
    overflow: 'visible'
  },
  container2: {
    height: scaleByVertical(29),
    flexDirection: 'row'
  },
  container3: {
    marginTop: scaleByVertical(9),
    height: scaleByVertical(16),
    flexDirection: 'row'
  },
  container5: {
    paddingHorizontal: scale(16),
    marginVertical: scaleByVertical(8),
    flexDirection: 'row'
  },
  currencyContainer: {
    paddingHorizontal: scale(8),
    marginBottom: scaleByVertical(8)
  },
  currencyLabel: {
    marginLeft: scale(3)
  },
  horizontalContainers: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: scale(16),
  },
  horizontalContainer2: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  changeLabel: {
    fontSize: scaleByVertical(14),
    color: colors.white38,
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  change: {
    fontSize: scaleByVertical(14),
    color: colors.aquamarine,
    ...platformSelect({
      fontFamily: fonts.sfProDisplayBold
    }, {
      fontFamily: fonts.robotoBold
    })
  },
  changeNegative: {
    color: colors.reddishPink
  },
  noInfo: {
    fontSize: scaleByVertical(14),
    color: colors.white38,
    ...platformSelect({
      fontFamily: fonts.sfProTextBold
    }, {
      fontFamily: fonts.robotoBold
    })
  }
});
