import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  text: {
    textAlign: 'center'
  },
  textContainer: {
    height: scaleByVertical(80),
    justifyContent: 'center'
  },
  bottomText: {
    marginTop: scaleByVertical(10),
  }
});
