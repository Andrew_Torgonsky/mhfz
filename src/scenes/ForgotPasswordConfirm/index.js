// @flow

import React from 'react';
import { View, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import SmsCode from '../../components/SmsCode';
import RetrySendButton from '../../components/RetrySendButton';
import styles from './styles';
import translations from '../../translations';
import globalStyles from '../../global/styles';
import { ActionCreators } from '../../redux/actions';
import type { AlertType, ReduxStateType } from '../../types';

type Props = {
  smsCode: string,
  smsCodeError: boolean,
  smsCodeTimestamp: number,
  // navigation: Object,
  changeForm: (key: string, value: any) => void,
  checkRecoverySecret: () => void,
  changeAlert: (text: string, type: AlertType) => void,
  requestPasswordRecovery: () => void,
}

class ForgotPasswordConfirm extends React.PureComponent<Props> {
  static navigationOptions: Object;
  render() {
    const {
      changeForm, changeAlert, smsCode, smsCodeError, checkRecoverySecret, requestPasswordRecovery, smsCodeTimestamp
    } = this.props;
    return (
      <BackgroundWrapper>
        <View style={styles.textContainer}>
          <Text style={[globalStyles.normalText, styles.text]}>
            {translations.t('FORGOT_SMS_CODE')}
          </Text>
        </View>
        <View style={styles.code}>
          <SmsCode
            onChangeText={(val) => {
              changeForm('smsCode', val);
              changeForm('smsCodeError', false);
              changeAlert('', '');
            }}
            onFinish={checkRecoverySecret}
            value={smsCode}
            error={smsCodeError}
          />
        </View>
        <View style={styles.bottomText}>
          <RetrySendButton onPress={requestPasswordRecovery} timestamp={smsCodeTimestamp}/>
        </View>
      </BackgroundWrapper>
    );
  }
}


ForgotPasswordConfirm.navigationOptions = () => ({
  title: translations.t('P_CONF')
});

const mapStateToProps = ({
  form: {
    smsCode, smsCodeError, smsCodeTimestamp
  }
}: ReduxStateType) => ({
  smsCode,
  smsCodeError,
  smsCodeTimestamp
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  changeAlert: ActionCreators.changeAlert,
  requestPasswordRecovery: ActionCreators.requestPasswordRecovery,
  checkRecoverySecret: ActionCreators.checkRecoverySecret,
}, dispatch);

export const ForgotPasswordConfirmConnected = connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordConfirm);
