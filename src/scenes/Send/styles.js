import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect, height } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    flex: 1
  },
  recipientInputContainer: {
    marginHorizontal: scale(16),
    marginTop: scaleByVertical(24),
    height: scaleByVertical(44),
  },
  buttonContainer: {
    flex: 1,
    height: scaleByVertical(32),
  },
  buttonsContainer: {
    marginHorizontal: scale(16),
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: scaleByVertical(16)
  },
  amountInputContainer: {
    marginHorizontal: scale(16),
    marginTop: scaleByVertical(24),
    height: scaleByVertical(44),
  },
  feeContainer: {
    marginTop: scaleByVertical(24),
    marginHorizontal: scale(16),
  },
  feeLabel: {
    marginLeft: scale(8),
    fontSize: scaleByVertical(16),
    color: colors.white87,
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  feeLabel2: {
    marginLeft: scale(16),
    opacity: 0.5
  },
  feeOptionContainer: {
    height: scaleByVertical(40)
  },
  sendButtonContainer: {
    marginHorizontal: scale(16),
    marginTop: scaleByVertical(24),
    height: scaleByVertical(44),
  },
  chooseWalletText: {
    color: colors.white60,
    fontSize: scaleByVertical(14),
    marginLeft: scale(16),
    marginRight: scale(16),
    marginVertical: scaleByVertical(8),
    ...platformSelect({
      fontFamily: fonts.sfProTextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  inFiat: {
    textAlign: 'center',
    fontSize: scaleByVertical(14),
    marginTop: scaleByVertical(16),
    color: colors.white60,
    ...platformSelect({
      fontFamily: fonts.sfProTextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  }
});
