// @flow

import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import ButtonWithBorder from '../../components/ButtonWithBorder';
import InputField from '../../components/InputField';
import translations from '../../translations';
import styles from './styles';
import { scale } from '../../utils/scale';
import images from '../../global/images';
import AmountInput from '../../components/AmountInput';
import FilledButton from '../../components/FilledButton';
import ChooseWalletItem from '../../components/ChooseWalletItem';
import type { ReduxStateType, CryptoCurrencyType, WalletsStatType, AlertType, ContactType, FiatCurrencyType } from '../../types';
import { ActionCreators } from '../../redux/actions';
import { Scenes } from '../../navigation';
import { switchAbbrAndName, amountAndFee, inFiat, } from '../../utils/currencies';
import RadioButton from '../../components/RadioButton';
import ContactInput from '../../components/ContactInput';

type Props = {
  type: 'fromContact' | 'toContact' | 'walletAddress',
  navigation: Object,
  addressToSend: string,
  amountToSend: number,
  feeOptions: Object[],
  chosenFeeOption: number,
  currencyToSend: CryptoCurrencyType,
  walletsStat: WalletsStatType,
  sendPreparing: boolean,
  contactToSend: ContactType,
  default_currency: FiatCurrencyType,
  changeForm: (key: string, value: any) => void,
  changeFormMultiple: (filelds: Object) => void,
  changeAlert: (text: string, type: AlertType) => void,
  pasteAddressFromClipboard: () => void,
  prepareToSend: () => void,
  send: (toContact: boolean) => void
}

export class Send extends React.PureComponent<Props> {
  onAddressChange = (val: string) => {
    this.props.changeForm('addressToSend', val);
    this.props.prepareToSend();
  };
  onAmountChange = (val: string) => {
    this.props.changeForm('amountToSend', val);
    this.props.prepareToSend();
  };
  componentWillUnmount() {
    this.props.changeFormMultiple({
      amountToSend: '',
      currencyToSend: 'bitcoin',
      addressToSend: '',
      contactToSend: undefined,
      feeOptions: [],
      chosenFeeOption: 0
    });
  }
  render() {
    const withFee = amountAndFee(this.props.amountToSend, this.props.feeOptions, this.props.chosenFeeOption);
    return (
      <BackgroundWrapper noScroll noGradient={this.props.type !== 'fromContact'}>
        <View style={styles.container}>
          <View style={styles.upperContainer}>
            <View style={styles.recipientInputContainer}>
              {this.props.type === 'walletAddress' ?
                <InputField
                  placeholder={translations.t('REC_WAL_ADDR')}
                  inputIndentaion={scale(16)}
                  value={this.props.addressToSend}
                  onChangeText={this.onAddressChange}
                /> :
                <ContactInput
                  onPress={() => {
                    if (this.props.type === 'fromContact') return;
                    this.props.navigation.navigate(Scenes.Recipients);
                  }}
                  value={this.props.contactToSend}
                  rightArrow={this.props.type === 'toContact'}
                />
              }
            </View>
            {this.props.type === 'walletAddress' ?
              <View style={styles.buttonsContainer}>
                <View style={styles.buttonContainer}>
                  <ButtonWithBorder
                    title={translations.t('SCAN_QR')}
                    icon={images.qrCode}
                    onPress={() => this.props.navigation.navigate(Scenes.ScanCode)}
                  />
                </View>
                <View style={[styles.buttonContainer, { marginLeft: scale(16) }]}>
                  <ButtonWithBorder
                    title={translations.t('PASTE')}
                    icon={images.copy}
                    onPress={this.props.pasteAddressFromClipboard}
                  />
                </View>
              </View> :
              null
            }
            <View style={styles.amountInputContainer}>
              <AmountInput
                currency={this.props.currencyToSend}
                value={this.props.amountToSend}
                onChangeText={this.onAmountChange}
              />
            </View>
            <View style={styles.feeContainer}>
              {this.props.feeOptions.map((item, index) => (
                <View key={index} style={styles.feeOptionContainer}>
                  <RadioButton active={index === this.props.chosenFeeOption} onPress={() => this.props.changeForm('chosenFeeOption', index)}>
                    <Text style={styles.feeLabel}>{translations.t(item.name.toUpperCase())}</Text>
                    <Text style={[styles.feeLabel, styles.feeLabel2]}>{translations.t('FEE')} = {item.fee} {switchAbbrAndName(this.props.currencyToSend)}</Text>
                  </RadioButton>
                </View>
              ))}
            </View>
            <View style={styles.sendButtonContainer}>
              <FilledButton
                red
                title={`${translations.t('SEND')}${withFee > 0 ? ` ${withFee} ${switchAbbrAndName(this.props.currencyToSend)}` : ''}`}
                onPress={() => this.props.send(this.props.type !== 'walletAddress')}
                pending={this.props.sendPreparing}
              />
            </View>
            {withFee > 0 ? <Text style={styles.inFiat}>{translations.t('EQUALS')} {inFiat(withFee, this.props.walletsStat[this.props.currencyToSend].rate, this.props.default_currency)}</Text> : null}
          </View>
          <View style={styles.lowerContainer}>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.chooseWalletText}>{translations.t('CHOOSE_WALLET')}</Text>
            </View>
            
            <ChooseWalletItem
              chosen
              currency={this.props.currencyToSend}
              rightArrow
              walletsStat={this.props.walletsStat}
              onPress={() => this.props.navigation.navigate(Scenes.SelectWallet)}
            />
          </View>
        </View>
      </BackgroundWrapper >
    );
  }
};

const mapStateToProps = ({
  form: {
    addressToSend, contactToSend, amountToSend, feeOptions, chosenFeeOption, currencyToSend, sendPreparing
  },
  session: {
    default_currency
  },
  walletsStat
}: ReduxStateType) => ({
  addressToSend,
  amountToSend,
  feeOptions,
  chosenFeeOption,
  currencyToSend,
  walletsStat,
  sendPreparing,
  contactToSend,
  default_currency
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  changeFormMultiple: ActionCreators.changeFormMultiple,
  changeAlert: ActionCreators.changeAlert,
  pasteAddressFromClipboard: ActionCreators.pasteAddressFromClipBoard,
  prepareToSend: ActionCreators.prepareToSend,
  send: ActionCreators.send
}, dispatch);

export const SendConnected = connect(mapStateToProps, mapDispatchToProps)(Send);
