import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingHorizontal: scale(16)
  },
  searchInputContainer: {
    marginVertical: scaleByVertical(16),
    height: scaleByVertical(44)
  },
  countryListContainer: {
    flex: 1,
  },
  listItem: {
    borderStyle: 'solid',
    borderBottomWidth: scaleByVertical(0.5),
    borderBottomColor: colors.white20,
    height: scaleByVertical(44),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  listItemText: {
    color: 'white',
    fontSize: scaleByVertical(16),
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  listItemCheck: {
    width: scale(24),
    height: scale(24),
    tintColor: colors.brightTeal
  },
});
