// @flow

import React from 'react';
import { View, Text, FlatList, TouchableOpacity, Image, Alert } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './styles';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import translations from '../../translations';
import { ActionCreators } from '../../redux/actions';
import SearchInput from '../../components/SearchInput';
import countries from '../../global/countries';
import images from '../../global/images';
import type { ReduxStateType, CountryType } from '../../types';

type Props = {
  navigation: Object,
  phonePrefix: string,
  countryIso: string,
  changeForm: (key: string, value: any) => void,
}

type State = {
  search: string
}

export class CountrySelect extends React.PureComponent<Props, State> {
  static navigationOptions: Object;
  state = {
    search: ''
  };
  onSearchChange = (text: string) => {
    this.setState({
      search: text
    });
  }
  filterCountries = (countryList: CountryType[]): CountryType[] => {
    const textToMatch = this.state.search.startsWith('+') ? this.state.search.slice(1).toLowerCase() : this.state.search.toLowerCase();
    return countryList.filter(item => item.search.reduce((acc, current) => current.startsWith(encodeURI(textToMatch)) || acc, false) || item.dialCode.startsWith(textToMatch));
  }

  renderListItem = ({ item }: any) => (
    <TouchableOpacity
      onPress={() => {
        this.props.changeForm('countryIso', item.iso2);
        this.props.changeForm('phonePrefix', item.dialCode);
        this.props.navigation.goBack();
      }}
    >
      <View style={styles.listItem}>
        <Text style={styles.listItemText}>{item.name} (+{item.dialCode})</Text>
        {
          this.props.countryIso === item.iso2 && this.props.phonePrefix === item.dialCode ?
            <Image source={images.check} style={styles.listItemCheck} /> :
            null
        }
      </View>
    </TouchableOpacity>
  )
  render() {
    return (
      <BackgroundWrapper noScrollWithKeyboard>
        <View style={styles.container}>
          <View style={styles.searchInputContainer}>
            <SearchInput
              value={this.state.search}
              onChangeText={this.onSearchChange}
            />
          </View>
          <View style={styles.countryListContainer}>
            <FlatList
              data={this.filterCountries(countries)}
              renderItem={this.renderListItem}
              keyboardShouldPersistTaps="handled"
            />
          </View>
        </View>
      </BackgroundWrapper>
    );
  }
}

CountrySelect.navigationOptions = () => ({
  title: translations.t('Y_COUNTRY')
});

const mapStateToProps = ({ form: { phonePrefix, countryIso } }: ReduxStateType) => (
  {
    phonePrefix,
    countryIso
  }
);

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
}, dispatch);

export const CountrySelectConnected = connect(mapStateToProps, mapDispatchToProps)(CountrySelect);
