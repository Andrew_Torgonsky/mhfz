import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    paddingHorizontal: scale(16)
  },
  fieldContainer1: {
    marginTop: scaleByVertical(88),
    height: scaleByVertical(44)
  },
  fieldContainer2: {
    marginTop: scaleByVertical(24),
    height: scaleByVertical(44)
  },
  buttonContainer: {
    alignSelf: 'center',
    marginTop: scaleByVertical(40),
    height: scaleByVertical(40),
    width: scale(200)
  },
});
