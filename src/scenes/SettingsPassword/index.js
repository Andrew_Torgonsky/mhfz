import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Text } from 'react-native';
import translations from '../../translations';
import { ActionCreators } from '../../redux/actions';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import PasswordInput from '../../components/PasswordInput';
import FilledButton from '../../components/FilledButton';
import styles from './styles';

type Props = {
  password: string,
  password2nd: string,
  passwordVisibility: boolean,
  passwordError: boolean,
  passwordError2nd: boolean,
  changeForm(key: string, value: string | boolean): void,
  changePassword(): void,
  changeAlert(key: string, value: string): void
};

export default function SettingPassword(props: Props) {
  const dismissErrors = () => {
    props.changeForm('passwordError', false);
    props.changeForm('passwordError2nd', false);
    props.changeAlert('', '');
  };
  return (
    <BackgroundWrapper>
      <View style={styles.container}>
        <View style={styles.fieldContainer1}>
          <PasswordInput
            onPress={dismissErrors}
            onFocus={dismissErrors}
            value={props.password}
            placeholder={translations.t('CURRENT_PWD')}
            visible={props.passwordVisibility}
            onEyePress={() => props.changeForm('passwordVisibility', !props.passwordVisibility)}
            onChangeText={val => {
              dismissErrors();
              props.changeForm('password', val);
            }}
            error={props.passwordError}
          />
        </View>
        <View style={styles.fieldContainer2}>
          <PasswordInput
            onPress={dismissErrors}
            onFocus={dismissErrors}
            value={props.password2nd}
            placeholder={translations.t('NEW_PWD')}
            visible={props.passwordVisibility}
            onEyePress={() => props.changeForm('passwordVisibility', !props.passwordVisibility)}
            onChangeText={val => {
              props.changeForm('password2nd', val);
              dismissErrors();
            }}
            error={props.passwordError2nd}
          />
        </View>
        <View style={styles.buttonContainer}>
          <FilledButton
            title={translations.t('CHANGE_PWD')}
            onPress={props.changePassword}
          />
        </View>
      </View>
    </BackgroundWrapper>
  );
}

SettingPassword.navigationOptions = () => ({
  title: translations.t('CHANGE_PWD')
});

const mapStateToProps = ({
  form: {
    password,
    password2nd,
    passwordVisibility,
    passwordError,
    passwordError2nd
  }
}) => ({
  password,
  password2nd,
  passwordVisibility,
  passwordError,
  passwordError2nd
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  changePassword: ActionCreators.changePassword,
  changeAlert: ActionCreators.changeAlert
}, dispatch);

export const SettingsPasswordConntected = connect(mapStateToProps, mapDispatchToProps)(SettingPassword);
