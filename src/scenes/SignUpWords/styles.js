import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, isAndroid } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    paddingHorizontal: scale(16),
    flex: 1,
    justifyContent: 'space-between'
  },
  textContainer1: {
    marginTop: scaleByVertical(16),
    alignItems: 'center'
  },
  inputContainer: {
    marginTop: scaleByVertical(24)
  },
  textContainer2: {
    alignItems: 'center'
  },
  text: {
    textAlign: 'center'
  },
  confirmContainer: {
    marginTop: scaleByVertical(24),
    marginBottom: scaleByVertical(16),
    height: scaleByVertical(40)
  }
});
