import React from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import styles from './styles';
import globalStyles from '../../global/styles';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import translations from '../../translations';
import FilledButton from '../../components/FilledButton';
import TwelveWordsInput from '../../components/TwelveWordsInput';
import { Scenes } from '../../navigation/shared';
import BackupCreatedModal from '../../components/BackupCreatedModal';

type Props = {
  navigation: Object;
  twelveWordsGenerated: string[];
}

export default class SignUpWords extends React.PureComponent<Props> {
  state = {
    showModal: true
  }
  toggleModal = () => {
    this.setState({
      showModal: !this.state.showModal
    });
  }
  render() {
    const {
      navigation, twelveWordsGenerated
    } = this.props;
    return (
      <BackgroundWrapper noScroll>
        <View style={styles.container}>
          <View>
            <View style={styles.textContainer1}>
              <Text
                style={[globalStyles.normalText, styles.text]}
              >
                {translations.t('WRITE_DOWN')}
              </Text>
            </View>
            <View style={styles.inputContainer}>
              <TwelveWordsInput
                editable={false}
                clearButton={false}
                value={twelveWordsGenerated}
              />
            </View>
          </View>
          <View>
            <View style={styles.textContainer2}>
              <Text
                style={[globalStyles.normalText, styles.text]}
              >
                {translations.t('WE_WILL_CONFIRM')}
              </Text>
            </View>
            <View style={styles.confirmContainer}>
              <FilledButton
                title={translations.t('HAVE_WRITTEN')}
                onPress={() => navigation.navigate(Scenes.SignUpWordsConfirm)}
              />
            </View>
          </View>
        </View>
        {this.state.showModal ? <BackupCreatedModal onPress={this.toggleModal} type={1} /> : null}
      </BackgroundWrapper>
    );
  }
}

SignUpWords.navigationOptions = () => ({
  title: translations.t('TW_WORDS_SEC'),
});


const mapStateToProps = ({
  form: {
    twelveWordsGenerated
  }
}) => (
    {
      twelveWordsGenerated,
    }
  );

export const SignUpWordsConnected = connect(mapStateToProps)(SignUpWords);
