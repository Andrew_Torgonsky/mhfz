// @flow

import React from 'react';
import Moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Text, TouchableOpacity, Clipboard } from 'react-native';
import { ActionCreators } from '../../redux/actions';
import translations from '../../translations';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import styles from './styles';
import { scale } from '../../utils/scale';
import { switchAbbrAndName } from '../../utils/currencies';
import type { ReduxStateType, CryptoCurrencyType, WalletsStatType, TransactionHistoryEntryType, ContactType, AlertType } from '../../types';
import { truncateString } from '../../utils/other';

type Props = {
  navigation: Object,
  walletsStat: WalletsStatType,
  contacts: ContactType[],
  changeAlert: (text: string, type: AlertType) => void,
  checkTxAsSeen: (txid: string) => void
}

class Transaction extends React.PureComponent<Props> {
  static navigationOptions: Object;
  componentWillMount() {
    const txid = this.props.navigation.getParam('txid');
    this.props.checkTxAsSeen(txid);
  }
  render() {
    const { navigation, walletsStat, contacts, changeAlert } = this.props;
    const txid = navigation.getParam('txid');
    const currency: CryptoCurrencyType = navigation.getParam('currency');
    const transaction: TransactionHistoryEntryType | void = walletsStat[currency].transactions.find(item => item.id === txid);
    if (!transaction) return null;
    const contactToShow: ContactType | void = contacts.find(contact => contact.wallets.reduce((acc, cur) => cur.walletAddress === transaction.address || acc, false));
    const details = [
      { key: transaction.amount > 0 ? translations.t('SENDER') : translations.t('RECIPIENT'), value: truncateString(contactToShow ? `${contactToShow.firstName} ${contactToShow.lastName}` : transaction.address, scale(20)) },
      { key: translations.t('AMOUNT'), value: `${transaction.amount} ${switchAbbrAndName(currency)}` },
      { key: translations.t('DATE'), value: Moment(transaction.timestamp).format('DD.MM.YYYY') },
      { key: translations.t('CONFIRMATIONS'), value: transaction.confirmations },
      { key: translations.t('CONF_IN_BLOCK'), value: transaction.block },
    ];
    const onTxIdCopy = () => {
      Clipboard.setString(txid);
      changeAlert(translations.t('COPIED'), 'green');
    }
    const onAddressCopy = () => {
      Clipboard.setString(transaction.address);
      changeAlert(translations.t('COPIED'), 'green');
    }
    return (
      <BackgroundWrapper>
        <View style={styles.txidContainer}>
          <Text style={styles.txAddrLabel}>{translations.t('TX_ADDRESS')}</Text>
          <TouchableOpacity onPress={onTxIdCopy}>
            <Text style={styles.txid}>{txid}</Text>
          </TouchableOpacity>
        </View>
        {details.map((item, index) => (
          <TouchableOpacity key={item.key} onPress={index === 0 ? onAddressCopy : undefined}>
            <View style={styles.detailItem}>
              <Text style={styles.detailKey}>
                {item.key}
              </Text>
              <Text style={styles.detailValue}>
                {item.value}
              </Text>
            </View>
          </TouchableOpacity>
        ))}
      </BackgroundWrapper>
    );
  }
}

Transaction.navigationOptions = () => ({
  title: translations.t('TRANSACTION'),
});

const mapStateToProps = ({
  walletsStat,
  contacts
}: ReduxStateType) => ({
  walletsStat,
  contacts
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changeAlert: ActionCreators.changeAlert,
  checkTxAsSeen: ActionCreators.checkTxAsSeen
}, dispatch);

export const TransactionConnected = connect(mapStateToProps, mapDispatchToProps)(Transaction);
