import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  txidContainer: {
    marginVertical: scaleByVertical(24),
    marginHorizontal: scale(16),
  },
  txAddrLabel: {
    fontSize: scaleByVertical(14),
    color: colors.white60,
    ...platformSelect({
      fontFamily: fonts.sfProTextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  txid: {
    marginTop: scaleByVertical(8),
    fontSize: scaleByVertical(16),
    color: colors.white87,
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  detailItem: {
    height: scaleByVertical(44),
    borderBottomColor: colors.white20,
    borderBottomWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: scale(16)
  },
  detailKey: {
    fontSize: scaleByVertical(16),
    color: colors.white50,
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  detailValue: {
    fontSize: scaleByVertical(16),
    color: colors.white87,
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  }
});
