import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, isAndroid } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    paddingHorizontal: scale(16),
    flex: 1,
    justifyContent: 'space-between'
  },
  textContainer: {
    marginVertical: scaleByVertical(24),
    alignItems: 'center'
  },
  confirmContainer: {
    // marginTop: isAndroid ? scaleByVertical(125) : scaleByVertical(155),
    height: scaleByVertical(40),
    marginVertical: scaleByVertical(16)
  }
});
