import React from 'react';
import { View, Text, SafeAreaView } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './styles';
import globalStyles from '../../global/styles';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import InputField from '../../components/InputField';
import CountryCodeInput from '../../components/CountryCodeInput';
import PasswordInput from '../../components/PasswordInput';
import { scale, scaleByVertical } from '../../utils/scale';
import translations from '../../translations';
import { ActionCreators } from '../../redux/actions';
import TextButton from '../../components/TextButton';
import FilledButton from '../../components/FilledButton';
import TwelveWordsInput from '../../components/TwelveWordsInput';

type Props = {
  navigation: Object;
  twelveWords: string[];
  changeFormTwelveWords: (word: string, index: number) => void;
  clearTwelveWords: () => void;
  submitSignInTwelveWords: () => void;
  changeAlert: (text: string, type: string) => void;
}

export default function SignInWords(props: Props) {
  const {
    navigation, twelveWords, changeFormTwelveWords, submitSignInTwelveWords, clearTwelveWords, changeAlert
  } = props;
  function onChange(word, index) {
    changeFormTwelveWords(word, index);
    changeAlert('', '');
  }
  return (
    <BackgroundWrapper noScroll>
      <View style={styles.container}>
        <View>
          <View style={styles.textContainer}>
            <Text style={globalStyles.normalText}>{translations.t('ENTER_12_WORDS')}</Text>
          </View>
          <View style={styles.inputContainer}>
            <TwelveWordsInput
              value={twelveWords}
              onChange={onChange}
              onClear={clearTwelveWords}
              onFocus={() => changeAlert('', '')}
            />
          </View>
        </View>
        <View style={styles.confirmContainer}>
          <FilledButton
            title={translations.t('CONFIRM')}
            onPress={submitSignInTwelveWords}
          />
        </View>
      </View>
    </BackgroundWrapper>
  );
}

SignInWords.navigationOptions = () => ({
  title: translations.t('TW_WORDS_SEC')
});


const mapStateToProps = ({
  form: {
    twelveWords
  }
}) => (
    {
      twelveWords,
    }
  );

const mapDispatchToProps = dispatch => bindActionCreators({
  changeFormTwelveWords: ActionCreators.changeFormTwelveWords,
  clearTwelveWords: ActionCreators.clearTwelveWords,
  submitSignInTwelveWords: ActionCreators.submitSignInTwelveWords,
  changeAlert: ActionCreators.changeAlert
}, dispatch);

export const SignInWordsConnected = connect(mapStateToProps, mapDispatchToProps)(SignInWords);
