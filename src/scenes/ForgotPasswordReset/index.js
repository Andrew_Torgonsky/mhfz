// @flow

import React from 'react';
import { View, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import globalStyles from '../../global/styles';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import FilledButton from '../../components/FilledButton';
import PasswordInput from '../../components/PasswordInput';
import TextButton from '../../components/TextButton';
import translations from '../../translations';
import styles from './styles';
import { ActionCreators } from '../../redux/actions';
import PasswordRequirementsModal from '../../components/PasswordRequirementsModal';
import { type AlertType, type ReduxStateType } from '../../types';

type Props = {
  password: string,
  passwordVisibility: boolean,
  passwordError: boolean,
  changeForm: (key: string, value: any) => void,
  updatePassword: () => void,
  changeAlert: (text: string, type: AlertType) => void,
}

type State = {
  modalOpen: boolean
}

export class ForgotPasswordReset extends React.PureComponent<Props, State> {
  static navigationOptions: Object;
  state = {
    modalOpen: false
  }
  dismissErrors = () => {
    this.props.changeForm('passwordError', false);
    this.props.changeAlert('', '');
  }
  toggleModal = () => {
    this.setState({
      modalOpen: !this.state.modalOpen
    });
  }
  render() {
    return (
      <BackgroundWrapper>
        <View style={styles.container}>
          <Text style={[globalStyles.normalText, styles.text]}>
            {translations.t('SET_NEW_PWD')}
          </Text>
          <View style={styles.field}>
            <PasswordInput
              visible={this.props.passwordVisibility}
              value={this.props.password}
              onChangeText={val => this.props.changeForm('password', val)}
              onEyePress={() => this.props.changeForm('passwordVisibility', !this.props.passwordVisibility)}
              error={this.props.passwordError}
              onFocus={this.dismissErrors}
              placeholder={translations.t('PWD')}
            />
          </View>
          <View style={styles.forgotContainer}>
            <TextButton
              title={translations.t('PWD_REQ')}
              onPress={this.toggleModal}
            />
          </View>
          <View style={styles.buttonContainer1}>
            <View style={styles.buttonContainer2}>
              <FilledButton
                title={translations.t('SAVE_PWD')}
                onPress={this.props.updatePassword}
              />
            </View>
          </View>
        </View>
        {this.state.modalOpen ? <PasswordRequirementsModal onPress={this.toggleModal} /> : null}
      </BackgroundWrapper>
    );
  }
}

ForgotPasswordReset.navigationOptions = () => ({
  title: translations.t('PWD_RESET')
});

const mapStateToProps = ({
  form: {
    password, passwordVisibility, passwordError
  }
}: ReduxStateType) => ({
  password,
  passwordVisibility,
  passwordError
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  updatePassword: ActionCreators.updatePassword,
  changeAlert: ActionCreators.changeAlert
}, dispatch);

export const ForgotPasswordResetConnected = connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordReset);
