import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    marginTop: scaleByVertical(40),
    paddingHorizontal: scale(16)
  },
  text: {
    textAlign: 'center'
  },
  field: {
    marginTop: scaleByVertical(24),
    height: scaleByVertical(44)
  },
  forgotContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: scaleByVertical(8)
  },
  buttonContainer1: {
    marginTop: scaleByVertical(24),
    alignItems: 'center'
  },
  buttonContainer2: {
    width: scale(180),
    height: scaleByVertical(40)
  }
});
