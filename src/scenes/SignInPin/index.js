// @flow

import React from 'react';
import { View, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './styles';
import globalStyles from '../../global/styles';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import translations from '../../translations';
import { ActionCreators } from '../../redux/actions';
import PinInput from '../../components/PinInput';
import { Scenes } from '../../navigation/shared';
import type { ReduxStateType } from '../../types';

type Props = {
  navigation: Object,
  pinCode: string,
  changeForm: (key: string, value: any) => void,
  pinCodeError: boolean
}

export const SignInPin = ({
  navigation, pinCode, changeForm, pinCodeError
}: Props) => (
  <BackgroundWrapper>
    <View style={styles.container}>
      <View style={styles.textContainer1}>
        <Text style={globalStyles.normalText}>{translations.t('SET_PIN_TO_SEC')}</Text>
      </View>
      <View style={styles.textContainer2}>
        <Text style={globalStyles.normalText}>{translations.t('ENTER_PIN')}</Text>
      </View>
      <View style={styles.pinInputContainer}>
        <PinInput
          value={pinCode}
          onChangeText={val => changeForm('pinCode', val)}
          onFinish={() => navigation.navigate(Scenes.SignInPinConfirm)}
          error={pinCodeError}
        />
      </View>
    </View>
  </BackgroundWrapper>
);

SignInPin.navigationOptions = () => ({
  title: translations.t('SET_PIN'),
  headerLeft: null
});

const mapStateToProps = ({
  form: {
    pinCode,
    pinCodeError
  }
}: ReduxStateType) => ({
  pinCode,
  pinCodeError
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
}, dispatch);

export const SignInPinConnected = connect(mapStateToProps, mapDispatchToProps)(SignInPin);
