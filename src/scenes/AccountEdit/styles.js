import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  addPhotoContainer1: {
    alignItems: 'center',
    marginTop: scaleByVertical(24)
  },
  addPhotoContainer2: {
    justifyContent: 'center',
    alignItems: 'center',
    width: scale(120),
    height: scale(120),
    borderRadius: scale(60),
    borderWidth: 1,
    borderColor: colors.sky
  },
  addPhotoContainer3: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  addPhotoIcon: {
    tintColor: colors.sky
  },
  addPhotoText: {
    marginTop: scaleByVertical(8),
    color: colors.sky
  },
  field: {
    marginTop: scaleByVertical(24),
    marginHorizontal: scale(16),
    height: scaleByVertical(44)
  },
  fieldIcon: {
    tintColor: colors.white38,
    marginHorizontal: scale(16)
  },
  checkButtonContainer: {
    paddingHorizontal: scale(20),
    paddingVertical: scale(10),
  },
  check: {
    tintColor: 'white'
  }
});
