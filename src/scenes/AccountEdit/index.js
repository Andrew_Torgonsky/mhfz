// @flow

import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import InputField from '../../components/InputField';
import EmailInput from '../../components/EmailInput';
import GhostButton from '../../components/GhostButton';
import translations from '../../translations';
import styles from './styles';
import images from '../../global/images';
import { ActionCreators } from '../../redux/actions';
import { Scenes } from '../../navigation/shared';
import LoadPhoto from '../../components/LoadPhoto';

type Props = {
  navigation: Object;
  name: string;
  email: string;
  nameError: boolean;
  emailError: boolean;
  defaultEmail: string;
  defaultName: string;
  defaultPhoneNumber: string;
  editAccount: () => void;
  changeForm: (key: string, value: string | boolean) => void;
  changeAlert: (text: string, type: string) => void;
  selectFromGallery(): void;
  selectFromCamera(): void;
}

type State = {
  loadPhoto: boolean
}

export class AccountEdit extends React.PureComponent<Props, State> {
  static navigationOptions: Object;
  state = {
    loadPhoto: false
  }
  componentDidMount() {
    this.props.navigation.setParams({
      editAccount: this.props.editAccount
    });
  }
  switchModalOn = () => this.setState({ loadPhoto: true })
  switchModalOff = () => this.setState({ loadPhoto: false })
  render() {
    const {
      name, email, nameError, emailError, defaultEmail, defaultPhoneNumber, defaultName, editAccount, changeForm, changeAlert
    } = this.props;
    const dismissErrors = () => {
      changeAlert('', '');
      changeForm('emailError', false);
      changeForm('nameError', false);
    };
    return (
      <BackgroundWrapper>
        <View style={styles.addPhotoContainer1}>
          <TouchableOpacity style={styles.addPhotoContainer2} onPress={this.switchModalOn}>
            <View style={styles.addPhotoContainer3}>
              <Image source={images.plus} style={styles.addPhotoIcon} />
              <Text style={styles.addPhotoText}>{translations.t('ADD_PHOTO')}</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View>
          <View style={styles.field}>
            <InputField
              before={<Image source={images.user} style={styles.fieldIcon} />}
              defaultValue={defaultName}
              placeholder={translations.t('NAME')}
              onChangeText={val => {
                changeForm('name', val);
                dismissErrors();
              }}
              error={nameError}
              onFocus={dismissErrors}
            />
          </View>
          <View style={styles.field}>
            <EmailInput
              defaultValue={defaultEmail}
              onChangeText={val => {
                changeForm('email', val);
                dismissErrors();
              }}
              error={emailError}
              onFocus={dismissErrors}
            />
          </View>
          <View style={styles.field}>
            <InputField
              before={<Image source={images.call} style={styles.fieldIcon} />}
              after={<GhostButton title={translations.t('CHANGE')} onPress={() => this.props.navigation.navigate(Scenes.SettingsPhone)} />}
              defaultValue={defaultPhoneNumber}
              editable={false}
            />
          </View>
        </View>
        {
          this.state.loadPhoto ?
            <LoadPhoto
              onCameraPress={() => {
                this.props.selectFromCamera();
                this.switchModalOff();
              }}
              onGalleryPress={() => {
                this.props.selectFromGallery();
                this.switchModalOff();
              }}
              onClose={this.switchModalOff}
            /> :
            null
        }
      </BackgroundWrapper>
    );
  }
}

AccountEdit.navigationOptions = ({ navigation }) => ({
  title: translations.t('ACCOUNT'),
  headerRight: (
    <View>
      <TouchableOpacity style={styles.checkButtonContainer} onPress={navigation.getParam('editAccount')}>
        <Image source={images.check} style={styles.check} />
      </TouchableOpacity>
    </View>
  )
});

const mapStateToProps = ({
  session: {
    name: defaultName, phone_number: defaultPhoneNumber, email: defaultEmail
  },
  form: {
    name,
    email,
    nameError,
    emailError
  }
}) => ({
  defaultName,
  defaultPhoneNumber,
  defaultEmail,
  name,
  email,
  nameError,
  emailError
});

const mapDispatchToProps = dispatch => bindActionCreators({
  editAccount: ActionCreators.editAccount,
  changeForm: ActionCreators.changeForm,
  changeAlert: ActionCreators.changeAlert,
  selectFromCamera: ActionCreators.selectFromCamera,
  selectFromGallery: ActionCreators.selectFromGallery
}, dispatch);


export const AccountEditConnected = connect(mapStateToProps, mapDispatchToProps)(AccountEdit);
