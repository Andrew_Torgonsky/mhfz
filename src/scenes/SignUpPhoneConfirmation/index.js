import React from 'react';
import { View, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import RetrySendButton from '../../components/RetrySendButton';
import SmsCode from '../../components/SmsCode';
import translations from '../../translations';
import { ActionCreators } from '../../redux/actions';
import styles from './styles';
import globalStyles from '../../global/styles';
import type { ReduxStateType } from '../../types';

type Props = {
  smsCode: string,
  smsCodeError: boolean,
  navigation: Object,
  smsCodeTimestamp: number,
  changeForm(key: string, value: string | boolean): void,
  checkSignUpSmsCode(): void,
  changeAlert: (text: string, type: string) => void,
  submitSignUp: () => void
}

export class SignUpPhoneConfirmation extends React.PureComponent<Props> {
  render() {
    const {
      changeForm, changeAlert, smsCode, smsCodeError, checkSignUpSmsCode, submitSignUp, smsCodeTimestamp
    } = this.props;
    return (
      <BackgroundWrapper>
        <Text style={[styles.text, styles.topText]}>{translations.t('SOON_SMS')}</Text>
        <Text style={[styles.text]}>{translations.t('ENTER_CODE')}</Text>

        <View style={styles.codeContainer}>
          <SmsCode
            onChangeText={(val) => {
              changeForm('smsCode', val);
              changeForm('smsCodeError', false);
              changeAlert('', '');
            }}
            onFinish={checkSignUpSmsCode}
            value={smsCode}
            error={smsCodeError}
          />
        </View>
        <View style={styles.bottomText}>
          <RetrySendButton onPress={submitSignUp} timestamp={smsCodeTimestamp} />
        </View>
      </BackgroundWrapper>
    );
  }
}

SignUpPhoneConfirmation.navigationOptions = () => ({
  title: translations.t('P_CONF'),
});

const mapStateToProps = ({
  form: {
    smsCode, smsCodeError, smsCodeTimestamp
  }
}: ReduxStateType) => ({
  smsCode,
  smsCodeError,
  smsCodeTimestamp
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  checkSignUpSmsCode: ActionCreators.checkSignUpSmsCode,
  changeAlert: ActionCreators.changeAlert,
  submitSignUp: ActionCreators.submitSignUp,
}, dispatch);

export const SignUpPhoneConfirmationConnected = connect(mapStateToProps, mapDispatchToProps)(SignUpPhoneConfirmation);
