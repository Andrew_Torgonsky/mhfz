import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  text: {
    textAlign: 'center',
    color: colors.white87,
    fontSize: scaleByVertical(16),
    lineHeight: scaleByVertical(24)
  },
  topText: {
    marginTop: scaleByVertical(20)
  },
  codeContainer: {
    marginTop: scaleByVertical(24)
  },
  bottomText: {
    marginTop: scaleByVertical(10),
  }
});
