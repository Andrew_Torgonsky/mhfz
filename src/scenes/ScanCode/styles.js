import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect, height } from '../../utils/scale';

export default StyleSheet.create({
  camera: {
    flex: 1
  },
  top: {
    backgroundColor: colors.dark,
    opacity: 0.75,
    flex: 1
  },
  middle: {
    flex: 3
  },
  bottom: {
    backgroundColor: colors.dark,
    opacity: 0.75,
    flex: 2
  },
  text: {
    marginTop: scaleByVertical(40),
    fontSize: scaleByVertical(16),
    color: colors.white87,
    textAlign: 'center',
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  }
});
