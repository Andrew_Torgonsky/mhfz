import React from 'react';
import { RNCamera } from 'react-native-camera';
import { bindActionCreators } from 'redux';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import AccessDenied from '../../components/AccessDenied';
import styles from './styles';
import translations from '../../translations';
import { ActionCreators } from '../../redux/actions';
import { processQRData } from '../../utils/cryptoCurrencies';

type Props = {
  navigation: Object,
  changeForm: (key: string, value: any) => void,
  prepareToSend: () => void
}

export const ScanCode = ({ changeForm, navigation, prepareToSend }: Props) => (
  <BackgroundWrapper noScrollWithKeyboard>
    <RNCamera
      notAuthorizedView={<AccessDenied type="camera" />}
      style={styles.camera}
      barCodeTypes={[RNCamera.Constants.BarCodeType.qr]}
      onBarCodeRead={(e) => {
        const { address, currency } = processQRData(e);
        if (!address) return;
        changeForm('addressToSend', address);
        changeForm('currencyToSend', currency);
        prepareToSend();
        navigation.goBack();
      }}
    >
      <View style={styles.top} />
      <View style={styles.middle} />
      <View style={styles.bottom} >
        <Text style={styles.text}>{translations.t('POINT_AT_QR')}</Text>
      </View>
    </RNCamera>
  </BackgroundWrapper>
);

ScanCode.navigationOptions = () => ({
  title: translations.t('SCAN_QR2')
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  prepareToSend: ActionCreators.prepareToSend
}, dispatch);

export const ScanCodeConnected = connect(null, mapDispatchToProps)(ScanCode);
