import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Text } from 'react-native';
import translations, { isRTL } from '../../translations';
import { ActionCreators } from '../../redux/actions';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import FilledButton from '../../components/FilledButton';
import CountryCodeInput from '../../components/CountryCodeInput';
import InputField from '../../components/InputField';
import SmsCode from '../../components/SmsCode';
import { Scenes } from '../../navigation/shared';
import globalStyles from '../../global/styles';
import styles from './styles';
import type { AlertType, ReduxStateType } from '../../types';
import RetrySendButton from '../../components/RetrySendButton';

type Props = {
  navigation: Object,
  phonePrefix: string,
  countryIso: string,
  phoneNumber: string,
  phoneError: boolean,
  smsCode: string,
  smsCodeError: boolean,
  smsCodeTimestamp: number,
  changeForm(key: string, value: string): void,
  changeAlert(text: string, type: AlertType): void,
  changePhone(): void,
  enterPhoneSms(): void
};

export const SettingsChangePhone = (props: Props) => {
  const {
    phonePrefix,
    countryIso,
    phoneError,
    phoneNumber,
    navigation,
    changePhone,
    enterPhoneSms,
    smsCode,
    smsCodeError,
    smsCodeTimestamp
  } = props;

  const dismissErrors = () => {
    props.changeForm('phoneError', false);
    props.changeAlert('', '');
  };

  return (
    <BackgroundWrapper>
      <View style={styles.textContainer}>
        <Text style={styles.text}>
          {translations.t('ENTER_NEW_PH_CHANGE')}
        </Text>
      </View>
      <View style={isRTL() ? styles.phoneContainerArabic : styles.phoneContainer}>
        <View style={isRTL() ? styles.inputCountryContainerArabic : styles.inputCountryContainer}>
          <CountryCodeInput
            onPress={() => {
              navigation.navigate(Scenes.CountrySelect);
              dismissErrors();
            }}
            phonePrefix={phonePrefix}
            countryISO={countryIso}
            error={phoneError}
            onFocus={dismissErrors}
          />
        </View>
        <View style={styles.inputNumberContainer}>
          <InputField
            style={styles.inputNumber}
            error={phoneError}
            keyboardType="phone-pad"
            value={phoneNumber}
            onChangeText={val => {
              props.changeForm('phoneNumber', val);
              dismissErrors();
            }}
            placeholder={translations.t('P_NUM')}
            onFocus={dismissErrors}
          />
        </View>
      </View>
      <View style={styles.buttonContainer1}>
        <View style={styles.buttonContainer2}>
          <FilledButton title={translations.t('CONTINUE')} onPress={() => changePhone()} />
        </View>
      </View>
    </BackgroundWrapper>
  );
}

SettingsChangePhone.navigationOptions = () => ({
  title: translations.t('CHANGE_PH_NUM')
});


const mapStateToProps = ({
  form: {
    phonePrefix,
    countryIso,
    phoneNumber,
    password,
    passwordVisibility,
    phoneError,
    passwordError,
    smsCode,
    smsCodeError,
    smsCodeTimestamp
  }
}: ReduxStateType) => (
    {
      phonePrefix,
      countryIso,
      phoneNumber,
      phoneError,
      smsCode,
      smsCodeError,
      smsCodeTimestamp
    }
  );

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  changeAlert: ActionCreators.changeAlert,
  changePhone: ActionCreators.changePhone,
  enterPhoneSms: ActionCreators.enterPhoneSms
}, dispatch);

export const SettingsChangePhoneConnected = connect(mapStateToProps, mapDispatchToProps)(SettingsChangePhone);
