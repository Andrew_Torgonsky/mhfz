import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    marginTop: scaleByVertical(40),
    paddingHorizontal: scale(16)
  },
  text: {
    textAlign: 'center'
  },
  fields: {
    flexDirection: 'row',
    marginTop: scaleByVertical(24),
    height: scaleByVertical(44)
  },
  fieldContainer1: {
    flex: 1,
    paddingRight: scale(4)
  },
  fieldContainer2: {
    flex: 1
  },
  containerButton1: {
    marginTop: scaleByVertical(48),
    alignItems: 'center'
  },
  containerButton2: {
    width: scale(180),
    height: scaleByVertical(42)
  }
});
