// @flow

import React from 'react';
import { View, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import globalStyles from '../../global/styles';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import CountryCodeInput from '../../components/CountryCodeInput';
import FilledButton from '../../components/FilledButton';
import InputField from '../../components/InputField';
import { scale } from '../../utils/scale';
import translations from '../../translations';
import { Scenes } from '../../navigation/shared';
import styles from './styles';
import { ActionCreators } from '../../redux/actions';
import type { AlertType, ReduxStateType } from '../../types';

type Props = {
  navigation: Object,
  phonePrefix: string,
  countryIso: string,
  phoneNumber: string,
  phoneError: boolean,
  changeForm: (key: string, value: any) => void,
  requestPasswordRecovery: () => void,
  changeAlert: (text: string, type: AlertType) => void,
}

export const ForgotPasswordPhone = (props: Props) => {
  const dismissErrors = () => {
    props.changeForm('phoneError', false);
    props.changeAlert('', '');
  };

  return (
    <BackgroundWrapper>
      <View style={styles.container}>
        <Text style={[globalStyles.normalText, styles.text]}>
          {translations.t('PWD_RESET_PHONE')}
        </Text>
        <View style={styles.fields}>
          <View style={styles.fieldContainer1}>
            <CountryCodeInput
              onPress={() => {
                props.navigation.navigate(Scenes.CountrySelect);
                dismissErrors();
              }}
              phonePrefix={props.phonePrefix}
              countryISO={props.countryIso}
              error={props.phoneError}
            />
          </View>
          <View style={styles.fieldContainer2}>
            <InputField
              inputIndentaion={scale(16)}
              keyboardType="phone-pad"
              value={props.phoneNumber}
              onChangeText={(val) => {
                props.changeForm('phoneNumber', val);
                dismissErrors();
              }}
              placeholder={translations.t('P_NUM')}
              error={props.phoneError}
              onFocus={dismissErrors}
            />
          </View>
        </View>
        <View style={styles.containerButton1}>
          <View style={styles.containerButton2}>
            <FilledButton
              title={translations.t('CONTINUE')}
              onPress={props.requestPasswordRecovery}
            />
          </View>
        </View>
      </View>
    </BackgroundWrapper>
  );
};

ForgotPasswordPhone.navigationOptions = () => ({
  title: translations.t('PWD_RESET')
});

const mapStateToProps = ({
  form: {
    phonePrefix, countryIso, phoneNumber, phoneError
  }
}: ReduxStateType) => (
  {
    phonePrefix,
    countryIso,
    phoneNumber,
    phoneError
  }
);

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  changeAlert: ActionCreators.changeAlert,
  requestPasswordRecovery: ActionCreators.requestPasswordRecovery,
}, dispatch);

export const ForgotPasswordPhoneConnected = connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordPhone);
