import React from 'react';
import { View, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './styles';
import globalStyles from '../../global/styles';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import translations from '../../translations';
import { ActionCreators } from '../../redux/actions';
import PinInput from '../../components/PinInput';

type Props = {
  navigation: Object,
  pinCodeConfirm: string,
  changeForm(key: string, value: string): void,
  compareSignUpPinCodes(): void,
  pinCodeError: boolean
}

export default function SignUpPinConfirm(props: Props) {
  const {
    navigation, pinCodeConfirm, changeForm, pinCodeError, compareSignUpPinCodes
  } = props;
  return (
    <BackgroundWrapper>
      <View style={styles.container}>
        <View style={styles.textContainer}>
          <Text style={globalStyles.normalText}>{translations.t('CONF_PIN')}</Text>
        </View>
        <View style={styles.pinInputContainer}>
          <PinInput
            value={pinCodeConfirm}
            onChangeText={val => changeForm('pinCodeConfirm', val)}
            error={pinCodeError}
            onFinish={compareSignUpPinCodes}
          />
        </View>
      </View>
    </BackgroundWrapper>
  );
}

SignUpPinConfirm.navigationOptions = () => ({
  title: translations.t('SET_PIN'),
  headerLeft: null
});

const mapStateToProps = ({
  form: {
    pinCodeConfirm,
    pinCodeError
  }
}) => (
  {
    pinCodeConfirm,
    pinCodeError
  }
);

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  compareSignUpPinCodes: ActionCreators.compareSignUpPinCodes,
}, dispatch);

export const SignUpPinConfirmConnected = connect(mapStateToProps, mapDispatchToProps)(SignUpPinConfirm);
