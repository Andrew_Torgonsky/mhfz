// @flow

import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import MenuItem from '../../components/MenuItem';
import translations from '../../translations';
import { ActionCreators } from '../../redux/actions';
import styles from './styles';
import images from '../../global/images';
import currencies from '../../global/currencies';
import type { ReduxStateType, FiatCurrencyType } from '../../types';

type Props = {
  default_currency: FiatCurrencyType,
  changeCurrency(currency: FiatCurrencyType): void,
  navigation(): void
}

export function DefaultCurrency(props: Props) {
  const { default_currency } = props;
  console.log('props', props);
  return (
    <BackgroundWrapper>
      {
        currencies.map((currency, i) => (
          <MenuItem
            key={i}
            title={currency.toUpperCase()}
            right={
              default_currency.toLowerCase() === currency &&
              <View style={styles.leftContainer}>
                <Image source={images.check} style={styles.icon} />
              </View>
            }
            onPress={() => props.changeCurrency(currency)}
          />
        ))
      }
    </BackgroundWrapper>
  );
}

DefaultCurrency.navigationOptions = () => ({
  title: translations.t('DEFAULT_C')
});

const mapStateToProps = ({ session: { default_currency } }: ReduxStateType) => ({ default_currency });

const mapDispatchToProps = dispatch => bindActionCreators({
  changeCurrency: ActionCreators.changeCurrency
}, dispatch);

export const DefaultCurrencyConnected = connect(mapStateToProps, mapDispatchToProps)(DefaultCurrency);
