import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  icon: {
    tintColor: colors.brightTeal,
    marginTop: scaleByVertical(-2)
  },
  leftContainer: {
    flexDirection: 'row'
  }
});
