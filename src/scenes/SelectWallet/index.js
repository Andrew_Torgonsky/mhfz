// @flow

import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import ChooseWalletItem from '../../components/ChooseWalletItem';
import type { ReduxStateType, CryptoCurrencyType, WalletsStatType } from '../../types';
import { ActionCreators } from '../../redux/actions';
import styles from './styles';
import translations from '../../translations';
import { isAndroid } from '../../utils/scale';

type Props = {
  navigation: Object,
  currencyToSend: CryptoCurrencyType,
  walletsStat: WalletsStatType,
  changeForm: (key: string, value: any) => void,
  prepareToSend: () => void
}

const options = ['bitcoin', 'ethereum', 'ripple'];

export const SelectWallet = ({
  navigation, currencyToSend, walletsStat, changeForm, prepareToSend
}: Props) => (
  <BackgroundWrapper>
    <View style={styles.container}>
      {options.map(item => (
        <ChooseWalletItem
          key={item}
          currency={item}
          active={item === currencyToSend}
          onPress={() => {
            changeForm('currencyToSend', item);
            prepareToSend();
            navigation.goBack();
          }}
          walletsStat={walletsStat}
        />
      ))}
    </View>
  </BackgroundWrapper>
);

SelectWallet.navigationOptions = ({ navigation }) => ({
  title: translations.t('SELECT_WALLET'),
});

const mapStateToProps = ({
  form: {
    currencyToSend
  },
  walletsStat
}: ReduxStateType) => ({
  currencyToSend,
  walletsStat
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  prepareToSend: ActionCreators.prepareToSend
}, dispatch);

export const SelectWalletConnected = connect(mapStateToProps, mapDispatchToProps)(SelectWallet);
