import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect, height } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    marginTop: scaleByVertical(16)
  },
});
