import React from 'react';
import { View, Text } from 'react-native';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import translations, { isRTL } from '../../translations';
import styles from './styles';

const T = ({ children, mtop, li, center }) => <Text style={[styles.text, isRTL() && styles.arabic, mtop && styles.mtop, center && styles.center]}>{li ? <Text>&bull; </Text> : null}{children}</Text>
const B = ({ children }) => <Text style={styles.bold}>{children}</Text>

export const PrivacyPolicy = () => (
  <BackgroundWrapper>
    <View style={styles.container}>
      <T mtop>
        The owner of the Website and mobile app (“App”) is Cryptocurrency Solutions OÜ.
      </T>
      <T mtop>
        This <B>Privacy Policy</B> (Policy) governs the access and use of the Website, App and its Content by User.
      </T>
      <T mtop>
        This Privacy Policy describes the ways Cryptocurrency Solutions OÜ (“we,” “us” or “our” as
        appropriate) collects, stores, uses and manages the information you provide to us or we collect in
        connection with your use of the Website or App.
      </T>
      <T mtop>
        BY SUBMITTING YOUR INFORMATION TO US AND USING THE WEBSITE OR APP YOU AGREE TO THIS
        PRIVACY POLICY AND THE PROCESSING OF YOUR INFORMATION, INCLUDING YOUR PERSONAL DATA,
        IN THE MANNER PROVIDED IN THIS POLICY.
      </T>
      <T mtop>
        If you do not agree with the terms of this Policy please do not use the Website or App.
      </T>
      <T mtop>
        <B>1. Definitions</B>
      </T>
      <T mtop li>
        <B>“Terms”</B> means the Cryptocurrency Solutions OÜ Terms and Conditions.
      </T>
      <T li>
        <B>“User”, “you”</B> means a person using the Website or App.
      </T>
      <T li>
        <B>“Website”</B> means the website accessible at Mahfazah.io, including its subdomains,
        programming code, related technologies, know-hows, databases and design.
      </T>
      <T li>
        <B>“App”</B> means the application available for Users to download from the Website owned by
        Cryptocurrency Solutions OÜ and to use through a mobile device by User.
      </T>
      <T li>
        <B>“Personal Data”</B> means information or its combination that can be used to identify you as an
        individual, such as, for example first and last name.
      </T>
      <T li>
        <B>“Usage Data”</B> means non-personally identifiable information about your usage of the
        Website or App that alone or in combination with similar information can not be used to
        identify you.
      </T>
      <T li>
        <B>“Information”</B> means collectively any Personal Data, Usage Data, the information you have
        made publicly available and/or any other protected or publicly available information.
      </T>
      <T mtop>
        <B>2. Personal Data</B>
      </T>
      <T mtop>
        To access and use the Website and App, Cryptocurrency Solutions OÜ may process and/or collect the
        following Personal Data (by the way of example):
      </T>
      <T mtop li>
        First and last name;
      </T>
      <T li>
        Email address;
      </T>
      <T li>
        Address;
      </T>
      <T li>
        Date of birth;
      </T>
      <T li>
        ID, residency and source of funds verification documents (e.g. copies of a passport/ID card
        and utility bills) and data contained in such documents (e.g. home or another physical
        address, phone/mobile number, gender).
      </T>
      <T mtop>
        You may explicitly decline to submit Personal Data in which case we may not be able to provide
        certain or all our services and products to you.
      </T>
      <T mtop>
        <B>3. Usage Data</B>
      </T>
      <T mtop>
        For you to access and use the Website and App, we collect Usage Data from your computer, mobile
        device and browser when you use the Website or App.
      </T>
      <T mtop>
        We (through the use of Google Analytics and/or other analytics services) automatically collect Usage
        Data, such as (by the way of example):
      </T>
      <T mtop li>
        Your IP address;
      </T>
      <T li>
        The type of device you use;
      </T>
      <T li>
        ID or serial numbers of your device;
      </T>
      <T li>
        Browser type, version and language;
      </T>
      <T li>
        Type and version of your operating system;
      </T>
      <T li>
        The number and frequency of visitors to the Website.
      </T>
      <T mtop>
        We also collect and log a record of all internet communications, details of transactions you carry
        through the Website or App, details of your activities and details of your visits to the Website and
        App.
      </T>
      <T mtop>
        We do not intentionally collect <B>sensitive personal information</B>, such as social security numbers,
        genetic data, health information, or religious information. Although Cryptocurrency Solutions OÜ
        does not request or intentionally collect any sensitive personal information, we realize that you
        might store this kind of information in your account.
      </T>
      <T mtop>
        If you&#39;re a <B>child under the age of 13</B>, you may not have an account on Cryptocurrency Solutions OÜ
        Website or App. Cryptocurrency Solutions OÜ does not knowingly collect information from or direct
        any of our content specifically to children under 13. If we learn or have reason to suspect that you
        are a user who is under the age of 13, we will unfortunately have to close your account.
      </T>
      <T mtop>
        <B>4. Cookies and Other Tracking Technologies</B>
      </T>
      <T mtop>
        A cookie is a small piece of text sent to your browser by a website you visit. It helps the Website or
        App to remember information about your visit. That can make your next visit easier and the site
        more useful to you.
      </T>
      <T mtop>
        We use both session cookies and persistent cookies. A persistent cookie remains after you close your
        browser. Persistent cookies may be used by your browser on subsequent visits to the Website or
        App. Persistent cookies can be removed by following your web browser help file directions. Session
        cookies are temporary and typically disappear after you close your browser.
      </T>
      <T mtop>
        We use our own cookies (first party cookies):
      </T>
      <T mtop li>
        To remember your choice about cookies on the Website or App.
      </T>
      <T li>
        To recognize you when you visit the Website or App.
      </T>
      <T li>
        To remember your preferences.
      </T>
      <T li>
        To perform security measures.
      </T>
      <T mtop>
        In addition, we work with reputable companies who can use their cookies when you use the Website
        or App (third party cookies). Third-party cookies are used, for example, to count how many visitors
        we receive to a page, to help us analyse how the Website or App is used (e.g. Google Analytics
        cookies). We use the information to compile reports and to help us improve the Website and App.
        The cookies collect information in an anonymous form, including the number of visitors to the
        Website or App, where visitors have come to the Website from and the pages they visited.
      </T>
      <T mtop>
        Most web browsers allow some control of most cookies through the browser settings. You may be
        able to reset your web browser to refuse all cookies or to indicate when a cookie is being sent.
        However, some features of the Website may not function properly if the ability to accept cookies is
        disabled.
      </T>
      <T mtop>
        If you choose to use the Website without blocking or disabling cookies, you will indicate your consent
        to our use of these cookies and to our use (in accordance with this Policy) of any Information that we
        collect using these technologies. <B>If you do not consent to the use of cookies, please be sure to block
        or disable them.</B>
      </T>
      <T mtop>
        We do not use cookies to store any Personal Data.
      </T>
      <T mtop>
        We also use local storage. Local storage is an industry-standard technology that allows us to store
        Information locally on your computer or mobile device. We may use local storage to store
        Information about your preferences or your authorization details.
      </T>
      <T mtop>
        <B>“Do not track&quot;</B> is a privacy preference you can set in your browser if you do not want online services
        to collect and share certain kinds of information about your online activity from third party tracking
        services. We do not track your online browsing activity on other online services over time and we do
        not permit third-party services to track your activity on our site beyond our basic Google Analytics
        tracking. Because we do not share this kind of data with third party services or permit this kind of
        third party data collection on Cryptocurrency Solutions OÜ for any of our users, and we do not track
        our users on third-party websites ourselves, we do not need to respond differently to an individual
        browser&#39;s Do Not Track setting.
      </T>
      <T mtop>
        <B>5. Use of Google Analytics and Similar Third-Party Services</B>
      </T>
      <T mtop>
        We use Google Analytics to collect information about use of the Website and App. Google Analytics
        collects information such as how often users visit this site, what pages they visit when they do so,
        and what other sites they used prior to coming to the Website or downloading the App. We use the
        information we get from Google Analytics only to improve the Website and App. Google Analytics
        collects only the IP address assigned to you on the date you visit this site, rather than your name or
        other identifying information.
      </T>
      <T mtop>
        We do not combine the information collected through the use of Google Analytics with your Personal
        Data. Although Google Analytics plants a permanent cookie on your web browser to identify you as a
        unique user the next time you visit the Website or App, the cookie cannot be used by anyone but
        Google. Google’s ability to use and share information collected by Google Analytics about your visits
        to this site is restricted by the <B>Google Analytics Terms of Use</B> and the <B>Google Privacy Policy.</B>
      </T>
      <T mtop>
        Google Analytics employs cookies to define user sessions, which allows for the collection of data
        about how visitors are using the Website or App. Google Analytics uses only first-party cookies for
        data analysis. This means that the cookies are linked to a specific website domain, and Google
        Analytics will only use that cookie data for statistical analysis related to your browsing behaviour on
        that specific website. According to Google, the data collected cannot be altered or retrieved by
        services from other domains.
      </T>
      <T mtop>
        You can prevent Google Analytics from recognizing you on return visits to the Website by disabling
        cookies on your browser. To opt out of being tracked by Google Analytics across all websites visit the
        <B>Google Analytics Opt-Out page.</B>
      </T>
      <T mtop>
        We may use other analytics tools that work in a way similar to Google Analytics.
      </T>
      <T mtop>
        <B>6. Why We Collect Information</B>
      </T>
      <T mtop>
        Information is used for:
      </T>
      <T mtop li>
        Processing payments, providing you an opportunity to use the Website, App and its
        functionality;
      </T>
      <T li>
        Setting up, operating and managing your account;
      </T>
      <T li>
        Identification and verification;
      </T>
      <T li>
        Monitoring your transactions for the purposes of preventing fraud, money laundering and
        cheating;
      </T>
      <T li>
        Analytical and statistical purposes;
      </T>
      <T li>
        Your seamless interaction with the Website or App;
      </T>
      <T li>
        Understanding of your interaction with the Website or App;
      </T>
      <T li>
        Website and App security;
      </T>
      <T li>
        Improvement and development of the Website and App or our other services and products;
      </T>
      <T li>
        Legal and regulatory reasons, including enforcement of this Policy and the Terms;
      </T>
      <T li>
        Sending you related information, including promotions, special offers, confirmations,
        newsletters, updates, and security alerts.
      </T>
      <T mtop>
        Using your name and email address, we will occasionally send you alerts or information on products,
        bonuses and promotions related to our services. You can opt out from these emails any time.
      </T>
      <T mtop>
        <B>7. How Long We Retain Information</B>
      </T>
      <T mtop>
        The time periods for which we retain your Information depend on the purposes for which we use it.
        We will keep your Information for as long as you are a User and, thereafter, for no longer than is
        reasonably necessary for reporting and reconciliation purposes pursuant to legal requirements and
        to provide you with any feedback or information you might request or require.
      </T>
      <T mtop>
        <B>8. Your Consent</B>
      </T>
      <T mtop>
        You expressly consent to our processing, collection, transfer, storage, disclosure and other uses of
        your Information as described in this Policy and the Terms at instance when you:
      </T>
      <T mtop li>
        Register with us;
      </T>
      <T li>
        Provide Information to us through the Website or through the App;
      </T>
      <T li>
        Browse and use the Website or App.
      </T>
      <T mtop>
        In addition, through this use and interactions with the Website or App you specifically consent to our
        (or our suppliers and subcontractors):
      </T>
      <T mtop li>
        Collection of Information by automated means, such as cookies, local storage or the
        intended functionality of the Website and App, and to our subsequent processing and using
        of Information in accordance with this Policy and Terms;
      </T>
      <T li>
        Storage and transfer of your Personal Data to locations wherever we and our service
        providers have facilities.
      </T>
      <T mtop>
        <B>9. Disclosure of Information</B>
      </T>
      <T mtop>
        We may release Information concerning your use of the Website or App:
      </T>
      <T mtop li>
        To provide you with the services and functionality of the Website or App;
      </T>
      <T li>
        If it is set out in the Terms;
      </T>
      <T li>
        When we believe in good faith that such release is appropriate to comply with the applicable
        law, regulation, legal process or governmental request (for example, pursuant to a statutory
        demand, subpoena, warrant or court order);
      </T>
      <T li>
        In order to detect, prevent and investigate fraud or money laundering issues;
      </T>
      <T li>
        To enforce this Policy and/or the Terms;
      </T>
      <T li>
        To detect, prevent or otherwise address security or technical issues;
      </T>
      <T li>
        To protect against abusive or unlawful use of the Website or App;
      </T>
      <T li>
        To protect the rights, safety, or property of Users, or any other third parties;
      </T>
      <T li>
        If we reasonably believe that a situation involving danger of death or injury to any person
        requires disclosure;
      </T>
      <T li>
        In other ways described in this Policy and the Terms.
      </T>
      <T mtop>
        <B>10. With Whom We Share Information</B>
      </T>
      <T mtop>
        We may share Information we collect only under any of the circumstances mentioned in this Policy
        and/or the Terms.
      </T>
      <T mtop>
        We will not share any Personal Data provided by you with third parties except within our associated
        or subsidiary companies, and with service providers or suppliers under contract who help with our
        business operations such as payment processing, compliance and User verification, fraud
        investigation, information management and analytics. Such providers are obligated not to disclose
        your Personal Data and authorized to use it only as necessary to provide their services to us. We may
        also make inquiries and disclose your Personal Data to agencies, security organisations and any other
        relevant third parties for fraud and money laundering prevention.
      </T>
      <T mtop>
        <B>11. Security of Personal Data</B>
      </T>
      <T mtop>
        We provide the same standard of privacy protection to all our users around the world, regardless of
        their country of origin or location, and we are proud of the levels of notice, choice, accountability,
        security, data integrity, access, and recourse we provide. We have appointed a Privacy Counsel and
        we work hard to comply with the applicable data privacy laws wherever we do business. Additionally,
        we require that if our vendors or affiliates have access to User Information, they must comply with
        our privacy policies and with applicable data privacy laws, including signing data transfer agreements
        such as Standard Contractual Clause agreements.
      </T>
      <T mtop>
        We will take commercially reasonable precautions to protect Personal Data from loss, misuse and
        unauthorized access, disclosure, alteration and destruction. We process all Information using
        industry-standard techniques.
      </T>
      <T mtop>
        We implement the following administrative, technical, and physical measures for the protection of
        Personal Data:
      </T>
      <T mtop li>
        Administrative measures. Access to your Personal Data is limited to authorized personnel
        who have a legitimate need to access it based on their job descriptions. In case third-party
        contractors process Personal Data on our behalf, similar requirements are imposed.
      </T>
      <T li>
        Technical measures. Personal Data is transmitted in encrypted format using SSL technology.
        We use up-to-date firewall protection and high-quality antivirus and anti-malware software.
      </T>
      <T li>
        Physical measures. Access to Personal Data is not permitted via the Internet except using an
        encrypted virtual private network. We store Personal Data on servers provided by third party
        hosting vendors with whom we have contracted.
      </T>
      <T mtop>
        However, we do not guarantee or warrant that such techniques will prevent unauthorized access to
        Information, including Personal Data that we store.
      </T>
      <T mtop>
        It’s your responsibility to keep your phone and access to the App secure. We therefore recommend
        that you do not jailbreak or root your phone, which is the process of removing software restrictions
        and limitations imposed by the official operating system of your device. It could make your phone
        vulnerable to malware/viruses/malicious programs, compromise your phone’s security features and
        it could mean that the App won’t work properly or at all.
      </T>
      <T mtop>
        <B>12. Business or Asset Transfer</B>
      </T>
      <T mtop>
        In the event that we, a line of our business, or our assets are transferred, sold or merged with
        another business, Information may be one of the transferred assets and we will not be obliged to
        notify you or need your consent in order to transfer Information.
      </T>
      <T mtop>
        <B>13. Modifications</B>
      </T>
      <T mtop>
        We reserve the right, at our sole discretion to put into effect, modify or revise this Policy at any time
        by posting the Policy or revised Policy on the Website and App. The Policy or any changes will
        become effective upon posting of the revised Policy. If we make any material changes to the Policy
        we will use reasonable efforts to inform you by email or by posting a notice on the Website.
      </T>
      <T mtop>
        We encourage you to review this Policy each time you visit the Website to make sure you understand
        the most current Policy. Your continued use of the Website signifies your acceptance of any
        implemented or changed terms.
      </T>
      <T mtop>
        <B>14. Changing or Updating Your Personal Data</B>
      </T>
      <T mtop>
        We take reasonable steps to ensure that the Personal Data we hold about you is accurate, complete
        and up-to-date. However, we rely on you to advise us of any changes to your Personal Data.
      </T>
      <T mtop>
        Please log into your account or contact us as soon as possible if there are any changes to your
        Personal Data or if you believe the Personal Data we hold about you is not accurate, complete or up-
        to-date so that we can update our records accordingly.
      </T>
      <T mtop>
        <B>15. Privacy Related Inquiries</B>
      </T>
      <T mtop>
        If, for any reason, you are concerned with the way that we may be using your Personal Data or you
        have questions about the privacy aspects of the Website or App, please, contact us at
        info@mahfazah.io.
      </T>
    </View>
  </BackgroundWrapper>
);

PrivacyPolicy.navigationOptions = () => ({
  title: translations.t('P_P'),
});
