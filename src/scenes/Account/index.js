import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import Avatar from '../../components/Avatar';
import translations from '../../translations';
import styles from './styles';
import { scale, scaleByVertical } from '../../utils/scale';
import images from '../../global/images';
import { ActionCreators } from '../../redux/actions';
import { Scenes } from '../../navigation/shared';
import { URL } from '../../config';


type Props = {
  name: string,
  email: string,
  phone_number: string,
  image: string
};

export const Account = (props: Props) => (
  <BackgroundWrapper>
    <Avatar uri={`${URL}${props.image}`} />
    <View style={styles.infoContainer}>
      <View style={styles.infoItem}>
        <Text style={styles.infoTextTitle}>{translations.t('NAME')}</Text>
        <Text style={styles.infoTextValue}>{props.name}</Text>
      </View>
      <View style={styles.infoItem}>
        <Text style={styles.infoTextTitle}>{translations.t('E_MAIL')}</Text>
        <Text style={styles.infoTextValue}>{props.email}</Text>
      </View>
      <View style={styles.infoItem}>
        <Text style={styles.infoTextTitle}>{translations.t('PHONE')}</Text>
        <Text style={styles.infoTextValue}>{props.phone_number}</Text>
      </View>
    </View>
  </BackgroundWrapper>
);


Account.navigationOptions = ({ navigation }) => ({
  title: translations.t('ACCOUNT'),
  headerRight: (
    <View style={styles.topButtonContainer}>
      <TouchableOpacity style={styles.topButton} onPress={() => navigation.navigate(Scenes.AccountEdit)}>
        <Image source={images.edit} />
      </TouchableOpacity>
      <TouchableOpacity style={styles.topButton} onPress={() => navigation.navigate(Scenes.Settings)}>
        <Image source={images.settings} />
      </TouchableOpacity>
    </View>
  )
});

const mapStateToProps = ({
  session: {
    name,
    email,
    phone_number,
    image
  }
}) => ({
  name,
  email,
  phone_number,
  image
});

export const AccountConnected = connect(mapStateToProps)(Account);

