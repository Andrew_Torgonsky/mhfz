import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  infoContainer: {
    height: '100%',
    marginTop: scaleByVertical(24),
  },
  infoItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: scaleByVertical(16),
    paddingHorizontal: scale(16),
    height: scaleByVertical(44),
    borderBottomWidth: scaleByVertical(1),
    borderColor: colors.white20
  },
  infoTextTitle: {
    color: colors.white50,
    fontSize: scaleByVertical(16)
  },
  infoTextValue: {
    color: colors.white87,
    fontSize: scaleByVertical(16)
  },
  topButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  topButton: {
    alignItems: 'center',
    justifyContent: 'center',
    width: scale(40),
    height: scale(40),
    marginRight: scale(5)
  }
});
