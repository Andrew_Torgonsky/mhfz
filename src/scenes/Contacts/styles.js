import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  searchContainer: {
    marginVertical: scaleByVertical(16),
    marginHorizontal: scale(16),
    height: scaleByVertical(44)
  },
  letterContainer: {
    paddingVertical: scaleByVertical(4),
    paddingHorizontal: scale(16),
    backgroundColor: colors.grey2
  },
  letter: {
    fontSize: scaleByVertical(17),
    color: colors.white87
  },
  contactContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: scaleByVertical(13),
    paddingHorizontal: scale(13),
  },
  contacts: {
    flex: 1
  },
  contact: {
    fontSize: scaleByVertical(16),
    color: colors.white87
  },
  editButton: {
    paddingHorizontal: scale(20),
    paddingVertical: scaleByVertical(15),
  },
  invite: {
    fontSize: scaleByVertical(14),
    color: colors.sky
  }
});
