// @flow

import R from 'ramda';
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, SectionList } from 'react-native';
import Permissions from 'react-native-permissions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Share from 'react-native-share';
import { ActionCreators } from '../../redux/actions';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import SearchInput from '../../components/SearchInput';
import translations from '../../translations';
import { Scenes } from '../../navigation';
import styles from './styles';
import type { ReduxStateType, CryptoCurrencyType, ContactType } from '../../types';
import AccessDenied from '../../components/AccessDenied';
import { APP_STORE_LINK, PLAY_MARKET_LINK } from '../../config';

const shareOptions = {
  title: 'Mahfazah',
  message: `Hey, I’m inviting you to try Mahfazah crypto wallet app calling click on the link to download:
App Store: https://${APP_STORE_LINK}
Google Play: https://${PLAY_MARKET_LINK}`,
  urls: []
};



type Props = {
  type: 'contacts' | 'recipients',
  contacts: ContactType[],
  navigation: Object,
  currencyToSend: CryptoCurrencyType,
  changeForm: (key: string, value: any) => void,
  prepareToSend: () => void
}

type State = {
  search: string,
  accessDenied: boolean
}

export default class Contacts extends Component<Props, State> {
  state = {
    search: '',
    accessDenied: false
  }
  componentWillMount() {
    Permissions.check('contacts').then((response) => {
      this.setState({
        accessDenied: response !== 'authorized'
      });
    });
  }
  onSearch = (val: string) => {
    this.setState({
      search: val
    });
  }
  onItemPress = (contact: ContactType) => {
    if (this.props.type === 'contacts') this.props.navigation.navigate(Scenes.Contact, contact);
    if (this.props.type === 'recipients') {
      const wallet = contact.wallets.find(wal => wal.currency === this.props.currencyToSend);
      if (!wallet) return;
      this.props.changeForm('contactToSend', contact);
      this.props.changeForm('addressToSend', wallet.walletAddress);
      this.props.prepareToSend();
      this.props.navigation.goBack();
    }
  }

  filterContacts = (contacts: ContactType[]) => contacts.filter(contact => (contact.firstName.toLowerCase().startsWith(this.state.search.toLowerCase()) ||
    contact.lastName.toLowerCase().startsWith(this.state.search.toLowerCase())) &&
    (this.props.type === 'contacts' || contact.wallets.length > 0));

  sortContacts = (contacts: ContactType[]) => {
    const extractName = (el: ContactType) => R.toUpper(el.lastName || el.firstName || '#');
    return R.pipe(
      R.sort((a, b) => {
        if (extractName(a) > extractName(b)) return 1;
        if (extractName(a) < extractName(b)) return -1;
        return 0;
      }),
      R.groupBy(el => extractName(el).slice(0, 1)),
      R.mapObjIndexed((data, title) => ({ title, data })),
      R.values
    )(contacts);
  }

  render() {
    const {
      contacts
    } = this.props;
    return (
      <BackgroundWrapper noScrollWithKeyboard>
        {
          !this.state.accessDenied ?
            <React.Fragment>
              <View style={styles.searchContainer}>
                <SearchInput
                  value={this.state.search}
                  onChangeText={this.onSearch}
                  placeholder={translations.t('SEARCH')}
                />
              </View>
              <SectionList
                renderSectionHeader={
                  ({ section: { title } }) => (
                    <View style={styles.letterContainer}>
                      <Text style={styles.letter}>{title}</Text>
                    </View>
                  )}
                keyExtractor={(item, index) => item.firstName + item.lastName + item.phoneNumbers[0]}
                renderItem={
                  ({ item }) => (
                    <TouchableOpacity style={styles.contactContainer} onPress={() => this.onItemPress(item)}>
                      <Text style={styles.contact}>
                        {
                          item.firstName || item.lastName ?
                            `${item.firstName} ${item.lastName}`
                            : `${item.phoneNumbers[0]}`
                        }
                      </Text>
                      {
                        !item.wallets.length ?
                          <TouchableOpacity onPress={() => Share.open(shareOptions).catch(() => { })}>
                            <Text style={styles.invite}>{translations.t('INVITE')}</Text>
                          </TouchableOpacity>
                          : null
                      }
                    </TouchableOpacity>
                  )}
                sections={
                  R.pipe(
                    this.filterContacts,
                    this.sortContacts
                  )(contacts)
                }
                keyboardShouldPersistTaps="handled"
              />
            </React.Fragment> :
            <AccessDenied type="contacts" />
        }
      </BackgroundWrapper>
    );
  }
}

const mapStateToProps = ({
  contacts,
  form: {
    currencyToSend
  }
}: ReduxStateType) => ({
  contacts,
  currencyToSend
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  prepareToSend: ActionCreators.prepareToSend
}, dispatch);

export const ContactsConnected = connect(mapStateToProps, mapDispatchToProps)(Contacts);
