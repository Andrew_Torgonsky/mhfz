import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  text: {
    marginRight: scale(10),
    fontSize: scaleByVertical(16),
    color: colors.white87
  },
  leftText: {
    color: colors.white50
  },
  icon: {
    marginTop: scaleByVertical(-2),
  },
  leftContainer: {
    flexDirection: 'row'
  }
});
