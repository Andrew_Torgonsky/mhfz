import React from 'react';
import { View, Text, TouchableOpacity, Image, Linking } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import MenuItem from '../../components/MenuItem';
import Toggle from '../../components/Toggle';
import translations from '../../translations';
import { ActionCreators } from '../../redux/actions';
import { Scenes } from '../../navigation/shared';
import styles from './styles';
import { scale, scaleByVertical, isIOS, isAndroid } from '../../utils/scale';
import { APP_STORE_LINK, PLAY_MARKET_LINK } from '../../config';

type Props = {
  default_currency: string;
  navigation: Object;
  logOut: () => void;
  language: string;
  pushNotifications: boolean;
  switchNotifications(): void;
}

export default function Settings(props: Props) {
  const { navigate } = props.navigation;
  return (
    <BackgroundWrapper>

      {/* DEFAULT CURRENCY */}
      <MenuItem
        title={translations.t('DEFAULT_C')}
        onPress={() => navigate(Scenes.DefaultCurrency)}
        rightArrow
        right={
          <View style={styles.leftContainer}>
            <Text style={[styles.text, styles.leftText]}>{props.default_currency.toUpperCase()}</Text>
          </View>
        }
      />

      {/* LANGUAGE */}
      <MenuItem
        title={translations.t('LANGUAGE')}
        rightArrow
        right={
          <View style={styles.leftContainer}>
            <Text style={[styles.text, styles.leftText]}>{translations.t('CURRENT_LANGUAGE')}</Text>
          </View>
        }
        onPress={() => props.navigation.navigate(Scenes.Language)}
      />

      {/* NOTIFICATIONS */}
      <MenuItem
        title={translations.t('NOTIFICATIONS')}
        right={<Toggle active={props.pushNotifications} />}
        onPress={() => props.switchNotifications()}
      />

      {/* CHANGE PIN */}
      <MenuItem
        title={translations.t('CHANGE_PIN')}
        onPress={() => navigate(Scenes.SettingsPin)}
        rightArrow
      />

      {/* CHANGE PASSWORD */}
      <MenuItem
        title={translations.t('CHANGE_PWD')}
        onPress={() => navigate(Scenes.SettingsPassword)}
        rightArrow
      />

      {/* CHANGE PHONE NUMBER */}
      <MenuItem
        title={translations.t('CHANGE_PH_NUM')}
        onPress={() => navigate(Scenes.SettingsPhone)}
        rightArrow
      />

      {/* FEEDBACK */}
      <MenuItem
        title={translations.t('FEEDBACK')}
        rightArrow
        right={
          <View style={styles.leftContainer}>
            <Text style={[styles.text, styles.leftText]}>{isIOS ? 'App Store' : 'Play Store'}</Text>
          </View>
        }
        onPress={() => {
          if (isIOS) Linking.openURL(`itms-apps://${APP_STORE_LINK}&action=write-review`).catch(() => {});
          if (isAndroid) Linking.openURL(`https://${PLAY_MARKET_LINK}`).catch(() => {});
        }}
      />

      {/* PRIVACY POLICY */}
      <MenuItem
        title={translations.t('PRIVACY_POLICY')}
        onPress={() => navigate(Scenes.PrivacyPolicy)}
        rightArrow
      />

      {/* TERMS OF SERVICE */}
      <MenuItem
        title={translations.t('T_O_S')}
        onPress={() => navigate(Scenes.TermsOfService)}
        rightArrow
      />

      {/* LOG OUT */}
      <View style={{ marginTop: scaleByVertical(44) }}>
        <MenuItem
          title={translations.t('LOG_OUT')}
          onPress={props.logOut}
          rightArrow
        />
      </View>

    </BackgroundWrapper>
  );
}

Settings.navigationOptions = () => ({
  title: translations.t('SETTINGS')
});

const mapStateToProps = ({ session: { default_currency }, pushNotifications }) => ({ default_currency, pushNotifications });
const mapDispatchToProps = dispatch => bindActionCreators({
  logOut: ActionCreators.logOut,
  switchNotifications: ActionCreators.switchNotifications
}, dispatch);

export const SettingsConnected = connect(mapStateToProps, mapDispatchToProps)(Settings);
