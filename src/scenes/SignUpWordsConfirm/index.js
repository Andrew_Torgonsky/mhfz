import React from 'react';
import { View, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './styles';
import globalStyles from '../../global/styles';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import { scale, scaleByVertical } from '../../utils/scale';
import translations from '../../translations';
import { ActionCreators } from '../../redux/actions';
import FilledButton from '../../components/FilledButton';
import TwelveWordsInput from '../../components/TwelveWordsInput';
import WordGrid from '../../components/WordGrid';

type Props = {
  navigation: Object,
  twelveWords: string[],
  twelveWordsRandom: string[],
  pickRandomWord: (word: string) => void,
  compareSignUpTwelveWords: () => void,
  clearTwelveWords: () => void,
  changeAlert: (text: string, type: string) => void
}

export default function SingUpWordsConfirm(props: Props) {
  const {
    navigation, twelveWords, twelveWordsRandom, pickRandomWord, clearTwelveWords, compareSignUpTwelveWords, changeAlert
  } = props;
  function chooseWord(word) {
    pickRandomWord(word);
    changeAlert('', '');
  }
  return (
    <BackgroundWrapper noScroll>
      <View style={styles.container}>
        <View>
          <View style={styles.textContainer}>
            <Text style={globalStyles.normalText}>{translations.t('LETS_CHECK_12_WORDS')}</Text>
          </View>
          <View style={styles.inputContainer}>
            <TwelveWordsInput
              editable={false}
              value={twelveWords}
              onClear={clearTwelveWords}
            />
          </View>
          {twelveWords.length < 12 ?
            <View style={styles.wordGridContainer}>
              <WordGrid words={twelveWordsRandom} onPress={chooseWord} />
            </View> : null
          }
        </View>
        {twelveWords.length === 12 ?
          <View style={styles.confirmContainer}>
            <FilledButton
              title={translations.t('CONFIRM')}
              onPress={compareSignUpTwelveWords}
            />
          </View> : null
        }
      </View>
    </BackgroundWrapper>
  );
}

SingUpWordsConfirm.navigationOptions = () => ({
  title: translations.t('TW_WORDS_SEC')
});


const mapStateToProps = ({
  form: {
    twelveWords,
    twelveWordsRandom
  }
}) => (
    {
      twelveWords,
      twelveWordsRandom
    }
  );

const mapDispatchToProps = dispatch => bindActionCreators({
  pickRandomWord: ActionCreators.pickRandomWord,
  compareSignUpTwelveWords: ActionCreators.compareSignUpTwelveWords,
  clearTwelveWords: ActionCreators.clearTwelveWords,
  changeAlert: ActionCreators.changeAlert
}, dispatch);

export const SingUpWordsConfirmConnected = connect(mapStateToProps, mapDispatchToProps)(SingUpWordsConfirm);
