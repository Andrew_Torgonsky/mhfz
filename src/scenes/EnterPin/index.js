// @flow

import React from 'react';
import { View, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './styles';
import globalStyles from '../../global/styles';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import translations from '../../translations';
import { ActionCreators } from '../../redux/actions';
import TextButton from '../../components/TextButton';
import PinInput from '../../components/PinInput';
import type { ReduxStateType } from '../../types';

type Props = {
  pinCode: string,
  changeForm: (key: string, value: any) => void,
  pinCodeError: boolean,
  checkPinCode: () => void,
  logOut: () => void
}

export class EnterPin extends React.PureComponent<Props> {
  render() {
    const {
      pinCode, changeForm, pinCodeError, checkPinCode, logOut
    } = this.props;
    return (
      <BackgroundWrapper>
        <View style={styles.container}>
          <View style={styles.textContainer}>
            <Text style={globalStyles.normalText}>{translations.t('ENTER_PIN')}</Text>
          </View>
          <View style={styles.pinInputContainer}>
            <PinInput
              value={pinCode}
              onChangeText={val => changeForm('pinCode', val)}
              onFinish={checkPinCode}
              error={pinCodeError}
            />
          </View>
          <View style={styles.logOutContainer}>
            <TextButton
              title={translations.t('LOG_OUT')}
              onPress={logOut}
            />
          </View>
        </View>
      </BackgroundWrapper>
    );
  }
}

const mapStateToProps = ({
  form: {
    pinCode,
    pinCodeError
  }
}: ReduxStateType) => (
  {
    pinCode,
    pinCodeError
  }
);

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  checkPinCode: ActionCreators.checkPinCode,
  logOut: ActionCreators.logOut
}, dispatch);

export const EnterPinConnected = connect(mapStateToProps, mapDispatchToProps)(EnterPin);
