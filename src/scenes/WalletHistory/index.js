// @flow

import React from 'react';
import Moment from 'moment';
import { connect } from 'react-redux';
import { View, Text, TouchableOpacity, ActivityIndicator } from 'react-native';
import { bindActionCreators } from 'redux';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import styles from './styles';
import { truncateString } from '../../utils/other';
import { scale, scaleByVertical } from '../../utils/scale';
import type { CryptoCurrencyType, ContactType, ReduxStateType, WalletsStatType } from '../../types';
import { Scenes } from '../../navigation/shared';
import { ActionCreators } from '../../redux/actions';
import TextButton from '../../components/TextButton';
import { colors } from '../../global';
import translations from '../../translations';

type Props = {
  navigation: Object,
  currency: CryptoCurrencyType,
  type: 'all' | 'received' | 'sent',
  walletsStat: WalletsStatType,
  contacts: ContactType[],
  updateHistory: (currency: CryptoCurrencyType, loadMore: boolean) => void,
  checkTxHistory: (currency: CryptoCurrencyType) => void
}

class WalletHistory extends React.PureComponent<Props> {
  componentWillUnmount() {
    if (this.props.type !== 'all') return;
    this.props.checkTxHistory(this.props.currency);
  }
  render() {
    const { transactions, receivingTransactions, endOfTransactionList, lastChecked } = this.props.walletsStat[this.props.currency];
    let transactionsToShow;
    switch (this.props.type) {
      case 'received':
        transactionsToShow = transactions.filter(t => t.amount > 0);
        break;
      case 'sent':
        transactionsToShow = transactions.filter(t => t.amount < 0);
        break;
      default:
        transactionsToShow = transactions;
    }
    return (
      <BackgroundWrapper
        noGradient
        allwaysScroll
        onScroll={(e) => {
          if (endOfTransactionList) return;
          const viewHeight = e.nativeEvent.layoutMeasurement.height;
          const contentHeight = e.nativeEvent.contentSize.height;
          const offset = e.nativeEvent.contentOffset.y;
          if (offset + viewHeight >= contentHeight - 500) this.props.updateHistory(this.props.currency, true);
        }}
      >
        <View style={styles.container}>
          {transactionsToShow.map((transaction) => {
            const contactToShow: ContactType | void = this.props.contacts.find(contact => contact.wallets.reduce((acc, cur) => cur.walletAddress === transaction.address || acc, false));
            return (
              <TouchableOpacity
                key={transaction.id}
                onPress={() => this.props.navigation.navigate(Scenes.Transaction, {
                  txid: transaction.id,
                  currency: this.props.currency
                })}
              >
                <View
                  style={[styles.transactionContainer, (lastChecked > transaction.timestamp || transaction.isSeen) && { opacity: 0.5 } ]}
                >
                  <View style={styles.row}>
                    <Text style={styles.name}>{truncateString(contactToShow ? `${contactToShow.firstName} ${contactToShow.lastName}` : transaction.address, scale(20))}</Text>
                    <Text style={[styles.amount, transaction.amount < 0 && styles.negative]}>{transaction.amount}</Text>
                  </View>
                  <View style={styles.row}>
                    <Text style={styles.status}>{translations.t(transaction.status.toUpperCase())}</Text>
                    <Text style={styles.status}>{Moment(transaction.timestamp).format('DD.MM.YYYY')}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            );
          })}
          <View style={styles.loadingContainer}>
          {
            receivingTransactions ?
              <ActivityIndicator color={colors.sky}/> :
              !endOfTransactionList ?
                <TextButton title={translations.t('LOAD_MORE')} onPress={() => this.props.updateHistory(this.props.currency, true)} /> :
                null
          }
          </View>
        </View>
      </BackgroundWrapper>
    );
  }
};

const mapStateToProps = ({
  walletsStat,
  contacts
}: ReduxStateType) => ({
  walletsStat,
  contacts
});

const mapDispatchToProps = dispatch => bindActionCreators({
  updateHistory: ActionCreators.updateHistory,
  checkTxHistory: ActionCreators.checkTxHistory
}, dispatch);

export const WalletHistoryConnected = connect(mapStateToProps, mapDispatchToProps)(WalletHistory);
