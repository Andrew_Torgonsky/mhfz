import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    marginTop: scaleByVertical(16),
    marginHorizontal: scale(16)
  },
  transactionContainer: {
    backgroundColor: colors.white20,
    height: scaleByVertical(48),
    borderRadius: 2,
    marginBottom: scaleByVertical(4),
    paddingHorizontal: scale(8),
    paddingVertical: scaleByVertical(6),
    justifyContent: 'space-between'
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  name: {
    color: 'white',
    fontSize: scaleByVertical(14),
    ...platformSelect({
      fontFamily: fonts.sfProTextSemibold
    }, {
      fontFamily: fonts.robotoMedium
    })
  },
  amount: {
    color: colors.brightTeal,
    fontSize: scaleByVertical(14),
    ...platformSelect({
      fontFamily: fonts.sfProDisplayRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  negative: {
    color: colors.reddishPink
  },
  status: {
    color: colors.white50,
    fontSize: scaleByVertical(14),
    ...platformSelect({
      fontFamily: fonts.sfProTextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  loadingContainer: {
    height: scaleByVertical(80),
    marginBottom: scaleByVertical(8),
    justifyContent: 'center',
    alignItems: 'center'
  }
});
