import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingHorizontal: scale(16)
  },
  titleContainer: {
    marginTop: scaleByVertical(30),
    alignItems: 'center',
  },
  inputContainer: {
    marginTop: scaleByVertical(40),
    flexDirection: 'row',
    height: scaleByVertical(44)
  },
  inputContainerArabic: {
    marginTop: scaleByVertical(40),
    flexDirection: 'row-reverse',
    height: scaleByVertical(44)
  },
  inputContainer2: {
    marginTop: scaleByVertical(24),
  },
  inputSubContainer1: {
    flex: 1,
    paddingRight: scale(4)
  },
  inputSubContainer1Arabic: {
    flex: 1,
    paddingLeft: scale(4)
  },
  inputSubContainer2: {
    flex: 1
  },
  forgotContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: scaleByVertical(8)
  },
  signInButtonContainer1: {
    height: scaleByVertical(40),
    marginTop: scaleByVertical(24),
    alignItems: 'center'
  },
  signInButtonContainer2: {
    width: scale(180),
    flex: 1
  },
  signUpContainer: {
    marginTop: scaleByVertical(40),
    alignItems: 'center'
  },
  signUpContainerArabic: {
    marginTop: scaleByVertical(40),
    justifyContent: 'center',
    flexDirection: 'row-reverse',
  },
  signUpContainerText: {
    paddingLeft: scale(10)
  }
});
