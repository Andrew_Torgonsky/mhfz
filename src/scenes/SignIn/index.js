// @flow

import React from 'react';
import { View, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './styles';
import globalStyles from '../../global/styles';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import InputField from '../../components/InputField';
import CountryCodeInput from '../../components/CountryCodeInput';
import PasswordInput from '../../components/PasswordInput';
import { scale } from '../../utils/scale';
import translations from '../../translations';
import { ActionCreators } from '../../redux/actions';
import TextButton from '../../components/TextButton';
import FilledButton from '../../components/FilledButton';
import { Scenes } from '../../navigation/shared';
import type { AlertType, ReduxStateType } from '../../types';
import { isRTL } from '../../translations';

type Props = {
  navigation: Object,
  phonePrefix: string,
  countryIso: string,
  phoneNumber: string,
  password: string,
  passwordVisibility: boolean,
  phoneError: boolean,
  passwordError: boolean,
  changeForm: (key: string, value: any) => void,
  submitSignInFields(): void,
  changeAlert: (text: string, type: AlertType) => void
}

export const SignIn = (props: Props) => {
  const { navigation } = props;
  const dismissErrors = () => {
    props.changeForm('phoneError', false);
    props.changeForm('passwordError', false);
    props.changeAlert('', '');
  };
  return (
    <BackgroundWrapper>
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={globalStyles.title1}>
            {translations.t('SIGN_IN')}
          </Text>
        </View>
        <View style={isRTL() ? styles.inputContainerArabic : styles.inputContainer}>
          <View style={isRTL() ? styles.inputSubContainer1Arabic : styles.inputSubContainer1}>
            <CountryCodeInput
              onPress={() => {
                navigation.navigate(Scenes.CountrySelect);
                dismissErrors();
              }}
              phonePrefix={props.phonePrefix}
              countryISO={props.countryIso}
              error={props.phoneError}
            />
          </View>
          <View style={styles.inputSubContainer2}>
            <InputField
              inputIndentaion={scale(16)}
              keyboardType="phone-pad"
              value={props.phoneNumber}
              onChangeText={val => props.changeForm('phoneNumber', val)}
              placeholder={translations.t('P_NUM')}
              error={props.phoneError}
              onFocus={dismissErrors}
            />
          </View>
        </View>
        <View style={[styles.inputContainer, styles.inputContainer2]}>
          <PasswordInput
            visible={props.passwordVisibility}
            value={props.password}
            onChangeText={val => props.changeForm('password', val)}
            onEyePress={() => props.changeForm('passwordVisibility', !props.passwordVisibility)}
            error={props.passwordError}
            onFocus={dismissErrors}
          />
        </View>
        <View style={styles.forgotContainer}>
          <TextButton
            title={translations.t('FORGOT_PWD')}
            onPress={() => navigation.navigate(Scenes.ForgotPasswordPhone)}
          />
        </View>
        <View style={styles.signInButtonContainer1}>
          <View style={styles.signInButtonContainer2}>
            <FilledButton
              title={translations.t('SIGN_IN')}
              onPress={props.submitSignInFields}
            />
          </View>
        </View>
        <View style={isRTL() ? styles.signUpContainerArabic : styles.signUpContainer}>
          <Text style={[
            globalStyles.greyText,
            styles.signUpContainerText
          ]}>
            {translations.t('NO_ACC_YET')}
          </Text>
          <TextButton
            style={{ marginLeft: 10 }}
            title={translations.t('SIGN_UP')}
            onPress={() => navigation.navigate(Scenes.SignUp)}
          />
        </View>
      </View>
    </BackgroundWrapper>
  );
};

SignIn.navigationOptions = {
  headerStyle: globalStyles.transparentHeader
};

const mapStateToProps = ({
  form: {
    phonePrefix, countryIso, phoneNumber, password, passwordVisibility, phoneError, passwordError
  }
}: ReduxStateType) => ({
  phonePrefix,
  countryIso,
  phoneNumber,
  password,
  passwordVisibility,
  phoneError,
  passwordError
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  changeAlert: ActionCreators.changeAlert,
  submitSignInFields: ActionCreators.submitSignInFields,
}, dispatch);

export const SignInConnected = connect(mapStateToProps, mapDispatchToProps)(SignIn);
