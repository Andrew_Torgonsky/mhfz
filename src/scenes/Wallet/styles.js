import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  coinIcon: {
    position: 'absolute',
    right: 0,
    top: scaleByVertical(7),
    opacity: 0.1,
    width: scale(140),
    height: scale(140)
  },
  container1: {
    marginTop: scaleByVertical(24),
    marginHorizontal: scale(16),
    flexDirection: 'row'
  },
  title1: {
    fontSize: scaleByVertical(14),
    color: colors.coolGrey,
    ...platformSelect({
      fontFamily: fonts.sfProTextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  container2: {
    marginTop: scaleByVertical(11),
    flexDirection: 'row',
    marginHorizontal: scale(16)
  },
  title2: {
    fontSize: scaleByVertical(24),
    color: 'white',
    ...platformSelect({
      fontFamily: fonts.sfProTextBold,
    }, {
      fontFamily: fonts.robotoBold
    })
  },
  title3: {
    marginLeft: scale(3),
    fontSize: scaleByVertical(24),
    color: 'white',
    ...platformSelect({
      fontFamily: fonts.sfProTextLight,
    }, {
      fontFamily: fonts.robotoLight
    })
  },
  container3: {
    marginTop: scaleByVertical(8),
    flexDirection: 'row',
    marginHorizontal: scale(16)
  },
  title4: {
    fontSize: scaleByVertical(14),
    color: 'white',
    ...platformSelect({
      fontFamily: fonts.sfProTextBold,
    }, {
      fontFamily: fonts.robotoBold
    })
  },
  title5: {
    marginLeft: scale(3),
    fontSize: scaleByVertical(14),
    color: 'white',
    ...platformSelect({
      fontFamily: fonts.sfProTextLight,
    }, {
      fontFamily: fonts.robotoLight
    })
  },
  container4: {
    marginTop: scaleByVertical(10),
    flexDirection: 'row',
    marginHorizontal: scale(16)
  },
  containerSend: {
    height: scaleByVertical(44),
    marginTop: scale(35),
    marginHorizontal: scale(16)
  },
  changeLabel: {
    marginRight: scale(3),
    fontSize: scaleByVertical(14),
    color: colors.white38,
    ...platformSelect({
      fontFamily: fonts.sfProTextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  change: {
    fontSize: scaleByVertical(14),
    color: colors.aquamarine,
    ...platformSelect({
      fontFamily: fonts.sfProTextBold
    }, {
      fontFamily: fonts.robotoBold
    })
  },
  changeNegative: {
    color: colors.reddishPink
  },
  containerHistoryButton: {
    marginTop: scaleByVertical(48)
  },
  noInfo: {
    fontSize: scaleByVertical(14),
    color: colors.white38,
    ...platformSelect({
      fontFamily: fonts.sfProTextBold
    }, {
      fontFamily: fonts.robotoBold
    })
  },
  alertIcon: {
    width: scale(120),
    height: scale(120)
  },
  alertText: {
    color: 'white',
    marginTop: scaleByVertical(24),
    fontSize: scaleByVertical(17),
    textAlign: 'center',
    ...platformSelect({
      fontFamily: fonts.sfProTextSemibold
    }, {
      fontFamily: fonts.robotoMedium
    })
  },
  alertText2: {
    color: colors.white87,
    marginTop: scaleByVertical(16),
    fontSize: scaleByVertical(16),
    lineHeight: scaleByVertical(24),
    textAlign: 'center',
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  alertContainer: {
    alignItems: 'center',
    marginTop: scaleByVertical(129)
  }
});
