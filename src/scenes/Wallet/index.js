// @flow

import React from 'react';
import { Subscription } from 'rxjs';
import { View, Text, Image, Clipboard } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import translations from '../../translations';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import FilledButton from '../../components/FilledButton';
import MenuItem from '../../components/MenuItem';
import Chart from '../../components/Chart';
import styles from './styles';
import { ActionCreators } from '../../redux/actions';
import { switchAbbrAndName, inFiat, getDailyChange } from '../../utils/currencies';
import { getHistoryForChart } from '../../utils/chart';
import { Scenes } from '../../navigation';
import { onSharePressWithCurrency$ } from '../../navigation/shared';
import type { CryptoCurrencyType, WalletsStatType, FiatCurrencyType, WalletsType, AlertType } from '../../types';
import ShareWalletModal from '../../components/ShareWalletModal';
import images from '../../global/images';
import colors from '../../global/colors';


type Props = {
  navigation: Object,
  currency: CryptoCurrencyType,
  balancePeriod: 0 | 1 | 2,
  walletsStat: WalletsStatType,
  changeForm: (key: string, value: any) => void,
  default_currency: FiatCurrencyType,
  wallets: WalletsType,
  changeAlert: (text: string, type: AlertType) => void
}

type State = {
  shareModalOpen: boolean
}

class Wallet extends React.PureComponent<Props, State> {
  state = {
    shareModalOpen: false
  }
  subscription: Subscription;
  componentWillMount() {
    this.subscription = onSharePressWithCurrency$.subscribe((activeCurrency) => {
      if (this.props.currency !== activeCurrency) return;
      this.toggleShareModal();
    });
  }
  componentWillUnmount() {
    this.subscription.unsubscribe();
  }
  toggleShareModal = () => {
    this.setState({
      shareModalOpen: !this.state.shareModalOpen
    });
  }
  onAddressPress = () => {
    Clipboard.setString(this.props.wallets[this.props.currency].walletAddress);
    console.log(this.props.wallets[this.props.currency].walletAddress);
    this.props.changeAlert(translations.t('COPIED'), 'green');
  }
  render() {
    const {
      balancePeriod, default_currency, walletsStat, currency, navigation, wallets, changeForm
    } = this.props;
    const walletStat = walletsStat[currency];
    const dailyChange = getDailyChange(walletStat);
    const history = getHistoryForChart(walletStat, balancePeriod);
    let historyScreenName;
    switch (currency) {
      case 'bitcoin':
        historyScreenName = 'BtcHistoryAll';
        break;
      case 'ethereum':
        historyScreenName = 'EthHistoryAll';
        break;
      case 'ripple':
        historyScreenName = 'XrpHistoryAll';
        break;
      default:
        historyScreenName = '';
    }
    return (
      <BackgroundWrapper noGradient>
        {
          !walletStat.alertText ?
            <View>
              <Image
                source={images[currency]}
                style={styles.coinIcon}
              />
              <View style={styles.container1}>
                <Text style={styles.title1}>{translations.t('BALANCE')}</Text>
              </View>
              <View style={styles.container2}>
                <Text style={styles.title2}>{walletStat.balance}</Text>
                <Text style={styles.title3}>{switchAbbrAndName(this.props.currency)}</Text>
              </View>
              <View style={styles.container3}>
                {walletStat.rate ?
                  <React.Fragment>
                    <Text style={styles.title4}>{inFiat(walletStat.balance, walletStat.rate)}</Text>
                    <Text style={styles.title5}>{default_currency.toUpperCase()}</Text>
                  </React.Fragment> :
                  <Text style={styles.noInfo}>{translations.t('NO_RATE')}</Text>
                }
              </View>
              <View style={styles.container4}>
                <Text style={styles.changeLabel}>{translations.t('24_CHANGE')}</Text>
                {
                  dailyChange === Infinity ?
                    <Text style={styles.noInfo}>{translations.t('NO_INFO')}</Text> :
                    <Text style={[styles.change, dailyChange < 0 && styles.changeNegative]}>{dailyChange}%</Text>
                }
              </View>
              <View>
                <Chart data={history} />
              </View>
              <View style={styles.containerSend}>
                <FilledButton
                  red
                  title={translations.t('SEND')}
                  onPress={() => {
                    changeForm('currencyToSend', currency);
                    navigation.navigate(Scenes.SendToWallet);
                  }}
                />
              </View>
              <View style={styles.containerHistoryButton}>
                <MenuItem
                  title={translations.t('TRANS_HISTORY')}
                  rightArrow
                  onPress={() => navigation.navigate(historyScreenName)}
                />
              </View>
            </View> :
            <View style={styles.alertContainer}>
              <Image
                style={[styles.alertIcon, {
                  tintColor: walletStat.alertColor === 'yellow' ? colors.sunshineYellow : colors.reddishPink
                }]}
                source={images.errorBig}
              />
              <Text style={styles.alertText}>{walletStat.alertText}</Text>
              <Text style={styles.alertText2}>{translations.t('DONT_WORRY')}</Text>
            </View>
        }
        {
          this.state.shareModalOpen ?
            <ShareWalletModal
              onClose={this.toggleShareModal}
              currency={currency}
              address={wallets[currency].walletAddress}
              onAddressPress={this.onAddressPress}
            /> :
            null
        }
      </BackgroundWrapper>
    );
  }
}

const mapStateToProps = ({
  session: { default_currency },
  walletsStat,
  wallets,
  balancePeriod
}) => ({
  balancePeriod, default_currency, walletsStat, wallets
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  changeAlert: ActionCreators.changeAlert
}, dispatch);

export const WalletConnected = connect(mapStateToProps, mapDispatchToProps)(Wallet);
