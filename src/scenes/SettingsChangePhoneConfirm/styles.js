import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  textContainer: {
    marginTop: scaleByVertical(20),
    paddingHorizontal: scale(20)
  },
  text: {
    fontSize: scaleByVertical(16),
    lineHeight: scaleByVertical(24),
    textAlign: 'center',
    color: colors.white87
  },
  phoneContainer: {
    flexDirection: 'row',
    marginTop: scaleByVertical(48),
    paddingHorizontal: scale(16),
    height: scaleByVertical(44)
  },
  phoneContainerArabic: {
    flexDirection: 'row-reverse',
    marginTop: scaleByVertical(48),
    paddingHorizontal: scale(16),
    height: scaleByVertical(44)
  },
  inputCountryContainer: {
    flex: 1,
    paddingRight: scale(4)
  },
  inputCountryContainerArabic: {
    flex: 1,
    paddingLeft: scale(4)
  },
  inputNumberContainer: {
    flex: 1
  },
  inputNumber: {
    paddingLeft: scale(16)
  },
  buttonContainer1: {
    marginTop: scaleByVertical(40),
    alignItems: 'center'
  },
  buttonContainer2: {
    width: scale(180),
    height: scaleByVertical(42)
  },
  codeContainer: {
    marginTop: scaleByVertical(24)
  },
  bottomText: {
    marginTop: scaleByVertical(10),
  },
  retrySending: {
    marginLeft: scale(5),
    transform: [
      {
        translateY: scaleByVertical(1)
      }
    ]
  }
});
