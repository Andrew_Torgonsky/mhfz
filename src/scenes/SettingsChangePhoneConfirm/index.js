// @flow

import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View, Text } from 'react-native';
import translations from '../../translations';
import { ActionCreators } from '../../redux/actions';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import FilledButton from '../../components/FilledButton';
import CountryCodeInput from '../../components/CountryCodeInput';
import InputField from '../../components/InputField';
import SmsCode from '../../components/SmsCode';
import globalStyles from '../../global/styles';
import styles from './styles';
import type { AlertType, ReduxStateType } from '../../types';
import RetrySendButton from '../../components/RetrySendButton';

type Props = {
  navigation: Object,
  phonePrefix: string,
  countryIso: string,
  phoneNumber: string,
  phoneError: boolean,
  smsCode: string,
  smsCodeError: boolean,
  smsCodeTimestamp: number,
  changeForm(key: string, value: any): void,
  changeAppState(key: string, value: any): void,
  changeSession(key: string, value: any): void,
  changeAlert(text: string, type: AlertType): void,
  changePhone(): void,
  enterPhoneSms(): void
};

export class SettingsChangePhoneConfirm extends React.PureComponent<Props> {
  static navigationOptions: Object;
  componentDidMount() {
    this.props.changeAppState('preventMinimize', true);
  }
  componentWillUnmount() {
    this.props.changeAppState('preventMinimize', false);
  }
  render() {
    const {
      phonePrefix,
      countryIso,
      phoneError,
      phoneNumber,
      navigation,
      changePhone,
      enterPhoneSms,
      smsCode,
      smsCodeError,
      smsCodeTimestamp,
    } = this.props;
  
    const dismissErrors = () => {
      this.props.changeForm('phoneError', false);
      this.props.changeAlert('', '');
    };
  
    return (
      <BackgroundWrapper>
        <View style={styles.textContainer}>
          <Text style={styles.text}>
            {translations.t('ENTER_SMS_CODE')}
          </Text>
        </View>
        <View style={styles.codeContainer}>
          <SmsCode
            onChangeText={(val) => {
              this.props.changeForm('smsCode', val);
              this.props.changeForm('smsCodeError', false);
              this.props.changeAlert('', '');
            }}
            onFinish={enterPhoneSms}
            value={smsCode}
            error={smsCodeError}
          />
        </View>
        <View style={styles.bottomText}>
          <RetrySendButton onPress={changePhone} timestamp={smsCodeTimestamp} />
        </View>
      </BackgroundWrapper>
    );
  }
}

SettingsChangePhoneConfirm.navigationOptions = () => ({
  title: translations.t('CHANGE_PH_NUM')
});


const mapStateToProps = ({
  form: {
    phonePrefix,
    countryIso,
    phoneNumber,
    password,
    passwordVisibility,
    phoneError,
    passwordError,
    smsCode,
    smsCodeError,
    smsCodeTimestamp
  }
}: ReduxStateType) => (
    {
      phonePrefix,
      countryIso,
      phoneNumber,
      phoneError,
      smsCode,
      smsCodeError,
      smsCodeTimestamp
    }
  );

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  changeAppState: ActionCreators.changeAppState,
  changeAlert: ActionCreators.changeAlert,
  changePhone: ActionCreators.changePhone,
  enterPhoneSms: ActionCreators.enterPhoneSms,
  changeSession: ActionCreators.changeSession
}, dispatch);

export const SettingsChangePhoneConfirmConnected = connect(mapStateToProps, mapDispatchToProps)(SettingsChangePhoneConfirm);
