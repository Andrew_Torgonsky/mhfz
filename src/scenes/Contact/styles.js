import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  avatarContainer: {
    marginTop: scaleByVertical(24),
    alignItems: 'center'
  },
  infoContainer: {
    marginTop: scaleByVertical(24),
  },
  info: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: scale(16),
    paddingVertical: scaleByVertical(13),
    borderBottomWidth: scaleByVertical(1),
    borderBottomColor: colors.white20
  },
  infoTitle: {
    color: colors.white50,
    fontSize: scaleByVertical(16)
  },
  infoValue: {
    color: colors.white87,
    fontSize: scaleByVertical(16)
  },
  walletsContainer: {
    marginTop: scaleByVertical(24)
  },
  titleContainer: {
    paddingHorizontal: scale(16)
  },
  title: {
    fontSize: scaleByVertical(14),
    color: colors.white60
  },
  walletContainer: {
    marginTop: scaleByVertical(8),
    paddingHorizontal: scale(8)
  }
});
