import React from 'react';
import { connect } from 'react-redux';
import { Text, View, Image, ScrollView } from 'react-native';
import { bindActionCreators } from 'redux';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import Avatar from '../../components/Avatar';
import ContactWallet from '../../components/ContactWallet';
import translations from '../../translations';
import styles from './styles';
import { ActionCreators } from '../../redux/actions';
import { Scenes } from '../../navigation/shared';

type Props = {
  navigation: Object,
  changeFormMultiple: (x: Object) => void,
  prepareToSend: () => void
};

export const Contact = (props: Props) => {
  const {
    emails,
    phoneNumbers,
    firstName,
    lastName,
    wallets,
    photo
  } = props.navigation.state.params;

  return (
    <BackgroundWrapper>
      <ScrollView>
        <View style={styles.avatarContainer}>
          <Avatar
            source={photo}
            text={!photo && (`${firstName[0] || ''}${lastName[0] || ''}` || '#')}
          />
        </View>

        <View style={styles.infoContainer}>
          <View style={styles.info}>
            <Text style={styles.infoTitle}>{translations.t('NAME')}</Text>
            <Text style={styles.infoValue}>{`${firstName} ${lastName}`}</Text>
          </View>
          {
            emails.map(email => (
              <View style={styles.info} key={email}>
                <Text style={styles.infoTitle}>{translations.t('E_MAIL')}</Text>
                <Text style={styles.infoValue}>{email}</Text>
              </View>
            ))
          }
          {
            phoneNumbers.map(number => (
              <View style={styles.info} key={number}>
                <Text style={styles.infoTitle}>{translations.t('PHONE')}</Text>
                <Text style={styles.infoValue}>{number}</Text>
              </View>
            ))
          }
        </View>

        {
          !wallets.length ?
            null :
            <View style={styles.walletsContainer}>
              <View style={styles.titleContainer}>
                <Text style={styles.title}>{translations.t('CONTACT_WALLETS')}</Text>
              </View>

              <View style={styles.walletContainer}>
                {
                  wallets.map((wallet, i) => (
                    <ContactWallet
                      key={i}
                      btc={wallet.currency === 'bitcoin'}
                      eth={wallet.currency === 'ethereum'}
                      xrp={wallet.currency === 'ripple'}
                      address={wallet.walletAddress}
                      onPress={() => {
                        props.changeFormMultiple({
                          contactToSend: props.navigation.state.params,
                          addressToSend: wallet.walletAddress,
                          currencyToSend: wallet.currency
                        });
                        props.prepareToSend();
                        props.navigation.navigate(Scenes.SendFromContact);
                      }}
                    />
                  ))
                }
              </View>
            </View>
        }
      </ScrollView>
    </BackgroundWrapper>
  );
};

Contact.navigationOptions = ({ navigation }) => ({
  title: `${navigation.state.params.firstName} ${navigation.state.params.lastName}`
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changeFormMultiple: ActionCreators.changeFormMultiple,
  prepareToSend: ActionCreators.prepareToSend
}, dispatch);

export const ContactConnected = connect(null, mapDispatchToProps)(Contact);
