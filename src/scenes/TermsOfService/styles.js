import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    margin: scale(16)
  },
  text: {
    fontSize: scaleByVertical(14),
    lineHeight: scaleByVertical(20),
    color: colors.white50,
    textAlign: 'left'
  },
  arabic: {
    textAlign: 'right'
  },
  mtop: {
    marginTop: scaleByVertical(20)
  },
  bold: {
    fontWeight: 'bold',
    color: colors.white60
  },
  center: {
    textAlign: 'center'
  }
});
