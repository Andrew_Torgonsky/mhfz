import React from 'react';
import { View, Text } from 'react-native';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import translations, { isRTL } from '../../translations';
import styles from './styles';


type Props = {
  navigation: Object
}

const T = ({ children, mtop, li, center }) => <Text style={[styles.text, isRTL() && styles.arabic, mtop && styles.mtop, center && styles.center]}>{li ? <Text>&bull; </Text> : null}{children}</Text>
const B = ({ children }) => <Text style={styles.bold}>{children}</Text>

export function TermsOfService(props: Props) {
  return (
    <BackgroundWrapper>
      <View style={styles.container}>
        <T mtop>
          The owner of this website and mobile App is Cryptocurrency Solutions OÜ.
        </T>
        <T mtop>
          These <B>Terms and Conditions</B> (Terms) govern the terms of access and use of the Website, App and its
          content by User and constitutes the legal agreement between the User and Cryptocurrency Solutions
          OÜ.
        </T>
        <T mtop>
          You should read these Terms carefully before you use the Website and mobile App.
        </T>
        <T mtop>
          BY DOWNLOADING OR USING THE APP AND/OR WEBSITE YOU AGREE TO THESE TERMS.
        </T>
        <T mtop>
          <B>1. Definitions</B>
        </T>
        <T mtop>
          <B>“Terms”</B> means the Cryptocurrency Solutions OÜ Terms of Use, including Schedule A ‘Important
          Notice’.
        </T>
        <T>
          <B>“We”, “us”</B> means the company that makes the Website available to Users: Cryptocurrency Solutions
          OÜ.
        </T>
        <T>
          <B>“User”, “you”</B> means a person using the Website or mobile App.
        </T>
        <T>
          <B>“Website”</B> means the website accessible at Mahfazah.io, including its subdomains, programming
          code, related technologies, know-hows, databases and design.
        </T>
        <T>
          <B>“App”</B> means the Application accessible at App Store and Google Play, including its subdomains,
          programming code, related technologies, know-hows, databases and design.
        </T>
        <T>
          <B>“Content”</B> means texts, design, data, video content or other intellectual property, content or
          information, made available to you by us.
        </T>
        <T>
          <B>“Digital Asset(s)”</B> means virtual currencies, tokens and cryptocurrencies made available for Users by
          Cryptocurrency Solutions OÜ.
        </T>
        <T>
          <B>“The Service”</B> means the software program, created by Cryptocurrency Solutions OÜ, which enables
          Users to access and transfer Digital Assets (also “the Wallet”).
        </T>
        <T mtop>
          <B>2. General</B>
        </T>
        <T mtop>
          <B>Choice of Law.</B> You agree that the laws of the Republic of Estonia govern these Terms, their subject
          matter, the relationship between you and us, any action related these Terms, and any claim or
          dispute it may arise, without regard to the conflict of laws rules.
        </T>
        <T mtop>
          <B>The Service.</B> You should only use Cryptocurrency Solutions OÜ Software if you are familiar with
          Digital Assets. Cryptocurrency Solutions OÜ highly recommends learning the basics for the Digital
          Assets that You plan on using with Cryptocurrency Solutions OÜ Software before running
          Cryptocurrency Solutions OÜ Software.
        </T>
        <T mtop>
          <B>Fees.</B> Cryptocurrency Solutions OÜ Software is licensed to you without charge. This agreement
          Applies to any technical support provided to You in relation to Cryptocurrency Solutions OÜ
          Software.
        </T>
        <T mtop>
          There may be transaction fees (e.g. mining fees) associated with Digital Asset transactions that are
          required by the system You engage with. Cryptocurrency Solutions OÜ shall not be responsible for
          any losses You incur due to transaction fees or losses that occur due to incorrectly set transaction
          fees (i.e. too low or high).
        </T>
        <T mtop>
          <B>Notices.</B> Any notices or other communications permitted or required under these Terms will be in
          writing and given by us:
        </T>
        <T mtop li>
          Via email (to the address that you provide during registration), or
        </T>
        <T li>
          By posting to the Website or App, including on your account dashboard.
        </T>
        <T mtop>
          <B>Transactions.</B> The only authentic record of Digital Assets transactions is the Applicable blockchain.
          Cryptocurrency Solutions OÜ Software provides functionality that allows You to send Digital Asset
          transfer instructions. Cryptocurrency Solutions OÜ does not guarantee that transactions You perform
          using Cryptocurrency Solutions OÜ Software shall be stored in any blockchain.
        </T>
        <T mtop>
          You must ensure that Your transactions conform to the Applicable rules of the software for the
          Digital Asset (especially with smart contract systems like Ethereum).
        </T>
        <T mtop>
          <B>3. User Account Security</B>
        </T>
        <T mtop>
          When you register, you will be asked to choose a username and a password. You are responsible for
          safeguarding your password and you agree not to disclose your password to any third party.
        </T>
        <T mtop>
          You agree that you will be solely responsible for any activities or actions taken under your username,
          whether or not you have authorized such activities or actions.
        </T>
        <T mtop>
          You must provide a valid email address and mobile number in order to complete the signup process.
          Any other information requested, such as your real name, is optional, unless you are accepting these
          terms on behalf of a legal entity (in which case we need more information about the legal entity) or if
          you opt for paid account, in which case additional information will be necessary for billing purposes.
        </T>
        <T mtop>
          You are responsible for keeping your account secure while you use our Service. We offer tools such
          as two-factor authentication to help you maintain your account&#39;s security, but the content of your
          account and its security are up to you.
        </T>
        <T mtop li>
          You are responsible for all content posted and activity that occurs under your account (even
          when content is posted by others who have accounts under your account).
        </T>
        <T li>
          You are responsible for maintaining the security of your account and password.
          Cryptocurrency Solutions OÜ cannot and will not be liable for any loss or damage from your
          failure to comply with this security obligation.
        </T>
        <T li>
          You will promptly notify Cryptocurrency Solutions OÜ if you become aware of any
          unauthorized use of, or access to, our Service through your account, including any
          unauthorized use of your password or account.
        </T>
        <T mtop>
          <B>4. User Restrictions and Obligations</B>
        </T>
        <T mtop>
          You agree that you will not do any of the following while using or accessing the Website and App:
        </T>
        <T li mtop>
          Impersonate or misrepresent your affiliation with any person or entity or otherwise commit
          fraud.
        </T>
        <T li>
          Reproduce, modify, adapt, prepare derivative works based on, perform, display, publish,
          distribute, transmit, broadcast, sell, license or otherwise exploit the Website and Content.
        </T>
        <T li>
          Use the Website or Content in a commercial manner.
        </T>
        <T li>
          Circumvent, disable or otherwise interfere with security related features of the Website, or
          features that prevent or restrict use or copying of any Content.
        </T>
        <T li>
          Forge any TCP/IP packet header or any part of the header information in any posting or in
          any way use the Website to send altered, deceptive or false source-identifying information.
        </T>
        <T li>
          Interfere with or disrupt (or attempt to interfere with or disrupt) any web pages available on
          the Website, servers or networks connected to the Website, or the technical delivery
          systems of our providers or disobey any requirements, procedures, policies or regulations of
          networks connected to the Website.
        </T>
        <T li>
          Attempt to probe, scan or test the vulnerability of any our system or network or breach or
          impair or circumvent any security or authentication measures protecting the Website and
          Content.
        </T>
        <T li>
          Attempt to decipher, decompile, disassemble or reverse engineer any of the software used
          to provide the Website.
        </T>
        <T li>
          Attempt to scrape, parse, access, search or meta-search the Website or Content with any
          engine, software, tool, agent, device or mechanism other than software and/or search
          agents provided by us or other generally available third party web browsers.
        </T>
        <T li>
          Use the Website or Content in any manner not permitted by these Terms.
        </T>
        <T li>
          Encourage or instruct any other individual to do any of the foregoing or to violate these
          Terms.
        </T>
        <T mtop>
          We have a few simple rules for accounts on Cryptocurrency Solutions OÜ&#39;s Service.
        </T>
        <T mtop li>
          You must be a human to create an account. Accounts registered by &quot;bots&quot; or other
          automated methods are not permitted. We do permit machine accounts:
        </T>
        <T li>
          A machine account is an account set up by an individual human who accepts the Terms on
          behalf of the account, provides a valid email address, and is responsible for its actions. A
          machine account is used exclusively for performing automated tasks. Multiple users may
          direct the actions of a machine account, but the owner of the account is ultimately
          responsible for the machine&#39;s actions. You may maintain no more than one free machine
          account in addition to your free personal account.
        </T>
        <T li>
          One person or legal entity may maintain no more than one free account (if you choose to
          control a machine account as well, that&#39;s fine, but it can only be used for running a machine).
        </T>
        <T li>
          You must be age 13 or older. While we are thrilled to see brilliant young coders get excited
          by learning to program, we must comply with United States law. Cryptocurrency Solutions
          OÜ does not target our Service to children under 13, and we do not permit any Users under
          13 on our Service. If your country’s minimum age may be older; in such a case, you are
          responsible for complying with your country’s laws.
        </T>
        <T li>
          Your login may only be used by one person — i.e., a single login may not be shared by
          multiple people. A paid organization account may create separate logins for as many users as
          its subscription allows.
        </T>
        <T li>
          Overall, the number of Users must not exceed the number of accounts you&#39;ve ordered from
          us.
        </T>
        <T mtop>
          <B>5. Intellectual Property Rights</B>
        </T>
        <T mtop>
          <B>Our Intellectual Property Rights.</B> Cryptocurrency Solutions OÜ owns all right, title and interest,
          including all worldwide intellectual property rights in the Website, App and Content.
        </T>
        <T mtop>
          <B>License Grant. Permitted Use.</B> Subject to your compliance with these Terms, Cryptocurrency
          Solutions OÜ licenses Cryptocurrency Solutions OÜ Software to You on a personal, worldwide,
          royalty-free, non-assignable and non-exclusive license for the purpose of holding and transferring
          Digital Assets. This license is for the sole purpose of enabling you to use and enjoy the benefits of the
          Website and App in the manner permitted by these Terms.
        </T>
        <T mtop>
          Any other use of the Website and Content requires our prior written consent.
        </T>
        <T mtop>
          The Website, App and Content may be used and accessed for lawful purposes only. You agree to
          abide by all Applicable local, state, national and foreign laws and regulations in connection with your
          use of the Website, App and Content.
        </T>
        <T mtop>
          <B>6. Your Feedback</B>
        </T>
        <T mtop>
          If you choose to communicate to us any feedback, ideas or suggestions for improvement of the
          Website or Content, you grant us free-of-charge, irrevocable, non-exclusive, transferable right to use,
          modify, share, distribute and communicate such feedback for any and all commercial or non-
          commercial purposes, without charge and free of any obligation of attribution. You warrant that any
          such feedback you communicated to us is not subject to any license or any other third party right.
        </T>
        <T mtop>
          <B>7. Disclaimer of Warranties</B>
        </T>
        <T mtop>
          Your use of the Website and any Content or other information made available through the Website is
          at your sole risk and discretion and we hereby disclaim all liability to you or any third party relating
          thereto.
        </T>
        <T mtop>
          The Website and all Content, materials, information, products and services included therein, are
          provided on an “AS IS” and “AS AVAILABLE” basis without warranties of any kind.
        </T>
        <T mtop>
          Cryptocurrency Solutions OÜ is committed to ensuring that the App is as useful and efficient as
          possible. For that reason, we reserve the right to make changes to the App or to charge for its
          services, at any time and for any reason.
        </T>
        <T mtop>
          WE EXPRESSLY DISCLAIM ALL WARRANTIES OF ANY KIND, EXPRESS, IMPLIED OR STATUTORY,
          RELATING TO THE WEBSITE AND CONTENT, INCLUDING WITHOUT LIMITATION THE WARRANTIES OF
          TITLE, MERCHANTABILITY, SUITABILITY FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT OF
          PROPRIETARY RIGHTS, COURSE OF DEALING OR COURSE OF PERFORMANCE.
        </T>
        <T mtop>
          We disclaim any warranties:
        </T>
        <T mtop li>
          Regarding the security, accuracy, reliability, timeliness and performance of the Website or
          App.
        </T>
        <T li>
          That the Website, App or any of Content will be error-free or that any errors will be
          corrected.
        </T>
        <T li>
          That the Website, App or any of Content will be of any particular quality, meet any standards
          or requirements, or conform to any of your expectations in this regard.
        </T>
        <T mtop>
          Some countries or jurisdictions do not allow the exclusion of certain warranties. Accordingly, some of
          the above exclusions may not apply to you.
        </T>
        <T mtop>
          <B>8. Limitation of Liability</B>
        </T>
        <T mtop>
          You should be aware that there are certain things that Cryptocurrency Solutions OÜ will not take
          responsibility for. Certain functions of the App require an active internet connection. The connection
          can be Wi-Fi, or provided by your mobile network provider, but Cryptocurrency Solutions OÜ cannot
          take responsibility for the App not working at full functionality if you don’t have access to Wi-Fi, and
          you don’t have any of your data allowance left.
        </T>
        <T mtop>
          If you’re using the App outside of an area with Wi-Fi, you should remember that the Terms of
          agreement with your mobile network provider will still apply. As a result, you may be charged by
          your mobile provider for the cost of data for the duration of the connection while accessing the App,
          or other third party charges. In using the App, you’re accepting responsibility for any such charges,
          including roaming data charges if you use the App outside of your home territory (i.e. region or
          country) without turning off data roaming. If you are not the bill payer for the device on which you’re
          using the App, please be aware that we assume that you have received permission from the bill
          payer for using the App.
        </T>
        <T mtop>
          IN NO EVENT WILL WE, OUR SHAREHOLDERS, OFFICERS, DIRECTORS, EMPLOYEES, CONTRACTORS OR
          AGENTS, BE LIABLE TO YOU FOR ANY DAMAGES WHATSOEVER, INCLUDING WITHOUT LIMITATION,
          DIRECT, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE OR CONSEQUENTIAL DAMAGES ARISING OUT OF
          OR IN CONNECTION WITH YOUR USE OF THE WEBSITE, APP AND CONTENT WHETHER THE DAMAGES
          ARE FORESEEABLE AND WHETHER OR NOT WE HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH
          DAMAGES. THE FOREGOING LIMITATION OF LIABILITY WILL APPLY TO THE FULLEST EXTENT
          PERMITTED BY LAW IN THE APPLICABLE JURISDICTION.
        </T>
        <T mtop>
          YOUR SOLE REMEDY FOR DISSATISFACTION WITH THE WEBSITE OR CONTENT IS TO STOP USING THE
          WEBSITE OR CONTENT.
        </T>
        <T mtop>
          YOU ACKNOWLEDGE AND AGREE THAT IF YOU INCUR ANY DAMAGES THAT ARISE OUT OF OUR ACTS
          OR OMISSIONS, THE DAMAGES, IF ANY, ARE NOT IRREPARABLE AND ARE NOT SUFFICIENT TO
          ENTITLE YOU TO AN INJUNCTION OR OTHER EQUITABLE RELIEF RESTRICTING OPERATION OF THE
          WEBSITE OR CIRCULATION OF CONTENT.
        </T>
        <T mtop>
          <B>9. Modifications</B>
        </T>
        <T mtop>
          At some point we may wish to update the App. The App is currently available on Android and iOS –
          the requirements for both systems (and for any additional systems we decide to extend the
          availability of the App to) may change, and you’ll need to download the updates if you want to keep
          using the App. Cryptocurrency Solutions OÜ does not promise that it will always update the App so
          that it is relevant to you and/or works with the iOS/Android version that you have installed on your
          device.
        </T>
        <T mtop>
          However, you promise to always accept updates to the Application when offered to you, We may
          also wish to stop providing the App, and may terminate use of it at any time without giving notice of
          termination to you. Unless we tell you otherwise, upon any termination, (a) the rights and licenses
          granted to you in these terms will end; (b) you must stop using the App, and (if needed) delete it
          from your device.
        </T>
        <T mtop>We reserve the right, at our sole discretion:</T>
        <T mtop li>
          To modify or revise these Terms at any time by posting the amended Terms on the Website,
          App or Content. Please check the most current Terms to ensure that you are aware of all the
          terms governing your use of the Website or App.
        </T>
        <T li>
          To make changes, update or discontinue the Website, App, Content or any format, feature or
          functionality thereof at any time with or without notifying you.
        </T>
        <T li>
          To terminate or restrict access to the Website, App or Content for any reason whatsoever.
        </T>
        <T mtop>
          Your continued use of the Website or App after a change or update has been made will constitute
          your acceptance to the revised Terms. If you do not agree with the modifications, please discontinue
          use of the Website or App immediately.
        </T>
        <T mtop>
          These Terms remain effective from the date of acceptance until terminated by you or us in
          accordance with these Terms.
        </T>
        <T mtop>
          <B>10. Termination and Account Cancellation</B>
        </T>
        <T mtop>
          You may terminate these Terms at any time by ceasing to use the Website or App and/or by
          cancelling your account.
        </T>
        <T mtop>
          Without limiting other remedies, we may suspend or terminate these Terms with you, or may
          terminate or suspend your use of the Website or App at any time if:
        </T>
        <T mtop li>
          You violate any provision of these Terms.
        </T>
        <T li>
          You infringe proprietary rights, rights of privacy, or intellectual property rights of any person,
          business or organization.
        </T>
        <T li>
          You engaged in other actions relating to or in the course of using the Website or App that
          may be illegal or cause liability, harm, embarrassment, harassment, abuse or disruption for
          you, other Users, us, any other third parties or for the Website, App or Content.
        </T>
        <T li>
          It is required by applicable law.
        </T>
        <T li>
          We cease offering the services and/or discontinued the Website or App.
        </T>
        <T mtop>
          Notwithstanding the foregoing, we also reserve the right to terminate the Website or App or cancel
          your account at any time and for any reason.
        </T>
        <T mtop>
          Upon termination of these Terms all licenses and rights to use the Website, App and Content shall
          immediately terminate; and you will immediately cease any and all use of the Website, App and
          Content.
        </T>
        <T mtop>
          Upon any termination your account you will no longer be able to access information and documents
          submitted by you. We will have no obligation to maintain any information and documents stored in
          our database related to your account or to forward any information to you or any third party.
        </T>
        <T mtop>
          Any suspension or termination of these Terms will not affect your obligations to us under these
          Terms, including, without limitation, proprietary rights and ownership and limitation of liability,
          which reasonably are intended to survive such suspension or termination.
        </T>
        <T mtop center>
          <B>Schedule A. IMPORTANT NOTICE</B>
        </T>
        <T mtop>
          The Website, the whitepaper, One-Pager, blog posts, presentations, publications of
          Cryptocurrency Solutions OÜ stakeholders, advisers, managers and consultants
          (The <B>Cryptocurrency Solutions OÜ Media’</B>) may utilize, contain or mention information or data in
          relation to Cryptocurrency Solutions OÜ business model, technology, products and services,
          as well as Cryptocurrency Solutions OÜ Website, App and Content (<B>‘Information’</B>).
        </T>
        <T mtop>
          PLEASE READ THIS NOTICE REGARDING Cryptocurrency Solutions OÜ MEDIA AND
          INFORMATION VERY CAREFULLY. IF YOU ARE IN DOUBT AS TO THE ACTION YOU SHOULD
          TAKE IN RELATION TO IT, PLEASE CONSULT YOUR LEGAL, COMMERCIAL, FINANCIAL, TAX OR
          OTHER PROFESSIONAL ADVISORS.
        </T>
        <T mtop center>
          <B>Descriptive nature only</B>
        </T>
        <T mtop>
          Information is of descriptive nature and not binding.
        </T>
        <T mtop>
          Information includes market and industry information and forecasts that have been obtained
          from internal or publicly available surveys, reports and publications. Such sources are
          believed to be reliable, but Cryptocurrency Solutions OÜ provides no assurance or guarantee
          as to the accuracy or completeness of such information and forecasts.
        </T>
        <T mtop>
          Regulatory measures, investigations or actions may impact Cryptocurrency Solutions OÜ
          products and services and limit or prevent them from being developed or implemented.
        </T>
        <T mtop>
          Cryptocurrency Solutions OÜ business model may change due to new regulatory and
          compliance requirements from any applicable laws in any jurisdiction.
        </T>
        <T mtop center>
          <B>Forward-looking statements</B>
        </T>
        <T mtop>
          All Information about future performance of Cryptocurrency Solutions OÜ, its products,
          services, Website, App and Content, including, without limitation, future revenues, earnings,
          strategies, prospects, consequences and all other statements that are not purely historical
          constitute “forward-looking statements”. Such forward-looking statements are subject to
          risks and uncertainties, which could cause actual results to differ materially from those
          anticipated.
        </T>
        <T mtop>
          When used herein, the words “anticipate,” “intend,” “estimate,” “believe,” “expect,” “plan,”
          “should,” “hypothetical,” “Approximately”, “potential,” “forecast,” “project,” “aim”, “target”,
          “could”, “if”, “may”, “possible”, “probable”, “would”, “will”, variations of such words and
          similar expressions or symbols are intended to identify forward-looking statements.
        </T>
        <T mtop>
          All of the forward-looking statements made in Cryptocurrency Solutions OÜ Media are
          qualified by these cautionary statements and Cryptocurrency Solutions OÜ can make no
          assurance that the results or developments of Cryptocurrency Solutions OÜ will be realized
          or even if realized, will have the expected consequences. No representation, warranty,
          undertaking, promise, or guarantee is given in respect of the forward-looking statements.
        </T>
        <T mtop center>
          <B>Not an offer or solicitation</B>
        </T>
        <T mtop>
          Cryptocurrency Solutions OÜ does not offer any securities or assets for investment purposes.
        </T>
        <T mtop>
          None of Cryptocurrency Solutions OÜ Media is intended to be a financial services offering
          document or a prospectus of any kind.
        </T>
        <T mtop>
          Information is not a solicitation for investment and does not pertain in any way to an offering
          of securities, shares, options or futures in any jurisdiction. Cryptocurrency Solutions OÜ
          Media contain a mere description of the functionality of Cryptocurrency Solutions OÜ
          products and services and the utilization of the Cryptocurrency Solutions OÜ Digital Assets
          within the Cryptocurrency Solutions OÜ ecosystem.
        </T>
        <T mtop center>
          <B>Not a recommendation or advice</B>
        </T>
        <T mtop>
          The use of Information does not and cannot guarantee that Users will make profits or will not
          incur losses. Such data and information is intended merely for informational purposes.
        </T>
        <T mtop>
          Cryptocurrency Solutions OÜ Media and Information shall not construed and is not intended
          to supply professional, business, legal, tax, investment or financial advice.
        </T>
        <T mtop>
          Cryptocurrency Solutions OÜ offers no advice regarding the nature, potential value or
          suitability of the Cryptocurrency Solutions OÜ Digital Assets.
        </T>
        <T mtop center>
          <B>Knowledge required; Risks</B>
        </T>
        <T mtop>
          Using Digital Asset-related software necessarily entails many risks. Cryptocurrency Solutions
          OÜ specifically disclaims and shall have no liability to You for the following risks:
        </T>
        <T mtop>
          a) operating system failures (mobile or desktop);
        </T>
        <T mtop>
          b) interactions between Your hardware, software and Cryptocurrency Solutions OÜ Software;
        </T>
        <T mtop>
          c) cloud backup software (e.g. certain Android distributions) may upload your private
          files/information to third party services;
        </T>
        <T mtop>
          d) malware, viruses or other malicious software on Your device that is able to take control of
          or interfere with Cryptocurrency Solutions OÜ Software;
        </T>
        <T mtop>
          e) communication delays between Your Cryptocurrency Solutions OÜ Software and a node or
          relay service for a Digital Asset (and vice versa);
        </T>
        <T mtop>
          f) failure to achieve a certain market value/price for a Digital Asset, whether through a third-
          party service or any other kind of transaction (i.e. due to delays);
        </T>
        <T mtop>
          g) theft of Digital Assets.
        </T>
        <T mtop center>
          <B>Restricted territories</B>
        </T>
        <T mtop>
          All Users shall make sure they act in conformity with the applicable laws, and they have
          found and learned the position of the regulatory authority in their jurisdiction.
        </T>
        <T mtop center>
          <B>Limitation of liability</B>
        </T>
        <T mtop>
          YOU ALONE ASSUME SOLE RESPONSIBILITY FOR ANY DECISIONS YOU MAKE BASED ON
          INFORMATION AND/OR THE USE OF INFORMATION.
        </T>
        <T mtop>
          WITHOUT PREJUDICE TO THE GENERALITY OF THE ‘LIMITATION LIABILITY’ CLAUSE OF THE
          TERMS, UNDER NO CIRCUMSTANCES SHALL Cryptocurrency Solutions OÜ, ITS FOUNDERS,
          TEAM MEMBERS AND ANY THIRD PARTY INVOLVED IN Cryptocurrency Solutions OÜ
          PROJECTS BE LIABLE FOR ANY LOSS OR DAMAGE YOU OR ANYONE ELSE INCURS AS A RESULT
          OF ANY ACTIVITY THAT YOU OR ANYONE ELSE ENGAGES IN BASED ON ANY INFORMATION
          YOU RECEIVE FROM Cryptocurrency Solutions OÜ MEDIA OR AS A RESULT OF THE USE OF
          INFORMATION AND/OR Cryptocurrency Solutions OÜ MEDIA, INCLUDING, BUT NOT LIMITED
          TO THE INCAPACITY TO USE Cryptocurrency Solutions OÜ SERVICES.
        </T>
        <T mtop center>
          <B>Disclaimer of warranties</B>
        </T>
        <T mtop>
          WITHOUT PREJUDICE TO THE GENERALITY OF THE ‘DISCLAIMER OF WARRANTIES’ CLAUSE OF
          THE TERMS, ALL INFORMATION AND Cryptocurrency Solutions OÜ MEDIA ARE PROVIDED “AS
          IS” WITHOUT WARRANTY OF ANY KIND. Cryptocurrency Solutions OÜ MAKES NO
          REPRESENTATIONS AND DISCLAIMS ALL EXPRESS, IMPLIED, AND STATUTORY WARRANTIES
          OF ANY KIND TO YOU AND/OR ANY THIRD PARTY, INCLUDING WARRANTIES AS TO
          ACCURACY, TIMELINESS, COMPLETENESS, OR FITNESS FOR ANY PARTICULAR PURPOSE.
        </T>
        <T mtop>
          <B>No Assignment.</B> You will not assign these Terms or assign any rights or delegate any
          obligations hereunder, in whole or in part, whether voluntarily or by operation of law,
          without our prior written consent. Any purported assignment or delegation by you without
          our Appropriate prior written consent will be null and void.
        </T>
        <T mtop>
          We may assign these Terms or any rights hereunder without your consent.
        </T>
        <T mtop>
          <B>No Waiver.</B> The failure by us to exercise, or delay in exercising, a legal right or remedy
          provided by these Terms or by law shall not constitute a waiver of our right or remedy.
        </T>
        <T mtop>
          <B>Severability and Integration.</B> If any part of these Terms is held invalid or unenforceable, that
          portion shall be construed in a manner consistent with Applicable law to reflect, as nearly as
          possible, the original intentions of the parties, and the remaining portions shall remain in full
          force and effect.
        </T>
        <T mtop>
          <B>11. Questions</B>
        </T>
        <T mtop>
          Questions about the Terms of Service? Contact us in the address : info@mahfazah.io.
        </T>
      </View>
    </BackgroundWrapper>
  );
}

TermsOfService.navigationOptions = () => ({
  title: translations.t('T_O_S'),
});

