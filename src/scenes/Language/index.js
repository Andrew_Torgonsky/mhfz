import React from 'react';
import { bindActionCreators } from 'redux';
import { TouchableOpacity, Image, AsyncStorage } from 'react-native';
import { connect } from 'react-redux';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import MenuItem from '../../components/MenuItem';
import translations from '../../translations';
import images from '../../global/images';
import styles from './styles';
import { ActionCreators } from '../../redux/actions';

type Props = {
  switchToLanguage(): void
}

export default function Language(props: Props) {
  return (
    <BackgroundWrapper>
      <MenuItem
        title="English"
        onPress={() => props.switchToLanguage('en')}
        right={
          translations.currentLocale().startsWith('en') ?
            <Image source={images.check} style={styles.icon} />
            : null
        }
      />
      <MenuItem
        title="العربية"
        onPress={() => props.switchToLanguage('ar')}
        right={
          translations.currentLocale().startsWith('ar') ?
            <Image source={images.check} style={styles.icon} />
            : null
        }
      />
    </BackgroundWrapper>
  );
}

Language.navigationOptions = () => ({
  title: translations.t('LANGUAGE')
});


const mapDispatchToProps = dispatch => bindActionCreators({
  switchToLanguage: ActionCreators.switchToLanguage
}, dispatch);

export const LanguageConnected = connect(null, mapDispatchToProps)(Language);
