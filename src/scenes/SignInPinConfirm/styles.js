import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingHorizontal: scale(16),
    alignItems: 'center'
  },
  textContainer: {
    marginTop: scaleByVertical(78)
  },
  pinInputContainer: {
    marginTop: scaleByVertical(32)
  },

});
