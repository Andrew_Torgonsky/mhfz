// @flow

import React from 'react';
import { View, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from './styles';
import globalStyles from '../../global/styles';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import translations from '../../translations';
import { ActionCreators } from '../../redux/actions';
import PinInput from '../../components/PinInput';
import { type ReduxStateType } from '../../types';

type Props = {
  pinCodeConfirm: string,
  changeForm: (key: string, value: any) => void,
  compareSignInPinCodes: () => void,
  pinCodeError: boolean
}

export const SignInPinConfirm = ({
  pinCodeConfirm, changeForm, pinCodeError, compareSignInPinCodes
}: Props) => (
  <BackgroundWrapper>
    <View style={styles.container}>
      <View style={styles.textContainer}>
        <Text style={globalStyles.normalText}>{translations.t('CONF_PIN')}</Text>
      </View>
      <View style={styles.pinInputContainer}>
        <PinInput
          value={pinCodeConfirm}
          onChangeText={val => changeForm('pinCodeConfirm', val)}
          error={pinCodeError}
          onFinish={compareSignInPinCodes}
        />
      </View>
    </View>
  </BackgroundWrapper>
);

SignInPinConfirm.navigationOptions = () => ({
  title: translations.t('SET_PIN'),
  headerLeft: null
});

const mapStateToProps = ({
  form: {
    pinCodeConfirm,
    pinCodeError
  }
}: ReduxStateType) => ({
  pinCodeConfirm,
  pinCodeError
});

const mapDispatchToProps = dispatch => bindActionCreators({
  changeForm: ActionCreators.changeForm,
  compareSignInPinCodes: ActionCreators.compareSignInPinCodes,
}, dispatch);

export const SignInPinConfirmConnected = connect(mapStateToProps, mapDispatchToProps)(SignInPinConfirm);
