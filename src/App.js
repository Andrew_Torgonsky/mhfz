// @flow

import React from 'react';
import { StatusBar, KeyboardAvoidingView } from 'react-native';
import { Provider } from 'react-redux';
import store from './redux/store';
import Navigator from './navigation';
import { PendingAnimationConnected as PendingAnimation } from './components/PendingAnimation';
import colors from './global/colors';
import { isIOS } from './utils/scale';


export default function App() {
  return (
    <Provider store={store}>
      <React.Fragment>
        <StatusBar
          barStyle="light-content"
        />
        <KeyboardAvoidingView
          style={{ flex: 1, backgroundColor: colors.dusk }}
          behavior="padding"
          enabled={isIOS}
        >
          <Navigator />
        </KeyboardAvoidingView>
        <PendingAnimation />
      </React.Fragment>
    </Provider>
  );
}
