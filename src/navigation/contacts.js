import React from 'react';

import { createStackNavigator } from 'react-navigation';

import { stackNavigationOptions, Scenes } from './shared';

import translations from '../translations';

import { ContactsConnected as Contacts } from '../scenes/Contacts';
import { ContactConnected as Contact } from '../scenes/Contact';
import { SendConnected as Send } from '../scenes/Send';
import { SelectWalletConnected as SelectWallet } from '../scenes/SelectWallet';


export const ContactsNavigator = createStackNavigator({
  [Scenes.Contacts]: {
    screen: props => <Contacts type="contacts" {...props} />,
    navigationOptions: () => ({
      title: translations.t('CONTACTS')
    })
  },
  [Scenes.Contact]: {
    screen: Contact
  },
  [Scenes.SendFromContact]: {
    screen: props => <Send type="fromContact" {...props} />,
    navigationOptions: () => ({
      title: translations.t('SEND')
    })
  },
  [Scenes.SelectWallet]: {
    screen: SelectWallet
  }
}, {
  navigationOptions: stackNavigationOptions,
  // cardStyle: {
  //   backgroundColor: 'transparent'
  // },
  // transitionConfig: () => ({
  //   containerStyle: {
  //     backgroundColor: 'transparent',
  //   }
  // })
});
