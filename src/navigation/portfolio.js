import React from 'react';
import { TouchableOpacity } from 'react-native';
import { createStackNavigator, createMaterialTopTabNavigator } from 'react-navigation';

import { stackNavigationOptions, Scenes, onSharePress$ } from './shared';

import { TopTabBarForWalletsConnected as TopTabBarForWallets } from '../components/TopTabBarForWallets';
import { TopTabBar } from '../components/TopTabBar';

import { WalletConnected as Wallet } from '../scenes/Wallet';
import { WalletHistoryConnected as WalletHistory } from '../scenes/WalletHistory';
import { MainWalletConnected as MainWallet } from '../scenes/MainWallet';
import { SendConnected as Send } from '../scenes/Send';
import { SelectWalletConnected as SelectWallet } from '../scenes/SelectWallet';
import { ScanCodeConnected as ScanCode } from '../scenes/ScanCode';
import { TransactionConnected as Transaction } from '../scenes/Transaction';
import { ContactsConnected as Contacts } from '../scenes/Contacts';


import ShareImage from '../components/Images/Share';

import translations from '../translations';

export const PortfolioNavigator = createStackNavigator({
  [Scenes.MainWallet]: {
    screen: MainWallet,
  },
  WalletsNavigator: {
    screen: createMaterialTopTabNavigator({
      [Scenes.BtcWallet]: props => <Wallet currency="bitcoin" {...props} />,
      [Scenes.EthWallet]: props => <Wallet currency="ethereum" {...props} />,
      [Scenes.XrpWallet]: props => <Wallet currency="ripple" {...props} />,
    }, {
      tabBarComponent: TopTabBarForWallets,
      backBehavior: 'none',
    }),
    navigationOptions: () => ({
      title: translations.t('PORTFOLIO'),
      headerRight: (
        <TouchableOpacity
          onPress={() => {
            onSharePress$.next();
          }}
        >
          <ShareImage />
        </TouchableOpacity>
      )
    })
  },
  BtcHistoryNavigator: {
    screen: createMaterialTopTabNavigator({
      [Scenes.BtcHistoryAll]: {
        screen: props => <WalletHistory currency="bitcoin" type="all" {...props} />,
        navigationOptions: () => ({
          title: translations.t('ALL')
        })
      },
      [Scenes.BtcHistoryReceived]: {
        screen: props => <WalletHistory currency="bitcoin" type="received" {...props} />,
        navigationOptions: () => ({
          title: translations.t('RECEIVED')
        })
      },
      [Scenes.BtcHistorySent]: {
        screen: props => <WalletHistory currency="bitcoin" type="sent" {...props} />,
        navigationOptions: () => ({
          title: translations.t('SENT')
        })
      }
    }, {
      tabBarComponent: TopTabBar,
      backBehavior: 'none'
    }),
    navigationOptions: () => ({
      title: translations.t('BTC_TR_HISTORY'),
    })
  },
  EthHistoryNavigator: {
    screen: createMaterialTopTabNavigator({
      [Scenes.EthHistoryAll]: {
        screen: props => <WalletHistory currency="ethereum" type="all" {...props} />,
        navigationOptions: () => ({
          title: translations.t('ALL')
        })
      },
      [Scenes.EthHistoryReceived]: {
        screen: props => <WalletHistory currency="ethereum" type="received" {...props} />,
        navigationOptions: () => ({
          title: translations.t('RECEIVED')
        })
      },
      [Scenes.EthHistorySent]: {
        screen: props => <WalletHistory currency="ethereum" type="sent" {...props} />,
        navigationOptions: () => ({
          title: translations.t('SENT')
        })
      }
    }, {
      tabBarComponent: TopTabBar,
      backBehavior: 'none'
    }),
    navigationOptions: () => ({
      title: translations.t('ETH_TR_HISTORY')
    })
  },
  XrpHistoryNavigator: {
    screen: createMaterialTopTabNavigator({
      [Scenes.XrpHistoryAll]: {
        screen: props => <WalletHistory currency="ripple" type="all" {...props} />,
        navigationOptions: () => ({
          title: translations.t('ALL')
        })
      },
      [Scenes.XrpHistoryReceived]: {
        screen: props => <WalletHistory currency="ripple" type="received" {...props} />,
        navigationOptions: () => ({
          title: translations.t('RECEIVED')
        })
      },
      [Scenes.XrpHistorySent]: {
        screen: props => <WalletHistory currency="ripple" type="sent" {...props} />,
        navigationOptions: () => ({
          title: translations.t('SENT')
        })
      }
    }, {
      tabBarComponent: TopTabBar,
      backBehavior: 'none'
    }),
    navigationOptions: () => ({
      title: translations.t('XRP_TR_HISTORY')
    })
  },
  SendTabs: {
    screen: createMaterialTopTabNavigator({
      [Scenes.SendToContact]: {
        screen: props => <Send type="toContact" {...props} />,
        navigationOptions: () => ({
          title: translations.t('TO_CONT')
        })
      },
      [Scenes.SendToWallet]: {
        screen: props => <Send type="walletAddress" {...props} />,
        navigationOptions: () => ({
          title: translations.t('TO_WALL_ADDR')
        })
      },
    }, {
      tabBarComponent: TopTabBar,
      backBehavior: 'none'
    }),
    navigationOptions: () => ({
      title: translations.t('SEND')
    })
  },
  [Scenes.SelectWallet]: {
    screen: SelectWallet
  },
  [Scenes.ScanCode]: {
    screen: ScanCode
  },
  [Scenes.Transaction]: {
    screen: Transaction
  },
  [Scenes.Recipients]: {
    screen: props => <Contacts type="recipients" {...props} />,
    navigationOptions: () => ({
      title: translations.t('SELECT_RECIPIENT')
    })
  }
}, {
  navigationOptions: stackNavigationOptions,
});
