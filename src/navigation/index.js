import React from 'react';
import { Keyboard, View } from 'react-native';
// import { connect } from 'react-redux';
import { createStackNavigator, createSwitchNavigator, NavigationActions, StackActions } from 'react-navigation';
// import { initializeListeners } from 'react-navigation-redux-helpers';
// import { navigationPropConstructor } from '../utils/redux';

import { stackNavigationOptions, Scenes, navigationChanges$ } from './shared';

import { SignInWordsConnected as SignInWords } from '../scenes/SignInWords';
import { TermsOfService } from '../scenes/TermsOfService';
import { PrivacyPolicy } from '../scenes/PrivacyPolicy';
import { SignUpPhoneConfirmationConnected as SignUpPhoneConfirmation } from '../scenes/SignUpPhoneConfirmation';
import { SignUpPinConnected as SignUpPin } from '../scenes/SignUpPin';
import { SignUpPinConfirmConnected as SignUpPinConfirm } from '../scenes/SignUpPinConfirm';
import { SignUpWordsConnected as SignUpWords } from '../scenes/SignUpWords';
import { SingUpWordsConfirmConnected as SignUpWordsConfirm } from '../scenes/SignUpWordsConfirm';
import { ForgotPasswordPhoneConnected as ForgotPasswordPhone } from '../scenes/ForgotPasswordPhone';
import { ForgotPasswordConfirmConnected as ForgotPasswordConfirm } from '../scenes/ForgotPasswordConfirm';
import { ForgotPasswordResetConnected as ForgotPasswordReset } from '../scenes/ForgotPasswordReset';
import { CountrySelectConnected as CountrySelect } from '../scenes/CountrySelect';
import { SignInConnected as SignIn } from '../scenes/SignIn';
import { SignInPinConnected as SignInPin } from '../scenes/SignInPin';
import { SignUpConnected as SignUp } from '../scenes/SignUp';
import { SignInPinConfirmConnected as SignInPinConfirm } from '../scenes/SignInPinConfirm';
import { EnterPinConnected as EnterPin } from '../scenes/EnterPin';

import { BottomTabsNavigator } from './bottomTabs';

import { getActiveRouteName } from '../utils/other';

export const SignUpNavigator = createStackNavigator({
  [Scenes.SignUp]: {
    screen: SignUp
  },
  [Scenes.CountrySelect]: {
    screen: CountrySelect,
  },
  [Scenes.TermsOfService]: {
    screen: TermsOfService
  },
  [Scenes.PrivacyPolicy]: {
    screen: PrivacyPolicy
  },
  [Scenes.SignUpPhoneConfirmation]: {
    screen: SignUpPhoneConfirmation
  },
  [Scenes.SignUpWords]: {
    screen: SignUpWords
  },
  [Scenes.SignUpWordsConfirm]: {
    screen: SignUpWordsConfirm
  },
  [Scenes.SignUpPin]: {
    screen: SignUpPin
  },
  [Scenes.SignUpPinConfirm]: {
    screen: SignUpPinConfirm
  },
}, {
  navigationOptions: stackNavigationOptions,
  // cardStyle: {
  //   backgroundColor: 'transparent'
  // },
  // transitionConfig: () => ({
  //   containerStyle: {
  //     backgroundColor: 'transparent',
  //   }
  // })
});

export const SignInNavigator = createStackNavigator({
  [Scenes.SignIn]: {
    screen: SignIn
  },
  [Scenes.CountrySelect]: {
    screen: CountrySelect,
  },
  [Scenes.ForgotPasswordPhone]: {
    screen: ForgotPasswordPhone
  },
  [Scenes.ForgotPasswordPhone]: {
    screen: ForgotPasswordPhone
  },
  [Scenes.ForgotPasswordConfirm]: {
    screen: ForgotPasswordConfirm
  },
  [Scenes.ForgotPasswordReset]: {
    screen: ForgotPasswordReset
  },
  [Scenes.SignInWords]: {
    screen: SignInWords
  },
  [Scenes.SignInPin]: {
    screen: SignInPin
  },
  [Scenes.SignInPinConfirm]: {
    screen: SignInPinConfirm
  },
}, {
  navigationOptions: stackNavigationOptions,
  // cardStyle: {
  //   backgroundColor: 'transparent'
  // },
  // transitionConfig: () => ({
  //   containerStyle: {
  //     backgroundColor: 'transparent',
  //   }
  // })
});

export const AppNavigator = createSwitchNavigator({
  initial: () => <View/>,
  SignInNavigator,
  [Scenes.EnterPin]: {
    screen: EnterPin
  },
  SignUpNavigator,
  BottomTabsNavigator
});

// type Props = {
//   nav: Object,
//   dispatch: Function
// }
// class Navigator extends React.Component<Props> {
//   componentDidMount() {
//     initializeListeners('root', this.props.nav);
//   }
//   render() {
//     const { dispatch, nav } = this.props;
//     const navigation = navigationPropConstructor(dispatch, nav);
//     return (
//       <AppNavigator
//         navigation={navigation}
//       />
//     );
//   }
// }
// export default connect(state => ({ nav: state.nav }))(Navigator);
export { Scenes };

let navigatorRef;
export const navigate = (routeName, params) => {
  navigatorRef.dispatch(NavigationActions.navigate({
    routeName,
    params,
  }));
};
export const goBack = () => {
  navigatorRef.dispatch(StackActions.pop());
};

export default () => (
  <AppNavigator
    ref={(ref) => { navigatorRef = ref; }}
    onNavigationStateChange={(prevState, currentState) => {
      navigationChanges$.next(getActiveRouteName(currentState));
      Keyboard.dismiss();
    }}
  />
);
