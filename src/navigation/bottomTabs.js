import React from 'react';

import { createStackNavigator, createSwitchNavigator, createBottomTabNavigator } from 'react-navigation';

import { AccountNavigator } from './account';
import { ContactsNavigator } from './contacts';
import { PortfolioNavigator } from './portfolio';

import BottomTabBar from '../components/BottomTabBar';
import PortfolioImage from '../components/Images/Portfolio';
import ContactsImage from '../components/Images/Contacts';
import AccountImage from '../components/Images/Account';

import globalStyles from '../global/styles';
import translations from '../translations';

export const BottomTabsNavigator = createBottomTabNavigator({
  Portfolio: {
    screen: PortfolioNavigator,
    navigationOptions: () => ({
      tabBarIcon: ({ focused }: { focused: boolean }) => (<PortfolioImage active={focused} />),
      tabBarLabel: translations.t('PORTFOLIO'),
    })
  },
  Contacts: {
    screen: ContactsNavigator,
    navigationOptions: () => ({
      tabBarIcon: ({ focused }: { focused: boolean }) => (<ContactsImage active={focused} />),
      tabBarLabel: translations.t('CONTACTS')
    })
  },
  Account: {
    screen: AccountNavigator,
    navigationOptions: () => ({
      tabBarIcon: ({ focused }: { focused: boolean }) => (<AccountImage active={focused} />),
      tabBarLabel: translations.t('ACCOUNT')
    })
  },
}, {
  tabBarOptions: {
    style: globalStyles.tabNavigator,
    activeTintColor: 'white',
    inactiveTintColor: 'grey',
    labelStyle: globalStyles.tabLabel,
  
  },
  resetOnBlur: true,
  tabBarComponent: BottomTabBar
});
