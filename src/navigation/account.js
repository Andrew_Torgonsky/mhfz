import React from 'react';
import { createStackNavigator, createSwitchNavigator, createBottomTabNavigator } from 'react-navigation';

import { stackNavigationOptions, Scenes } from './shared';


import { AccountConnected as Account } from '../scenes/Account';
import { AccountEditConnected as AccountEdit } from '../scenes/AccountEdit';
import { SettingsConnected as Settings } from '../scenes/Settings';
import { DefaultCurrencyConnected as DefaultCurrency } from '../scenes/DefaultCurrency';
import { CountrySelectConnected as CountrySelect } from '../scenes/CountrySelect';
import { LanguageConnected as Language } from '../scenes/Language';
import { SettingsPinConnected as SettingsPin } from '../scenes/SettingsPin';
import { SettingsPasswordConntected as SettingsPassword } from '../scenes/SettingsPassword';
import { SettingsChangePhoneConnected as SettingsChangePhone } from '../scenes/SettingsChangePhone';
import { SettingsChangePhoneConfirmConnected as SettingsChangePhoneConfirm } from '../scenes/SettingsChangePhoneConfirm';
import { TermsOfService } from '../scenes/TermsOfService';
import { PrivacyPolicy } from '../scenes/PrivacyPolicy';

export const AccountNavigator = createStackNavigator({
  [Scenes.Account]: {
    screen: Account
  },
  [Scenes.AccountEdit]: {
    screen: AccountEdit
  },
  [Scenes.Settings]: {
    screen: Settings
  },
  [Scenes.CountrySelect]: {
    screen: CountrySelect
  },
  [Scenes.DefaultCurrency]: {
    screen: DefaultCurrency
  },
  [Scenes.Language]: {
    screen: Language
  },
  [Scenes.SettingsPin]: {
    screen: props => <SettingsPin {...props} />
  },
  [Scenes.SettingsPinEnter]: {
    screen: props => <SettingsPin enter {...props} />
  },
  [Scenes.SettingsPinConfirm]: {
    screen: props => <SettingsPin confirm {...props} />
  },
  [Scenes.SettingsPassword]: {
    screen: SettingsPassword
  },
  [Scenes.SettingsPhone]: {
    screen: SettingsChangePhone
  },
  [Scenes.SettingsCode]: {
    screen: SettingsChangePhoneConfirm
  },
  [Scenes.TermsOfService]: {
    screen: TermsOfService
  },
  [Scenes.PrivacyPolicy]: {
    screen: PrivacyPolicy
  }
}, {
  navigationOptions: stackNavigationOptions,
  // cardStyle: {
  //   backgroundColor: 'transparent'
  // },
  // transitionConfig: () => ({
  //   containerStyle: {
  //     backgroundColor: 'transparent',
  //   }
  // })
});
