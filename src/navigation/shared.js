// @flow

import { ReplaySubject, Subject, Observable } from 'rxjs';
import { map, withLatestFrom } from 'rxjs/operators';
import globalStyles from '../global/styles';
import BackButtonImage from '../components/Images/BackButton';
import type { CryptoCurrencyType } from '../types';

export const Scenes = {
  SignInPin: 'SignInPin',
  SignInPinConfirm: 'SignInPinConfirm',
  SignIn: 'SignIn',
  CountrySelect: 'CountrySelect',
  SignInWords: 'SignInWords',
  SignUp: 'SignUp',
  TermsOfService: 'TermsOfService',
  PrivacyPolicy: 'PrivacyPolicy',
  SignUpPhoneConfirmation: 'SignUpPhoneConfirmation',
  SignUpPin: 'SignUpPin',
  SignUpPinConfirm: 'SignUpPinConfirm',
  SignUpWords: 'SignUpWords',
  SignUpWordsConfirm: 'SignUpWordsConfirm',
  EnterPin: 'EnterPin',
  MainWallet: 'MainWallet',
  Account: 'Account',
  AccountEdit: 'AccountEdit',
  Settings: 'Settings',
  SettingsPin: 'SettingsPin',
  SettingsPinEnter: 'SettingsPinEnter',
  SettingsPinConfirm: 'SettingsPinConfirm',
  SettingsPassword: 'SettingsPassword',
  SettingsPhone: 'SettingsPhone',
  SettingsCode: 'SettingsCode',
  DefaultCurrency: 'DefaultCurrency',
  Language: 'Language',
  Contacts: 'Contacts',
  Contact: 'Contact',
  ForgotPasswordPhone: 'ForgotPasswordPhone',
  ForgotPasswordConfirm: 'ForgotPasswordConfirm',
  ForgotPasswordReset: 'ForgotPasswordReset',
  BtcWallet: 'BtcWallet',
  EthWallet: 'EthWallet',
  XrpWallet: 'XrpWallet',
  BtcHistoryAll: 'BtcHistoryAll',
  BtcHistoryReceived: 'BtcHistoryReceived',
  BtcHistorySent: 'BtcHistorySent',
  EthHistoryAll: 'EthHistoryAll',
  EthHistoryReceived: 'EthHistoryReceived',
  EthHistorySent: 'EthHistorySent',
  XrpHistoryAll: 'XrpHistoryAll',
  XrpHistoryReceived: 'XrpHistoryReceived',
  XrpHistorySent: 'XrpHistorySent',
  Send: 'Send',
  SendFromContact: 'SendFromContact',
  SendToContact: 'SendToContact',
  SendToWallet: 'SendToWallet',
  SelectWallet: 'SelectWallet',
  ScanCode: 'ScanCode',
  Transaction: 'Transaction',
  Recipients: 'Recipients'
};

export const stackNavigationOptions = {
  headerStyle: globalStyles.header,
  headerTitleStyle: globalStyles.headerTitle,
  headerTintColor: 'white',
  headerBackImage: BackButtonImage,
  headerBackTitle: null,
  headerTruncatedBackTitle: null
};

export const navigationChanges$: ReplaySubject<string> = new ReplaySubject(1);
export const onSharePress$: Subject<void> = new Subject();
export const onSharePressWithCurrency$: Observable<CryptoCurrencyType> = onSharePress$.pipe(
  withLatestFrom(navigationChanges$),
  map(([first, second]) => {
    switch (second) {
      case Scenes.EthWallet:
        return 'ethereum';
      case Scenes.XrpWallet:
        return 'ripple';
      default:
        return 'bitcoin';
    }
  }),
);

export const routesWithoutTabs = [
  Scenes.BtcHistoryAll,
  Scenes.BtcHistoryReceived,
  Scenes.BtcHistorySent,
  Scenes.EthHistoryAll,
  Scenes.EthHistoryReceived,
  Scenes.EthHistorySent,
  Scenes.XrpHistoryAll,
  Scenes.XrpHistoryReceived,
  Scenes.XrpHistorySent,
  Scenes.SelectWallet,
  Scenes.ScanCode,
  Scenes.Transaction,
  Scenes.SendToContact,
  Scenes.SendToWallet,
  Scenes.Contact,
  Scenes.SendFromContact,
  Scenes.SettingsPin,
  Scenes.SettingsPinEnter,
  Scenes.SettingsPinConfirm,
  Scenes.SettingsCode,
  Scenes.SettingsPhone,
  Scenes.CountrySelect,
  Scenes.SettingsPassword
];
