import DeviceInfo from 'react-native-device-info';

export const PRODUCTION = DeviceInfo.getBundleId() === 'io.mahfazah';
export const USE_BLOCKCHAIN_TESTNET = !PRODUCTION;
export const BITCOIN_NODE = USE_BLOCKCHAIN_TESTNET ? 'http://178.128.244.253:3002/insight-api/' : 'http://178.128.244.253:3001/insight-api/';
// export const ETHEREUM_NODE = USE_BLOCKCHAIN_TESTNET ? 'http://178.128.252.30:18545' : 'http://178.128.252.30:8545';
export const ETHEREUM_NODE = USE_BLOCKCHAIN_TESTNET ? 'http://178.128.252.30:18545' : 'https://mainnet.infura.io/v3/661c676d69184d60bc040fbfa23dc528';

export const RIPPLE_NODE = USE_BLOCKCHAIN_TESTNET ? 'wss://s.altnet.rippletest.net:51233' : 'wss://s2.ripple.com:443';
export const IP = PRODUCTION ? '188.166.201.243' : '178.128.127.92';
export const URL = `http://${IP}`;
export const BACKEND = `${URL}/api/v1`;
export const USE_MOCK_BACKEND = false;
export const PUSH_KEY = PRODUCTION ? '23dbd245-bec4-4f79-81e5-ddf8be73b811' : 'bf81efae-3553-4dd3-982b-ec3ec4badec0';

export const APP_STORE_LINK = 'itunes.apple.com/us/app/mahfazah/id1435880498?mt=8';
export const PLAY_MARKET_LINK = 'play.google.com/store/apps/details?id=io.mahfazah';