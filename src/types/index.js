// @flow
import { Types } from '../redux/actions';

export type ActionType = { type: $Keys<typeof Types> };

export type ExtractReturnType = <R>(() => R) => R;

export type AlertType = 'red' | 'green' | ''; // alerts in bottom of the screen

export type AlertColorType = 'red' | 'yellow'; // alerts in wallets

export type TransactionStatusType = 'success' | 'pending' | 'failed';

export type CryptoCurrencyType = 'bitcoin' | 'ethereum' | 'ripple';

export type FiatCurrencyType = 'usd' | 'eur' | 'sar';

export type BalanceHistoryEntryType = {
  timestamp: number,
  average: number,
  value?: number
}

export type Point = {
  x: number,
  y: number
}

export type TransactionHistoryEntryType = {
  id: string,
  address: string,
  amount: number,
  status: TransactionStatusType,
  timestamp: number,
  confirmations: number,
  block: number,
  isSeen: boolean
}

export type WalletsStatItemType = {
  balance: number,
  rate: number,
  history: BalanceHistoryEntryType[],
  newTx: boolean,
  alertText: string,
  alertColor: AlertColorType,
  transactions: TransactionHistoryEntryType[],
  receivingTransactions: boolean,
  endOfTransactionList: boolean,
  lastChecked: number
}

export type WalletsStatType = {
  // [key: CryptoCurrencyType]: WalletsStatItemType,
  bitcoin: WalletsStatItemType,
  ethereum: WalletsStatItemType,
  ripple: WalletsStatItemType,
}

export type SessionType = {
  token?: string,
  name: string,
  email: string,
  phone_number: string,
  default_currency: FiatCurrencyType,
  image: string,
  created_at: string,
  updated_at: string,
}

export type WalletType = {
  walletAddress: string,
  privateKey: string
}

export type WalletsType = {
  // [key: CryptoCurrencyType]: WalletType
  bitcoin: WalletType,
  ethereum: WalletType,
  ripple: WalletType
}

export type AlertStateType = {
  alertText: string,
  alertType: AlertType,
  alertTimestamp: number
}

export type PendingType = {
  signIn: boolean,
  signUp: boolean,
  smsCode: boolean,
  logOut: boolean,
  auth: boolean,
  startUp: boolean,
  passwordRecovery: boolean,
  account: boolean,
  send: boolean,
}

export type RandomWordType = {
  text: string,
  visible: boolean
}

export type CountryType = {
  name: string,
  search: string[],
  iso2: string,
  dialCode: string,
  key: string
}

export type ContactWalletType = {
  walletAddress: string,
  currency: CryptoCurrencyType
}

export type ContactType = {
  firstName: string,
  lastName: string,
  photo: string,
  emails: string[],
  phoneNumbers: string[],
  wallets: ContactWalletType[]
}

export type RatesType = {
  bitcoin: number,
  ethereum: number,
  ripple: number,
  currency: FiatCurrencyType
}

export type FormType = {
  name: string,
  email: string,
  phonePrefix: string,
  countryIso: string,
  phoneNumber: string,
  password: string,
  password2nd: string,
  agree: boolean,
  passwordVisibility: boolean,
  pinCode: string,
  pinCodeConfirm: string,
  smsCode: string,
  twelveWords: Array<string | null>,
  twelveWordsGenerated: string[],
  twelveWordsRandom: RandomWordType[],
  nameError: boolean,
  emailError: boolean,
  phoneError: boolean,
  passwordError: boolean,
  passwordError2nd: boolean,
  pinCodeError: boolean,
  smsCodeError: boolean,
  addressToSend: string,
  amountToSend: string,
  feeOptions: Object[],
  chosenFeeOption: number,
  currencyToSend: CryptoCurrencyType,
  contactToSend: ContactType | void,
  sendPreparing: boolean,
  smsCodeTimestamp: number
}


export type ReduxStateType = {
  form: FormType,
  alertState: AlertStateType,
  pending: PendingType,
  wallets: WalletsType,
  session: SessionType,
  walletsStat: WalletsStatType,
  contacts: ContactType[],
  balancePeriod: 0 | 1 | 2,
  appMinimized: boolean,
  preventMinimize: boolean,
  pushNotifications: boolean
}
