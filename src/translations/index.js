import I18n from 'react-native-i18n';
import { AsyncStorage, I18nManager } from 'react-native';
import en from './en';
import ar from './ar';

I18n.defaultLocale = 'en';
I18n.fallbacks = true;

I18n.translations = {
  en,
  ar
};

async function setInitialLanguage() {
  try {
    if (I18n.currentLocale().startsWith('ar')) {
      I18n.locale = 'ar';
      I18nManager.allowRTL(true);
      I18nManager.forceRTL(true);
      await AsyncStorage.setItem('language', 'ar').catch(() => { });
    } else {
      I18n.locale = 'en';
      I18nManager.allowRTL(false);
      I18nManager.forceRTL(false);
      await AsyncStorage.setItem('language', 'en').catch(() => { });
    }
  } catch (e) {
    // nothing to do here
  }
}

export async function setLanguage(language: 'en' | 'ar') {
  try {
    await AsyncStorage.setItem('language', language);
    if (language === 'ar') {
      I18nManager.allowRTL(true);
      I18nManager.forceRTL(true);
    } else {
      I18nManager.allowRTL(false);
      I18nManager.forceRTL(false);
    }
  } catch (e) {
    // nothing to do here
  }
}

export async function languageInit() {
  try {
    const language = await AsyncStorage.getItem('language');
    if (language === 'ar') {
      I18n.locale = 'ar';
      I18nManager.allowRTL(true);
      I18nManager.forceRTL(true);
    }
    if (language === 'en') {
      I18n.locale = 'en';
      I18nManager.allowRTL(false);
      I18nManager.forceRTL(false);
    }
    if (!language) {
      await setInitialLanguage();
    }
  } catch (e) {
    await setInitialLanguage();
  }
}

export const isRTL = () => {
  return I18n.currentLocale().indexOf('he') === 0 || I18n.currentLocale().indexOf('ar') === 0
};

export default I18n;