export default {
  atami: 'Atami-Regular',
  firaSansExtraLight: 'FiraSans-ExtraLight',
  firaSansMedium: 'FiraSans-Medium',
  robotoRegular: 'Roboto-Regular',
  robotoLight: 'Roboto-Light',
  robotoMedium: 'Roboto-Medium',
  robotoThin: 'Roboto-Thin',
  robotoBold: 'Roboto-Bold',
  sfProDisplayLight: 'SFProDisplay-Light',
  sfProDisplayMedium: 'SFProDisplay-Medium',
  sfProDisplayRegular: 'SFProDisplay-Regular',
  sfProDisplayThin: 'SFProDisplay-Thin',
  sfProDisplayBold: 'SFProDisplay-Bold',
  sfProDisplaySemibold: 'SFProDisplay-Semibold',
  sfProTextLight: 'SFProText-Light',
  sfProTextRegular: 'SFProText-Regular',
  sfProTextBold: 'SFProText-Bold',
  sfProTextSemibold: 'SFProText-Semibold',
  sfUITextLight: 'SFUIText-Light',
  sfUITextMedium: 'SFUIText-Medium',
  sfUITextRegular: 'SFUIText-Regular',
  sfUITextSemibold: 'SFUIText-Semibold'
};
