
const colors = {
  aquamarine: '#04d6a8',
  berry: '#940f37',
  black26: 'rgba(0, 0, 0, 0.26)',
  black50: 'rgba(0, 0, 0, 0.5)',
  brightTeal: '#00fbc5',
  charcoalGrey: '#3a4155',
  coolGrey: '#a7abb9',
  darkGreyBlue: 'rgb(45, 56, 84)',
  darkGreyBlueTwo: '#2e3857',
  grey: '#3a4155',
  grey2: '#38425e',
  blue: '#2e3857',
  dark: '#222A41',
  darkTwo: '#242c43',
  darkSeaGreen: '#0e8f71',
  darkSkyBlue: 'rgb(61, 165, 217)',
  dusk: '#495473',
  lightBlue21: 'rgba(124, 201, 241, 0.21)',
  reddishPink: '#ff2366',
  reddishPinkTwo: '#ff2465',
  sky: 'rgb(121, 208, 253)',
  slate: 'rgb(77, 85, 105)',
  sunshineYellow: '#ffeb3b',
  white20: 'rgba(255, 255, 255, 0.2)',
  white38: 'rgba(255, 255, 255, 0.38)',
  white50: 'rgba(255, 255, 255, 0.5)',
  white60: 'rgba(255, 255, 255, 0.6)',
  white87: 'rgba(255, 255, 255, 0.87)',

};

export default colors;
