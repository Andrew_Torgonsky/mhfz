import countries from '../assets/countries/countries';
import { type CountryType } from '../types';

const flatCountryList: CountryType[] = [];

countries.forEach((element) => {
  if (Array.isArray(element.areaCodes)) {
    element.areaCodes.forEach((innerElement) => {
      const item = {
        name: element.name,
        search: element.name.split(/ |\(|\)/).filter(x => x).map(x => encodeURI(x.toLowerCase())),
        iso2: element.iso2,
        dialCode: element.dialCode + innerElement,
        key: element.dialCode + innerElement + element.iso2
      };
      flatCountryList.push(item);
    });
  } else {
    const item = {
      name: element.name,
      search: element.name.split(/ |\(|\)/).filter(x => x).map(x => encodeURI(x.toLowerCase())),
      iso2: element.iso2,
      dialCode: element.dialCode,
      key: element.dialCode + element.iso2
    };
    flatCountryList.push(item);
  }
});

export default flatCountryList;
