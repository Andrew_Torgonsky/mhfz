import { StyleSheet } from 'react-native';
import colors from './colors';
import fonts from './fonts';
import { scale, scaleByVertical, platformSelect } from '../utils/scale';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  title1: {
    fontSize: scaleByVertical(17),
    color: 'white',
    ...platformSelect({
      fontFamily: fonts.sfProTextSemibold,
    }, {
      fontFamily: fonts.robotoMedium
    })
  },
  header: {
    backgroundColor: colors.dark,
    borderBottomWidth: 0,
    ...platformSelect({
      height: scaleByVertical(44),
    }, {
      height: scaleByVertical(56),
      elevation: 10,
    })
  },
  transparentHeader: {
    backgroundColor: colors.darkGreyBlueTwo,
    borderBottomWidth: 0,
    ...platformSelect({
      height: scaleByVertical(44),
    }, {
      height: scaleByVertical(56),
      elevation: 0,
    })
  },
  headerTitle: {
    color: 'white',
    fontWeight: '600',
    ...platformSelect({
      fontFamily: fonts.sfProTextSemibold,
      fontSize: scaleByVertical(17),
    }, {
      fontFamily: fonts.robotoMedium,
      fontSize: scaleByVertical(20),
    })
  },
  greyText: {
    fontSize: scaleByVertical(14),
    lineHeight: scaleByVertical(20),
    color: colors.white50,
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  normalText: {
    fontSize: scaleByVertical(16),
    lineHeight: scaleByVertical(24),
    color: colors.white87,
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  walletTitle1: {
    fontSize: scaleByVertical(14),
    color: colors.white60,
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  walletTitle2: {
    fontSize: scaleByVertical(24),
    color: 'white',
    ...platformSelect({
      fontFamily: fonts.sfProDisplayBold,
    }, {
      fontFamily: fonts.robotoBold
    })
  },
  walletTitle3: {
    fontSize: scaleByVertical(14),
    color: 'white',
    ...platformSelect({
      fontFamily: fonts.sfProTextBold,
    }, {
      fontFamily: fonts.robotoBold
    })
  },
  walletTitle4: {
    fontSize: scaleByVertical(24),
    color: 'white',
    ...platformSelect({
      fontFamily: fonts.sfProTextLight,
    }, {
      fontFamily: fonts.robotoLight
    })
  },
  walletTitle5: {
    fontSize: scaleByVertical(14),
    color: 'white',
    ...platformSelect({
      fontFamily: fonts.sfProTextLight,
    }, {
      fontFamily: fonts.robotoLight
    })
  },
  tabNavigator: {
    backgroundColor: colors.dark,
    height: scaleByVertical(49),
    shadowColor: colors.black26,
    shadowOffset: {
      width: 0,
      height: -1.5
    },
    shadowRadius: scale(4),
    shadowOpacity: 1,
    borderTopWidth: 0
  },
  tabLabel: {
    fontSize: scaleByVertical(10),
    ...platformSelect({
      fontFamily: fonts.sfProTextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
});

