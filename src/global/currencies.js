// @flow

import type { FiatCurrencyType } from '../types';

const currencies: FiatCurrencyType[] = [
  'usd',
  'eur',
  'sar'
];

export default currencies;