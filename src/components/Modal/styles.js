import { StyleSheet } from 'react-native';
import { scale, scaleByVertical } from '../../utils/scale';
import colors from '../../global/colors';
import fonts from '../../global/fonts';

export default StyleSheet.create({
  shadow: {
    justifyContent: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: colors.black50
  },
  modalWrapper: {
    width: '100%',
    paddingLeft: scale(16),
    paddingRight: scale(16),
  },
  modal: {
    paddingTop: scaleByVertical(24),
    paddingBottom: scaleByVertical(24),
    paddingLeft: scale(16),
    paddingRight: scale(16),
    backgroundColor: colors.grey
  },
  content: {
    marginTop: scaleByVertical(24)
  }
});
