import React from 'react';
import { Modal, View } from 'react-native';
import styles from './styles';

type Props = {
  children: any
};

export default (props: Props) => (
  <Modal
    transparent
  >
    <View style={styles.shadow}>
      <View style={styles.modalWrapper}>
        <View style={styles.modal}>
          <View className={styles.content}>
            {props.children}
          </View>
        </View>
      </View>
    </View>
  </Modal>
);
