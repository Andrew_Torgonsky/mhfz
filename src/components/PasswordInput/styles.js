import { StyleSheet } from 'react-native';
import { scale, scaleByVertical } from '../../utils/scale';
import colors from '../../global/colors';
import fonts from '../../global/fonts';

export default StyleSheet.create({
  password: {
    width: scale(24),
    height: scale(24),
    tintColor: colors.white87,
    marginLeft: scale(16)
  },
  eye: {
    width: scale(24),
    height: scale(24),
    tintColor: colors.white38,
    marginRight: scale(8)
  },
  eyeVisible: {
    tintColor: colors.white87,
  }
});
