import React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import styles from './styles';
import InputField from '../InputField';
import images from '../../global/images';
import { scale } from '../../utils/scale';
import translations from '../../translations';

type Props = {
  visible: boolean,
  onEyePress: () => void
}

export default (props: Props) => {
  const { visible, onEyePress, ...rest } = props;
  const beforeElements = (
    <Image
      source={images.password}
      style={styles.password}
    />
  );
  const afterElements = (
    <TouchableOpacity
      onPress={onEyePress}
    >
      <Image
        source={images.eye}
        style={[styles.eye, visible && styles.eyeVisible]}
      />
    </TouchableOpacity>
  );
  return (
    <InputField
      before={beforeElements}
      after={afterElements}
      inputIndentaion={scale(16)}
      secureTextEntry={!visible}
      placeholder={translations.t('PWD')}
      {...rest}
    />
  );
};
