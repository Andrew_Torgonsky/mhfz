import React from 'react';
import QRCode from 'react-native-qrcode';
import { View, Text, TouchableOpacity, Image, Share } from 'react-native';
import FilledButton from '../FilledButton';
import TextButton from '../TextButton';
import Modal from '../Modal';
import styles from './styles';
import translations from '../../translations';
import images from '../../global/images';
import type { CryptoCurrencyType } from '../../types';
import { scale } from '../../utils/scale';

type Props = {
  onClose: () => void,
  currency: CryptoCurrencyType,
  address: string,
  onAddressPress: () => void
};

export default ({ currency, address, onClose, onAddressPress }: Props) => (
  <Modal>
    <Text style={styles.title}>{translations.t('SHARE')}</Text>
    <View style={styles.qrContainer}>
      <View style={styles.qrBackground}>
        <QRCode
          value={`${currency}:${address}`}
          size={scale(150)}
          bgColor="black"
          fgColor="white"
        />
      </View>
      
    </View>
    <TouchableOpacity onPress={onAddressPress}>
      <Text style={styles.address}>{address}</Text>
    </TouchableOpacity>
    <View style={styles.shareButtonContainer}>
      <FilledButton
        title={translations.t('SHARE')}
        onPress={() => {
          Share.share({
            title: translations.t('SHARE_WALLET'),
            message: address,
            url: address
          }, {
            subject: translations.t('SHARE_WALLET'),
            dialogTitle: translations.t('SHARE_WALLET'),
          });
        }}
      />
    </View>
    <View style={styles.closeButtonContainer}>
      <TextButton title={translations.t('CLOSE')} onPress={onClose} />
    </View>
    <TouchableOpacity onPress={onClose} style={styles.closeButtonContainer2}>
      <Image
        source={images.close}
        style={styles.closeIcon}
      />
    </TouchableOpacity>
  </Modal>
);

