import { StyleSheet } from 'react-native';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';
import colors from '../../global/colors';
import fonts from '../../global/fonts';

export default StyleSheet.create({
  title: {
    textAlign: 'center',
    fontSize: scaleByVertical(16),
    color: 'white',
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  qrContainer: {
    alignItems: 'center',
    marginTop: scaleByVertical(24)
  },
  qrBackground: {
    backgroundColor: 'white',
    padding: scale(10)
  },
  address: {
    flexWrap: 'wrap',
    marginTop: scaleByVertical(16),
    textAlign: 'center',
    fontSize: scaleByVertical(14),
    color: colors.white50,
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  shareButtonContainer: {
    marginTop: scaleByVertical(24),
    height: scaleByVertical(40)
  },
  closeButtonContainer: {
    marginTop: scaleByVertical(24),
    alignItems: 'center'
  },
  closeIcon: {
    width: scale(16),
    height: scale(16)
  },
  closeButtonContainer2: {
    position: 'absolute',
    top: scaleByVertical(-10),
    right: scale(-5)
  }
});
