import React from 'react';
import { Image, TouchableOpacity, View, Text } from 'react-native';
import images from '../../global/images';
import colors from '../../global/colors';
import translations, { isRTL } from '../../translations';
import styles from './styles';

type Props = {
  btc: boolean,
  eth: boolean,
  xrp: boolean,
  address: string,
  onPress: () => void
};

export default (props: Props) => {
  const {
    btc,
    eth,
    xrp,
    address,
    onPress
  } = props;
  return (
    <View
      style={styles.container}
    >
      <View style={styles.iconContainer}>
        <Image
          source={
            btc && images.bitcoin ||
            eth && images.ethereum ||
            xrp && images.ripple
          }
          style={styles.icon}
        />
      </View>
      <View style={styles.infoContainer}>
        <Text style={[
          styles.title,
          { textAlign: isRTL() ? 'right': 'left' }
        ]}>
          {
            btc && `${translations.t('BITCOIN')} - BTC` ||
            eth && `${translations.t('ETHEREUM')} - ETH` ||
            xrp && `${translations.t('RIPPLE')} - XRP`
          }
        </Text>
        <Text style={[
          styles.address,
          { textAlign: isRTL() ? 'right': 'left' }
        ]}>{address}</Text>
      </View>
      <TouchableOpacity
        style={styles.sendContainer}
        onPress={onPress}
      >
        <Image style={styles.sendIcon} source={images.send} />
      </TouchableOpacity>
    </View>
  );
};
