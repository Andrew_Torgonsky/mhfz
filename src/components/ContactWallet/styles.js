import { StyleSheet } from 'react-native';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';
import colors from '../../global/colors';
import fonts from '../../global/fonts';

export default StyleSheet.create({
  container: {
    marginTop: scaleByVertical(8),
    backgroundColor: colors.white20,
    borderRadius: 2,
    height: scaleByVertical(64),
    flexDirection: 'row'
  },
  iconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: scale(64)
  },
  icon: {
    width: scale(40),
    height: scale(40)
  },
  infoContainer: {
    flex: 1
  },
  title: {
    marginTop: scaleByVertical(6),
    fontSize: scaleByVertical(14),
    color: colors.white87,
    fontWeight: 'bold'
  },
  address: {
    marginTop: scaleByVertical(6),
    fontSize: scaleByVertical(12),
    color: colors.white50
  },
  sendContainer: {
    width: scale(56),
    justifyContent: 'center',
    alignItems: 'center'
  },
  sendIcon: {
    tintColor: colors.brightTeal,
    width: scale(24),
    height: scale(24)
  }
});
