import { StyleSheet } from 'react-native';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';
import colors from '../../global/colors';
import fonts from '../../global/fonts';

export default StyleSheet.create({
  touchable: {
    flex: 1,
  },
  container: {
    borderRadius: 2,
    backgroundColor: colors.white20,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  userIcon: {
    width: scale(24),
    height: scale(24),
    tintColor: colors.white38,
    marginLeft: scale(16)
  },
  arrowIcon: {
    width: scale(8),
    height: scale(13),
    tintColor: colors.white38,
    marginRight: scale(8)
  },
  leftContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    marginLeft: scale(16),
    color: colors.white38,
    fontSize: scaleByVertical(16),
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  chosen: {
    color: colors.white87
  }
});
