// @flow

import React from 'react';
import { Image, TouchableOpacity, View, Text } from 'react-native';
import styles from './styles';
import images from '../../global/images';
import translations, { isRTL } from '../../translations';
import type { ContactType } from '../../types';

type Props = {
  onPress: () => void,
  value: ContactType | void,
  rightArrow: boolean
}

export default ({ onPress, value, rightArrow = true }: Props) => (
  <TouchableOpacity
    style={styles.touchable}
    onPress={onPress}
  >
    <View style={styles.container}>
      <View style={styles.leftContainer}>
        <Image
          source={images.user}
          style={styles.userIcon}
        />
        {value ?
          <Text style={[styles.text, styles.chosen]}>{value.firstName} {value.lastName}</Text> :
          <Text style={styles.text}>{translations.t('SELECT_CONTACT')}</Text>
        }
      </View>
      { rightArrow ?
        <Image
          source={images.rightArrow}
          style={[styles.arrowIcon, isRTL() && { transform: [{ rotate: '180deg'}]}]}
        /> :
        null
      }
    </View>
  </TouchableOpacity>
);
