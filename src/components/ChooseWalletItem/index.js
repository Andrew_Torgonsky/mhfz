import React from 'react';
import { TouchableOpacity, View, Text, Image } from 'react-native';
import styles from './styles';
import images from '../../global/images';
import type { CryptoCurrencyType, WalletsStatType } from '../../types';
import { switchAbbrAndName } from '../../utils/currencies';
import { capitalizeString } from '../../utils/other';
import { isRTL } from '../../translations';
import { isAndroid } from '../../utils/scale';
import translations from '../../translations';

type Props = {
  currency: CryptoCurrencyType,
  active: boolean,
  chosen: boolean,
  rightArrow: boolean,
  walletsStat: WalletsStatType,
  onPress: () => void
}

export default (props: Props) => (
  <TouchableOpacity style={[styles.touchable, props.active && styles.active, props.chosen && styles.chosen ]} onPress={props.onPress}>
    <View style={styles.sideContainer}>
      <Image
        source={images[props.currency]}
        style={styles.icon}
      />
      <Text style={styles.label}>
        {translations.t(props.currency.toUpperCase())}
      </Text>
    </View>
    <View style={styles.sideContainer}>
      <Text style={styles.balance}>
        {props.walletsStat ? props.walletsStat[props.currency].balance : null} {switchAbbrAndName(props.currency)}
      </Text>
      {
        props.rightArrow ?
          <Image
            source={images.rightArrow}
            style={[styles.rightArrow, isRTL() && { transform: [{ rotate: '180deg' }] }, isAndroid && { transform: [{ rotate: '-90deg' }] }]}
          /> :
          null
      }
    </View>
  </TouchableOpacity>
);
