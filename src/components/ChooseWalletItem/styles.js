import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  touchable: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: colors.white20
  },
  sideContainer: {
    flexDirection: 'row',
    height: scaleByVertical(44),
    alignItems: 'center'
  },
  icon: {
    width: scale(24),
    height: scale(24),
    marginLeft: scale(16)
  },
  active: {
    backgroundColor: colors.white20,
    borderBottomColor: 'transparent'
  },
  chosen: {
    borderBottomWidth: 0,
    backgroundColor: colors.dark,
  },
  label: {
    marginLeft: scale(16),
    color: colors.white50,
    fontSize: scaleByVertical(16),
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  balance: {
    color: colors.white87,
    fontSize: scaleByVertical(16),
    marginRight: scale(16),
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  rightArrow: {
    width: scale(8),
    height: scale(13),
    marginRight: scale(16)
  }
});
