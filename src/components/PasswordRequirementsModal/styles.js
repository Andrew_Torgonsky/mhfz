import { StyleSheet } from 'react-native';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';
import colors from '../../global/colors';
import fonts from '../../global/fonts';

export default StyleSheet.create({
  popupItem: {
    marginTop: scaleByVertical(20),
    flexDirection: 'row'
  },
  popupItemText: {
    marginLeft: scale(7),
    paddingRight: scale(16),
    fontSize: scaleByVertical(14),
    lineHeight: scale(20),
    color: 'white',
    opacity: 0.5
  },
  title: {
    textAlign: 'center',
    fontSize: scaleByVertical(16),
    color: 'white',
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  popupContainerButton: {
    marginTop: scaleByVertical(32),
    height: scaleByVertical(40)
  }
});
