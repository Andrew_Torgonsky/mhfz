import React from 'react';
import { View, Text, Image } from 'react-native';
import FilledButton from '../FilledButton';
import Modal from '../Modal';
import styles from './styles';
import translations from '../../translations';
import images from '../../global/images';

type Props = {
  onPress: () => void;
};

export default (props: Props) => (
  <Modal>
    <Text style={styles.title}>{translations.t('PWD_REQ')}</Text>
    <View style={styles.popupItem}>
      <Image source={images.greenCheck} />
      <Text style={styles.popupItemText}>
        {
          translations.t('Password Requirement 1')
        }
      </Text>
    </View>
    <View style={styles.popupItem}>
      <Image source={images.greenCheck} />
      <Text style={styles.popupItemText}>
        {
          translations.t('Password Requirement 2')
        }
      </Text>
    </View>
    <View style={styles.popupItem}>
      <Image source={images.greenCheck} />
      <Text style={styles.popupItemText}>
        {
          translations.t('Password Requirement 3')
        }
      </Text>
    </View>
    <View style={styles.popupContainerButton}>
      <FilledButton
        title={translations.t('CLOSE')}
        onPress={props.onPress}
      />
    </View>
  </Modal>
);
