import React from 'react';
import { TouchableOpacity, View, Text, Image } from 'react-native';
import styles from './styles';
import images from '../../global/images';
import { isRTL } from '../../translations';

type Props = {
  title: string,
  right: Object,
  rightArrow: boolean,
  onPress: () => void
}

export default (props: Props) => (
  <TouchableOpacity style={styles.menuItem} onPress={props.onPress}>
    <Text style={styles.text}>{props.title}</Text>
    <View style={styles.rightContainer}>
      {
        props.right
      }
      {
        props.rightArrow ?
          <Image
            source={images.rightArrow}
            style={[
              styles.rightArrow,
              isRTL() && { transform: [{ rotate: '180deg' }] }
            ]}
          /> :
          null
      }
    </View>
  </TouchableOpacity>
);
