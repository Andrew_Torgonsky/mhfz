import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  menuItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: scale(44),
    paddingHorizontal: scale(16),
    borderBottomWidth: scaleByVertical(1),
    borderBottomColor: colors.white20
  },
  text: {
    fontSize: scaleByVertical(16),
    color: colors.white87,
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  rightContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  rightArrow: {
    height: scale(13),
    width: scale(8)
  },
});
