import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  button: {
    marginRight: scale(6),
    paddingHorizontal: scale(17),
    paddingVertical: scaleByVertical(7),
    borderWidth: 1,
    borderColor: colors.sky
  },
  text: {
    color: colors.sky
  }
});
