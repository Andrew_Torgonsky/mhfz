import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import styles from './styles';

type Props = {
  style: Object,
  title: string,
  onPress(): void,
}

export default (props: Props) => (
  <TouchableOpacity
    style={[
      styles.button,
      props.style
    ]}
    onPress={props.onPress}
  >
    <Text style={styles.text}>{props.title}</Text>
  </TouchableOpacity>
);
