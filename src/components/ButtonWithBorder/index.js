// @flow

import React from 'react';
import { Text, TouchableOpacity, Image } from 'react-native';
import styles from './styles';
import { type RandomWordType } from '../../types';

type Props = {
  onPress: () => void,
  icon: string,
  title: string
}

export default ({
  icon, title, onPress, ...rest
}: Props) => (
  <TouchableOpacity
    style={styles.touchable}
    onPress={onPress}
    {...rest}
  >
    <Image
      source={icon}
      style={styles.icon}
    />
    <Text style={styles.text}>{title}</Text>
  </TouchableOpacity>
);
