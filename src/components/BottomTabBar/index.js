import React from 'react';
import OriginalBottomTabBar from 'react-navigation-tabs/dist/views/BottomTabBar';
import { navigationChanges$, routesWithoutTabs } from '../../navigation/shared';
import { keyboardOpen$ } from '../../api/keyboard';


class BottomTabBar extends React.PureComponent {
  state = {
    keyBoardOpen: false,
    routeWithoutTabs: false
  }

  componentWillMount() {
    this.subscription = keyboardOpen$.subscribe(state => this.setState({ keyBoardOpen: state }));
    this.subscription2 = navigationChanges$.subscribe(this.onNavigation);
  }

  componentWillUnmount() {
    this.subscription.unsubscribe();
    this.subscription2.unsubscribe();
  }
  onNavigation = (newRoute) => {
    if (routesWithoutTabs.includes(newRoute)) {
      this.setState({
        routeWithoutTabs: true
      });
      return;
    }
    this.setState({
      routeWithoutTabs: false
    });
  }

  render() {
    return !this.state.keyBoardOpen && !this.state.routeWithoutTabs ?
      <OriginalBottomTabBar {...this.props} />
      :
      null;
  }
}

export default BottomTabBar;
