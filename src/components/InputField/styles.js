import { StyleSheet } from 'react-native';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';
import colors from '../../global/colors';
import fonts from '../../global/fonts';

export default StyleSheet.create({
  inputContainer: {
    borderRadius: 2,
    backgroundColor: colors.white20,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  input: {
    fontSize: scaleByVertical(16),
    color: 'white',
    flex: 1,
    lineHeight: scaleByVertical(19),
    padding: 0,
    // alignSelf: 'center'
    textAlign: 'left'
  },
  error: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: colors.reddishPink
  },
  arabic: {
    textAlign: 'right'
  }
});
