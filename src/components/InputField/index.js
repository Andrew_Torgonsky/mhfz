import React from 'react';
import { TextInput, View } from 'react-native';
import styles from './styles';
import colors from '../../global/colors';
import { isRTL } from '../../translations';

type Props = {
  style: Object,
  error: boolean,
  before: any,
  after: any,
  inputIndentaion: number
}

export default class extends React.PureComponent<Props> {
  focus = () => {
    this.reference?.focus();
  }
  blur = () => {
    this.reference?.blur();
  }
  render() {
    const {
      style, error, before, after, inputIndentaion, ...rest
    } = this.props;
    return (
      <View style={[styles.inputContainer, style, error && styles.error]}>
        {before}
        <TextInput
          style={[
            styles.input,
            inputIndentaion && { paddingLeft: inputIndentaion },
            isRTL() && styles.arabic
          ]}
          placeholderTextColor={colors.white38}
          keyboardAppearance="dark"
          autoCapitalize="none"
          autoCorrect={false}
          underlineColorAndroid="transparent"
          ref={(ref) => { this.reference = ref }}
          {...rest}
        />
        {after}
      </View>
    );
  }
};
