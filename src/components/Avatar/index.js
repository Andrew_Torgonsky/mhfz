import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import styles from './styles';

type Props = {
  source: Object,
  text: string,
  uri: string
};

export default (props: Props) => (
  <View style={styles.avatarContainer}>
    <View style={styles.avatarShadow}>
      <View style={styles.avatar}>
        {props.text ? <Text style={styles.text}>{props.text}</Text> : null}
        {props.source ? <Image source={{ uri: `file://${props.source}` }} style={styles.avatar} /> : null}
        {props.uri ? <Image source={{ uri: props.uri }} style={styles.avatar} /> : null}
      </View>
    </View>
  </View>
);
