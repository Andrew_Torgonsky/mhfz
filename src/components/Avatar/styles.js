import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  avatarContainer: {
    padding: scaleByVertical(22),
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatarShadow: {
    width: scale(136),
    height: scale(136),
    borderRadius: scale(68),
    backgroundColor: colors.lightBlue21,
    alignItems: 'center',
    justifyContent: 'center'
  },
  avatar: {
    width: scale(120),
    height: scale(120),
    borderRadius: scale(60),
    backgroundColor: colors.darkGreyBlue,
    borderWidth: scale(2),
    borderColor: colors.sky,
    // shadowColor: colors.sky,
    // shadowOffset: {
    //   width: 0,
    //   height: 0
    // },
    // shadowRadius: scale(10),
    // shadowOpacity: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontSize: scaleByVertical(48),
    color: colors.white87,
    textAlign: 'center',
  }
});
