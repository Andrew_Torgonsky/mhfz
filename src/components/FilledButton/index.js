import React from 'react';
import { Text, TouchableOpacity, ActivityIndicator } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../global/colors';
import styles from './styles';

type Props = {
  title: string,
  red: boolean,
  pending: boolean,
  onPress: boolean
}

export default (props: Props) => {
  const {
    title, red, pending, onPress, ...rest
  } = props;
  return (
    <TouchableOpacity
      style={styles.touchable}
      onPress={() => {
        if (pending) return;
        onPress();
      }}
      {...rest}
    >
      <LinearGradient
        colors={red ? [colors.berry, colors.reddishPinkTwo] : [colors.darkSkyBlue, colors.sky]}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        style={[styles.linearGradient, pending && styles.pending]}
      >
        {
          pending ? <ActivityIndicator size="small" color="white" /> : <Text style={[styles.text, red && { color: 'white' }]}>{title}</Text>
        }
      </LinearGradient>
    </TouchableOpacity>

  );
};

