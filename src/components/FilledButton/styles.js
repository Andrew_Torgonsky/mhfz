import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  touchable: {
    flex: 1,
  },
  linearGradient: {
    flex: 1,
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center'
  },
  pending: {
    opacity: 0.5
  },
  text: {
    color: colors.dark,
    fontSize: scaleByVertical(17),
    ...platformSelect({
      fontFamily: fonts.sfUITextMedium,
    }, {
      fontFamily: fonts.robotoMedium
    })
  },
});
