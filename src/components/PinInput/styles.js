import { StyleSheet } from 'react-native';
import { scale, scaleByVertical, wWidth } from '../../utils/scale';
import colors from '../../global/colors';
import fonts from '../../global/fonts';

export default StyleSheet.create({
  container: {
    // flex: 1,
    // height: scaleByVertical(16)
  },
  input: {
    display: 'none'
  },
  pinItems: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  keyboardContainer: {
    marginTop: scaleByVertical(40)
  },
  pinItem: {
    backgroundColor: colors.white20,
    borderRadius: scaleByVertical(8),
    width: scaleByVertical(16),
    height: scaleByVertical(16),
    marginHorizontal: scaleByVertical(8)
  },
  active: {
    backgroundColor: colors.white87
  },
  error: {
    backgroundColor: colors.reddishPink
  }
});
