import React from 'react';
import { View, TextInput, TouchableOpacity } from 'react-native';
import NumericKeyBoard from '../NumericKeyboard';
import styles from './styles';

type Props = {
  error: boolean,
  value: string,
  onChangeText?: (newText: string) => void,
  onFinish?: () => void,
}

class PinInput extends React.PureComponent<Props> {
  onPress = (symbol) => {
    let value = `${this.props.value}`;
    if (value.length >= 4) {
      this.props.onChangeText(this.props.value.slice(0, 4));
      return;
    }
    value = `${this.props.value}${symbol}`;
    this.props.onChangeText(value);
    if (value.length > 3) this.props.onFinish();
  }
  onDelete = () => {
    let value = `${this.props.value}`;
    if (value.length === 0) return;
    value = value.slice(0, value.length - 1);
    this.props.onChangeText(value);
  }
  render() {
    const {
      error, value
    } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.pinItems}>
          <View style={[styles.pinItem, value.length > 0 && styles.active, error && styles.error]} />
          <View style={[styles.pinItem, value.length > 1 && styles.active, error && styles.error]} />
          <View style={[styles.pinItem, value.length > 2 && styles.active, error && styles.error]} />
          <View style={[styles.pinItem, value.length > 3 && styles.active, error && styles.error]} />
        </View>
        <View style={styles.keyboardContainer}>
          <NumericKeyBoard onPress={this.onPress} onDelete={this.onDelete} />
        </View>
      </View>
    );
  }
}

PinInput.defaultProps = {
  onChangeText: () => {},
  onFinish: () => {}
};

export default PinInput;
