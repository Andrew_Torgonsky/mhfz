import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import { scaleByVertical, width, height } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    position: 'absolute',
    width,
    height
  },
  pending: {
    backgroundColor: colors.black50,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  }
});
