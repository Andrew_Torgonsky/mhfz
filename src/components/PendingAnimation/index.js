import React from 'react';
import { connect } from 'react-redux';
import { View, ActivityIndicator } from 'react-native';
import styles from './styles';
import { type PendingType, type ReduxStateType } from '../../types';

type Props = {
  pending: PendingType
}

export const PendingAnimation = (props: Props) => {
  const { pending } = props;
  const keys = Object.keys(pending);
  const show = keys.reduce((accumulator, currentValue) => pending[currentValue] || accumulator, false);
  if (!show) {
    return null;
  }
  return (
    <View
      style={styles.container}
    >
      <View style={styles.pending}>
        <ActivityIndicator size="large" color="white" />
      </View>
    </View>
  );
};

const mapStateToProps = ({ pending }: ReduxStateType) => (
  {
    pending
  }
);

export const PendingAnimationConnected = connect(mapStateToProps)(PendingAnimation);
