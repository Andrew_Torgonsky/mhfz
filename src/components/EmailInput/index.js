import React from 'react';
import { Image } from 'react-native';
import InputField from '../InputField';
import images from '../../global/images';
import { scale } from '../../utils/scale';
import translations from '../../translations';
import styles from './styles';

type Props = {
  style: Object,
  color: string,
}

export default (props: Props) => {
  const icon = (
    <Image
      source={images.mail}
      style={[
        styles.icon,
        props.color && { tintColor: props.color }
      ]}
    />
  );

  const { style, ...other } = props;

  return (
    <InputField
      style={style}
      before={icon}
      inputIndentaion={scale(16)}
      keyboardType="email-address"
      placeholder={translations.t('E_MAIL')}
      {...other}
    />
  );
};
