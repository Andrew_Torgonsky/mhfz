import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  icon: {
    width: scale(24),
    height: scale(24),
    tintColor: colors.white50,
    marginLeft: scale(16)
  },
});
