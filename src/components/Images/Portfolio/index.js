import React from 'react';
import { Image } from 'react-native';
import images from '../../../global/images';
import { scale } from '../../../utils/scale';

const style = {
  width: scale(24),
  height: scale(20),
};
const activeStyle = {
  tintColor: 'white'
};
type Props = {
  active: boolean
}
export default ({ active }: Props) => (
  <Image source={images.portfolio} style={[style, active && activeStyle]} />
);
