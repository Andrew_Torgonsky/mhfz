import React from 'react';
import { Image } from 'react-native';
import images from '../../../global/images';
import { scale } from '../../../utils/scale';
import { isRTL } from '../../../translations';


export default () => {
  const style = {
    // width: scale(23),
    height: scale(44),
    resizeMode: 'contain',
    marginLeft: scale(16),
    transform: isRTL() ? [{ rotate: '180deg' }] : []
  };
  return <Image source={images.arrowleft} style={style} />;
};

