import React from 'react';
import { Image } from 'react-native';
import images from '../../../global/images';
import { scale } from '../../../utils/scale';

const style = {
  tintColor: 'white',
  width: scale(27),
  height: scale(27),
  marginRight: scale(18)
};

export default () => (
  <Image
    source={images.share}
    style={style}
  />
);
