import { StyleSheet } from 'react-native';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';
import colors from '../../global/colors';
import fonts from '../../global/fonts';

export default StyleSheet.create({
  imageContainer: {
    alignItems: 'center'
  },
  image: {
    width: scale(100),
    height: scale(111)
  },
  image2: {
    width: scale(116),
    height: scale(111)
  },
  title: {
    textAlign: 'center',
    fontSize: scaleByVertical(16),
    color: 'white',
    marginTop: scaleByVertical(24),
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  text: {
    marginTop: scaleByVertical(16)
  },
  popupContainerButton: {
    marginTop: scaleByVertical(24),
    height: scaleByVertical(40)
  }
});
