import React from 'react';
import { View, Text, Image } from 'react-native';
import FilledButton from '../FilledButton';
import Modal from '../Modal';
import styles from './styles';
import translations from '../../translations';
import images from '../../global/images';
import globalStyles from '../../global/styles';

type Props = {
  onPress: () => void;
  type: 1 | 2;
};

const BackupCreatedModal = (props: Props) => (
  <Modal>
    <View style={styles.imageContainer}>
      {
        props.type < 2 ?
          <Image
            source={images.openLock}
            style={styles.image}
          /> :
          <Image
            source={images.roundCheck}
            style={styles.image2}
          />
      }
    </View>
    {
      props.type < 2 ?
        <Text style={styles.title}>{translations.t('WAL_BAC')}</Text> :
        <Text style={styles.title}>{translations.t('WAL_BAC_CR')}</Text>
    }
    <Text style={[globalStyles.greyText, styles.text]}>{translations.t('SAVE_Y_COMB')}</Text>
    <Text style={[globalStyles.greyText, styles.text]}>{translations.t('TAKE_COMB')}</Text>
    <View style={styles.popupContainerButton}>
      <FilledButton
        title={props.type < 2 ? translations.t('CONTINUE') : translations.t('UNDERSTOOD')}
        onPress={props.onPress}
      />
    </View>
  </Modal>
);

export default BackupCreatedModal;

