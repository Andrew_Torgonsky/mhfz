// @flow

import React from 'react';
import { View, TouchableOpacity, Text, Image } from 'react-native';
import styles from './styles';
import images from '../../global/images';
import { isRTL } from '../../translations';

type Props = {
  onPress: (symbol: string) => void,
  onDelete: () => void,
  error: boolean
}

const NumericKeyboard = (props: Props) => {
  const buttonRows = [[1,2,3],[4,5,6],[7,8,9],[null,0,'del']];

  return (
    <View style={styles.container}>
      {
        buttonRows.map((buttonRow, index) => (
          <View style={styles.row} key={index}>
            {
              buttonRow.map((button, index) => (
                <View style={styles.column} key={index}>
                  <TouchableOpacity onPress={() => {
                    if (typeof button === 'number') props.onPress(`${button}`);
                    if (button === 'del') props.onDelete();
                  }}>
                    <View style={[styles.button, props.error && styles.errorBorder, typeof button !== 'number' && styles.noBorder ]}>
                      {
                        button === 'del' ?
                          <Image source={images.delete} style={[styles.delIcon, isRTL() ? { transform: [{ rotate: '180deg' }] } : {}]} /> :
                          <Text style={[styles.text, props.error && styles.errorColor ]}>{button}</Text>
                      }
                    </View>
                  </TouchableOpacity>
                </View>
              ))
            }
          </View>
        ))
      }
    </View>
  );
};

NumericKeyboard.defaultProps = {
  onPress: (v) => { console.log(v) },
  onDelete: () => { console.log('onDelete') }
};

export default NumericKeyboard;
