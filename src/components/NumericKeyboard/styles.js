import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    alignItems: 'center'
  },
  row: {
    flexDirection: 'row',
    marginVertical: scale(10)
  },
  column: {
    marginHorizontal: scale(10)
  },
  button: {
    width: scale(60),
    height: scale(60),
    borderRadius: scale(30),
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: colors.sky,
    justifyContent: 'center',
    alignItems: 'center',
  },
  errorBorder: {
    borderColor: colors.reddishPink
  },
  noBorder: {
    borderColor: 'transparent'
  },
  text: {
    color: colors.sky,
    fontSize: scaleByVertical(20),
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  errorColor: {
    color: colors.reddishPink
  },
  delIcon: {
    height: scale(25),
    resizeMode: 'contain'
  }
});
