import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  touchable: {
    height: scaleByVertical(32),
    borderRadius: 6,
    borderWidth: 1,
    borderStyle: 'solid',
    borderColor: colors.sky,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: scale(5),
  },
  transparentBorder: {
    borderColor: 'transparent',
  },
  transparentColor: {
    color: 'transparent',
  },
  text: {
    color: colors.sky,
    fontSize: scaleByVertical(16),
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  }
});
