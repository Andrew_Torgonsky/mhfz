// @flow

import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import styles from './styles';
import { type RandomWordType } from '../../types';

type Props = {
  word: RandomWordType,
  onPress: () => void
}

export default ({ word, onPress, ...rest }: Props) => (
  <TouchableOpacity
    onPress={() => {
      if (!word.visible || !onPress) {
        return;
      }
      onPress();
    }}
    {...rest}
  >
    <View
      style={[styles.touchable, word && !word.visible && styles.transparentBorder]}
    >
      <Text style={[styles.text, !word.visible && styles.transparentColor]}>{word && word.text ? word.text : ''}</Text>
    </View>
    
  </TouchableOpacity>
);
