import React from 'react';
import { View, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import BackgroundWrapper from '../../components/BackgroundWrapper';
import TextButton from '../../components/TextButton';
import SmsCode from '../../components/SmsCode';
import translations from '../../translations';
import { ActionCreators } from '../../redux/actions';
import styles from './styles';
import globalStyles from '../../global/styles';

type Props = {
  onPress: () => void,
  timestamp: number
}

export default class RetrySendButton extends React.PureComponent<Props> {
  state = {
    remaining: 60
  }
  componentWillMount() {
    this.interval = setInterval(() => {
      const remaining = 60000 - (Date.now() - this.props.timestamp);
      this.setState({
        remaining: Math.round(remaining/1000)
      })
    }, 1000);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={globalStyles.greyText}>
          {translations.t('DT_REC_CODE')}
        </Text>
        {
          this.state.remaining <= 0 ?
            <TextButton
              title={translations.t('RETRY_SENDING')}
              onPress={this.props.onPress}
            /> :
            <View style={{ flexDirection: 'row' }}>
              <Text style={globalStyles.greyText}>
                {translations.t('YOU_CAN_ASK')}
              </Text>
              <Text style={globalStyles.greyText}>
                {this.state.remaining}
              </Text>
              <Text style={globalStyles.greyText}>
                {translations.t('S')}
              </Text>
            </View> 
        }
      </View>
    );
  }
}