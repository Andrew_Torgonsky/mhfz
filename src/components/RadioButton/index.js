import React from 'react';
import { Text, TouchableOpacity, Image } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import colors from '../../global/colors';
import images from '../../global/images';
import styles from './styles';

type Props = {
  active: boolean,
  children: any
}

export default ({
  children, active, ...rest
}: Props) => (
  <TouchableOpacity
    style={styles.touchable}
    {...rest}
  >
    <Image
      source={active ? images.on : images.off}
      style={styles.icon}
    />
    {children}
  </TouchableOpacity>
);

