import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { images } from '../../global';
import styles from './styles';

type Props = {
	active: boolean
};

function Toggle (props: Props) {
	return (
		<View style={styles.container}>
			<Image
				source={
					props.active ?
					images.switchOn :
					images.switchOff
				}
			/>
		</View>
	);
}

export default Toggle;