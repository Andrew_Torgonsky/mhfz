import React from 'react';
import { Text } from 'react-native';
import InputField from '../InputField';
import { scale } from '../../utils/scale';
import translations from '../../translations';
import styles from './styles';
import type { CryptoCurrencyType } from '../../types';
import { switchAbbrAndName } from '../../utils/currencies';

type Props = {
  currency: CryptoCurrencyType,
  value: string,
  onChangeText: (value: string) => void
}

export default (props: Props) => {
  const {
    currency, onChangeText, value, ...rest
  } = props;
  return (
    <InputField
      placeholder={translations.t('AMOUNT')}
      inputIndentaion={scale(16)}
      after={<Text style={styles.currency}>{switchAbbrAndName(currency)}</Text>}
      keyboardType="decimal-pad"
      value={value ? `${value}` : ''}
      onChangeText={val => onChangeText(val.replace(/[^0-9]/, '.'))}
      {...rest}
    />
  );
};
