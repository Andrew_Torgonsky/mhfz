import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  currency: {
    marginRight: scale(16),
    color: colors.white87,
    fontSize: scaleByVertical(16),
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  }
});
