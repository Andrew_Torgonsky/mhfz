import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import styles from './styles';

type Props = {
  title: string,
  onPress: () => void,
  style: string
}

export default ({ title, onPress, style }: Props) => (
  <TouchableOpacity
    onPress={onPress}
    style={style}
  >
    <Text style={styles.text}>{title}</Text>
  </TouchableOpacity>
);
