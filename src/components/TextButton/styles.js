import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  text: {
    color: colors.sky,
    fontSize: scaleByVertical(14),
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  }
});
