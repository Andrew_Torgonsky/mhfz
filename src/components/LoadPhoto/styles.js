import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  shadow: {
    justifyContent: 'flex-end',
    paddingHorizontal: scale(16),
    width: '100%',
    height: '100%',
    backgroundColor: colors.black50
  },
  container: {
    backgroundColor: colors.grey
  },
  closeContainer: {
    alignItems: 'flex-end',
    marginTop: scaleByVertical(8),
    paddingHorizontal: scale(8),
  },
  close: {
    width: scale(16),
    height: scale(16),
    tintColor: colors.white87
  },
  menuItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: scale(16),
    height: scaleByVertical(44)
  },
  item1: {
    marginTop: scaleByVertical(16)
  },
  item2: {
    marginTop: scaleByVertical(8),
    marginBottom: scaleByVertical(16)
  },
  icon: {
    tintColor: colors.white87
  },
  text: {
    marginLeft: scale(24),
    color: colors.white87
  }
});
