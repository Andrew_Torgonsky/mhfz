import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Modal, View, Text, Image, TouchableOpacity } from 'react-native';
import styles from './styles';
import images from '../../global/images';
import translations from '../../translations';
import { ActionCreators } from '../../redux/actions';

type Props = {
  onGalleryPress: () => void,
  onCameraPress: () => void,
  onClose: () => void
}

const LoadPhoto = (props: Props) => (
  <Modal
    transparent
    onRequestClose={props.onClose}
  >
    <TouchableOpacity style={styles.shadow} onPress={props.onClose} activeOpacity={0}>
      <View style={styles.container}>
        <View style={styles.closeContainer}>
          <TouchableOpacity onPress={props.onClose}>
            <Image source={images.close} style={styles.close} />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          style={[styles.menuItem, styles.item1]}
          onPress={props.onCameraPress}
        >
          <Image source={images.camera} style={styles.icon} />
          <Text style={styles.text}>{translations.t('TAKE_PHOTO')}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.menuItem, styles.item2]}
          onPress={props.onGalleryPress}
        >
          <Image source={images.gallery} style={styles.icon} />
          <Text style={styles.text}>{translations.t('CHOOSE_F_GALLERY')}</Text>
        </TouchableOpacity>
      </View>
    </TouchableOpacity>
  </Modal>
);

export default LoadPhoto;
