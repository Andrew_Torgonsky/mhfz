import React from 'react';
import RadialGradient from 'react-native-radial-gradient';
import { ScrollView, SafeAreaView } from 'react-native';
import styles from './styles';
import colors from '../../global/colors';
import { AlertPanelConnected as AlertPanel } from '../AlertPanel';
import { width, height } from '../../utils/scale';
import { keyboardOpen$ } from '../../api/keyboard';

type Props = {
  children: any,
  allwaysScroll: boolean,
  noScroll: boolean,
  noScrollWithKeyboard: boolean,
  noGradient: boolean,
  onScroll?: (e: any) => void
}

export default class extends React.PureComponent<Props> {
  state = {
    keyBoardOpen: false
  }
  componentWillMount() {
    this.subscription = keyboardOpen$.subscribe(state => this.setState({ keyBoardOpen: state }));
  }
  componentWillUnmount() {
    this.subscription.unsubscribe();
  }
  render() {
    let contentContainerStyle = {};
    if (this.props.noScroll && !this.state.keyBoardOpen || this.props.noScrollWithKeyboard) {
      contentContainerStyle = { flex: 1 };
    } else if (this.props.noScroll && this.state.keyBoardOpen) {
      contentContainerStyle = { minHeight: '100%' };
    } else {
      contentContainerStyle = {};
    }
    return (
      <SafeAreaView style={{ flex: 1, backgroundColor: colors.darkGreyBlueTwo }}>
        <ScrollView
          contentContainerStyle={contentContainerStyle}
          style={{ flex: 1 }}
          alwaysBounceVertical={this.props.allwaysScroll || false}
          keyboardShouldPersistTaps="handled"
          onScroll={this.props.onScroll}
          scrollEventThrottle={160}
        >
          {this.props.children}
        </ScrollView>
        <AlertPanel />
      </SafeAreaView>
    );
  }
}
