// @flow

import React from 'react';
import { View } from 'react-native';
import styles from './styles';
import WordButton from '../WordButton';
import { type RandomWordType } from '../../types';

type Props = {
  words: RandomWordType[];
  onPress: (word: string) => void
}

const WordGrid = (props: Props) => {
  const four = [...Array(4)];
  const onPress = word => () => {
    props.onPress(word && word.text ? word.text : '');
  };
  return (
    <View style={styles.container}>
      <View style={[styles.row, styles.firstRow]}>
        {four.map((val, index) => <WordButton key={index} word={props.words[index]} onPress={onPress(props.words[index])} />)}
      </View>
      <View style={styles.row}>
        {four.map((val, index) => <WordButton key={index + 4} word={props.words[index + 4]} onPress={onPress(props.words[index + 4])} />)}
      </View>
      <View style={[styles.row, styles.lastRow]}>
        {four.map((val, index) => <WordButton key={index + 8} word={props.words[index + 8]} onPress={onPress(props.words[index + 8])} />)}
      </View>
    </View>
  );
};

export default WordGrid;
