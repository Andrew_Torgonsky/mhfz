import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  firstRow: {
    marginBottom: scaleByVertical(16)
  },
  lastRow: {
    marginTop: scaleByVertical(16)
  },
});
