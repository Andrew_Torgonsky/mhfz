import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  topTabNavigator: {
    backgroundColor: colors.darkTwo,
    height: scaleByVertical(40),
  },
  topTabIndicator: {
    backgroundColor: colors.sky,
  },
  topTab: {
    height: scaleByVertical(40),
  },
  label: {
    color: 'white',
    fontSize: scaleByVertical(14),
    ...platformSelect({
      fontFamily: fonts.sfUITextSemibold,
      height: scaleByVertical(14),
    }, {
      fontFamily: fonts.robotoMedium
    })
  },
  // custom tabs for android
	container: {
		backgroundColor: colors.darkTwo,
    height: scaleByVertical(42)
  },
  tabs: {
    height: scaleByVertical(40),
    flexDirection: 'row'
  },
  activeLabel: {
    color: 'white'
  },
  touchable: {
    flex: 1
  },
  tab: {
    flex: 1,
    height: scaleByVertical(40),
    justifyContent: 'center',
    alignItems: 'center'
  },
  activeTab: {
    borderBottomWidth: 2,
    borderBottomColor: colors.sky
  }
});
