import React from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import R from 'ramda';
import { LineChart } from 'react-native-svg-charts';
import * as shape from 'd3-shape';
import styles from './styles';
import colors from '../../global/colors';
import { scaleByVertical } from '../../utils/scale';
import type { Point } from '../../types';
import translations, { isRTL } from '../../translations';

type Props = {
  data: Point[];
};

const mockData: Point[] = '103232203351'.split('').map((val, index) => ({ x: index, y: +val }));

const Chart = ({ data }: Props) => {
  const dataToDisplay = R.pipe(
    R.ifElse(R.isEmpty, R.always(mockData), R.identity),
    R.ifElse(isRTL, R.map(point => ({ ...point, x: -point.x })), R.identity)
  )(data);
  return (
    <View style={styles.chartContainer}>
      <LineChart
        style={styles.chart}
        data={dataToDisplay}
        xAccessor={({ item }) => item.x}
        yAccessor={({ item }) => item.y}
        contentInset={{ top: 20, bottom: 20 }}
        curve={shape.curveMonotoneX}
        svg={{
          stroke: colors.aquamarine,
          strokeOpacity: data.length === 0 ? 0.1 : 1,
          strokeWidth: scaleByVertical(3)
        }}
      />
      {
        data.length === 0 ?
          <Text style={styles.noDataText}>{translations.t('NO_CHART')}</Text> :
          null
      }
    </View>
  );
}

export default Chart;
