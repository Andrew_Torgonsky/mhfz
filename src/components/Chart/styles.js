import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  chartContainer: {
    height: scaleByVertical(125),
    position: 'relative'
  },
  chart: {
    height: scaleByVertical(125),
  },
  // noDataContainer: {
  //   justifyContent: 'center',
  //   alignItems: 'center'
  // },
  noDataText: {
    position: 'absolute',
    width: '100%',
    top: scaleByVertical(55),
    // marginTop: scaleByVertical(4),
    lineHeight: scaleByVertical(20),
    textAlign: 'center',
    color: colors.white50,
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  }
});
