import { StyleSheet } from 'react-native';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';
import colors from '../../global/colors';
import fonts from '../../global/fonts';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: scaleByVertical(16)
  },
  icon: {
    tintColor: colors.sky,
    width: scaleByVertical(16),
    height: scaleByVertical(16),
    marginRight: scale(8)
  },
  text: {
    color: colors.sky,
    fontSize: scaleByVertical(14),
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  }
});
