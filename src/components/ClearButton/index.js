import React from 'react';
import { Image, TouchableOpacity, View, Text } from 'react-native';
import styles from './styles';
import InputField from '../InputField';
import images from '../../global/images';
import { scale } from '../../utils/scale';
import translations from '../../translations';

type Props = {
}

export default (props: Props) => (
  <View style={styles.container}>
    <TouchableOpacity
      style={styles.container}
      {...props}
    >
      <Image
        source={images.close}
        style={styles.icon}
      />
      <Text style={styles.text}>{translations.t('CLEAR')}</Text>
    </TouchableOpacity>
  </View>
);

