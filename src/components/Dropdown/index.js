import React from 'react';
import { TouchableOpacity, Text, Image, View } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import images from '../../global/images';
import { platformSelect, scaleByVertical } from '../../utils/scale';
import styles from './styles';

type Props = {
  options: string[];
  value: number;
  onChange: (value: number) => void;
  dropdownShift: number;
};

class Dropdown extends React.PureComponent<Props> {
  state = {
    dropdownOpen: false
  }
  render() {
    const {
      options, value, onChange, dropdownShift
    } = this.props;
    return platformSelect(
      (
        <View style={styles.container}>
          <TouchableOpacity
            onPress={() => {
              this.setState({
                dropdownOpen: !this.state.dropdownOpen
              });
            }}
          >
            <View style={styles.button}>
              <Text style={styles.text}>{options[value]}</Text>
              <Image
                source={images.dropdown}
                style={styles.icon}
              />
            </View>
          </TouchableOpacity>
          {
            this.state.dropdownOpen ?
              <View style={styles.dropdown}>
                {options.map((item, index) => (
                  <TouchableOpacity
                    key={index}
                    onPress={() => {
                      onChange(index);
                      this.setState({
                        dropdownOpen: false
                      });
                    }}
                  >
                    <View style={[styles.dropdownItem, index === value && styles.activeItem]}>
                      <Text style={styles.text}>{item}</Text>
                    </View>
                  </TouchableOpacity>
                ))}
              </View>
              : null
          }
        </View>
      ), (
        <ModalDropdown
          renderRow={(option, index, isSelected) => (
            <View style={[styles.dropdownItem, isSelected && styles.activeItem]}>
              <Text style={styles.text}>{option}</Text>
            </View>
          )}
          defaultIndex={value}
          defaultValue={options[value]}
          renderSeparator={() => <View />}
          style={styles.btn}
          textStyle={styles.btnText}
          dropdownStyle={[
            styles.modDrop,
            dropdownShift ?
              { transform: [{ translateX: dropdownShift }, { translateY: scaleByVertical(6) }] } :
              { transform: [{ translateY: scaleByVertical(6) }] }
          ]}
          options={options}
          onSelect={val => onChange(+val)}
        />
      )
    );
  }
}

Dropdown.defaultProps = {
  onChange: () => { },
};

export default Dropdown;
