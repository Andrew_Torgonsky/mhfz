import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect, width } from '../../utils/scale';

export default StyleSheet.create({
  container: {
  },
  button: {
    borderColor: colors.white50,
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 4,
    height: scaleByVertical(24),
    width: scale(92),
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  text: {
    fontSize: scaleByVertical(14),
    color: 'white',
    flex: 1,
    textAlign: 'center',
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  icon: {
    width: scale(8),
    marginRight: scale(8),
    height: scaleByVertical(16)
  },
  dropdown: {
    position: 'absolute',
    width: scale(92),
    top: scaleByVertical(28),
    borderRadius: 4,
    elevation: 5,
    backgroundColor: colors.charcoalGrey,
    shadowColor: colors.black26,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: scale(15),
    shadowOpacity: 1,
    zIndex: 110,
  },
  dropdownItem: {
    height: scaleByVertical(44),
    alignItems: 'center',
    flexDirection: 'row',
  },
  activeItem: {
    backgroundColor: colors.dark
  },
  // for android
  btn: {
    borderColor: colors.white50,
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 4,
    height: scaleByVertical(24),
    width: scale(92),
  },
  btnText: {
    fontSize: scaleByVertical(14),
    color: 'white',
    fontFamily: fonts.robotoRegular,
    textAlign: 'center',
  },
  modDrop: {
    width: scale(92),
    backgroundColor: colors.charcoalGrey,
    borderWidth: 0,
    elevation: 6,
    height: scaleByVertical(44 * 3) // for 3 items
  }
});
