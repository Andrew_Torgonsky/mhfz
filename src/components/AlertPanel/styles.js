import { StyleSheet, Platform } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    height: scaleByVertical(48),
    backgroundColor: 'red',
    flexDirection: 'row',
    alignItems: 'center'
  },
  icon: {
    width: scale(24),
    height: scale(24),
    marginLeft: scale(16),
    tintColor: 'white'
  },
  textContainer: {
    flex: 1,
    height: scaleByVertical(48),
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: {
    marginLeft: scale(16),
    fontSize: scaleByVertical(14),
    color: 'white',
    ...platformSelect({
      fontFamily: fonts.sfUITextMedium,
    }, {
      fontFamily: fonts.robotoMedium
    }),
    textAlign: 'left'
  },
  arabic: {
    ...platformSelect({
      writingDirection: 'rtl'
    }, {})
  },
  touchable: {
    width: scaleByVertical(48),
    height: scaleByVertical(48),
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon2: {
    width: scale(16),
    height: scale(16),
    tintColor: 'white'
  },
});
