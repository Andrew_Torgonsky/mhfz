// @flow

import React from 'react';
import { Text, TouchableOpacity, Image, View } from 'react-native';
import { bindActionCreators } from 'redux';
import LinearGradient from 'react-native-linear-gradient';
import { connect } from 'react-redux';
import colors from '../../global/colors';
import images from '../../global/images';
import styles from './styles';
import { ActionCreators } from '../../redux/actions';
import type { AlertStateType, AlertType, ReduxStateType } from '../../types';
import translations, { isRTL } from '../../translations';

type Props = {
  alertState: AlertStateType,
  changeAlert: (text: string, type: AlertType) => void
}

export const AlertPanel = (props: Props) => {
  if (!props.alertState || !props.alertState.alertText || !props.alertState.alertType) {
    return null;
  }
  const { alertState: { alertText, alertType }, changeAlert } = props;
  const gradientColors = alertType === 'green' ? [colors.darkSeaGreen, colors.brightTeal] : [colors.berry, colors.reddishPinkTwo];
  return (
    <LinearGradient
      colors={gradientColors}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 0 }}
      style={styles.container}
    >
      <Image
        source={alertType === 'green' ? images.check : images.error}
        style={styles.icon}
      />
      <View style={styles.textContainer}>
        <Text style={[styles.text, isRTL() && styles.arabic]}>{alertText}</Text>
      </View>
      <TouchableOpacity
        style={styles.touchable}
        onPress={() => changeAlert('', '')}
      >
        <Image
          source={images.close}
          style={styles.icon2}
        />
      </TouchableOpacity>
    </LinearGradient>
  );
};

const mapStateToProps = ({ alertState }: ReduxStateType) => (
  {
    alertState
  }
);
const mapDispatchToProps = dispatch => bindActionCreators({
  changeAlert: ActionCreators.changeAlert
}, dispatch);

export const AlertPanelConnected = connect(mapStateToProps, mapDispatchToProps)(AlertPanel);
