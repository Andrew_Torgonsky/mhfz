// @flow

import React, { Component } from 'react';
import { of, concat, Subscription } from 'rxjs';
import { pairwise, filter, pluck, delay, mergeMap, map } from 'rxjs/operators';
import { Text, View, TouchableOpacity, Animated as A } from 'react-native';
import { TabBar } from 'react-native-tab-view';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import type { WalletsStatType } from '../../types';
import { Scenes, navigationChanges$ } from '../../navigation/shared';
import styles from './styles';
import colors from '../../global/colors';
import { width, isIOS, isAndroid } from '../../utils/scale';
import { isRTL } from '../../translations';

type Props = {
  walletsStat: WalletsStatType;
  navigationState: any,
  navigation: any,
  getLabelText: any,
  panX: A.Value,
  offsetX: A.Value,
  jumpTo: any,
  position: any,
  layout: any,
  useNativeDriver: any
}

type State = {
  position: any
}

class TopTabBarForWallets extends React.Component<Props, State> {
  tabWidth: number;
  subscription: Subscription;
  constructor(props: Props) {
    super(props);
    const amountOfTabs = props.navigationState.routes.length;
    this.tabWidth = width / amountOfTabs;
    const position = isRTL() && isAndroid ?
      A.add(A.multiply(A.add(props.panX, props.offsetX), -(this.tabWidth / width)), this.tabWidth * (amountOfTabs - 1)) :
      A.multiply(A.add(props.panX, props.offsetX), -(this.tabWidth / width));
    this.state = {
      position
    };
    // crutch for updating position of indicator on navigation changes because navigator doesn't do it properly
    const routeNames = props.navigationState.routes.map(route => route.routeName);
    const adjustIndicatorPosition = (index: number) => {
      props.offsetX.setValue(index * width * (-1));
      props.panX.setValue(0);
    }
    const adjustIndicatorPositionAndroidRtl = (index: number) => {
      props.offsetX.setValue((amountOfTabs - 1 - index) * width);
      props.panX.setValue(0);
    }
    this.subscription = navigationChanges$.pipe(
      pairwise(),
      filter(routes => !routeNames.includes(routes[0]) && routeNames.includes(routes[1])),
      pluck(1),
      map(route => routeNames.findIndex(el => el === route)),
      mergeMap(index => concat(
        of(index),
        of(index).pipe(
          delay(30)
        ),
        of(index).pipe(
          delay(30)
        ),
      ))
    ).subscribe(isRTL() && isAndroid ? adjustIndicatorPositionAndroidRtl : adjustIndicatorPosition);
  }
  componentWillUnmount() {
    this.subscription.unsubscribe();
  }
  render() {
    const { walletsStat } = this.props;
    return (
      <View style={styles.container}>
        <View style={[styles.tabs]}>
          {this.props.navigationState.routes.map((el, index) => {
            const { routeName } = el;
            let label;
            let dot;
            let color;
            switch (routeName) {
              case Scenes.BtcWallet:
                label = 'BTC';
                dot = walletsStat.bitcoin.alertText || walletsStat.bitcoin.newTx ? true : false;
                color = walletsStat.bitcoin.alertColor;
                break;
              case Scenes.EthWallet:
                label = 'ETH';
                dot = walletsStat.ethereum.alertText || walletsStat.ethereum.newTx ? true : false;
                color = walletsStat.ethereum.alertColor;
                break;
              case Scenes.XrpWallet:
                label = 'XRP';
                dot = walletsStat.ripple.alertText || walletsStat.ripple.newTx ? true : false;
                color = walletsStat.ripple.alertColor;
                break;
              default:
                return null;
            }
            return (
              <TouchableOpacity
                style={styles.touchable}
                onPress={() => this.props.jumpTo(el.key)}
                key={routeName}
              >
                <View style={[styles.tab]}>
                  <A.Text style={[styles.label]}>{label}</A.Text>
                  {dot ? <View style={[styles.topTabLabelDot, { backgroundColor: color === 'red' ? colors.reddishPink : colors.sunshineYellow }]} /> : null}
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
        <View style={{ position: 'relative', height: 2 }}>
          <A.View style={{
            position: 'absolute',
            top: 0,
            left: this.state.position,
            width: this.tabWidth,
            height: 2,
            backgroundColor: colors.sky,
          }} />
        </View>
      </View>
    );
  }
}

const mapStateToProps = ({
  walletsStat
}) => ({
  walletsStat
});


export const TopTabBarForWalletsConnected = connect(mapStateToProps)(TopTabBarForWallets);
