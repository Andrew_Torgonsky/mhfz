import React from 'react';
import { Image } from 'react-native';
import styles from './styles';
import InputField from '../InputField';
import images from '../../global/images';
import { scale } from '../../utils/scale';

type Props = {
}

export default (props: Props) => {
  const beforeElements = (
    <Image
      source={images.search}
      style={styles.search}
    />
  );
  return (
    <InputField
      before={beforeElements}
      inputIndentaion={scale(16)}
      {...props}
    />
  );
};
