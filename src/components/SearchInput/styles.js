import { StyleSheet } from 'react-native';
import { scale, scaleByVertical } from '../../utils/scale';
import colors from '../../global/colors';
import fonts from '../../global/fonts';

export default StyleSheet.create({
  search: {
    width: scale(24),
    height: scale(24),
    tintColor: colors.white38,
    marginLeft: scale(16)
  },
});
