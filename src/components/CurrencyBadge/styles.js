import { StyleSheet } from 'react-native';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';
import colors from '../../global/colors';
import fonts from '../../global/fonts';

export default StyleSheet.create({
  container: {
    backgroundColor: colors.white20,
    borderRadius: 2,
    height: scaleByVertical(64),
    flexDirection: 'row'
  },
  iconContainer: {
    justifyContent: 'center'
  },
  icon: {
    marginLeft: scale(12),
    width: scale(40),
    height: scale(40)
  },
  infoContaner: {
    flex: 1,
    justifyContent: 'space-between',
    paddingVertical: scaleByVertical(12),
    paddingHorizontal: scale(12)
  },
  infoLine: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  title: {
    fontSize: scaleByVertical(14),
    color: colors.white87,
    ...platformSelect({
      fontFamily: fonts.sfProTextSemibold,
    }, {
      fontFamily: fonts.robotoMedium
    })
  },
  fiat: {
    fontSize: scaleByVertical(14),
    color: colors.white87,
    ...platformSelect({
      fontFamily: fonts.sfProTextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  balance: {
    fontSize: scaleByVertical(14),
    color: colors.white50,
    ...platformSelect({
      fontFamily: fonts.sfProTextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  differencePlus: {
    fontSize: scaleByVertical(14),
    color: colors.brightTeal,
    ...platformSelect({
      fontFamily: fonts.sfProTextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  differenceMinus: {
    fontSize: scaleByVertical(14),
    color: colors.reddishPink,
    ...platformSelect({
      fontFamily: fonts.sfProTextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  alert: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: scaleByVertical(-4),
  },
  alertDot: {
    position: 'absolute',
    top: scaleByVertical(4),
    left: scale(4),
    width: scale(12),
    height: scale(12),
    borderRadius: scale(6)
  },
  alertText: {
    fontSize: scaleByVertical(14),
    marginLeft: scale(6),
    ...platformSelect({
      fontFamily: fonts.sfProTextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  alertIcon: {
    width: scale(24),
    height: scale(24)
  },
  noInfo: {
    fontSize: scaleByVertical(14),
    color: colors.white38,
    ...platformSelect({
      fontFamily: fonts.sfProTextBold
    }, {
      fontFamily: fonts.robotoBold
    })
  },
});
