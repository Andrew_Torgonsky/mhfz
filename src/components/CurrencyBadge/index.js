// @flow

import React from 'react';
import { Image, TouchableOpacity, View, Text } from 'react-native';
import styles from './styles';
import images from '../../global/images';
import colors from '../../global/colors';
import { inFiat, getDailyChange } from '../../utils/currencies';
import type { FiatCurrencyType, AlertColorType, WalletsStatItemType } from '../../types';

type Props = {
  icon: string,
  title: string,
  defaultCurrency: FiatCurrencyType,
  onPress: () => void,
  walletStat: WalletsStatItemType
}

export default (props: Props) => {
  const { walletStat: { balance, rate, newTx, alertText } } = props;
  const alertColor = props.walletStat.alertColor === 'red' ? colors.reddishPink : colors.sunshineYellow;
  const dailyChange = getDailyChange(props.walletStat);
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={props.onPress}
    >
      <View style={styles.iconContainer}>
        {
          alertText || newTx ?
            <View style={[styles.alertDot, { backgroundColor: alertColor }]} />
            : null
        }
        <Image
          source={props.icon}
          style={styles.icon}
        />
      </View>
      <View style={styles.infoContaner}>
        <View style={styles.infoLine}>
          <Text style={styles.title}>{props.title}</Text>
          {
            !alertText ?
              <Text style={styles.fiat}>{inFiat(balance, rate, props.defaultCurrency)}</Text>
              : null
          }
        </View>
        {
          !alertText ?
            <View style={styles.infoLine}>
              <Text style={styles.balance}>{balance}</Text>
              {
                dailyChange !== Infinity ?
                  <Text style={dailyChange < 0 ? styles.differenceMinus : styles.differencePlus}>{dailyChange > 0 ? '+' : null}{dailyChange}%</Text> :
                  null
              }
            </View> :
            <View style={styles.alert}>
              <Image
                source={images.error}
                style={[styles.alertIcon, { tintColor: alertColor }]}
              />
              <Text style={[styles.alertText, { color: alertColor }]}>{alertText}</Text>
            </View>
        }
      </View>
    </TouchableOpacity>
  );
};

