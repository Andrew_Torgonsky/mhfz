import { StyleSheet } from 'react-native';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';
import colors from '../../global/colors';
import fonts from '../../global/fonts';

export default StyleSheet.create({
  container: {
    height: scaleByVertical(288),
    flexDirection: 'row',
  },
  row: {
    height: scaleByVertical(32),
    marginBottom: scaleByVertical(16),
    flexDirection: 'row'
  },
  columnLeft: {
    flex: 1,
    marginRight: scale(14)
  },
  columnRight: {
    flex: 1,
    marginLeft: scale(14)
  },
  labelContainer: {
    width: scale(20),
    marginRight: scale(8),
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  label: {
    color: colors.coolGrey,
    fontSize: scaleByVertical(16),
    ...platformSelect({
      fontFamily: fonts.sfProTextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  inputContainer: {
    height: scaleByVertical(32),
    flex: 1
  },
  clearContainer: {
    marginTop: scaleByVertical(8)
  }
});
