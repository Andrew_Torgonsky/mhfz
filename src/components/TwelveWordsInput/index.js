import React from 'react';
import { View, Text } from 'react-native';
import styles from './styles';
import InputField from '../InputField';
import { scale } from '../../utils/scale';
import ClearButton from '../ClearButton';

type Props = {
  value: string[];
  onChange: (newText: string, index: number) => void;
  onClear: () => void;
  editable: boolean;
  clearButton: boolean;
  onFocus: () => void;
}

export default class extends React.PureComponent<Props> {
  onChangeText = index => (newText) => {
    if (!this.props.onChange) {
      return;
    }
    this.props.onChange(newText, index);
  };
  clearAll = () => {
    if (!this.props.onClear) {
      return;
    }
    this.props.onClear();
  };
  onSubmit = (index) => () => {
    if (index < 11) {
      this.inputRefs[index + 1]?.focus();
    }
  }
  inputRefs = [];
  render() {
    const { value = [], editable = true, clearButton = true } = this.props;
    const elements = [...Array(6)];
    const columns = ['Left', 'Right'];
    return (
      <View>
        <View style={styles.container}>
          {
            columns.map((col) => (
              <View style={styles[`column${col}`]} key={col}>
                {
                  elements.map((val, index_) => {
                    const index = col === 'Right' ? index_ + 6 : index_;
                    return (
                      <View style={styles.row} key={index}>
                        <View style={styles.labelContainer}>
                          <Text style={styles.label}>{index + 1}</Text>
                        </View>
                        <View style={styles.inputContainer}>
                          <InputField
                            value={value[index]}
                            inputIndentaion={scale(16)}
                            onChangeText={this.onChangeText(index)}
                            editable={editable}
                            onFocus={this.props.onFocus}
                            returnKeyType={index === 11 ? "done" : "next"}
                            blurOnSubmit={index === 11}
                            ref={(ref) => { this.inputRefs[index] = ref; }}
                            onSubmitEditing={this.onSubmit(index)}
                          />
                        </View>
                      </View>
                    );
                  })
                }
              </View>
            ))
          }
        </View>
        {clearButton ?
          <View style={styles.clearContainer}>
            <ClearButton
              onPress={this.clearAll}
            />
          </View> : null
        }
      </View>
    );
  }

};
