import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  container: {
  },
  input: {
    display: 'none'
  },
  codeItems: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  codeItem: {
    backgroundColor: colors.white20,
    width: scaleByVertical(44),
    height: scaleByVertical(44),
    marginHorizontal: scaleByVertical(8)
  },
  codeItemText: {
    textAlign: 'center',
    fontSize: scaleByVertical(17),
    lineHeight: scaleByVertical(44),
    color: 'white'
  },
  error: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: colors.reddishPink
  },
  keyboardContainer: {
    marginTop: scaleByVertical(40)
  },
});
