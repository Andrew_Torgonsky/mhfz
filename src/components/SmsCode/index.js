import React from 'react';
import { View, Text, TextInput, TouchableOpacity } from 'react-native';
import NumericKeyBoard from '../NumericKeyboard';

import styles from './styles';

type Props = {
  style: Object,
  error: boolean,
  value: string,
  onChangeText(newText: string): void,
  onFinish(): void
}

class SmsCode extends React.PureComponent<Props> {
  onPress = (symbol) => {
    let value = `${this.props.value}`;
    if (value.length >= 4) {
      this.props.onChangeText(this.props.value.slice(0, 4));
      return;
    }
    value = `${this.props.value}${symbol}`;
    this.props.onChangeText(value);
    if (value.length > 3) this.props.onFinish();
  }
  onDelete = () => {
    let value = `${this.props.value}`;
    if (value.length === 0) return;
    value = value.slice(0, value.length - 1);
    this.props.onChangeText(value);
  }
  render() {
    const {
      style,
      value,
      error
    } = this.props;
    const code = value.split('');
    return (
      <View style={[styles.container, style]}>
        <View style={styles.codeItems}>
          <View style={[styles.codeItem, error && styles.error]}>
            <Text style={styles.codeItemText}>{code[0]}</Text>
          </View>
          <View style={[styles.codeItem, error && styles.error]}>
            <Text style={styles.codeItemText}>{code[1]}</Text>
          </View>
          <View style={[styles.codeItem, error && styles.error]}>
            <Text style={styles.codeItemText}>{code[2]}</Text>
          </View>
          <View style={[styles.codeItem, error && styles.error]}>
            <Text style={styles.codeItemText}>{code[3]}</Text>
          </View>
        </View>
        <View style={styles.keyboardContainer}>
          <NumericKeyBoard onPress={this.onPress} onDelete={this.onDelete} />
        </View>
      </View>
    );
  }
}

SmsCode.defaultProps = {
};

export default SmsCode;

