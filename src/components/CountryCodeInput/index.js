import React from 'react';
import { Image, TouchableOpacity, Text, View } from 'react-native';
import images from '../../global/images';
import styles from './styles';
import InputField from '../InputField';
import flags from '../../global/flags';
import { isRTL } from '../../translations';


type Props = {
  onPress: () => void,
  countryISO: string,
  phonePrefix: string,
  error: boolean
}

export default (props: Props) => {
  const {
    onPress, countryISO = '', phonePrefix = '', error
  } = props;
  return (
    <TouchableOpacity
      style={styles.touch}
      onPress={onPress}
    >
      <View style={[styles.container, error && styles.error, isRTL() && styles.arabic]}>
        <Image source={images.call} style={styles.call} />
          { flags[countryISO] ? <Image source={flags[countryISO]} style={styles.flag} /> : null}
        <Image source={images.triangle} style={styles.triangle} />
        <Text style={styles.code}>+{phonePrefix}</Text>
      </View>
    </TouchableOpacity>
  );
};
