import { StyleSheet } from 'react-native';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
export default StyleSheet.create({
  touch: {
    flex: 1
  },
  call: {
    width: scale(24),
    height: scale(24),
    marginHorizontal: scale(8),
    tintColor: colors.white87
  },
  flag: {
    width: scale(28),
    height: scale(20),
    marginHorizontal: scale(8),
  },
  triangle: {
    width: scale(8),
    height: scale(6),
    marginHorizontal: scale(4),
    tintColor: colors.white87
  },
  code: {
    marginHorizontal: scale(3),
    fontSize: scaleByVertical(16),
    lineHeight: scaleByVertical(19),
    color: 'white',
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular,
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  container: {
    borderRadius: 2,
    backgroundColor: colors.white20,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  error: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: colors.reddishPink
  },
  arabic: {
    flexDirection: 'row-reverse',
  }
});
