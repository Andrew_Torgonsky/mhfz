import React from 'react';
import { TouchableOpacity, Image } from 'react-native';
import images from '../../global/images';
import styles from './styles';

type Props = {
  checked: boolean,
  onPress: () => void,
  style: Object,
};

const CheckBox = (props: Props) => (
  <TouchableOpacity
    style={[
      styles.checkbox,
      props.style
    ]}
    onPress={props.onPress}
  >
    {
      props.checked ?
        <Image
          style={styles.check}
          source={images.checkboxOn}
        /> :
        <Image
          style={styles.uncheck}
          source={images.checkboxOn}
        />
    }
  </TouchableOpacity>
);

export default CheckBox;
