import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical } from '../../utils/scale';

export default StyleSheet.create({
  checkbox: {
    marginTop: scaleByVertical(5),
    justifyContent: 'center',
    alignItems: 'center',
    width: scale(24),
    height: scale(24),
    borderRadius: 3,
  },
  check: {
    width: scale(24),
    height: scale(24),
    tintColor: colors.sky
  },
  uncheck: {
    width: scale(24),
    height: scale(24),
    tintColor: 'grey'
  }
});
