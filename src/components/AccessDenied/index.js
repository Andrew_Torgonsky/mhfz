import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import Permissions from 'react-native-permissions';
import AndroidOpenSettings from 'react-native-android-open-settings'
import styles from './styles';
import translations from '../../translations';
import { isAndroid, isIOS } from '../../utils/scale';
import images from '../../global/images';
import TextButton from '../TextButton';

type Props = {
  type: 'contacts' | 'camera'
};

export default (props: Props) => {
  let text;
  switch(props.type) {
    case 'contacts':
      text = <Text style={styles.text}>{translations.t('CONTACTS_DENIED')}</Text>;
      break;
    case 'camera':
      text = <Text style={styles.text}>{translations.t('CAMERA_DENIED')}</Text>;
      break;
    default:
      text = null;
  }
  return (
    <View style={styles.container}>
      <Image
        source={images.errorBig}
        style={styles.errorIcon}
      />
      <Text style={styles.title}>{translations.t('ACCESS_DENIED')}</Text>
      {
        text
      }
      <TextButton
        title={translations.t('OPEN_SETTINGS')}
        onPress={() => {
          if (isIOS) Permissions.openSettings();
          if (isAndroid) AndroidOpenSettings.appDetailsSettings();
        }}
      />
    </View>
  )
};
