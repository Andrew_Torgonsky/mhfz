import { StyleSheet } from 'react-native';
import colors from '../../global/colors';
import fonts from '../../global/fonts';
import { scale, scaleByVertical, platformSelect } from '../../utils/scale';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: scale(16)
  },
  errorIcon: {
    width: scale(111),
    height: scale(111),
    marginBottom: scaleByVertical(24)
  },
  title: {
    textAlign: 'center',
    color: 'white',
    fontSize: scaleByVertical(20),
    marginBottom: scaleByVertical(24),
    ...platformSelect({
      fontFamily: fonts.sfProTextSemibold
    }, {
      fontFamily: fonts.robotoMedium
    })
  },
  text: {
    textAlign: 'center',
    color: colors.white87,
    fontSize: scaleByVertical(16),
    lineHeight: scaleByVertical(24),
    marginBottom: scaleByVertical(24),
    ...platformSelect({
      fontFamily: fonts.sfUITextRegular
    }, {
      fontFamily: fonts.robotoRegular
    })
  },
  
});
