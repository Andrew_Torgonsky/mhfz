// @flow

import OneSignal from 'react-native-onesignal';
import { fromEvent } from 'rxjs';
import { PUSH_KEY } from '../config';

OneSignal.init(PUSH_KEY);

export function notifications$() {
  return fromEvent(OneSignal, 'received');
} 

OneSignal.addEventListener('opened', () => {
  OneSignal.clearOneSignalNotifications();
});
// OneSignal.addEventListener('ids', (result) => console.log('ids', result));
OneSignal.inFocusDisplaying(0);

export function switchNotifications(status: boolean): void {
  OneSignal.setSubscription(status);
}

export function notificationsAuth(mnemonicHash: string | void): void {
  // OneSignal.registerForPushNotifications();
  OneSignal.sendTag('id', mnemonicHash ? mnemonicHash.slice(0, 32) : '');
}

export function notificationsLogOut(): void {
  OneSignal.deleteTag('id');
}

export function getNotificationStatus(): Promise<boolean> {
  return new Promise((resolve) => {
    OneSignal.getPermissionSubscriptionState(resolve);
  }).then((result) => {
    // console.log(result);
    return result.subscriptionEnabled === 'true' || result.subscriptionEnabled === true;
  }).catch((e) => {
    return false;
  });
}