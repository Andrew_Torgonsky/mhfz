// @flow

import { Keyboard } from 'react-native';
import { Observable } from 'rxjs';


export const keyboardOpen$: Observable<boolean> = Observable.create((observer) => {
  const keyboardWillShowSub = Keyboard.addListener('keyboardDidShow', () => observer.next(true));
  const keyboardWillHideSub = Keyboard.addListener('keyboardDidHide', () => observer.next(false));
  return () => {
    keyboardWillShowSub.remove();
    keyboardWillHideSub.remove();
  }
})

