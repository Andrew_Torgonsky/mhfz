// @flow

import { api, errors } from './shared';
import { type SessionType } from '../types';
import { USE_MOCK_BACKEND } from '../config';
import { delay } from '../utils/other';

export type PreSignInResultType = { error?: 'UNAUTHORIZED' | 'NETWORK' | 'BAD_INPUT' | 'SOMETHING_ELSE' };
export async function preSignIn(phone_number: string, password: string): Promise<PreSignInResultType> {
  if (USE_MOCK_BACKEND) {
    await delay(1500);
    return {};
  }
  const result = await api.post('/sessions/new', {
    phone_number,
    password
  });
  if (result.ok) {
    return {};
  }
  if (result.problem === 'NETWORK_ERROR') {
    return {
      error: errors.NETWORK
    };
  }
  if (result.status === 400) {
    return {
      error: errors.BAD_INPUT
    };
  }
  if (result.status === 401) {
    return {
      error: errors.UNAUTHORIZED
    };
  }
  return {
    error: errors.SOMETHING_ELSE
  };
}

export type SignInResultType = SessionType | { error: 'UNAUTHORIZED' | 'NETWORK' | 'BAD_INPUT' | 'SOMETHING_ELSE' };
export async function signIn(phone_number: string, password: string, hash: string): Promise<SignInResultType> {
  if (USE_MOCK_BACKEND) {
    await delay(1500);
    return {
      token: 'asdsadasdgfdsgfdhgnf',
      name: 'John Doe',
      email: 'johndoe@gmail.com',
      phone_number: '88005553535',
      default_currency: 'usd',
      image: '',
      created_at: '',
      updated_at: ''
    };
  }
  const result = await api.post('/sessions', {
    phone_number,
    password,
    hash
  });
  if (result.ok) {
    try {
      const { data } = result;
      const { user } = data;
      return {
        token: data.token,
        name: user.name,
        email: user.email,
        phone_number: user.phone_number,
        default_currency: ['usd', 'eur', 'sar'].includes(user.default_currency.toLowerCase()) ? user.default_currency.toLowerCase() : 'usd',
        image: user.image,
        created_at: user.created_at,
        updated_at: user.updated_at,
        // wallets ?
      };
    } catch (e) {
      return {
        error: errors.SOMETHING_ELSE
      };
    }
  }
  if (result.problem === 'NETWORK_ERROR') {
    return {
      error: errors.NETWORK
    };
  }
  if (result.status === 400) {
    return {
      error: errors.BAD_INPUT
    };
  }
  if (result.status === 401) {
    return {
      error: errors.UNAUTHORIZED
    };
  }
  return {
    error: errors.SOMETHING_ELSE
  };
}

export type SignOutResultType = { error?: 'UNAUTHORIZED' | 'NETWORK' | 'BAD_INPUT' | 'SOMETHING_ELSE' };
export async function signOut(token: string): Promise<SignOutResultType> {
  if (USE_MOCK_BACKEND) {
    await delay(1500);
    return {};
  }
  const result = await api.delete('/sessions/current', {}, { headers: { Authorization: `Bearer ${token}` } });
  if (result.ok) {
    return {};
  }
  if (result.problem === 'NETWORK_ERROR') {
    return {
      error: errors.NETWORK
    };
  }
  if (result.status === 400) {
    return {
      error: errors.BAD_INPUT
    };
  }
  if (result.status === 401) {
    return {
      error: errors.UNAUTHORIZED
    };
  }
  return {
    error: errors.SOMETHING_ELSE
  };
}
