// @flow
import R, { pipe, filter, always, unless, allPass, is, lt } from 'ramda';
import Big from 'big.js';

Big.RM = 0; // set rounding mode
Big.NE = -10; // set fractional part

export const isValidNumber = allPass([is(Number), R.complement(Number.isNaN), Number.isFinite]);

export const finalizeInput = (str: any): number => unless(isValidNumber, always(0))(parseFloat(str));

export const cleanupFeeOptions = pipe(
  filter(R.propSatisfies(
    pipe(
      parseFloat,
      isValidNumber,
    ),
    'fee',
  )),
  filter(R.propSatisfies(x => R.lte(0, finalizeInput(x)), 'fee')),
  R.uniqBy(R.prop('fee')),
);


export const bigSum = R.curry((x: number, y: number): number =>
  +Big(x)
    .plus(y)
    .toString());

export const coinselectFormat = (x: string | number): number =>
  parseInt(Big(x)
    .times(1e8)
    .round(0, 0)
    .toString());

export const bigMultiply = R.curry((x: number, y: number): number =>
  +Big(x)
    .times(y)
    .toString());

export const bigToFixed = (fixed: number) => (x: string | number): number => Big(x).toFixed(fixed);
