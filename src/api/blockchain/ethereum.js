// @flow

import R from 'ramda';
import axios from 'axios';
import Web3 from 'web3';
import hdkey from 'ethereumjs-wallet/hdkey';
import Transaction from 'ethereumjs-tx';
import moment from 'moment';
import {
  finalizeInput,
  cleanupFeeOptions,
  bigMultiply,
  bigSum,
  bigToFixed,
} from './utils';
import type { TransactionHistoryEntryType } from '../../types';
import { delay } from '../../utils/other';
import { ETHEREUM_NODE, USE_BLOCKCHAIN_TESTNET } from '../../config';

const etherscanApiHost = USE_BLOCKCHAIN_TESTNET ? 'http://api-kovan.etherscan.io/' : 'http://api.etherscan.io/';
const hdPath = "m/44'/60'/0'/0/0";
const etherscanApiToken = 'GX2W8T3X7CAJIRAPDIXA2GHYU3X2VINQQN';
const web3 = new Web3();
web3.setProvider(new web3.providers.HttpProvider(ETHEREUM_NODE));

const { fromWei } = web3.utils;

export function generateWallet(seed: Uint8Array) {
  try {
    const keyPair = hdkey.fromMasterSeed(seed).derivePath(hdPath);
    const walletAddress: string = keyPair.getWallet().getAddressString();
    const privateKey: string = keyPair.getWallet().getPrivateKeyString();
    return {
      walletAddress,
      privateKey,
    };
  } catch (e) {
    return {};
  }
}

/**
 * Get balance of specific address from specific node
 */
async function getBalanceFromEtherscan(walletAddress: string) {
  const url = `${etherscanApiHost}api?module=account&action=balance&address=${walletAddress}&tag=latest&apikey=${etherscanApiToken}`;
  const { data: { result } } = await axios.get(url);
  const total = finalizeInput(web3.utils.fromWei(result, 'ether'));
  return { total, unconfirmed: 0, confirmed: total };
}

async function getBalanceFromNode(walletAddress: string) {
  const wei = await web3.eth.getBalance(walletAddress);
  const total: number = finalizeInput(web3.utils.fromWei(wei, 'ether'));
  return { total, unconfirmed: 0, confirmed: total };
}

export const getBalance = (walletAddress: string) => Promise.race([
  getBalanceFromNode(walletAddress).catch(() => {}),
  delay(15000, {})
]);

/**
 * Get fee
 * @param priority {number}
 * @param estimateGas {number}
 * @param avgGasPrice {number}
 * @returns {string} - Commission price in ETH
 */
async function getFee(nodeAddress, priority, gasUsed, avgGasPrice) {
  try {
    const { fromWei } = web3.utils;

    const gasPrice = Math.round(priority * avgGasPrice);
    const fee = R.pipe(
      finalizeInput,
      bigMultiply(gasUsed),
      R.toString,
      x => fromWei(x, 'ether'), // fw
    )(gasPrice);

    return {
      fee,
      gasPrice,
    };
  } catch (e) {
    return null;
  }
}

/**
 * Returns estimated fee rates options
 */
// eslint-disable-next-line no-unused-vars
export async function getFeeOptions(fromAddress: string, toAddress: string, amount: string) {
  try {
    const options = [
      {
        name: 'regular',
        priority: 1,
      },
      {
        name: 'fast',
        priority: 1.1,
      },
    ];

    const { toWei, toHex } = web3.utils;
    const [avgGasPrice, estimateGas] = await Promise.all([
      web3.eth.getGasPrice(),
      web3.eth.estimateGas({
        value: toHex(toWei(amount.toString(), 'ether')),

        from: fromAddress,
        to: toAddress,
      }),
    ]);

    const res = await Promise.all(options.map(async option => ({
      ...option,
      estimateGas,
      ...(await getFee(null, option.priority, estimateGas, avgGasPrice)),
    })));
    // $FlowToDo
    return cleanupFeeOptions(res);
  } catch (e) {
    return [];
  }
}


async function sendWOTimeout(
  fromAddress: string,
  privateKey: string,
  toAddress: string,
  amount: string | number,
  feeOption: Object,
): Promise<{ txid?: string, error?: 'INSUFFICIENT_FUNDS' | 'SOMETHING_ELSE' | 'LOW_FEE' }> {
  try {
    const txCount = await web3.eth.getTransactionCount(fromAddress, 'pending');
    const { toWei, toHex } = web3.utils;
    const { gasPrice, estimateGas } = feeOption;
    console.log('txCount', txCount);
    const rawTx = {
      nonce: toHex(txCount),
      gasPrice: R.pipe(
        finalizeInput,
        R.toString,
        toHex,
      )(gasPrice),
      value: R.pipe(
        finalizeInput,
        bigToFixed(8),
        x => toWei(x, 'ether'),
        toHex,
      )(amount),
      from: fromAddress,
      to: toAddress,
      gas: toHex(estimateGas),
    };
    const tx = new Transaction(rawTx);
    tx.sign(Buffer.from(privateKey.slice(2), 'hex'));
    /**
     * return transaction hash
     */
    const res = await new Promise((resolve, reject) => {
      web3.eth
        .sendSignedTransaction(`0x${tx.serialize().toString('hex')}`)
        .on('transactionHash', resolve)
        .on('error', reject);
    });
    return { txid: res };
  } catch (e) {
    console.log('eth send error', e);
    if (typeof e.message === 'string' && e.message.startsWith('Returned error: Insufficient funds')) return { error: 'INSUFFICIENT_FUNDS' };
    if (typeof e.message === 'string' && e.message.startsWith('Returned error: Transaction gas price is too low')) return { error: 'LOW_FEE' };
    return { error: 'SOMETHING_ELSE' };
  }
}

export const send = (
  fromAddress: string,
  privateKey: string,
  toAddress: string,
  amount: string | number,
  feeOption: Object,
) => Promise.race([
  sendWOTimeout(
    fromAddress,
    privateKey,
    toAddress,
    amount,
    feeOption,
  ),
  delay(300*1000, { error: 'SOMETHING_ELSE' })
]);

export async function getHistory(walletAddress: string, existingIds: string[]): Promise<{
  transactions: TransactionHistoryEntryType[],
  status: 'OK' | 'ERROR' | 'END_OF_LIST'
}> {
  try {
    const url = `${etherscanApiHost}api?module=account&action=txlist&address=${walletAddress}&page=${Math.floor(existingIds.length / 30 + 1)}&offset=30&sort=desc&apikey=${etherscanApiToken}`;
    const { data } = await axios.get(url);
    const transactions = data.result.map((item) => {
      try {
        return {
          id: item.hash,
          address: walletAddress.toLowerCase() === item.from.toLowerCase() ? item.to.toLowerCase() : item.from.toLowerCase(),
          amount: walletAddress.toLowerCase() === item.from.toLowerCase() ? -fromWei(item.value, 'ether') : +fromWei(item.value, 'ether'),
          status: +item.isError ? 'failed' : (+item.confirmations ? 'success' : 'pending'),
          timestamp: moment(item.timeStamp * 1000).valueOf(),
          confirmations: +item.confirmations,
          block: +item.blockNumber,
          isSeen: false
        };
      } catch (e) {
        return false;
      }
    }).filter(item => item);
    return { transactions, status: data.result.length < 30 ? 'END_OF_LIST' : 'OK' };
  } catch (e) {
    // console.log('ethereum history error', e);
    return { transactions: [], status: 'ERROR' };
  }
}
