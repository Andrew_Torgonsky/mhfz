// @flow

import R from 'ramda';
import axios from 'axios';
import bitcoinjs from 'bitcoinjs-lib';
import Big from 'big.js';
import coinselect from 'coinselect';
import moment from 'moment';
import {
  cleanupFeeOptions,
  bigSum,
  coinselectFormat,
} from './utils';
import type { TransactionHistoryEntryType } from '../../types';
import { USE_BLOCKCHAIN_TESTNET, BITCOIN_NODE } from '../../config';

Big.RM = 0; // set rounding mode
Big.NE = -10; // set fractional part

const hdPath = USE_BLOCKCHAIN_TESTNET ? "m/44'/1'/0'/0/0" : "m/44'/0'/0'/0/0";
const bitcoinNetwork = USE_BLOCKCHAIN_TESTNET ? bitcoinjs.networks.testnet : bitcoinjs.networks.bitcoin;

export function generateWallet(seed: Uint8Array) {
  try {
    const { fromSeedBuffer } = bitcoinjs.HDNode;
    const { keyPair } = fromSeedBuffer(seed, bitcoinNetwork).derivePath(hdPath);
    const walletAddress: string = keyPair.getAddress();
    const privateKey: string = keyPair.toWIF();
    return {
      walletAddress,
      privateKey,
    };
  } catch (e) {
    return {};
  }
}

/**
 * Get balance of specific address from specific node
 */
export const getBalance = async (walletAddress: string) => {
  const url = `${BITCOIN_NODE}addr/${walletAddress}/`;
  try {
    const res = await axios.get(url);
    const unconfirmed: number = res.data.unconfirmedBalance;
    const confirmed: number = res.data.balance;
    const total: number = bigSum(confirmed, unconfirmed);
    return { total, unconfirmed, confirmed };
  } catch (e) {
    return {};
  }
};

/**
 * Returns utxo
 */
async function getUnspentOutputs(nodeAddress, walletAddress) {
  // try {
    const { data } = await axios.get(`${nodeAddress}addr/${walletAddress}/utxo`);
    return data;
  // } catch (error) {
  //   return [];
  // }
}

/**
 * Returns estimated fee rate
 */
async function getFeeRate(nodeAddress, blocks) {
  // try {
    const { data } = await axios.get(`${nodeAddress}utils/estimatefee?nbBlocks=${blocks}`);
    const feeRate = Math.round(data[blocks] * 1e8 / 1024);
    return feeRate > 1 ? feeRate : 2;
  // } catch (error) {
  //   return null;
  // }
}

/**
 * Returns estimated fee rate
 */
async function getFee(nodeAddress, blocks, fromAddress, toAddress, amount) {
  const feeRate = await getFeeRate(nodeAddress, blocks);
  const utxo = await getUnspentOutputs(nodeAddress, fromAddress);
  const transformedUtxo = R.map(output => R.assoc('value', output.satoshis, output))(utxo);
  const { fee } = coinselect(
    transformedUtxo,
    [
      {
        address: toAddress,
        value: parseInt(new Big(amount)
          .times(1e8)
          .round(0, 0)
          .toString()),
      },
    ],
    feeRate,
  );
  return new Big(fee).div(1e8).toString();
}

/**
 * Returns estimated fee rates options
 */
export async function getFeeOptions(fromAddress: string, toAddress: string, amount: string) {
  try {
    const options = [
      {
        name: 'regular',
        priority: 6,
      },
      {
        name: 'fast',
        priority: 2,
      },
    ];

    const res = await Promise.all(options.map(async option => ({
      ...option,
      fee: await getFee(BITCOIN_NODE, option.priority, fromAddress, toAddress, amount),
    })));
    return cleanupFeeOptions(res);
  } catch (e) {
    return [];
  }
}

export async function send(
  fromAddress: string,
  privateKey: string,
  toAddress: string,
  amount: string | number,
  feeOption: Object,
): Promise<{ txid?: string, error?: 'INSUFFICIENT_FUNDS' | 'SOMETHING_ELSE' }> {
  try {
    const [utxo, feeRate] = await Promise.all([
      getUnspentOutputs(BITCOIN_NODE, fromAddress),
      getFeeRate(BITCOIN_NODE, feeOption.priority),
    ]);
    const transformedUtxo = R.map(output => R.assoc('value', output.satoshis, output))(utxo);
    // eslint-disable-next-line no-unused-vars
    const { inputs, outputs, fee } = coinselect(
      transformedUtxo,
      [
        {
          address: toAddress,
          value: coinselectFormat(amount),
        },
      ],
      feeRate,
    );
    if (inputs && outputs) {
      const tx = new bitcoinjs.TransactionBuilder(bitcoinNetwork, 0);
      inputs.forEach((input) => {
        tx.addInput(input.txid, input.vout);
      });
      outputs.forEach((output) => {
        tx.addOutput(output.address || fromAddress, output.value);
      });
      const pair = bitcoinjs.ECPair.fromWIF(privateKey, bitcoinNetwork);
      inputs.forEach((input, i) => tx.sign(i, pair));

      const result = await axios.post(`${BITCOIN_NODE}tx/send`, { rawtx: tx.build().toHex() });
      return { txid: result.data.txid };
    }
    return { error: 'INSUFFICIENT_FUNDS' };
  } catch (e) {
    // console.log('btc send error', e);
    return { error: 'SOMETHING_ELSE' };
  }
}

export async function getHistory(walletAddress: string, existingIds: string[]): Promise<{ 
  transactions: TransactionHistoryEntryType[],
  status: 'OK' | 'ERROR' | 'END_OF_LIST'
}> {
  try {
    const { data } = await axios.get(`${BITCOIN_NODE}addrs/${walletAddress}/txs?from=${existingIds.length}&to=${existingIds.length + 30}`);
    // console.log(data);
    const transactions = data.items.map((item) => {
      try {
        const amountIn = item.vin.reduce((accumulator, current) => {
          return current.addr === walletAddress ? accumulator.plus(current.value) : accumulator;
        }, new Big(0));
        const amountOut = item.vout.reduce((accumulator, current) => {
          return current.scriptPubKey.addresses.includes(walletAddress) ? accumulator.plus(current.value) : accumulator;
        }, new Big(0));
        const amount = +amountOut.minus(amountIn).toString();
        const address = amount > 0 ?
          item.vin.reduce((accumulator, current) => current.addr !== walletAddress ? current.addr : accumulator, '') :
          item.vout.reduce((accumulator, current) => !current.scriptPubKey.addresses.includes(walletAddress) ? current.scriptPubKey.addresses[0] : accumulator, '');
        return {
          id: item.txid,
          address,
          amount,
          status: +item.blockheight > -1 ? 'success' : 'pending',
          timestamp: moment(item.time * 1000).valueOf(),
          confirmations: +item.confirmations,
          block: +item.blockheight,
          isSeen: false
        };
      } catch (e) {
        return false;
      }
    }).filter(item => item);
    return { transactions, status: data.totalItems === data.to ? 'END_OF_LIST' : 'OK' };
  } catch (e) {
    // console.log('bitcoin history error', e);
    return { transactions: [], status: 'ERROR' };
  }
}
