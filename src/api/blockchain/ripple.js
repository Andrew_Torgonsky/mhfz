// @flow

import R from 'ramda';
import moment from 'moment';
import rippleKeypairs from 'ripple-keypairs';
import { RippleAPI } from 'ripple-lib';
import {
  finalizeInput,
  cleanupFeeOptions,
} from './utils';
import type { TransactionHistoryEntryType } from '../../types';
import { delay } from '../../utils/other';
import { RIPPLE_NODE } from '../../config';

export function generateWallet(seed: Uint8Array) {
  try {
    const privateKey: string = rippleKeypairs.generateSeed({ entropy: seed });
    const keypair = rippleKeypairs.deriveKeypair(privateKey);
    const walletAddress: string = rippleKeypairs.deriveAddress(keypair.publicKey);
    return {
      walletAddress,
      privateKey,
    };
  } catch (e) {
    return {};
  }
}

/**
 * Get balance of specific address from specific node
 */
const getBalanceWOTimeout = async (walletAddress: string) => {
  try {
    const api = new RippleAPI({
      server: RIPPLE_NODE,
    });
    await api.connect();
    const { xrpBalance } = await api.getAccountInfo(walletAddress);
    const total: number = finalizeInput(xrpBalance);
    const unconfirmed = 0;
    const confirmed = total;
    await api.disconnect();
    return { total, unconfirmed, confirmed };
  } catch (e) {
    // console.log('ripple balance error', e);
    if (e.message === 'actNotFound') {
      return { total: 0, unconfirmed: 0, confirmed: 0 };
    }
    return {};
  }
};

export const getBalance = (walletAddress: string) => Promise.race([
  getBalanceWOTimeout(walletAddress),
  delay(15000, {})
]);

function getPayment(fromAddress, toAddress, rawAmount) {
  const amount = {
    currency: 'XRP',
    value: rawAmount.toString(),
  };
  return {
    source: {
      address: fromAddress,
      maxAmount: amount,
    },
    destination: {
      address: toAddress,
      amount,
    },
  };
}

async function getFee(nodeAddress, priority, fromAddress, toAddress, rawAmount) {
  const api = new RippleAPI({
    server: nodeAddress,
  });
  const payment = getPayment(fromAddress, toAddress, rawAmount);
  await api.connect();
  const {
    instructions: { fee },
  } = await api.preparePayment(fromAddress, payment);
  await api.disconnect();
  return fee;
}

/**
 * Returns estimated fee rates options
 */
export async function getFeeOptions(fromAddress: string, toAddress: string, amount: string) {
  try {
    const options = [
      {
        name: 'regular',
        priority: 0,
      },
    ];
    const res = await Promise.all(options.map(async option => ({
      ...option,
      fee: await getFee(RIPPLE_NODE, option.priority, fromAddress, toAddress, amount),
    })));
    return cleanupFeeOptions(res);
  } catch (e) {
    return [];
  }
}

async function sendWOTimeout(
  fromAddress: string,
  privateKey: string,
  toAddress: string,
  amount: string | number,
): Promise<{ txid?: string, error?: 'SOMETHING_ELSE' | 'INSUFFICIENT_FUNDS' | 'DESTINATION_NOT_EXIST' }> {
  try {
    const api = new RippleAPI({
      server: RIPPLE_NODE,
    });
    const payment = getPayment(fromAddress, toAddress, amount);
    await api.connect();
    const { txJSON } = await api.preparePayment(fromAddress, payment);
    const { signedTransaction, id } = api.sign(txJSON, privateKey);
    const txResult = await api.submit(signedTransaction);
    await api.disconnect(); // $FlowToDo
    if (txResult.resultCode === 'tesSUCCESS') return { txid: id };
    if (txResult.resultCode === 'tecUNFUNDED_PAYMENT') return { error: 'INSUFFICIENT_FUNDS' };
    if (txResult.resultCode === 'tecNO_DST_INSUF_XRP') return { error: 'DESTINATION_NOT_EXIST' };
    // console.log('ripple send response', txResult, id);
    throw new Error();
  } catch (e) {
    // console.log('ripple send error', e);
    return { error: 'SOMETHING_ELSE' };
  }
}

export const send = (
  fromAddress: string,
  privateKey: string,
  toAddress: string,
  amount: string | number
) => Promise.race([
  sendWOTimeout(
    fromAddress,
    privateKey,
    toAddress,
    amount
  ),
  delay(15000, { error: 'SOMETHING_ELSE' })
]);

export async function getHistory(walletAddress: string, existingIds: string[]): Promise<{
  transactions: TransactionHistoryEntryType[],
  status: 'OK' | 'ERROR' | 'END_OF_LIST'
}> {
  try {
    const api = new RippleAPI({
      server: RIPPLE_NODE,
    });
    await api.connect();
    const transactions = await api.getTransactions(walletAddress, {
      start: existingIds ? existingIds[existingIds.length - 1] : undefined,
      limit: 30
    });
    const lastLedger = await api.getLedgerVersion();
    await api.disconnect();
    // console.log(transactions);
    const parsedTransactions = transactions.map((item, index) => {
      try {
        if (item.type !== 'payment' || item.outcome.deliveredAmount.currency !== 'XRP') return false;
        return {
          id: item.id,
          address: item.specification.source.address === walletAddress ? item.specification.destination.address : item.specification.source.address,
          amount: item.specification.source.address === walletAddress ? -item.outcome.deliveredAmount.value : +item.outcome.deliveredAmount.value,
          status: item.outcome.result === 'tesSUCCESS' ? 'success' : 'failed',
          timestamp: moment(item.outcome.timestamp).valueOf(),
          confirmations: lastLedger - item.outcome.ledgerVersion,
          block: +item.outcome.ledgerVersion,
          isSeen: false
        };
      } catch (e) {
        return false;
      }
    }).filter(item => item);
    return { transactions: parsedTransactions, status: transactions.length < 30 ? 'END_OF_LIST' : 'OK' };
  } catch (e) {
    // console.log('ripple history error', e);
    return { transactions: [], status: 'ERROR' };
  }
}