// @flow

import { Observable, timer } from 'rxjs';
import { retry, share, retryWhen, delayWhen } from 'rxjs/operators';

import { IP } from '../config';
import type { RatesType } from '../types';

export function ws$(token: string): Observable<any> {
  return Observable.create((observer) => {
    const socket = new WebSocket(`ws://${IP}/ws?token=${token}`);
    socket.onmessage = (event: any) => {
      try {
        const data = JSON.parse(event.data);
        observer.next(data);
      } catch (e) {
        observer.error(e)
      }
    };
    socket.onerror = (err) => {
      observer.error(err);
    };
    return () => {
      socket.close();
    };
  });
}

