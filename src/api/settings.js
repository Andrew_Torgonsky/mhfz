// @flow


import { resolve } from 'path';
import FormData from 'form-data';
import { Image } from 'react-native-image-crop-picker';
import { api, errors } from './shared';
import type { SessionType, FiatCurrencyType, RatesType } from '../types';
import { USE_MOCK_BACKEND } from '../config';
import { delay } from '../utils/other';

export type ChangePasswordResponseType = { error?: 'BAD_INPUT' | 'NETWORK' | 'SOMETHING_ELSE' | 'UNAUTHORIZED' };
export async function changePassword(newPassword: string, currentPassword: string, token: string): Promise<ChangePasswordResponseType> {
  if (USE_MOCK_BACKEND) {
    await delay(1500);
    return {};
  }
  const result = await api.put('/users/me/password', {
    new: newPassword,
    current: currentPassword
  }, { headers: { Authorization: `Bearer ${token}` } });
  if (result.ok) return {};
  if (result.problem === 'NETWORK_ERROR') return { error: errors.NETWORK };
  if (result.status === 400) return { error: errors.BAD_INPUT };
  if (result.status === 401) return { error: errors.UNAUTHORIZED };
  return { error: errors.SOMETHING_ELSE };
}

export type EnterNewPhoneResponseType = { error?: 'BAD_INPUT' | 'ALREADY_EXIST' | 'UNAUTHORIZED' | 'NETWORK' | 'SOMETHING_ELSE' | 'TOO_MANY_REQUESTS' };
export async function enterNewPhone(phone: string, token: string): Promise<EnterNewPhoneResponseType> {
  if (USE_MOCK_BACKEND) {
    await delay(1500);
    return {};
  }
  const result = await api.post('/users/me/phone/new', { phone_number: phone }, { headers: { 'Authorization': `Bearer ${token}` } });
  if (result.ok) return {};
  if (result.problem === 'NETWORK_ERROR') return { error: errors.NETWORK };
  if (result.status === 400) return { error: errors.BAD_INPUT };
  if (result.status === 401) return { error: errors.UNAUTHORIZED };
  if (result.status === 409) return { error: errors.ALREADY_EXIST };
  if (result.status === 429) return { error: errors.TOO_MANY_REQUESTS };
  return { error: errors.SOMETHING_ELSE };
}

export type CheckSmsCodeResponseType = { error?: 'BAD_INPUT' | 'NO_TEMP_SESSION' | 'UNAUTHORIZED' | 'NETWORK' | 'SOMETHING_ELSE' };
export async function checkSmsCode(smsCode: string, token: string): Promise<CheckSmsCodeResponseType> {
  if (USE_MOCK_BACKEND) {
    await delay(1500);
    return {};
  }
  const result = await api.put('/users/me/phone', { secret: smsCode }, { headers: { 'Authorization': `Bearer ${token}` }});
  if (result.ok) return {};
  if (result.problem === 'NETWORK_ERROR') return { error: errors.NETWORK };
  if (result.status === 400) return { error: errors.BAD_INPUT };
  if (result.status === 401) return { error: errors.UNAUTHORIZED };
  if (result.status === 412) return { error: errors.NO_TEMP_SESSION };
  return { error: errors.SOMETHING_ELSE };
}

export type ChangeAvatarResponseType = { image: string } | { error: 'UNAUTHORIZED' | 'NETWORK' | 'SOMETHING_ELSE' };
export async function changeAvatar(image: Image, token: string): Promise<ChangeAvatarResponseType> {
  if (USE_MOCK_BACKEND) {
    await delay(1500);
    return { image: 'ololo.png' };
  }
  const data = new FormData();
  data.append('image', {
    uri: image.path,
    type: image.mime,
    name: image.filename
  });
  const result = await api.put(
    '/users/me/image',
    data,
    {
      headers: {
        'Authorization': `Bearer ${token}`,
        'Content-type': 'multipart/form-data'
      }
    }
  );
  if (result.ok) return { image: result.data.image };
  if (result.problem === 'NETWORK_ERROR') return { error: errors.NETWORK };
  if (result.status === 401) return { error: errors.UNAUTHORIZED };
  return { error: errors.SOMETHING_ELSE };
}

export type ChangeNameAndEmailResponseType = { error?: 'BAD_INPUT' | 'UNAUTHORIZED' | 'NETWORK' | 'SOMETHING_ELSE' };
export async function changeNameAndEmail(name: string, email: string, token: string): Promise<ChangeNameAndEmailResponseType> {
  if (USE_MOCK_BACKEND) {
    await delay(1500);
    return {};
  }
  const result = await api.patch('/users/me', { name, email }, { headers: { 'Authorization': `Bearer ${token}` } });
  if (result.ok) return {};
  if (result.problem === 'NETWORK_ERROR') return { error: errors.NETWORK };
  if (result.status === 400) return { error: errors.BAD_INPUT };
  if (result.status === 401) return { error: errors.UNAUTHORIZED };
  return { error: errors.SOMETHING_ELSE };
}

export type ChangeCurrencyResponseType = RatesType | { error: 'UNAUTHORIZED' | 'NETWORK' | 'SOMETHING_ELSE' };
export async function changeCurrency(currency: FiatCurrencyType, token: string): Promise<ChangeCurrencyResponseType> {
  if (USE_MOCK_BACKEND) {
    await delay(1500);
    return {
      bitcoin: 0,
      ethereum: 0,
      ripple: 0,
      currency: 'usd'
    };
  }
  const result = await api.put('/users/me/currency', { value: currency }, { headers: { 'Authorization': `Bearer ${token}` } });
  if (result.ok) {
    try {
      if (!['usd', 'eur', 'sar'].includes(result.data.currency.toLowerCase())) throw new Error();
      return {
        bitcoin: +result.data.BTC,
        ethereum: +result.data.ETH,
        ripple: +result.data.XRP,
        currency: result.data.currency.toLowerCase()
      };
    } catch (e) {
      return { error: errors.SOMETHING_ELSE };
    }
  }
  if (result.problem === 'NETWORK_ERROR') return { error: errors.NETWORK };
  if (result.status === 401) return { error: errors.UNAUTHORIZED };
  return { error: errors.SOMETHING_ELSE };
}
