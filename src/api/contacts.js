// @flow

import R from 'ramda';
import RNC from 'react-native-contacts';
import { parseNumber } from 'libphonenumber-js';
import { PermissionsAndroid } from 'react-native';
import { api, errors } from './shared';
import type { ContactType } from '../types';
import { isAndroid } from '../utils/scale';
import { USE_MOCK_BACKEND } from '../config';
import translations from '../translations';
// import { chunkArray } from '../utils/other';

export type GetWalletsForContactsType = { contacts: ContactType[] } | { error: 'UNAUTHORIZED' | 'BAD_INPUT' | 'SOMETHING_ELSE' | 'NETWORK' | 'NO_CONTACTS' };
export async function getWalletsForContacts(contacts: ContactType[], token: string): Promise<GetWalletsForContactsType> {
  if (USE_MOCK_BACKEND) return Promise.resolve({ contacts });
  const dataToSend = contacts.map((item, i) => ({
    fake_id: i,
    phone_numbers: item.phoneNumbers
  })).filter(item => item.phone_numbers.length > 0);
  const chunksToSend = R.splitEvery(99, dataToSend);
  const results = await Promise.all(chunksToSend.map(chunk => api.patch('/users/me/contacts', { data: chunk }, { headers: { Authorization: `Bearer ${token}` } })));
  if (results.reduce((acc, cur) => cur.ok && acc, true)) {
    try {
      const data = results.reduce((acc, cur) => Array.isArray(cur.data.data) ? [...acc, ...cur.data.data] : acc, []);
      const contactsWithWallets: ContactType[] = contacts.map((contact, index) => {
        let wallets = [];
        try {
          const { data: walletSets } = data.find(item2 => item2.fake_id === index);
          walletSets.forEach((walletSet) => {
            wallets.push(
              {
                walletAddress: walletSet.wallets.btc_address,
                currency: 'bitcoin'
              }, {
                walletAddress: walletSet.wallets.eth_address,
                currency: 'ethereum'
              }, {
                walletAddress: walletSet.wallets.xrp_address,
                currency: 'ripple'
              }
            );
          });
        } catch (e) {
          wallets = [];
        }
        return { ...contact, wallets };
      });
      return { contacts: contactsWithWallets };
    } catch (e) {
      return { error: errors.NO_CONTACTS };
    }
  }
  // if (result.problem === 'NETWORK_ERROR') return { error: errors.NETWORK };
  // if (result.status === 400) return { error: errors.BAD_INPUT };
  // if (result.state === 401) return { error: errors.UNAUTHORIZED };
  return { error: errors.SOMETHING_ELSE };
}

export async function getAllContacts(userPhone: string): Promise<Array<ContactType>> {
  const { country: defaultCountry } = parseNumber(userPhone);
  if (isAndroid) {
    try {
      const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS);
      if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
        console.log('denied contacts permissions');
        return [];
      }
    } catch (e) {
      console.log('can\'t get permissions info');
      return [];
    }
  }
  return new Promise((resolve, reject) => {
    RNC.getAll((err, contacts) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(contacts);
    });
  }).then(contacts => contacts.map((contact) => {
    try {
      return {
        firstName: contact.givenName || '',
        lastName: contact.familyName || '',
        photo: contact.thumbnailPath || '',
        emails: contact.emailAddresses.map(item => item.email || '').filter(item => item),
        phoneNumbers: contact.phoneNumbers.map((item) => {
          const parsed = parseNumber(item.number, defaultCountry, { extended: true });
          if (!parsed.countryCallingCode || !parsed.phone) return false;
          return `${parsed.countryCallingCode}${parsed.phone}`;
        }).filter(item => item),
        wallets: []
      };
    } catch (e) {
      return false;
    }
  }).filter(item => item)).catch(() => []);
}
