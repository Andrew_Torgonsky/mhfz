// @flow

import { api, errors } from './shared';
import { type SessionType } from '../types';
import { USE_MOCK_BACKEND } from '../config';
import { delay } from '../utils/other';

export type CheckUserResultType = { error?: 'ALREADY_EXIST' | 'NETWORK' | 'BAD_INPUT' | 'TOO_MANY_REQUESTS' | 'SOMETHING_ELSE' };
export async function checkUser(email: string, phone_number: string, password: string): Promise<CheckUserResultType> {
  if (USE_MOCK_BACKEND) {
    await delay(1500);
    return {};
  }
  const result = await api.post('/users/new', {
    email,
    phone_number,
    password
  });

  if (result.ok) {
    return {};
  }
  if (result.problem === 'NETWORK_ERROR') {
    return {
      error: errors.NETWORK
    };
  }
  if (result.status === 400) {
    return {
      error: errors.BAD_INPUT
    };
  }
  if (result.status === 409) {
    return {
      error: errors.ALREADY_EXIST
    };
  }
  if (result.status === 429) {
    return {
      error: errors.TOO_MANY_REQUESTS
    };
  }
  return {
    error: errors.SOMETHING_ELSE
  };
}

export type CheckUserSecretResultType = { error?: 'NO_TEMP_SESSION' | 'NETWORK' | 'BAD_INPUT' | 'SOMETHING_ELSE' };
export async function checkUserSecret(phone_number: string, secret: string): Promise<CheckUserSecretResultType> {
  if (USE_MOCK_BACKEND) {
    await delay(1500);
    return secret === '1111' ? {} : { error: errors.NO_TEMP_SESSION };
  }
  const result = await api.post('/users/new/secret', {
    phone_number,
    secret
  });
  // console.log(result);
  if (result.ok) {
    return {};
  }
  if (result.problem === 'NETWORK_ERROR') {
    return {
      error: errors.NETWORK
    };
  }
  if (result.status === 400) {
    return {
      error: errors.BAD_INPUT
    };
  }
  if (result.status === 412) {
    return {
      error: errors.NO_TEMP_SESSION
    };
  }
  return {
    error: errors.SOMETHING_ELSE
  };
}

export type AddUserResultType = SessionType | { error: 'NO_TEMP_SESSION' | 'NETWORK' | 'BAD_INPUT' | 'SOMETHING_ELSE' | 'ALREADY_EXIST' };
export async function addUser(phone_number: string, hash: string, eth_address: string, btc_address: string, xrp_address: string, secret: string): Promise<AddUserResultType> {
  if (USE_MOCK_BACKEND) {
    await delay(1500);
    return {
      token: 'asdsadasdgfdsgfdhgnf',
      name: 'John Doe',
      email: 'johndoe@gmail.com',
      phone_number: '88005553535',
      default_currency: 'usd',
      image: '',
      created_at: '',
      updated_at: ''
    };
  }
  const result = await api.post('/users', {
    phone_number,
    hash,
    wallets: {
      eth_address,
      btc_address,
      xrp_address
    },
    secret
  });
  // console.log(result);
  if (result.ok) {
    try {
      const { data } = result;
      const { user } = data;
      return {
        token: data.token,
        name: user.name,
        email: user.email,
        phone_number: user.phone_number,
        default_currency: ['usd', 'eur', 'sar'].includes(user.default_currency.toLowerCase()) ? user.default_currency.toLowerCase() : 'usd',
        image: user.image,
        created_at: user.created_at,
        updated_at: user.updated_at
        // wallets ?
      };
    } catch (e) {
      return {
        error: errors.SOMETHING_ELSE
      };
    }
  }
  if (result.problem === 'NETWORK_ERROR') {
    return {
      error: errors.NETWORK
    };
  }
  if (result.status === 400) {
    return {
      error: errors.BAD_INPUT
    };
  }
  if (result.status === 409) {
    return {
      error: errors.ALREADY_EXIST
    };
  }
  if (result.status === 412) {
    return {
      error: errors.NO_TEMP_SESSION
    };
  }
  return {
    error: errors.SOMETHING_ELSE
  };
}

export type GetMeResultType = SessionType | { error: 'UNAUTHORIZED' | 'NETWORK' | 'SOMETHING_ELSE' };
export async function getMe(token: string): Promise<GetMeResultType> {
  if (USE_MOCK_BACKEND) {
    await delay(5000);
    return {
      name: 'John Doe',
      email: 'johndoe@gmail.com',
      phone_number: '88005553535',
      default_currency: 'usd',
      image: '',
      created_at: '',
      updated_at: ''
    };
  }
  const result = await api.get('/users/me', {}, { headers: { Authorization: `Bearer ${token}` } });
  // console.log(result);
  if (result.ok) {
    try {
      const { data } = result;
      return {
        name: data.name,
        email: data.email,
        phone_number: data.phone_number,
        default_currency: ['usd', 'eur', 'sar'].includes(data.default_currency.toLowerCase()) ? data.default_currency.toLowerCase() : 'usd',
        image: data.image,
        created_at: data.created_at,
        updated_at: data.updated_at
        // wallets ?
      };
    } catch (e) {
      return {
        error: errors.SOMETHING_ELSE
      };
    }
  }
  if (result.problem === 'NETWORK_ERROR') {
    return {
      error: errors.NETWORK
    };
  }
  if (result.status === 401) {
    return {
      error: errors.UNAUTHORIZED
    };
  }
  return {
    error: errors.SOMETHING_ELSE
  };
}

export type RequestRecoveryResultType = { error?: 'UNAUTHORIZED' | 'NETWORK' | 'BAD_INPUT' | 'TOO_MANY_REQUESTS' | 'SOMETHING_ELSE' };
export async function requestRecovery(phone_number: string): Promise<RequestRecoveryResultType> {
  if (USE_MOCK_BACKEND) {
    await delay(1500);
    return {};
  }
  const result = await api.post('/users/recovery', {
    phone_number
  });
  // console.log(result);
  if (result.ok) {
    return {};
  }
  if (result.problem === 'NETWORK_ERROR') {
    return {
      error: errors.NETWORK
    };
  }
  if (result.status === 400) {
    return {
      error: errors.BAD_INPUT
    };
  }
  if (result.status === 412) {
    return {
      error: errors.UNAUTHORIZED
    };
  }
  if (result.status === 429) {
    return {
      error: errors.TOO_MANY_REQUESTS
    };
  }
  return {
    error: errors.SOMETHING_ELSE
  };
}

export type CheckRecoverySecretResultType = { error?: 'NO_TEMP_SESSION' | 'NETWORK' | 'BAD_INPUT' | 'SOMETHING_ELSE' };
export async function checkRecoverySecret(phone_number: string, secret: string): Promise<CheckRecoverySecretResultType> {
  if (USE_MOCK_BACKEND) {
    await delay(1500);
    return secret === '1111' ? {} : { error: errors.NO_TEMP_SESSION };
  }
  const result = await api.post('/users/recovery/secret', {
    phone_number,
    secret
  });
  // console.log(result);
  if (result.ok) {
    return {};
  }
  if (result.problem === 'NETWORK_ERROR') {
    return {
      error: errors.NETWORK
    };
  }
  if (result.status === 400) {
    return {
      error: errors.BAD_INPUT
    };
  }
  if (result.status === 412) {
    return {
      error: errors.NO_TEMP_SESSION
    };
  }
  return {
    error: errors.SOMETHING_ELSE
  };
}

export type UpdatePasswordResultType = { error?: 'NO_TEMP_SESSION' | 'NETWORK' | 'BAD_INPUT' | 'SOMETHING_ELSE' };
export async function updatePassword(phone_number: string, secret: string, password: string): Promise<UpdatePasswordResultType> {
  if (USE_MOCK_BACKEND) {
    await delay(1500);
    return {};
  }
  const result = await api.put('/users/recovery/password', {
    phone_number,
    secret,
    password
  });
  // console.log(result);
  if (result.ok) {
    return {};
  }
  if (result.problem === 'NETWORK_ERROR') {
    return {
      error: errors.NETWORK
    };
  }
  if (result.status === 400) {
    return {
      error: errors.BAD_INPUT
    };
  }
  if (result.status === 412) {
    return {
      error: errors.NO_TEMP_SESSION
    };
  }
  return {
    error: errors.SOMETHING_ELSE
  };
}
