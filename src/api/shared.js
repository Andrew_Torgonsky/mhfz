// @flow

import { create } from 'apisauce';
import { BACKEND } from '../config';

export const api = create({
  baseURL: BACKEND
});

export const errors = {
  NO_CONTACTS: 'NO_CONTACTS',
  BAD_INPUT: 'BAD_INPUT',
  ALREADY_EXIST: 'ALREADY_EXIST',
  UNAUTHORIZED: 'UNAUTHORIZED',
  NO_TEMP_SESSION: 'NO_TEMP_SESSION',
  TOO_MANY_REQUESTS: 'TOO_MANY_REQUESTS',
  NETWORK: 'NETWORK',
  SOMETHING_ELSE: 'SOMETHING_ELSE'
};
