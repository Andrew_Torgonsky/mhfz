// @flow

import { AppState, DeviceEventEmitter } from 'react-native';
import { Observable, fromEvent } from 'rxjs';
import { filter } from 'rxjs/operators';
import { isAndroid, isIOS } from '../utils/scale';

type ActivityStateType = 'active' | 'background';

const activityStateAndroid$: Observable<ActivityStateType> = Observable.create((observer) => {
  const emitterSubscription = DeviceEventEmitter.addListener('ActivityStateChange', (e) => {
    if (e.event === 'background') {
      observer.next('background')
    }
    if (e.event === 'active') {
      observer.next('active')
    }
  });
  return () => emitterSubscription.remove();
});

const activityStateIOS$: Observable<ActivityStateType> = fromEvent(AppState, 'change').pipe(
  filter(state => ['active', 'background'].includes(state))
);

export const activityState$ = () => isIOS ? activityStateIOS$ : activityStateAndroid$;

