// @flow

import R from 'ramda';
import { api, errors } from './shared';
import Moment from 'moment';
import { AsyncStorage } from 'react-native';
import { type SessionType } from '../types';
import { USE_MOCK_BACKEND } from '../config';
import { delay } from '../utils/other';
import type { CryptoCurrencyType, BalanceHistoryEntryType, TransactionHistoryEntryType } from '../types';
import { switchAbbrAndName } from '../utils/currencies';

export type AddTransactionResultType = { error?: 'NETWORK' | 'BAD_INPUT' | 'UNAUTHORIZED' | 'SOMETHING_ELSE' };
export async function addTransaction(token: string, hash: string, currency: CryptoCurrencyType, recipient: string, value: number): Promise<AddTransactionResultType> {
  if (USE_MOCK_BACKEND) {
    await delay(1500);
    return {};
  }
  const data = {
    hash,
    currency: switchAbbrAndName(currency).toLowerCase(),
    target_address: recipient,
    value
  };
  const result = await api.post('/transactions', data, { headers: { Authorization: `Bearer ${token}` } });
  if (result.ok) {
    return {};
  }
  if (result.problem === 'NETWORK_ERROR') return { error: errors.NETWORK };
  if (result.status === 400) return { error: errors.BAD_INPUT };
  if (result.status === 401)  return { error: errors.UNAUTHORIZED };
  return { error: errors.SOMETHING_ELSE };
}

export type GetBalanceHistoryResultType = { data: BalanceHistoryEntryType[] } | { error: 'NETWORK' | 'BAD_INPUT' | 'UNAUTHORIZED' | 'SOMETHING_ELSE' };
export async function getBalanceHistory(token: string, currency: CryptoCurrencyType): Promise<GetBalanceHistoryResultType> {
  const result = await api.get(`/transactions/balance-chart?currency=${switchAbbrAndName(currency).toLowerCase()}&period=y`,
    {},
    { headers: { Authorization: `Bearer ${token}` } }
  );
  // console.log(currency, result);
  if (result.ok) {
    try {
      const data = result.data.data.map(entry => {
        try {
          return {
            timestamp: Moment(entry.time).valueOf(),
            value: entry.last_value,
            average: entry.value
          };
        } catch (e) {
          return false;
        }
      }).filter(item => item);
      return { data };
    } catch (e) {
      return { error: errors.SOMETHING_ELSE };
    }
  }
  if (result.problem === 'NETWORK_ERROR') return { error: errors.NETWORK }; 
  if (result.status === 400) return { error: errors.BAD_INPUT };
  if (result.status === 401) return { error: errors.UNAUTHORIZED };
  return { error: errors.SOMETHING_ELSE };
}

export async function setPendingTxTimestamps(currency: CryptoCurrencyType, transactions: TransactionHistoryEntryType[]): Promise<void> {
  const pending = R.pipe(
    R.filter(R.propEq('status', 'pending')),
    R.map(R.pick(['id', 'timestamp']))
  )(transactions);
  try {
    await AsyncStorage.setItem(`${currency}PendingTxTimestamps`, JSON.stringify(pending));
  } catch (e) {
    return;
  }
}


export async function getPendingTxTimestamps(currency: CryptoCurrencyType, transactions: TransactionHistoryEntryType[]): Promise<TransactionHistoryEntryType[]> {
  let savedTxs = [];
  try {
    const result = await AsyncStorage.getItem(`${currency}PendingTxTimestamps`);
    savedTxs = JSON.parse(result) || [];
  } catch (e) {
    return transactions;
  }
  return R.map((transaction) => {
    if (transaction.status !== 'pending') return transaction;
    const savedTx = R.find(R.propEq('id', transaction.id))(savedTxs);
    if (!savedTx) return transaction;
    return { ...transaction, timestamp: savedTx.timestamp };
  })(transactions);
}


export async function setLastCheckedTx(currency: CryptoCurrencyType, timestamp: number): Promise<void>  {
  try {
    await AsyncStorage.setItem(`${currency}LastChecked`, `${timestamp}`);
  } catch (e) {
    return;
  }
}

export async function getLastCheckedTx(currency: CryptoCurrencyType): Promise<number>  {
  try {
    const result = await AsyncStorage.getItem(`${currency}LastChecked`);
    return +result;
  } catch (e) {
    return 0;
  }
}