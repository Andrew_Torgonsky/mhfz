// flow

export function getActiveRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
    return getActiveRouteName(route);
  }
  return route.routeName;
}

export function capitalizeString(string: string): string {
  return string.slice(0, 1).toUpperCase() + string.slice(1).toLowerCase();
}

export function truncateString(string: string, length: number): string {
  const roundedLength = Math.floor(length);
  if (string.length <= roundedLength) {
    return string;
  }
  return `${string.slice(0, roundedLength)}...`;
}

export function delay<T>(ms: number, value?: T): Promise<T> {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(value);
    }, ms);
  });
}

export function validatePassword(password: string): boolean {
  const regex = /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{8,14}$/;
  return regex.test(password);
}

