import { Platform, Dimensions } from 'react-native';

export const isIOS = Platform.OS === 'ios';
export const isAndroid = Platform.OS === 'android';

const designedWidth = Platform.OS === 'ios' ? 375 : 360;
const designedHeight = Platform.OS === 'ios' ? 667 : 640;

export const { width } = Dimensions.get('window');
const { height: initialHeight } = Dimensions.get('window');
export const isIphoneX = initialHeight === 812 && Platform.OS === 'ios';
export const height = isIphoneX ? 785 : initialHeight;

export const scale = (value, specialWidth = width) => (specialWidth / designedWidth) * value;
export const scaleByVertical = (value, specialHeight = height) => (specialHeight / designedHeight) * value;

export const platformSelect = (ios, android) => (
  Platform.select({ ios, android })
);

