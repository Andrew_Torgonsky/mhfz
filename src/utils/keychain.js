import * as Keychain from 'react-native-keychain';
import CryptoJS from 'crypto-js';

export async function get(key, password) {
  try {
    const credentials = await Keychain.getGenericPassword({ service: key });
    const decrypted = CryptoJS.AES.decrypt(credentials.password, password).toString(CryptoJS.enc.Utf8);
    return decrypted || false;
  } catch (e) {
    // console.log(e);
    return false;
  }
}

export async function set(key, password, value) {
  try {
    const encrypted = CryptoJS.AES.encrypt(value, password).toString();
    await Keychain.setGenericPassword('user', encrypted, { service: key });
    return true;
  } catch (e) {
    // console.log(e);
    return false;
  }
}

export async function check(key) {
  try {
    const result = await Keychain.getGenericPassword({ service: key });
    return !!result;
  } catch (e) {
    // console.log(e);
    return false;
  }
}

export async function reset(key) {
  try {
    await Keychain.resetGenericPassword({ service: key });
    return true;
  } catch (e) {
    // console.log(e);
    return false;
  }
}
