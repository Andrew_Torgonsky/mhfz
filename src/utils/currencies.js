// @flow

import Big from 'big.js';
import Moment from 'moment';
import R from 'ramda';
import type { WalletsStatType, WalletsStatItemType, FiatCurrencyType, BalanceHistoryEntryType } from '../types';

Big.RM = 0; // set rounding mode
Big.NE = -10; // set fractional part

export function getCurrencySign(currency?: FiatCurrencyType): string {
  switch (currency) {
    case 'usd':
      return '$';
    case 'eur':
      return '€';
    case 'sar':
      return 'SR';
    default:
      return '';
  }
}

export function inFiat(balance: number = 0, rate: number = 0, defaultCurrency?: FiatCurrencyType): string {
  return getCurrencySign(defaultCurrency) + new Big(balance).times(rate).toFixed(2).toString();
}

export function getDailyChange(walletStat: WalletsStatItemType): number {
  const yesterdayTimestamp: number = Moment().subtract(1, 'days').valueOf();
  const prevBalance: BalanceHistoryEntryType = R.reduce(
    (acc, cur) => cur.timestamp > acc.timestamp && cur.timestamp <= yesterdayTimestamp ? cur : acc,
    { timestamp: 0, value: 0 }, walletStat.history
  );
  if (prevBalance.value === 0) {
    return Infinity
  }
  return +(new Big(walletStat.balance).minus(prevBalance.value).div(prevBalance.value).times(100).toFixed(2).toString());
}

export function getCombinedDailyChange(walletsStat: WalletsStatType): number {
  const yesterdayTimestamp: number = Moment().subtract(1, 'days').valueOf();
  const yesterdayAndTodayInFiat = R.pipe(
    R.map((walletStat: WalletsStatItemType) => ({
      yesterday: R.reduce(
        (acc, cur) => cur.timestamp > acc.timestamp && cur.timestamp <= yesterdayTimestamp ? cur : acc,
        { timestamp: 0, value: 0 }, walletStat.history
      ).value,
      today: walletStat.balance,
      rate: walletStat.rate
    })),
    R.values,
    R.filter(el => el.yesterday > 0),
    R.reduce(
      (acc, cur) => ({ yesterday: acc.yesterday + cur.yesterday * cur.rate, today: acc.today + cur.today * cur.rate }),
      { today: 0, yesterday: 0 }
    )
  )(walletsStat);
  if (yesterdayAndTodayInFiat.yesterday === 0) {
    return Infinity;
  }
  return +(new Big(yesterdayAndTodayInFiat.today).minus(yesterdayAndTodayInFiat.yesterday).div(yesterdayAndTodayInFiat.yesterday).times(100).toFixed(2).toString());
}

export function combineBalancesToBtc(walletsStat: WalletsStatType): number {
  const { bitcoin: { rate } } = walletsStat;
  if (rate === 0) return 0;
  return R.pipe(
    R.values,
    R.map(el => new Big(el.balance).times(el.rate).div(rate)),
    R.reduce((acc, cur) => acc.plus(cur), new Big(0)),
    big => +big.toFixed(4).toString()
  )(walletsStat);
}

export function combineBalancesToFiat(walletsStat: WalletsStatType): number {
  return R.pipe(
    R.values,
    R.map(el => +new Big(el.balance).times(el.rate)),
    R.reduce((acc, cur) => acc.plus(cur), new Big(0)),
    big => +big.toFixed(2).toString()
  )(walletsStat);
}


export function switchAbbrAndName(name: string): string {
  switch (name) {
    case 'BTC':
      return 'bitcoin';
    case 'ETH':
      return 'ethereum';
    case 'XRP':
      return 'ripple';
    case 'bitcoin':
      return 'BTC';
    case 'ethereum':
      return 'ETH';
    case 'ripple':
      return 'XRP';
    default:
      return '';
  }
}


export function amountAndFee(amountToSend: number | string, feeOptions: Array<Object>, chosenFeeOption: number): number {
  try {
    const feeOption = feeOptions[chosenFeeOption];
    return +(new Big(amountToSend).plus(feeOption.fee).toString());
  } catch (e) {
    return 0;
  }
}
