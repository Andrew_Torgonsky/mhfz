// @flow

import R from 'ramda';
import Moment from 'moment';
import type { BalanceHistoryEntryType, WalletsStatItemType, Point, WalletsStatType } from '../types';

export function getHistoryForChart(walletStat: WalletsStatItemType, balancePeriod: 0 | 1 | 2, currentTimestamp?: number = Moment().valueOf()): Point[] {
  const { history } = walletStat;
  if (history.length === 0) return [];
  const periods = [7, 30, 365];
  const limitTimestamp: number = Moment(currentTimestamp).subtract(periods[balancePeriod], 'days').valueOf();
  const firstTimestamp: number = R.reduce((acc, elem) => elem.timestamp < acc ? elem.timestamp : acc, Moment().valueOf(), history);
  const firstPoint: BalanceHistoryEntryType = { average: 0, timestamp: Moment(firstTimestamp).subtract(1, 'days').valueOf() }
  const endPoint: BalanceHistoryEntryType = { average: walletStat.balance, timestamp: currentTimestamp };

  const beforeLimit: BalanceHistoryEntryType = R.reduce(
    (acc, elem) => elem.timestamp > acc.timestamp && elem.timestamp < limitTimestamp ? elem : acc,
    { timestamp: 0, average: 0 }, history
  );
  const limitPoint: BalanceHistoryEntryType = { average: beforeLimit.average, timestamp: limitTimestamp };

  return R.pipe(
    R.prepend(limitPoint),
    R.prepend(firstPoint),
    R.append(endPoint),
    R.map(el => ({ y: el.average, x: el.timestamp - limitTimestamp })),
    R.filter(el => el.x >= 0),
    R.sort((a, b) => a.x - b.x)
  )(history);
}

export function getCombinedHistoryForChart(walletsStat: WalletsStatType, balancePeriod: 0 | 1 | 2, currentTimestamp?: number = Moment().valueOf()): Point[] {
  const histories: Point[][] = R.pipe(
    R.map(
      element => R.pipe(
        getHistoryForChart,
        R.map(point => ({ ...point, y: point.y * element.rate }))
      )(element, balancePeriod, currentTimestamp)
    ),
    R.values
  )(walletsStat);

  const allXCoordinates: number[] = R.pipe(
    R.flatten,
    R.pluck('x'),
    R.uniq,
    R.sort((a, b) => a - b)
  )(histories);

  return R.map(
    x => ({
      x,
      y: R.reduce((acc, cur) => {
        const value = R.findLast(el => el.x <= x)(cur);
        return value ? value.y + acc : acc
      }, 0, histories)
    })
  )(allXCoordinates);
}