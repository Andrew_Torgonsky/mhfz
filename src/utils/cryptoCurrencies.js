// @flow

import bip39 from 'bip39';
import WAValidator from 'wallet-address-validator';
import * as blockchainAPI from '../api/blockchain';
import type { CryptoCurrencyType } from '../types';


export function processQRData(event: any): { address: string, currency: CryptoCurrencyType } {
  try {
    let { data } = event;
    if (data.includes(':')) data = data.slice(data.indexOf(':') + 1);
    if (data.includes('?')) data = data.slice(0, data.indexOf('?'));
    if (WAValidator.validate(data, 'bitcoin', 'both')) return { address: data, currency: 'bitcoin' };
    if (WAValidator.validate(data, 'ethereum', 'both')) return { address: data, currency: 'ethereum' };
    if (WAValidator.validate(data, 'ripple', 'both')) return { address: data, currency: 'ripple' };
    return { address: '', currency: 'bitcoin' };
  } catch (e) {
    return { address: '', currency: 'bitcoin' };
  }
}

export function generate12Words(): Array<string> {
  return bip39.generateMnemonic().split(' ');
}

export function generateWallets(words: Array<string>) {
  const seed = bip39.mnemonicToSeed(words.join(' '));
  const bitcoinWallet = blockchainAPI.bitcoin.generateWallet(seed);
  const ethereumWallet = blockchainAPI.ethereum.generateWallet(seed);
  const rippleWallet = blockchainAPI.ripple.generateWallet(seed);
  return {
    bitcoin: bitcoinWallet,
    ethereum: ethereumWallet,
    ripple: rippleWallet,
  };
}

export function validate12WordsAndTakeHash(words: Array<string | null>): string | void {
  if (!Array.isArray(words) || words.length !== 12) {
    return;
  }
  if (words.reduce((accumulator, currentVal) => typeof currentVal !== 'string' || currentVal.trim().length === 0 || accumulator, false)) {
    return;
  }
  return bip39.mnemonicToSeedHex(words.join(' '));
}

