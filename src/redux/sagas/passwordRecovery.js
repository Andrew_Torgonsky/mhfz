// @flow

import { put, call, fork, select, all, take, takeLatest } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { Keyboard } from 'react-native';
import { isValidNumber } from 'libphonenumber-js';
import { AppNavigator, navigate } from '../../navigation';
import { Scenes } from '../../navigation/shared';
import { Types, ActionCreators } from '../actions';
import translations from '../../translations';
import * as API from '../../api';
import { validatePassword } from '../../utils/other';

export function* requestPasswordRecovery(): Generator<*, *, *> {
  const { form: { phonePrefix, phoneNumber } } = yield select();
  if (!isValidNumber(`+${phonePrefix}${phoneNumber}`)) {
    yield put(ActionCreators.changeForm('phoneError', true));
    yield put(ActionCreators.changeAlert(translations.t('WRONG_PHONE'), 'red'));
    return;
  }
  yield call(Keyboard.dismiss);
  yield put(ActionCreators.changePending('passwordRecovery', true));
  const result: API.RequestRecoveryResultType = yield call(API.requestRecovery, phonePrefix + phoneNumber);
  if (result.error) {
    if (result.error === API.errors.UNAUTHORIZED) {
      yield put(ActionCreators.changeForm('phoneError', true));
      yield put(ActionCreators.changeAlert(translations.t('NO_PHONE'), 'red'));
    }
    if (result.error === API.errors.NETWORK) {
      yield put(ActionCreators.changeAlert(translations.t('NETWORK_ERROR'), 'red'));
    }
    if (result.error === API.errors.BAD_INPUT) {
      yield put(ActionCreators.changeAlert(translations.t('INVALID_INPUT'), 'red'));
    }
    if (result.error === API.errors.TOO_MANY_REQUESTS) {
      yield put(ActionCreators.changeAlert(translations.t('TOO_MANY_REQ'), 'red')); // do smt here
    }
    if (result.error === API.errors.SOMETHING_ELSE) {
      yield put(ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red'));
    }
    yield put(ActionCreators.changePending('passwordRecovery', false));
    return;
  }
  yield put(ActionCreators.changeForm('smsCodeTimestamp', Date.now()));
  yield put(ActionCreators.changePending('passwordRecovery', false));
  // yield put(AppNavigator.router.getActionForPathAndParams(Scenes.ForgotPasswordConfirm));
  yield call(navigate, Scenes.ForgotPasswordConfirm);
}

export function* checkRecoverySecret(): Generator<*, *, *> {
  const { form: { phonePrefix, phoneNumber, smsCode } } = yield select();
  yield put(ActionCreators.changePending('passwordRecovery', true));
  const result: API.CheckRecoverySecretResultType = yield call(API.checkRecoverySecret, phonePrefix + phoneNumber, smsCode);
  if (result.error) {
    if ([API.errors.NO_TEMP_SESSION, API.errors.BAD_INPUT].includes(result.error)) {
      yield put(ActionCreators.changeAlert(translations.t('WRONG_CODE'), 'red'));
      yield put(ActionCreators.changeForm('smsCodeError', true));
    }
    if (result.error === API.errors.NETWORK) {
      yield put(ActionCreators.changeAlert(translations.t('NETWORK_ERROR'), 'red'));
    }
    if (result.error === API.errors.SOMETHING_ELSE) {
      yield put(ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red'));
    }
    yield put(ActionCreators.changeForm('smsCode', ''));
    yield put(ActionCreators.changePending('passwordRecovery', false));
    yield call(delay, 2000);
    return;
  }
  yield put(ActionCreators.changePending('passwordRecovery', false));
  yield call(navigate, Scenes.ForgotPasswordReset);
}

export function* updatePassword(): Generator<*, *, *> {
  const {
    form: {
      phonePrefix, phoneNumber, smsCode, password
    }
  } = yield select();
  if (!validatePassword(password)) {
    yield put(ActionCreators.changeForm('passwordError', true));
    yield put(ActionCreators.changeAlert(translations.t('INVALID_PASSWORD'), 'red'));
    return;
  }
  yield call(Keyboard.dismiss);
  yield put(ActionCreators.changePending('passwordRecovery', true));
  const result: API.UpdatePasswordResultType = yield call(API.updatePassword, phonePrefix + phoneNumber, smsCode, password);
  if (result.error) {
    if (result.error === API.errors.NO_TEMP_SESSION) {
      yield put(ActionCreators.changeAlert(translations.t('CODE_EXPIRED'), 'red'));
      // yield put(AppNavigator.router.getActionForPathAndParams(Scenes.ForgotPasswordPhone));
      yield call(navigate, Scenes.ForgotPasswordPhone);
    }
    if (result.error === API.errors.NETWORK) {
      yield put(ActionCreators.changeAlert(translations.t('NETWORK_ERROR'), 'red'));
    }
    if (result.error === API.errors.BAD_INPUT) {
      yield put(ActionCreators.changeAlert(translations.t('INVALID_INPUT'), 'red'));
    }
    if (result.error === API.errors.SOMETHING_ELSE) {
      yield put(ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red'));
    }
    yield put(ActionCreators.changePending('passwordRecovery', false));
    return;
  }
  yield put(ActionCreators.changeForm('password', ''));
  yield put(ActionCreators.changePending('passwordRecovery', false));
  yield put(ActionCreators.changeAlert(translations.t('PWD_CHANGED'), 'green'));
  yield put(ActionCreators.changeForm('smsCode', ''));
  yield call(navigate, Scenes.SignIn);
}

export default function* signUpSaga(): Generator<*, *, *> {
  yield takeLatest(Types.REQUEST_PASSWORD_RECOVERY, requestPasswordRecovery);
  yield takeLatest(Types.CHECK_RECOVERY_SECRET, checkRecoverySecret);
  yield takeLatest(Types.UPDATE_PASSWORD, updatePassword);
}
