// @flow

import { AsyncStorage, Clipboard, Keyboard } from 'react-native';
import { put, call, fork, select, all, take, cancel, takeLatest, takeEvery } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import WAValidator from 'wallet-address-validator';
import { AppNavigator, Scenes, navigate, goBack } from '../../navigation';
import { Types, ActionCreators } from '../actions';
import * as blockchainAPI from '../../api/blockchain';
import { USE_BLOCKCHAIN_TESTNET } from '../../config';
import type { ReduxStateType, CryptoCurrencyType } from '../../types';
import translations, { isRTL } from '../../translations';
import { switchAbbrAndName } from '../../utils/currencies';
import * as API from '../../api';

const currencies: CryptoCurrencyType[] = ['bitcoin', 'ethereum', 'ripple'];

export function* prepareToSend(): Generator<*, *, *> {
  yield put(ActionCreators.changeForm('sendPreparing', true));
  yield put(ActionCreators.changeForm('feeOptions', []));
  yield call(delay, 1000);
  const {
    form: {
      addressToSend,
      amountToSend,
      currencyToSend,
      contactToSend,
    }
  }: ReduxStateType = yield select();

  // syncing differences between address, currency and contact to send
  if (contactToSend && contactToSend.wallets.findIndex(wallet => wallet.walletAddress === addressToSend) === -1) {
    yield put(ActionCreators.changeForm('contactToSend', undefined));
    yield put(ActionCreators.prepareToSend());
    return;
  }
  if (contactToSend && contactToSend.wallets.findIndex(wallet => wallet.walletAddress === addressToSend && wallet.currency === currencyToSend) === -1) {
    const newWalletToSend = contactToSend.wallets.find(wallet => wallet.currency === currencyToSend);
    if (newWalletToSend) {
      yield put(ActionCreators.changeForm('addressToSend', newWalletToSend.walletAddress));
    } else {
      yield put(ActionCreators.changeForm('addressToSend', ''));
    }
    yield put(ActionCreators.prepareToSend());
    return;
  }
  // =================================================================
  const {
    wallets: {
      [currencyToSend]: {
        walletAddress
      },
    },
  }: ReduxStateType = yield select();
  const result = yield call(blockchainAPI[currencyToSend].getFeeOptions, walletAddress, addressToSend, amountToSend);
  yield put(ActionCreators.changeForm('feeOptions', result));
  yield put(ActionCreators.changeForm('chosenFeeOption', 0));
  yield put(ActionCreators.changeForm('sendPreparing', false));
}

export function* send({ toContact }: any): Generator<*, *, *> {
  const {
    form: {
      addressToSend,
      amountToSend,
      currencyToSend,
      feeOptions,
      chosenFeeOption,
      contactToSend
    }, 
    session: {
      token
    }
  }: ReduxStateType = yield select();
  const {
    wallets: {
      [currencyToSend]: {
        walletAddress,
        privateKey
      }
    }
  }: ReduxStateType = yield select();
  if (!WAValidator.validate(addressToSend, currencyToSend, USE_BLOCKCHAIN_TESTNET ? 'testnet' : 'prod')) {
    yield put(ActionCreators.changeAlert(translations.t('INVALID_ADDRESS'), 'red'));
    return;
  }
  if (feeOptions.length < chosenFeeOption + 1) {
    yield put(ActionCreators.prepareToSend());
    return;
  }
  if (toContact && !contactToSend) {
    yield put(ActionCreators.prepareToSend());
    return;
  }
  if (!amountToSend || +amountToSend === 0) {
    return;
  }
  yield call(Keyboard.dismiss);
  yield put(ActionCreators.changePending('send', true));
  const result = yield call(blockchainAPI[currencyToSend].send, walletAddress, privateKey, addressToSend, amountToSend, feeOptions[chosenFeeOption]);
  if (!result || result.error) {
    if (result.error === 'INSUFFICIENT_FUNDS') yield put(ActionCreators.changeAlert(translations.t('INSUFFICIEND_FUNDS'), 'red'));
    if (result.error === 'LOW_FEE') yield put(ActionCreators.changeAlert(translations.t('LOW_FEE'), 'red'));
    if (result.error === 'DESTINATION_NOT_EXIST') yield put(ActionCreators.changeAlert(translations.t('DEST_ADDR_NOT_CREATED'), 'red'));
    if (result.error === 'SOMETHING_ELSE') yield put(ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red'));
    yield put(ActionCreators.changePending('send', false));
    return;
  }
  // notify back about this transaction
  yield call(API.addTransaction, token, result.txid, currencyToSend, addressToSend, +amountToSend);
  const recipient = contactToSend && (contactToSend.firstName || contactToSend.lastName) ? `${contactToSend.firstName} ${contactToSend.lastName}` : addressToSend;
  const message = `${amountToSend} ${switchAbbrAndName(currencyToSend)} ${translations.t('HAS_BEEN_SENT')}\n${recipient}`;
  yield put(ActionCreators.changeAlert(message, 'green'));
  yield call(goBack);
  yield put(ActionCreators.changePending('send', false));
  // request balance and history
  yield put(ActionCreators.requestBalances());
  yield put(ActionCreators.requestHistories());
}

function* pasteAddressFromClipboard(): Generator<*, *, *> {
  const string = yield call(Clipboard.getString);
  yield put(ActionCreators.changeForm('addressToSend', string));
  yield call(prepareToSend);
}

function* updateBalance(currency: CryptoCurrencyType): Generator<*, *, *> {
  const {
    wallets: {
      [currency]: {
        walletAddress
      }
    }
  }: ReduxStateType = yield select();
  const balance = yield call(blockchainAPI[currency].getBalance, walletAddress);
  if (balance && balance.total !== undefined) {
    yield put(ActionCreators.changeWalletsStatMultiple(currency, {
      balance: balance.total,
      alertText: '',
      alertColor: 'red'
    }));
  } else {
    yield put(ActionCreators.changeWalletsStatMultiple(currency, {
      balance: 0,
      alertText: translations.t('WALLET_MAINTANENCE'),
      alertColor: 'yellow'
    }));
  }
}

function* updateHistory(currency: CryptoCurrencyType, loadMore: boolean): Generator<*, *, *> {
  yield put(ActionCreators.changeWalletsStat(currency, 'receivingTransactions', true));
  const {
    wallets: {
      [currency]: {
        walletAddress
      },
    },
    walletsStat: {
      [currency]: {
        transactions
      }
    }
  }: ReduxStateType = yield select();
  const result = yield call(blockchainAPI[currency].getHistory, walletAddress, loadMore ? transactions.map(tx => tx.id) : []);

  const withRestoredTimestamps = yield call(API.getPendingTxTimestamps, currency, result.transactions);
  yield put(ActionCreators.putTransactions(currency, withRestoredTimestamps));
  const {
    walletsStat: {
      [currency]: {
        transactions: mergedTransactions
      }
    }
  }: ReduxStateType = yield select();
  yield call(API.setPendingTxTimestamps, currency, mergedTransactions);
  if (result.status === 'END_OF_LIST') yield put(ActionCreators.changeWalletsStat(currency, 'endOfTransactionList', true));
  yield call(delay, 2000);
  yield put(ActionCreators.changeWalletsStat(currency, 'receivingTransactions', false));
}

function* requestBalancesWatcher(): Generator<*, *, *> {
  yield all(currencies.map((currency) => fork(function* () {
    while (true) {
      yield take(Types.REQUEST_BALANCES);
      yield call(updateBalance, currency);
    }
  })));
}

function* updateHistoryWatcher(): Generator<*, *, *> {
  yield all(currencies.map(currency => fork(function* () {
    while (true) {
      const action = yield take(Types.UPDATE_HISTORY);
      if (currency !== action.currency) continue;
      yield call(updateHistory, action.currency, action.loadMore);
    }
  })));
}

function* requestHistories(): Generator<*, *, *> {
  yield all(currencies.map(currency => put(ActionCreators.updateHistory(currency, false))));
}

function* checkTxHistory({ currency }): Generator<*, *, *> {
  const timestamp = Date.now();
  yield put(ActionCreators.changeWalletsStat(currency, 'lastChecked', timestamp));
  yield put(ActionCreators.changeWalletsStat(currency, 'newTx', false));
  yield call(API.setLastCheckedTx, currency, timestamp);
}

function* updateBalanceHistory({ currency }): Generator<*, *, *> {
  const { session: { token } }: ReduxStateType = yield select();
  const result: API.GetBalanceHistoryResultType = yield call(API.getBalanceHistory, token, currency);
  if (result.error) {
    // handle error, for example schedule another update in 30s
    return;
  }
  yield put(ActionCreators.changeWalletsStat(currency, 'history', result.data));
}

export default function* (): Generator<*, *, *> {
  yield fork(requestBalancesWatcher);
  yield fork(updateHistoryWatcher);
  yield takeLatest(Types.REQUEST_HISTORIES, requestHistories);
  yield takeLatest(Types.PREPARE_TO_SEND, prepareToSend);
  yield takeLatest(Types.SEND, send);
  yield takeLatest(Types.PASTE_ADDRESS_FROM_CLIPBOARD, pasteAddressFromClipboard);
  yield takeEvery(Types.CHECK_TX_HISTORY, checkTxHistory);
  yield takeEvery(Types.UPDATE_BALANCE_HISTORY, updateBalanceHistory);
}
