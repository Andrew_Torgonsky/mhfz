// @flow

import R from 'ramda';
import { AsyncStorage, Keyboard } from 'react-native';
import { put, call, fork, select, all, take, takeLatest } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { isValidNumber } from 'libphonenumber-js';
import emailValidator from 'email-validator';
import shuffle from 'shuffle-array';
import { AppNavigator, navigate } from '../../navigation';
import { Scenes } from '../../navigation/shared';
import { Types, ActionCreators } from '../actions';
import translations from '../../translations';
import * as API from '../../api';
import { generate12Words, validate12WordsAndTakeHash, generateWallets } from '../../utils/cryptoCurrencies';
import { validatePassword } from '../../utils/other';
import * as Keychain from '../../utils/keychain';
import { updateBalance, currencies } from './other';
import type { ReduxStateType } from '../../types';

export function* submitSignUp(): Generator<*, *, *> {
  console.log('submit sign up');
  const {
    form: {
      email,
      phonePrefix,
      phoneNumber,
      password,
      agree
    }
  }: ReduxStateType = yield select();
  if (!emailValidator.validate(email)) {
    yield put(ActionCreators.changeForm('emailError', true));
    yield put(ActionCreators.changeAlert(translations.t('INVALID_EMAIL'), 'red'));
    return;
  }

  if (!isValidNumber(`+${phonePrefix}${phoneNumber}`)) {
    yield put(ActionCreators.changeForm('phoneError', true));
    yield put(ActionCreators.changeAlert(translations.t('WRONG_PHONE'), 'red'));
    return;
  }
  if (!validatePassword(password)) {
    yield put(ActionCreators.changeForm('passwordError', true));
    yield put(ActionCreators.changeAlert(translations.t('INVALID_PASSWORD'), 'red'));
    return;
  }
  if (!agree) {
    yield put(ActionCreators.changeAlert(translations.t('NOT_AGREE'), 'red'));
    return;
  }
  yield call(Keyboard.dismiss);
  yield put(ActionCreators.changePending('signUp', true));
  const result: API.CheckUserResultType = yield call(API.checkUser, email, phonePrefix + phoneNumber, password);
  if (result.error) {
    if (result.error === API.errors.ALREADY_EXIST) {
      yield put(ActionCreators.changeFormMultiple({
        emailError: true,
        phoneError: true
      }));
      yield put(ActionCreators.changeAlert(translations.t('USER_EXISTS'), 'red'));
    }
    if (result.error === API.errors.NETWORK) {
      yield put(ActionCreators.changeAlert(translations.t('NETWORK_ERROR'), 'red'));
    }
    if (result.error === API.errors.BAD_INPUT) {
      yield put(ActionCreators.changeAlert(translations.t('INVALID_INPUT'), 'red'));
    }
    if (result.error === API.errors.TOO_MANY_REQUESTS) {
      yield put(ActionCreators.changeAlert(translations.t('TOO_MANY_REQ'), 'red'));
    }
    if (result.error === API.errors.SOMETHING_ELSE) {
      yield put(ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red'));
    }
    yield put(ActionCreators.changePending('signUp', false));
    return;
  }
  yield put(ActionCreators.changeForm('smsCodeTimestamp', Date.now()));
  yield put(ActionCreators.changePending('signUp', false));
  yield put(ActionCreators.changeForm('smsCode', ''));
  // yield put(AppNavigator.router.getActionForPathAndParams(Scenes.SignUpPhoneConfirmation));
  yield call(navigate, Scenes.SignUpPhoneConfirmation);
}


export function* checkSignUpSmsCode(): Generator<*, *, *> {
  const { form: { phonePrefix, phoneNumber, smsCode } }: ReduxStateType = yield select();
  yield call(Keyboard.dismiss);
  yield put(ActionCreators.changePending('signUp', true));
  const result: API.CheckUserSecretResultType = yield call(API.checkUserSecret, phonePrefix + phoneNumber, smsCode);
  if (result.error) {
    if (result.error === API.errors.NO_TEMP_SESSION) {
      yield put(ActionCreators.changeForm('smsCodeError', true));
      yield put(ActionCreators.changeAlert(translations.t('WRONG_CODE'), 'red'));
    }
    if (result.error === API.errors.NETWORK) yield put(ActionCreators.changeAlert(translations.t('NETWORK_ERROR'), 'red'));
    if (result.error === API.errors.BAD_INPUT) yield put(ActionCreators.changeAlert(translations.t('INVALID_INPUT'), 'red'));
    if (result.error === API.errors.SOMETHING_ELSE) yield put(ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red'));
    yield put(ActionCreators.changeForm('smsCode', ''));
    yield put(ActionCreators.changePending('signUp', false));
    return;
  }
  yield put(ActionCreators.changePending('signUp', false));
  // generating words for the next step
  const generatedWords = generate12Words();
  const randWords = shuffle([...generatedWords]).map(val => ({ text: val, visible: true }));
  yield put(ActionCreators.changeFormMultiple({
    twelveWordsGenerated: generatedWords,
    twelveWordsRandom: randWords
  }));
  // yield put(AppNavigator.router.getActionForPathAndParams(Scenes.SignUpWords));
  yield call(navigate, Scenes.SignUpWords);
}


export function* compareSignUpTwelveWords(): Generator<*, *, *> {
  const {
    form: {
      phonePrefix, phoneNumber, twelveWords, twelveWordsGenerated, smsCode
    }
  }: ReduxStateType = yield select();
  if (!R.equals(twelveWords, twelveWordsGenerated)) {
    yield put(ActionCreators.changeAlert(translations.t('WRONG_COMB'), 'red'));
    yield put(ActionCreators.clearTwelveWords());
    return;
  }
  yield put(ActionCreators.changePending('signUp', true));
  yield call(delay, 50);
  const mnemonicHash = validate12WordsAndTakeHash(twelveWordsGenerated);
  const wallets = generateWallets(twelveWordsGenerated);
  const result: API.AddUserResultType = yield call(
    API.addUser,
    phonePrefix + phoneNumber,
    mnemonicHash,
    wallets.ethereum.walletAddress,
    wallets.bitcoin.walletAddress,
    wallets.ripple.walletAddress,
    smsCode
  );
  if (result.error) {
    if (result.error === API.errors.ALREADY_EXIST) {
      yield put(ActionCreators.changeFormMultiple({
        emailError: true,
        phoneError: true
      }));
      yield put(ActionCreators.changeAlert(translations.t('USER_EXISTS'), 'red'));
      // yield put(AppNavigator.router.getActionForPathAndParams(Scenes.SignUp));
      yield call(navigate, Scenes.SignUp);
    }
    if (result.error === API.errors.NO_TEMP_SESSION) {
      yield put(ActionCreators.changeAlert(translations.t('CODE_EXPIRED'), 'red'));
      // yield put(AppNavigator.router.getActionForPathAndParams(Scenes.SignUp));
      yield call(navigate, Scenes.SignUp);
    }
    if (result.error === API.errors.NETWORK) {
      yield put(ActionCreators.changeAlert(translations.t('NETWORK_ERROR'), 'red'));
    }
    if (result.error === API.errors.BAD_INPUT) {
      yield put(ActionCreators.changeAlert(translations.t('INVALID_INPUT'), 'red'));
    }
    if (result.error === API.errors.SOMETHING_ELSE) {
      yield put(ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red'));
    }
    yield put(ActionCreators.changePending('signUp', false));
    return;
  }
  yield put(ActionCreators.changePending('signUp', false));
  yield put(ActionCreators.changeSessionMultiple(result));
  yield put(ActionCreators.changeAppState('wallets', wallets));
  // yield put(AppNavigator.router.getActionForPathAndParams(Scenes.SignUpPin));
  yield call(navigate, Scenes.SignUpPin);
}


export function* compareSignUpPinCodes(): Generator<*, *, *> {
  const {
    form: {
      pinCode, pinCodeConfirm, twelveWords
    },
    session: {
      token,
    },
    wallets
  }: ReduxStateType = yield select();
  if (pinCode !== pinCodeConfirm) {
    yield put(ActionCreators.changeForm('pinCodeError', true));
    yield put(ActionCreators.changeAlert(translations.t('PIN_NOT_MATCH'), 'red'));
    yield delay(2000);
    // yield put(AppNavigator.router.getActionForPathAndParams(Scenes.SignUpPin, { noModal: true }));
    yield call(navigate, Scenes.SignUpPin, { noModal: true });
    yield put(ActionCreators.changeFormMultiple({
      pinCodeError: false,
      pinCode: '',
      pinCodeConfirm: ''
    }));
    yield put(ActionCreators.changeAlert('', ''));
    return;
  }
  yield put(ActionCreators.changeFormMultiple({
    pinCode: '',
    pinCodeConfirm: ''
  }));
  yield put(ActionCreators.changePending('signUp', true));
  yield call(delay, 50);
  // turn on push notifications
  const mnemonicHash = validate12WordsAndTakeHash(twelveWords);
  yield call(API.notificationsAuth, mnemonicHash);
  yield call(API.switchNotifications, true);
  // Get notifications status
  const notificationStatus = yield call(API.getNotificationStatus);
  yield put(ActionCreators.changeAppState('pushNotifications', notificationStatus));

  yield call(Keychain.set, 'mahfazahToken', pinCode, token);
  // yield call(Keychain.set, 'mahfazahMnemonicHash', pinCode, mnemonicHash);
  yield call(Keychain.set, 'mahfazahWallets', pinCode, JSON.stringify(wallets));
  // yield put(AppNavigator.router.getActionForPathAndParams(Scenes.MainWallet));
  yield call(navigate, Scenes.MainWallet);
  yield put(ActionCreators.resetForm());
  yield put(ActionCreators.changePending('signUp', false));
  // Get contacts
  yield put(ActionCreators.getContacts());
  // start receiving balance, history and rates
  yield fork(updateBalance);
  yield put(ActionCreators.startWs());
  // Reset last checked tx history
  yield all(currencies.map(currency => call(function* () {
    const timestamp = Date.now();
    yield put(ActionCreators.changeWalletsStat(currency, 'lastChecked', timestamp));
    yield call(API.setLastCheckedTx, currency, timestamp);
  })));
}

export default function* signUpSaga(): Generator<*, *, *> {
  yield takeLatest(Types.SUBMIT_SIGN_UP, submitSignUp);
  yield takeLatest(Types.CHECK_SIGN_UP_SMS_CODE, checkSignUpSmsCode);
  yield takeLatest(Types.COMPARE_SIGN_UP_TWELVE_WORDS, compareSignUpTwelveWords);
  yield takeLatest(Types.COMPARE_SIGN_UP_PIN_CODES, compareSignUpPinCodes);
}
