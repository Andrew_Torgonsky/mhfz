// @flow

import { put, call, fork, select, all, take, cancel, takeLatest } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { Types, ActionCreators } from '../actions';
import * as api from '../../api';
import type { ContactType, ReduxStateType } from '../../types';

export function* getContacts(): Generator<*, *, *> {
  const {
    session: {
      token,
      phone_number
    }
  }: ReduxStateType = yield select();
  const contacts: ContactType[] = yield call(api.getAllContacts, `+${phone_number}`);
  if (contacts.length === 0) {
    // retry after 20 seconds if fail
    yield call(delay, 20 * 1000);
    yield put(ActionCreators.getContacts());
    return;
  }
  const result: api.GetWalletsForContactsType = yield call(api.getWalletsForContacts, contacts, token);
  if (result.error) {
    if (result.error === api.errors.UNAUTHORIZED) {
      yield put(ActionCreators.logOut());
      return;
    }
    yield put(ActionCreators.changeAppState('contacts', contacts));
    // retry after 20 seconds if fail
    yield call(delay, 20 * 1000);
    yield put(ActionCreators.getContacts());
    return;
  }
  yield put(ActionCreators.changeAppState('contacts', result.contacts));
}

export default function* accountSaga(): Generator<*, *, *> {
  yield takeLatest(Types.GET_CONTACTS, getContacts);
}
