// @flow

import { AsyncStorage, Keyboard } from 'react-native';
import { put, call, fork, select, all, take, cancel, takeLatest, takeEvery } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { AppNavigator, navigate } from '../../navigation';
import { Scenes } from '../../navigation/shared';
import { Types, ActionCreators } from '../actions';
import translations, { languageInit } from '../../translations';
import signUpSaga from './signUp';
import signInSaga from './signIn';
import settingsSaga from './settings';
import contactsSaga from './contacts';
import passwordRecovery from './passwordRecovery';
import blockchainSaga from './blockchain';
import * as API from '../../api';
import * as Keychain from '../../utils/keychain';
import type { ReduxStateType, AlertType, CryptoCurrencyType } from '../../types';
import { changeAlert, updateBalance, currencies } from './other';
import { checkPinCode } from './reSignIn';


export function* startUp(): Generator<*, *, *> {
  yield put(ActionCreators.changePending('startUp', true));
  const isAuthorized = yield all({
    token: call(Keychain.check, 'mahfazahToken'),
    wallets: call(Keychain.check, 'mahfazahWallets'),
  });
  const { appMinimized }: ReduxStateType = yield select();
  if (appMinimized) {
    yield take(Types.APP_MAXIMIZED);
    yield put(ActionCreators.changeAppState('appMinimized', false));
  }
  if (isAuthorized.token && isAuthorized.wallets) {
    yield put(ActionCreators.resetForm());
    yield call(navigate, Scenes.EnterPin);
  } else {
    yield call(navigate, Scenes.SignIn);
  }
  yield takeLatest(Types.CHECK_PIN_CODE, checkPinCode);
  yield fork(signInSaga);
  yield fork(signUpSaga);
  yield fork(passwordRecovery);
  yield fork(settingsSaga);
  yield fork(contactsSaga);
  yield fork(blockchainSaga);
  yield put(ActionCreators.changePending('startUp', false));
}


export function* logOut(): Generator<*, *, *> {
  yield put(ActionCreators.changePending('logOut', true));
  const { session: { token } }: ReduxStateType = yield select();
  yield all([
    call(Keychain.reset, 'mahfazahToken'),
    call(Keychain.reset, 'mahfazahWallets'),
    call(API.signOut, token),
    call(API.notificationsLogOut),
    call(API.switchNotifications, false),
    ...currencies.map(currency => call(function* () {
      const timestamp = yield call(API.setLastCheckedTx, currency, 0);
    }))
  ]);
  yield put(ActionCreators.changePending('logOut', false));
  yield put(ActionCreators.reset());
  yield put(ActionCreators.resetForm());
  yield call(navigate, Scenes.SignIn);
}

export function* minimize(): Generator<*, *, *> {
  yield put(ActionCreators.changeAppState('appMinimized', true));
  yield put(ActionCreators.reset());
  yield call(navigate, 'initial');
}

export default function* (): Generator<*, *, *> {
  yield call(languageInit);
  yield takeEvery(Types.CHANGE_ALERT, changeAlert);
  while (true) {
    const startUpTasks = yield fork(startUp);
    let action = {};
    while (true) {
      action = yield take([Types.LOG_OUT, Types.APP_MINIMIZED]);
      const { preventMinimize }: ReduxStateType = yield select();
      if (action.type === Types.APP_MINIMIZED && preventMinimize) continue;
      break;
    }
    yield put(ActionCreators.stopWs());
    yield cancel(startUpTasks);
    if (action.type === Types.LOG_OUT) {
      yield call(logOut);
    }
    if (action.type === Types.APP_MINIMIZED) {
      yield call(minimize);
    }
  }
}
