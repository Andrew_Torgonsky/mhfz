// @flow


import { put, call, fork, select, all, take, cancel, takeLatest, takeEvery } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { AsyncStorage, Keyboard } from 'react-native';
import { Types, ActionCreators } from '../actions';
import * as API from '../../api';
import * as Keychain from '../../utils/keychain';
import { AppNavigator, navigate } from '../../navigation';
import { Scenes } from '../../navigation/shared';
import type { ReduxStateType, AlertType, CryptoCurrencyType } from '../../types';
import translations from '../../translations';
import { updateBalance, currencies } from './other';

export function* checkPinCode(): Generator<*, *, *> {
  const { form: { pinCode } }: ReduxStateType = yield select();
  yield put(ActionCreators.changePending('auth', true));
  const token = yield call(Keychain.get, 'mahfazahToken', pinCode);
  const wallets = yield call(Keychain.get, 'mahfazahWallets', pinCode);
  if (!token || !wallets) {
    yield put(ActionCreators.changePending('auth', false));
    yield put(ActionCreators.changeForm('pinCodeError', true));
    yield put(ActionCreators.changeAlert(translations.t('WRONG_PIN'), 'red'));
    yield delay(2000);
    yield put(ActionCreators.changeFormMultiple({
      pinCodeError: false,
      pinCode: '',
      pinCodeConfirm: ''
    }));
    yield put(ActionCreators.changeAlert('', ''));
    return;
  }
  const result: API.GetMeResultType = yield call(API.getMe, token);
  if (result.error) {
    if (result.error === API.errors.UNAUTHORIZED) {
      yield put(ActionCreators.logOut());
    }
    if (result.error === API.errors.NETWORK) {
      yield put(ActionCreators.changeAlert(translations.t('NETWORK_ERROR'), 'red'));
      yield put(ActionCreators.changeForm('pinCode', ''));
    }
    if (result.error === API.errors.SOMETHING_ELSE) {
      yield put(ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red'));
      yield put(ActionCreators.changeForm('pinCode', ''));
    }
    yield put(ActionCreators.changePending('auth', false));
    return;
  }
  yield put(ActionCreators.changeFormMultiple({
    pinCode: ''
  }));
  yield put(ActionCreators.changeSessionMultiple({
    token,
    ...result
  }));
  yield put(ActionCreators.changeAppState('wallets', JSON.parse(wallets)));
  // Restore last checked tx history
  
  yield all(currencies.map(currency => call(function* () {
    const timestamp = yield call(API.getLastCheckedTx, currency);
    yield put(ActionCreators.changeWalletsStat(currency, 'lastChecked', timestamp));
  })));
  yield call(navigate, Scenes.MainWallet);
  yield put(ActionCreators.changePending('auth', false));

  // start receiving balance, history and rates
  yield fork(updateBalance);
  yield put(ActionCreators.startWs());
  yield all(currencies.map(currency => put(ActionCreators.updateBalanceHistory(currency))));

  // Get notifications status
  const notificationStatus = yield call(API.getNotificationStatus);
  yield put(ActionCreators.changeAppState('pushNotifications', notificationStatus));
  // get contacts
  yield put(ActionCreators.getContacts());
}