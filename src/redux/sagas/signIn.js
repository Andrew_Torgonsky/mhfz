// @flow

import { AsyncStorage } from 'react-native';
import { put, call, fork, select, all, take, takeLatest } from 'redux-saga/effects';
import { Keyboard } from 'react-native';
import { delay } from 'redux-saga';
import { isValidNumber } from 'libphonenumber-js';
import { AppNavigator, Scenes, navigate } from '../../navigation';
import { Types, ActionCreators } from '../actions';
import translations from '../../translations';
import * as API from '../../api';
import { validate12WordsAndTakeHash, generateWallets } from '../../utils/cryptoCurrencies';
import { validatePassword } from '../../utils/other';
import * as Keychain from '../../utils/keychain';
import { updateBalance, currencies } from './other';
import type { ReduxStateType } from '../../types';

export function* submitSignInFields(): Generator<*, *, *> {
  const { form: { phonePrefix, phoneNumber, password } }: ReduxStateType = yield select();
  if (!isValidNumber(`+${phonePrefix}${phoneNumber}`)) {
    yield put(ActionCreators.changeForm('phoneError', true));
    yield put(ActionCreators.changeAlert(translations.t('WRONG_PHONE'), 'red'));
    return;
  }
  if (!validatePassword(password)) {
    yield put(ActionCreators.changeForm('passwordError', true));
    yield put(ActionCreators.changeAlert(translations.t('INVALID_PASSWORD'), 'red'));
    return;
  }
  yield call(Keyboard.dismiss);
  yield put(ActionCreators.changePending('signIn', true));
  const result: API.PreSignInResultType = yield call(API.preSignIn, phonePrefix + phoneNumber, password);
  if (result.error) {
    if (result.error === API.errors.UNAUTHORIZED) {
      yield put(ActionCreators.changeFormMultiple({
        passwordError: true,
        phoneError: true
      }));
      yield put(ActionCreators.changeAlert(translations.t('WRONG_U_OR_P'), 'red'));
    }
    if (result.error === API.errors.BAD_INPUT) {
      yield put(ActionCreators.changeAlert(translations.t('INVALID_INPUT'), 'red'));
    }
    if (result.error === API.errors.NETWORK) {
      yield put(ActionCreators.changeAlert(translations.t('NETWORK_ERROR'), 'red'));
    }
    if (result.error === API.errors.SOMETHING_ELSE) {
      yield put(ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red'));
    }
    yield put(ActionCreators.changePending('signIn', false));
    return;
  }
  yield put(ActionCreators.changePending('signIn', false));
  // yield put(AppNavigator.router.getActionForPathAndParams(Scenes.SignInWords));
  yield call(navigate, Scenes.SignInWords);
}


export function* submitSignInTwelveWords(): Generator<*, *, *> {
  const {
    form: {
      phonePrefix, phoneNumber, password, twelveWords
    }
  }: ReduxStateType = yield select();
  yield call(Keyboard.dismiss);
  yield put(ActionCreators.changePending('signIn', true));
  yield call(delay, 50);
  const mnemonicHash = validate12WordsAndTakeHash(twelveWords);
  if (!mnemonicHash) {
    yield put(ActionCreators.changeAlert(translations.t('WRONG_COMB'), 'red'));
    yield put(ActionCreators.clearTwelveWords());
    yield put(ActionCreators.changePending('signIn', false));
    return;
  }
  const wallets = generateWallets(twelveWords);
  const result: API.SignInResultType = yield call(API.signIn, phonePrefix + phoneNumber, password, mnemonicHash);
  if (result.error) {
    if (result.error === API.errors.UNAUTHORIZED) {
      yield put(ActionCreators.changeAlert(translations.t('WRONG_COMB'), 'red'));
      yield put(ActionCreators.clearTwelveWords());
    }
    if (result.error === API.errors.BAD_INPUT) {
      yield put(ActionCreators.changeAlert(translations.t('INVALID_INPUT'), 'red'));
    }
    if (result.error === API.errors.NETWORK) {
      yield put(ActionCreators.changeAlert(translations.t('NETWORK_ERROR'), 'red'));
    }
    if (result.error === API.errors.SOMETHING_ELSE) {
      yield put(ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red'));
    }
    yield put(ActionCreators.changePending('signIn', false));
    return;
  }
  yield put(ActionCreators.changePending('signIn', false));
  yield put(ActionCreators.changeSessionMultiple(result));
  yield put(ActionCreators.changeAppState('wallets', wallets));
  // yield put(AppNavigator.router.getActionForPathAndParams(Scenes.SignInPin));
  yield call(navigate, Scenes.SignInPin);
}


export function* compareSignInPinCodes(): Generator<*, *, *> {
  const {
    form: {
      pinCode, pinCodeConfirm, twelveWords
    },
    session: {
      token,
    },
    wallets
  }: ReduxStateType = yield select();
  if (pinCode !== pinCodeConfirm) {
    yield put(ActionCreators.changeForm('pinCodeError', true));
    yield put(ActionCreators.changeAlert(translations.t('PIN_NOT_MATCH'), 'red'));
    yield delay(2000);
    // yield put(AppNavigator.router.getActionForPathAndParams(Scenes.SignInPin));
    yield call(navigate, Scenes.SignInPin);
    yield put(ActionCreators.changeFormMultiple({
      pinCodeError: false,
      pinCode: '',
      pinCodeConfirm: ''
    }));
    yield put(ActionCreators.changeAlert('', ''));
    return;
  }
  yield put(ActionCreators.changePending('signIn', true));
  yield call(delay, 50);
  // turn on push notifications
  const mnemonicHash = validate12WordsAndTakeHash(twelveWords);
  yield call(API.notificationsAuth, mnemonicHash);
  yield call(API.switchNotifications, true);
  // Get notifications status
  const notificationStatus = yield call(API.getNotificationStatus);
  yield put(ActionCreators.changeAppState('pushNotifications', notificationStatus));

  yield call(Keychain.set, 'mahfazahToken', pinCode, token);
  // yield call(Keychain.set, 'mahfazahMnemonicHash', pinCode, mnemonicHash);
  yield call(Keychain.set, 'mahfazahWallets', pinCode, JSON.stringify(wallets));
  // yield put(AppNavigator.router.getActionForPathAndParams(Scenes.MainWallet));
  yield call(navigate, Scenes.MainWallet);
  yield put(ActionCreators.resetForm());
  yield put(ActionCreators.changePending('signIn', false));
  // Get contacts
  yield put(ActionCreators.getContacts());
  // start receiving balance, history and rates
  yield fork(updateBalance);
  yield put(ActionCreators.startWs());
  yield all(currencies.map(currency => put(ActionCreators.updateBalanceHistory(currency))));
  // Reset last checked tx history
  yield all(currencies.map(currency => call(function* () {
    const timestamp = Date.now();
    yield put(ActionCreators.changeWalletsStat(currency, 'lastChecked', timestamp));
    yield call(API.setLastCheckedTx, currency, timestamp);
  })));
}


export default function* signInSaga(): Generator<*, *, *> {
  yield takeLatest(Types.SUBMIT_SIGN_IN_FIELDS, submitSignInFields);
  yield takeLatest(Types.SUBMIT_SIGN_IN_TWELVE_WORDS, submitSignInTwelveWords);
  yield takeLatest(Types.COMPARE_SIGN_IN_PIN_CODES, compareSignInPinCodes);
}
