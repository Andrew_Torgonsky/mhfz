// @flow


import { put, call, fork, select, all, take, cancel, takeLatest, takeEvery } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { Types, ActionCreators } from '../actions';
import type { ReduxStateType, AlertType, CryptoCurrencyType, AlertStateType } from '../../types';

export function* changeAlert({ alertText, alertType }: AlertStateType): Generator<*, *, *> {
  const timestamp = Date.now();
  yield put(ActionCreators.changeAppState('alertState', {
    alertText, alertType, alertTimestamp: timestamp
  }));
  if (!alertText) return;
  yield call(delay, 3000);
  const { alertState: { alertTimestamp } }: ReduxStateType = yield select();
  if (timestamp !== alertTimestamp) return;
  yield put(ActionCreators.changeAppState('alertState', {
    alertText: '', alertType: 'red', alertTimestamp: Date.now()
  }));
}

export function* updateBalance(): Generator<*, *, *> {
  while (true) {
    yield put(ActionCreators.requestBalances());
    yield put(ActionCreators.requestHistories());
    yield call(delay, 30 * 1000);
  }
}

export const currencies: CryptoCurrencyType[] = ['bitcoin', 'ethereum', 'ripple'];