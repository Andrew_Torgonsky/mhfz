// @flow

import ImagePicker from 'react-native-image-crop-picker';
import { Keyboard } from 'react-native';
import { put, call, select, takeLatest } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import emailValidator from 'email-validator';
import { isValidNumber } from 'libphonenumber-js';
import { Types, ActionCreators } from '../actions';
import * as Keychain from '../../utils/keychain';
import translations, { setLanguage } from '../../translations';
import { validatePassword } from '../../utils/other';
import { Scenes, navigate } from '../../navigation';
import * as api from '../../api';
import type { ReduxStateType } from '../../types';

export function* editAccount(): Generator<*, *, *> {
  const {
    form: {
      name,
      email
    },
    session: {
      name: defaultName, email: defaultEmail, token
    }
  }: ReduxStateType = yield select();

  const newName = name || defaultName;
  const newEmail = email || defaultEmail;

  if (!emailValidator.validate(newEmail)) {
    yield put(ActionCreators.changeForm('emailError', true));
    yield put(ActionCreators.changeAlert(translations.t('INVALID_EMAIL'), 'red'));
    return;
  }
  yield call(Keyboard.dismiss);
  yield put(ActionCreators.changePending('account', true));
  const result: api.ChangeNameAndEmailResponseType = yield call(api.changeNameAndEmail, newName, newEmail, token);
  if (result.error) {
    if (result.error === 'UNAUTHORIZED') yield put(ActionCreators.logOut());
    if (result.error === 'NETWORK_ERROR') yield put(ActionCreators.changeAlert(translations.t('NETWORK_ERROR'), 'red'));
    if (result.error === 'SOMETHING_ELSE') yield put(ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red'));
    if (result.error === 'BAD_INPUT') yield put(ActionCreators.changeAlert(translations.t('INVALID_INPUT'), 'red'));
    yield put(ActionCreators.changePending('account', false));
    return;
  }
  yield put(ActionCreators.changeSessionMultiple({
    name: newName,
    email: newEmail
  }));
  yield put(ActionCreators.changeAlert(translations.t('CHANGES_SAVED'), 'green'));
  yield put(ActionCreators.changeFormMultiple({ 'name': '', 'email': '' }));
  yield put(ActionCreators.changePending('account', false));
  yield call(navigate, Scenes.Account);
}

export function* checkPinBeforeChange(): Generator<*, *, *> {
  const { form: { pinCode } }: ReduxStateType = yield select();
  const token = yield call(Keychain.get, 'mahfazahToken', pinCode);
  const wallets = yield call(Keychain.get, 'mahfazahWallets', pinCode);

  if (!token || !wallets) {
    yield put(ActionCreators.changeForm('pinCodeError', true));
    yield put(ActionCreators.changeAlert(translations.t('WRONG_PIN'), 'red'));
    yield call(delay, 2000);
    yield put(ActionCreators.changeFormMultiple({
      pinCodeError: false,
      pinCode: '',
      pinCodeConfirm: ''
    }));
    yield put(ActionCreators.changeAlert('', ''));
    return;
  }
  yield put(ActionCreators.changeForm('pinCode', ''));
  yield call(navigate, Scenes.SettingsPinEnter);
}

export function* setNewPinCode(): Generator<*, *, *> {
  const {
    form: {
      pinCode,
      pinCodeConfirm
    },
    session: {
      token
    },
    wallets
  }: ReduxStateType = yield select();
  if (pinCode !== pinCodeConfirm) {
    yield put(ActionCreators.changeForm('pinCodeError', true));
    yield put(ActionCreators.changeAlert(translations.t('PIN_NOT_MATCH'), 'red'));
    yield delay(2000);
    yield call(navigate, Scenes.SettingsPinEnter);
    yield put(ActionCreators.changeFormMultiple({
      pinCodeError: false,
      pinCode: '',
      pinCodeConfirm: ''
    }));
    yield put(ActionCreators.changeAlert('', ''));
    return;
  }
  yield put(ActionCreators.changeFormMultiple({
    pinCode: '',
    pinCodeConfirm: ''
  }));
  yield put(ActionCreators.changeAlert(translations.t('PIN_CHANGED'), 'green'));
  yield call(Keychain.set, 'mahfazahToken', pinCode, token);
  yield call(Keychain.set, 'mahfazahWallets', pinCode, JSON.stringify(wallets));
  yield call(navigate, Scenes.Settings);
}

export function* changePassword(): Generator<*, *, *> {
  const {
    form: {
      password,
      password2nd
    },
    session: {
      token
    }
  }: ReduxStateType = yield select();
  if (!validatePassword(password)) {
    yield put(ActionCreators.changeForm('passwordError', true));
    yield put(ActionCreators.changeAlert(translations.t('INVALID_PASSWORD'), 'red'));
    return;
  }
  if (!validatePassword(password2nd)) {
    yield put(ActionCreators.changeForm('passwordError2nd', true));
    yield put(ActionCreators.changeAlert(translations.t('INVALID_PASSWORD'), 'red'));
    return;
  }

  if (password === password2nd) {
    yield put(ActionCreators.changeForm('passwordError2nd', true));
    yield put(ActionCreators.changeAlert(translations.t('PWD_MATCH'), 'red'));
    return;
  }
  yield call(Keyboard.dismiss);
  yield put(ActionCreators.changePending('account', true));
  const result: api.ChangePasswordResponseType = yield call(api.changePassword, password2nd, password, token);
  yield put(ActionCreators.changePending('account', false));

  if (result.error) {
    if (result.error === 'BAD_INPUT') {
      yield put(ActionCreators.changeForm('passwordError', true));
      yield put(ActionCreators.changeAlert(translations.t('INVALID_PASSWORD'), 'red'));
    }
    if (result.error === 'UNAUTHORIZED') yield put(ActionCreators.logOut());
    if (result.error === 'NETWORK') yield put(ActionCreators.changeAlert(translations.t('NETWORK_ERROR'), 'red'));
    if (result.error === 'SOMETHING_ELSE') yield put(ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red'));
    return;
  }

  yield put(ActionCreators.changeAlert(translations.t('PWD_CHANGED'), 'green'));
  yield put(ActionCreators.changeFormMultiple({
    password: '',
    password2nd: ''
  }));
  yield call(navigate, Scenes.Settings);
}

export function* changePhone(): Generator<*, *, *> {
  const {
    form: {
      phonePrefix,
      phoneNumber
    },
    session: {
      token,
      phone_number
    }
  }: ReduxStateType = yield select();
  const phone = phonePrefix + phoneNumber;

  if (!isValidNumber(`+${phone}`)) {
    yield put(ActionCreators.changeForm('phoneError', true));
    yield put(ActionCreators.changeAlert(translations.t('WRONG_PHONE'), 'red'));
    return;
  }

  if (phone === phone_number) {
    yield put(ActionCreators.changeForm('phoneError', true));
    yield put(ActionCreators.changeAlert(translations.t('PHONE_MATCH'), 'red'));
    return;
  }
  yield call(Keyboard.dismiss);
  yield put(ActionCreators.changePending('account', true));

  const result: api.EnterNewPhoneResponseType = yield call(api.enterNewPhone, phonePrefix + phoneNumber, token);

  yield put(ActionCreators.changePending('account', false));

  if (result.error) {
    if (result.error === api.errors.BAD_INPUT) {
      yield put(ActionCreators.changeForm('phoneError', true));
      yield put(ActionCreators.changeAlert(translations.t('WRONG_PHONE'), 'red'));
    }
    if (result.error === api.errors.ALREADY_EXIST) {
      yield put(ActionCreators.changeForm('phoneError', true));
      yield put(ActionCreators.changeAlert(translations.t('PHONE_EXISTS'), 'red'));
    }
    if (result.error === api.errors.NETWORK) yield put(ActionCreators.changeAlert(translations.t('NETWORK_ERROR'), 'red'));
    if (result.error === api.errors.TOO_MANY_REQUESTS) yield put(ActionCreators.changeAlert(translations.t('TOO_MANY_REQ'), 'red'));
    if (result.error === api.errors.SOMETHING_ELSE) yield put(ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red'));
    if (result.error === api.errors.UNAUTHORIZED) yield put(ActionCreators.logOut());
    return;
  }
  yield put(ActionCreators.changeForm('smsCodeTimestamp', Date.now()));
  yield call(navigate, Scenes.SettingsCode);
}

export function* enterPhoneSms(): Generator<*, *, *> {
  const {
    form: {
      smsCode,
      phonePrefix,
      phoneNumber
    },
    session: {
      token
    }
  }: ReduxStateType = yield select();
  yield put(ActionCreators.changePending('account', true));
  const result: api.CheckSmsCodeResponseType = yield call(api.checkSmsCode, smsCode, token);
  yield put(ActionCreators.changePending('account', false));
  if (result.error) {
    if (result.error === 'BAD_INPUT') {
      yield put(ActionCreators.changeForm('smsCodeError', true));
      yield put(ActionCreators.changeAlert(translations.t('WRONG_CODE'), 'red'));
    }
    if (result.error === 'UNAUTHORIZED') yield put(ActionCreators.logOut());
    if (result.error === 'NETWORK') yield put(ActionCreators.changeAlert(translations.t('NETWORK_ERROR'), 'red'));
    if (result.error === 'SOMETHING_ELSE') yield put(ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red'));
    if (result.error === 'NO_TEMP_SESSION') {
      yield put(ActionCreators.changeAlert(translations.t('CODE_EXPIRED'), 'red'));
      yield call(navigate, Scenes.SettingsPhone);
    }
    yield put(ActionCreators.changeForm('smsCode', ''));
    return;
  }
  yield put(ActionCreators.changeSession('phone_number', phonePrefix + phoneNumber));
  yield put(ActionCreators.changeFormMultiple({
    smsCode: '',
    phoneNumber: ''
  }));
  yield put(ActionCreators.changeAlert(translations.t('PHONE_CHANGED'), 'green'));
  yield call(navigate, Scenes.Settings);
}

export function* switchToLanguage({ language }: any): Generator<*, *, *> {
  yield call(setLanguage, language);
  yield put(ActionCreators.changeAlert(translations.t('RESTART_APP'), 'green'));
  yield call(navigate, Scenes.Settings);
}

export function* changeAvatar(fromGallery: boolean): Generator<*, *, *> {
  const {
    session: {
      token
    }
  }: ReduxStateType = yield select();
  let image;
  yield put(ActionCreators.changeAppState('preventMinimize', true));
  yield call(delay, 200);
  try {
    image = yield call(fromGallery ? ImagePicker.openPicker : ImagePicker.openCamera, {
      width: 300,
      height: 400,
      cropping: true
    });
  } catch (e) {
    yield put(ActionCreators.changeAppState('preventMinimize', false));
    return;
  }
  yield put(ActionCreators.changeAppState('preventMinimize', false));
  yield put(ActionCreators.changePending('account', true));
  const result: api.ChangeAvatarResponseType = yield call(api.changeAvatar, image, token);
  if (result.error) {
    if (result.error === 'UNAUTHORIZED') yield put(ActionCreators.logOut());
    if (result.error === 'NETWORK_ERROR')  yield put(ActionCreators.changeAlert(translations.t('NETWORK_ERROR'), 'red'));
    if (result.error === 'SOMETHING_ELSE') yield put(ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red'));
    yield put(ActionCreators.changePending('account', false));
    return;
  }
  yield put(ActionCreators.changePending('account', false));
  yield put(ActionCreators.changeSession('image', result.image));
  yield call(navigate, Scenes.Account);
}

export function* selectFromGallery(): Generator<*, *, *> {
  yield call(changeAvatar, true);
}

export function* selectFromCamera(): Generator<*, *, *> {
  yield call(changeAvatar, false);
}

export function* changeCurrency({ currency }: any): Generator<*, *, *> {
  const {
    session: {
      token,
      default_currency
    }
  }: ReduxStateType = yield select();
  if (default_currency === currency) return;
  yield put(ActionCreators.changePending('account', true));
  const result: api.ChangeCurrencyResponseType = yield call(api.changeCurrency, currency, token);
  if (result.error) {
    if (result.error === 'UNAUTHORIZED') yield put(ActionCreators.logOut());
    if (result.error === 'NETWORK_ERROR') yield put(ActionCreators.changeAlert(translations.t('NETWORK_ERROR'), 'red'));
    if (result.error === 'SOMETHING_ELSE') yield put(ActionCreators.changeAlert(translations.t('SMT_WRONG'), 'red'));
    yield put(ActionCreators.changePending('account', false));
    return;
  }
  if (result.bitcoin) yield put(ActionCreators.changeWalletsStat('bitcoin', 'rate', result.bitcoin));
  if (result.ethereum) yield put(ActionCreators.changeWalletsStat('ethereum', 'rate', result.ethereum));
  if (result.ripple) yield put(ActionCreators.changeWalletsStat('ripple', 'rate', result.ripple));
  yield put(ActionCreators.changeSession('default_currency', result.currency));
  yield put(ActionCreators.changePending('account', false));
}

export function* switchNotifications(): Generator<*, *, *> {
  const status = yield call(api.getNotificationStatus);
  yield call(api.switchNotifications, !status);
  const newStatus = yield call(api.getNotificationStatus);
  yield put(ActionCreators.changeAppState('pushNotifications', newStatus));
}

export default function* accountSaga(): Generator<*, *, *> {
  yield takeLatest(Types.EDIT_ACCOUNT, editAccount);
  yield takeLatest(Types.CHECK_PIN_BEFORE_CHANGE, checkPinBeforeChange);
  yield takeLatest(Types.SET_NEW_PIN_CODE, setNewPinCode);
  yield takeLatest(Types.CHANGE_PASSWORD, changePassword);
  yield takeLatest(Types.CHANGE_PHONE, changePhone);
  yield takeLatest(Types.ENTER_PHONE_SMS, enterPhoneSms);
  yield takeLatest(Types.SWITCH_TO_LANGUAGE, switchToLanguage);
  yield takeLatest(Types.SELECT_FROM_GALLERY, selectFromGallery);
  yield takeLatest(Types.SELECT_FROM_CAMERA, selectFromCamera);
  yield takeLatest(Types.CHANGE_CURRENCY, changeCurrency);
  yield takeLatest(Types.SWITCH_NOTIFICATIONS, switchNotifications);
}
