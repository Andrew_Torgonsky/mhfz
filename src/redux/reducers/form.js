// @flow

import type { ReduxStateType } from '../../types';

export const changeForm = (state: ReduxStateType, action: any): ReduxStateType => ({
  ...state,
  form: {
    ...state.form,
    [action.key]: action.value
  }
});

export const changeFormMultiple = (state: ReduxStateType, action: any): ReduxStateType => ({
  ...state,
  form: {
    ...state.form,
    ...action.fields
  }
});

export const changeTwelveWords = (state: ReduxStateType, action: any): ReduxStateType => {
  const newWords = [...state.form.twelveWords];
  newWords[action.index] = action.word;
  return {
    ...state,
    form: {
      ...state.form,
      twelveWords: newWords
    }
  };
};

export const insertConfirmationWord = (state: ReduxStateType, action: any): ReduxStateType => {
  const newWords = [...state.form.twelveWords, action.word];
  const index2 = state.form.twelveWordsRandom.findIndex(val => val.text === action.word && val.visible);
  if (index2 === -1) {
    return state;
  }
  const newRandWords = [...state.form.twelveWordsRandom];
  newRandWords[index2] = { ...newRandWords[index2], visible: false };
  return {
    ...state,
    form: {
      ...state.form,
      twelveWords: newWords,
      twelveWordsRandom: newRandWords
    }
  };
};

export const clearTwelveWords = (state: ReduxStateType, action: any): ReduxStateType => {
  const newRandWords = state.form.twelveWordsRandom.map(val => ({ ...val, visible: true }));
  return { 
    ...state,
    form: {
      ...state.form,
      twelveWordsRandom: newRandWords,
      twelveWords: []
    }
  };
};

