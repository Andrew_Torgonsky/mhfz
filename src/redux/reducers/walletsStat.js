// @flow

import R from 'ramda';
import { ActionCreators } from '../actions';
import type { ReduxStateType, TransactionHistoryEntryType, ExtractReturnType } from '../../types';

export const changeWalletsStat = (state: ReduxStateType, action: any): ReduxStateType => ({
  ...state,
  walletsStat: {
    ...state.walletsStat,
    [action.currency]: {
      ...state.walletsStat[action.currency],
      [action.key]: action.value
    }
  }
});


export const changeWalletsStatMultiple = (state: ReduxStateType, action: any): ReduxStateType => ({
  ...state,
  walletsStat: {
    ...state.walletsStat,
    [action.currency]: {
      ...state.walletsStat[action.currency],
      ...action.fields
    }
  }
});

export const putTransactions = (state: ReduxStateType, { currency, transactions }: any): ReduxStateType => {
  // keep isSeen status for transactions that gonna be overwritten (with the same id)
  const newTransactions = R.map(
    R.when(el => R.find(R.whereEq({ id: el.id, isSeen: true }), state.walletsStat[currency].transactions))(el => ({ ...el, isSeen: true }))
  )(transactions);
  const oldTransactions = R.filter(el => !R.contains(el.id, R.pluck('id', newTransactions)))(state.walletsStat[currency].transactions);
  const allTransactions = R.pipe(
    R.uniqBy(tx => tx.id),
    R.sort((a, b) => b.timestamp - a.timestamp)
  )([...oldTransactions, ...newTransactions]);
  // switch newTx to true if there are transactions with timestamp newer than lastChecked
  const newTx = allTransactions.reduce((acc, cur) => (cur.timestamp >= state.walletsStat[currency].lastChecked && cur.amount > 0) || acc, false);
  return {
    ...state,
    walletsStat: {
      ...state.walletsStat,
      [currency]: {
        ...state.walletsStat[currency],
        transactions: allTransactions,
        newTx: newTx || state.walletsStat[currency].newTx
      }
    }
  };
}

export const checkTxAsSeen = (state: ReduxStateType, { txid }: any): ReduxStateType => {
  const { currency, transactions } = R.pipe(
    R.mapObjIndexed((walletStat, currency) => ({ currency, transactions: walletStat.transactions })),
    R.values,
    R.find(el => R.find(R.propEq('id', txid), el.transactions))
  )(state.walletsStat) || {};

  if (!currency) return state;

  const newTxs = R.map(
    R.when(R.propEq('id', txid), el => ({ ...el, isSeen: true }))
  )(transactions);

  return {
    ...state,
    walletsStat: {
      ...state.walletsStat,
      [currency]: {
        ...state.walletsStat[currency],
        transactions: newTxs
      }
    }
  };
}