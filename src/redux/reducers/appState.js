// @flow

import type { ReduxStateType } from '../../types';

export const changeAppState = (state: ReduxStateType, action: any): ReduxStateType => ({
  ...state,
  [action.key]: action.value
});
