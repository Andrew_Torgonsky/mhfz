// @flow

import type { ReduxStateType } from '../../types';

export const changePending = (state: ReduxStateType, action: any): ReduxStateType => ({
  ...state,
  pending: {
    ...state.pending,
    [action.key]: action.value
  }
});

