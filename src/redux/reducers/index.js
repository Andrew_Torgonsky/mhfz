// @flow

import { createReducer } from 'reduxsauce';
import { Types } from '../actions';
import type { ReduxStateType } from '../../types';
import { changeForm, changeFormMultiple, changeTwelveWords, clearTwelveWords, insertConfirmationWord } from './form';
import { changePending } from './pending';
import { changeWalletsStat, changeWalletsStatMultiple, putTransactions, checkTxAsSeen } from './walletsStat';
import { changeSession, changeSessionMultiple } from './session';
import { changeAppState } from './appState';

export const INITIAL_STATE: ReduxStateType = {
  form: {
    name: '',
    email: '',
    phonePrefix: '966',
    countryIso: 'sa',
    phoneNumber: '',
    password: '',
    password2nd: '',
    agree: true,
    passwordVisibility: false,
    pinCode: '',
    pinCodeConfirm: '',
    smsCode: '',
    twelveWords: [],
    twelveWordsGenerated: [],
    twelveWordsRandom: [],
    nameError: false,
    emailError: false,
    phoneError: false,
    passwordError: false,
    passwordError2nd: false,
    pinCodeError: false,
    smsCodeError: false,
    addressToSend: '',
    amountToSend: '',
    feeOptions: [],
    chosenFeeOption: 0,
    currencyToSend: 'bitcoin',
    contactToSend: undefined,
    sendPreparing: false,
    smsCodeTimestamp: 0
  },
  alertState: {
    alertText: '',
    alertType: '',
    alertTimestamp: 0
  },
  pending: {
    signIn: false,
    signUp: false,
    smsCode: false,
    logOut: false,
    auth: false,
    startUp: false,
    passwordRecovery: false,
    account: false,
    send: false,
  },
  wallets: {
    bitcoin: {
      walletAddress: '',
      privateKey: '',
    },
    ethereum: {
      walletAddress: '',
      privateKey: '',
    },
    ripple: {
      walletAddress: '',
      privateKey: '',
    }
  },
  session: {
    token: '',
    name: '',
    email: '',
    phone_number: '',
    default_currency: 'usd',
    image: '',
    created_at: '',
    updated_at: '',
  },
  walletsStat: {
    bitcoin: {
      balance: 0,
      rate: 0,
      history: [],
      newTx: false,
      alertText: '',
      alertColor: 'red',
      transactions: [],
      receivingTransactions: false,
      endOfTransactionList: false,
      lastChecked: 0
    },
    ethereum: {
      balance: 0,
      rate: 0,
      history: [],
      newTx: false,
      alertText: '',
      alertColor: 'red',
      transactions: [],
      receivingTransactions: false,
      endOfTransactionList: false,
      lastChecked: 0
    },
    ripple: {
      balance: 0,
      rate: 0,
      history: [],
      newTx: false,
      alertText: '',
      alertColor: 'red',
      transactions: [],
      receivingTransactions: false,
      endOfTransactionList: false,
      lastChecked: 0
    }
  },
  contacts: [],
  appMinimized: false,
  preventMinimize: false,
  pushNotifications: false,
  balancePeriod: 0,
};

export const reset = (state: ReduxStateType): ReduxStateType => ({
  ...INITIAL_STATE,
  appMinimized: state.appMinimized,
  form: { ...state.form }
});

export const resetForm = (state: ReduxStateType): ReduxStateType => ({
  ...state,
  form: { ...INITIAL_STATE.form }
})

export const HANDLERS = {
  [Types.CHANGE_APP_STATE]: changeAppState,
  [Types.CHANGE_PENDING]: changePending,
  // forms
  [Types.CHANGE_FORM]: changeForm,
  [Types.CHANGE_FORM_MULTIPLE]: changeFormMultiple,
  [Types.CHANGE_FORM_TWELVE_WORDS]: changeTwelveWords,
  [Types.CLEAR_TWELVE_WORDS]: clearTwelveWords,
  [Types.PICK_RANDOM_WORD]: insertConfirmationWord,
  // wallets' statitistics
  [Types.CHANGE_WALLETS_STAT]: changeWalletsStat,
  [Types.CHANGE_WALLETS_STAT_MULTIPLE]: changeWalletsStatMultiple,
  [Types.PUT_TRANSACTIONS]: putTransactions,
  [Types.CHECK_TX_AS_SEEN]: checkTxAsSeen,
  // session
  [Types.CHANGE_SESSION]: changeSession,
  [Types.CHANGE_SESSION_MULTIPLE]: changeSessionMultiple,
  // reset
  [Types.RESET]: reset,
  [Types.RESET_FORM]: resetForm,
};

export default createReducer(INITIAL_STATE, HANDLERS);
