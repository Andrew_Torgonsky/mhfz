// @flow

import type { ReduxStateType } from '../../types';

export const changeSession = (state: ReduxStateType, action: any): ReduxStateType => ({
  ...state,
  session: {
    ...state.session,
    [action.key]: action.value
  }
});

export const changeSessionMultiple = (state: ReduxStateType, action: any): ReduxStateType => ({
  ...state,
  session: {
    ...state.session,
    ...action.fields
  }
});
