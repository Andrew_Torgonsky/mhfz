import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createEpicMiddleware } from 'redux-observable';
import rootSaga from '../sagas';
import rootEpic from '../epics';

import reducers, { INITIAL_STATE } from '../reducers';

const configureStore = (initialState) => {
  const sagaMiddleware = createSagaMiddleware();
  const epicMiddleware = createEpicMiddleware();

  const middleware = [
    sagaMiddleware,
    epicMiddleware
  ];

  const store = process.env.NODE_ENV === 'development' ? createStore(
    reducers,
    initialState,
    compose(applyMiddleware(...middleware), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
  ) : createStore(
    reducers,
    initialState,
    compose(applyMiddleware(...middleware))
  );
  sagaMiddleware.run(rootSaga);
  epicMiddleware.run(rootEpic);

  return store;
};

export default configureStore(INITIAL_STATE);
