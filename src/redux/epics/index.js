// @flow

import { combineEpics } from 'redux-observable';
import { notifications } from './notifications';
import { wsConnection } from './ws';
import { rateUpdate } from './rates';
import { balanceHistoryUpdateWsWatcher } from './balanceHistory';
import { activityStateWatcher } from './activityState';


export default combineEpics(
  wsConnection,
  notifications,
  rateUpdate,
  balanceHistoryUpdateWsWatcher,
  activityStateWatcher
);
