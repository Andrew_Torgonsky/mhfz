// @flow

import { StateObservable, ofType } from 'redux-observable';
import { of, Observable, empty } from 'rxjs';
import { switchMap, withLatestFrom, map, pluck, takeUntil, retry, tap, filter, mergeMap } from 'rxjs/operators';
import { Types, ActionCreators } from '../actions';
import type { ReduxStateType, ActionType, CryptoCurrencyType } from '../../types';
import { notifications$ } from '../../api/notifications';

export const notifications = (action$: Observable<ActionType>, state$: StateObservable<ReduxStateType>): Observable<ActionType> => {
  return notifications$().pipe(
    pluck('payload', 'additionalData', 'currency'),
    mergeMap(val => {
      switch (val) {
        case 'BTC': return of(ActionCreators.updateHistory('bitcoin', false), ActionCreators.requestBalances());
        case 'ETH': return of(ActionCreators.updateHistory('ethereum', false), ActionCreators.requestBalances());
        case 'XRP': return of(ActionCreators.updateHistory('ripple', false), ActionCreators.requestBalances());
        default: return empty();
      }
    })
  );
}