// @flow

import R from 'ramda';
import { combineEpics, StateObservable, ofType } from 'redux-observable';
import { of, Observable, empty, from } from 'rxjs';
import { switchMap, withLatestFrom, map, pluck, takeUntil, retry, tap, filter, mergeMap } from 'rxjs/operators';
import { Types, ActionCreators } from '../actions';
import type { ReduxStateType, ActionType, CryptoCurrencyType } from '../../types';


export const rateUpdate = (action$: Observable<ActionType>, state$: StateObservable<ReduxStateType>): Observable<ActionType> => {
  return action$.pipe(
    ofType(Types.WS_EVENT),
    pluck('payload'),
    filter(R.propEq('type', 'exchange_rate')),
    pluck('data'),
    filter(R.is(Object)),
    mergeMap((data) => {
      const actions = [];
      if (data.BTC) actions.push(ActionCreators.changeWalletsStat('bitcoin', 'rate', data.BTC));
      if (data.ETH) actions.push(ActionCreators.changeWalletsStat('ethereum', 'rate', data.ETH));
      if (data.XRP) actions.push(ActionCreators.changeWalletsStat('ripple', 'rate', data.XRP));
      if (['USD', 'EUR', 'SAR'].includes(data.currency)) actions.push(ActionCreators.changeSession('default_currency', data.currency.toLowerCase()));
      return from(actions);
    })
  )
}