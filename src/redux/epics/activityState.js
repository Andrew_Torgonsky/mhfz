// @flwo

import { StateObservable } from 'redux-observable';
import { of, Observable, empty, from } from 'rxjs';
import { switchMap, withLatestFrom, map, pluck, takeUntil, retry, tap, filter, mergeMap } from 'rxjs/operators';
import { Types, ActionCreators } from '../actions';
import type { ReduxStateType, ActionType } from '../../types';
import { activityState$ } from '../../api/activityState';

export const activityStateWatcher = (action$: Observable<ActionType>, state$: StateObservable<ReduxStateType>): Observable<ActionType> => {
  return activityState$().pipe(
    map((state) => {
      switch(state) {
        case 'active': return ActionCreators.appMaximized();
        case 'background': return ActionCreators.appMinimized();
      }
    })
  )
}
