// @flow

import R from 'ramda';
import { combineEpics, StateObservable, ofType } from 'redux-observable';
import { of, Observable, empty, from } from 'rxjs';
import { switchMap, withLatestFrom, map, pluck, takeUntil, retry, tap, filter, mergeMap } from 'rxjs/operators';
import { Types, ActionCreators } from '../actions';
import type { ReduxStateType, ActionType, CryptoCurrencyType } from '../../types';
import { switchAbbrAndName } from '../../utils/currencies';

export const balanceHistoryUpdateWsWatcher = (action$: Observable<ActionType>, state$: StateObservable<ReduxStateType>): Observable<ActionType> => {
  return action$.pipe(
    ofType(Types.WS_EVENT),
    pluck('payload'),
    filter(R.propEq('type', 'balance')),
    pluck('data', 'updated'),
    filter(R.is(Array)),
    map(
      R.pipe(
        R.map(switchAbbrAndName),
        R.filter(el => el)
      )
    ),
    mergeMap(
      R.pipe(
        R.map(ActionCreators.updateBalanceHistory),
        from
      )
    )
  );
}
