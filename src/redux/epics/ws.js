// @flow

import R from 'ramda';
import { combineEpics, StateObservable, ofType } from 'redux-observable';
import { of, Observable, empty, from } from 'rxjs';
import { switchMap, withLatestFrom, map, pluck, takeUntil, retry, tap, filter, mergeMap } from 'rxjs/operators';
import { Types, ActionCreators } from '../actions';
import type { ReduxStateType, ActionType, CryptoCurrencyType } from '../../types';
import { ws$ } from '../../api/ws';

export const wsConnection = (action$: Observable<ActionType>, state$: StateObservable<ReduxStateType>): Observable<ActionType> => {
  return action$.pipe(
    ofType(Types.START_WS),
    withLatestFrom(state$),
    pluck('1', 'session', 'token'),
    switchMap(token => ws$(token).pipe(
      retry(),
      takeUntil(action$.pipe(
        ofType(Types.STOP_WS)
      ))
    )),
    map(ActionCreators.wsEvent)
  );
}