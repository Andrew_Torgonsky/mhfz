// @flow

import type { 
  AlertType,
  CryptoCurrencyType,
  WalletsType,
  ContactType,
  TransactionHistoryEntryType,
  BalanceHistoryEntryType,
  FiatCurrencyType,
  FormType,
  ActionType,
  ReduxStateType,
  PendingType,
  WalletsStatItemType,
  SessionType
} from '../../types';

export const Types = {
  // actions that change state directly ====================================================
  CHANGE_FORM: 'CHANGE_FORM',
  RESET_FORM: 'RESET_FORM',
  CHANGE_FORM_MULTIPLE: 'CHANGE_FORM_MULTIPLE',
  CHANGE_FORM_TWELVE_WORDS: 'CHANGE_FORM_TWELVE_WORDS',
  CLEAR_TWELVE_WORDS: 'CLEAR_TWELVE_WORDS',
  CHANGE_PENDING: 'CHANGE_PENDING',
  PICK_RANDOM_WORD: 'PICK_RANDOM_WORD',
  CHANGE_WALLETS_STAT: 'CHANGE_WALLETS_STAT',
  CHANGE_WALLETS_STAT_MULTIPLE: 'CHANGE_WALLETS_STAT_MULTIPLE',
  CHECK_TX_AS_SEEN: 'CHECK_TX_AS_SEEN',
  CHANGE_SESSION: 'CHANGE_SESSION',
  CHANGE_SESSION_MULTIPLE: 'CHANGE_SESSION_MULTIPLE',
  RESET: 'RESET',
  PUT_TRANSACTIONS: 'PUT_TRANSACTIONS',
  CHANGE_APP_STATE: 'CHANGE_APP_STATE',
  
  // actions that only trigger sagas =====================================================
  CHANGE_ALERT: 'CHANGE_ALERT',
  APP_MINIMIZED: 'APP_MINIMIZED',
  APP_MAXIMIZED: 'APP_MAXIMIZED',
  LOG_OUT: 'LOG_OUT',
  GET_CONTACTS: 'GET_CONTACTS',
  SELECT_FROM_GALLERY: 'SELECT_FROM_GALLERY',
  SELECT_FROM_CAMERA: 'SELECT_FROM_CAMERA',
  CHANGE_CURRENCY: 'CHANGE_CURRENCY',
  SWITCH_NOTIFICATIONS: 'SWITCH_NOTIFICATIONS',
  GET_NOTIFICATION: 'GET_NOTIFICATION',

  CHECK_PIN_CODE: 'CHECK_PIN_CODE',

  // Settings
  CHECK_PIN_BEFORE_CHANGE: 'CHECK_PIN_BEFORE_CHANGE',
  SET_NEW_PIN_CODE: 'SET_NEW_PIN_CODE',
  CHANGE_PASSWORD: 'CHANGE_PASSWORD',
  CHANGE_PHONE: 'CHANGE_PHONE',
  ENTER_PHONE_SMS: 'ENTER_PHONE_SMS',
  SWITCH_TO_LANGUAGE: 'SWITCH_TO_LANGUAGE',

  COMPARE_SIGN_IN_PIN_CODES: 'COMPARE_SIGN_IN_PIN_CODES',
  SUBMIT_SIGN_IN_FIELDS: 'SUBMIT_SIGN_IN_FIELDS',
  SUBMIT_SIGN_IN_TWELVE_WORDS: 'SUBMIT_SIGN_IN_TWELVE_WORDS',

  REQUEST_PASSWORD_RECOVERY: 'REQUEST_PASSWORD_RECOVERY',
  CHECK_RECOVERY_SECRET: 'CHECK_RECOVERY_SECRET',
  UPDATE_PASSWORD: 'UPDATE_PASSWORD',

  SUBMIT_SIGN_UP: 'SUBMIT_SIGN_UP',
  CHECK_SIGN_UP_SMS_CODE: 'CHECK_SIGN_UP_SMS_CODE',
  COMPARE_SIGN_UP_TWELVE_WORDS: 'COMPARE_SIGN_UP_TWELVE_WORDS',
  COMPARE_SIGN_UP_PIN_CODES: 'COMPARE_SIGN_UP_PIN_CODES',

  REQUEST_BALANCES: 'REQUEST_BALANCES',
  REQUEST_HISTORIES: 'REQUEST_HISTORIES',
  UPDATE_HISTORY: 'UPDATE_HISTORY',
  PASTE_ADDRESS_FROM_CLIPBOARD: 'PASTE_ADDRESS_FROM_CLIPBOARD',
  PREPARE_TO_SEND: 'PREPARE_TO_SEND',
  SEND: 'SEND',

  EDIT_ACCOUNT: 'EDIT_ACCOUNT',
  CHECK_TX_HISTORY: 'CHECK_TX_HISTORY',

  UPDATE_BALANCE_HISTORY: 'UPDATE_BALANCE_HISTORY',
  // epic actions ======================================================================
  START_WS: 'START_WS',
  STOP_WS: 'STOP_WS',
  WS_EVENT: 'WS_EVENT',
};


export const ActionCreators = {
  changeForm: (key: $Keys<FormType>, value: any): ActionType => ({ type: Types.CHANGE_FORM, key, value }),
  changeAppState: (key: $Keys<ReduxStateType>, value: any): ActionType => ({ type: Types.CHANGE_APP_STATE, key, value }),
  resetForm: (): ActionType => ({ type: Types.RESET_FORM }),
  changeFormMultiple: (fields: $Shape<FormType>): ActionType => ({ type: Types.CHANGE_FORM_MULTIPLE, fields }),
  changeFormTwelveWords: (word: string, index: number): ActionType => ({
    type: Types.CHANGE_FORM_TWELVE_WORDS, word, index
  }),
  clearTwelveWords: (): ActionType => ({ type: Types.CLEAR_TWELVE_WORDS }),
  changeAlert: (alertText: string, alertType: AlertType): ActionType => ({ type: Types.CHANGE_ALERT, alertText, alertType }),
  changePending: (key: $Keys<PendingType>, value: boolean): ActionType => ({ type: Types.CHANGE_PENDING, key, value }),
  pickRandomWord: (word: string): ActionType => ({ type: Types.PICK_RANDOM_WORD, word }),
  changeWalletsStat: (currency: CryptoCurrencyType, key: $Keys<WalletsStatItemType>, value: any): ActionType => ({
    type: Types.CHANGE_WALLETS_STAT, currency, key, value
  }),
  changeWalletsStatMultiple: (currency: CryptoCurrencyType, fields: $Shape<WalletsStatItemType>): ActionType => ({
    type: Types.CHANGE_WALLETS_STAT_MULTIPLE, currency, fields
  }),
  putTransactions: (currency: CryptoCurrencyType, transactions: TransactionHistoryEntryType[]): ActionType => ({
    type: Types.PUT_TRANSACTIONS,
    currency,
    transactions
  }),
  checkTxAsSeen: (txid: string): ActionType => ({
    type: Types.CHECK_TX_AS_SEEN,
    txid
  }),
  changeSession: (key: $Keys<SessionType>, value: any): ActionType => ({ type: Types.CHANGE_SESSION, key, value }),
  changeSessionMultiple: (fields: $Shape<SessionType>): ActionType => ({ type: Types.CHANGE_SESSION_MULTIPLE, fields }),

  reset: (): ActionType => ({ type: Types.RESET }),
  appMinimized: (): ActionType => ({ type: Types.APP_MINIMIZED }),
  appMaximized: (): ActionType => ({ type: Types.APP_MAXIMIZED }),

  getContacts: (): ActionType => ({ type: Types.GET_CONTACTS }),

  // Settings
  checkPinBeforeChange: (): ActionType => ({ type: Types.CHECK_PIN_BEFORE_CHANGE }),
  setNewPinCode: (): ActionType => ({ type: Types.SET_NEW_PIN_CODE }),

  changePassword: (): ActionType => ({ type: Types.CHANGE_PASSWORD }),
  changePhone: (): ActionType => ({ type: Types.CHANGE_PHONE }),
  enterPhoneSms: (): ActionType => ({ type: Types.ENTER_PHONE_SMS }),
  switchToLanguage: (language: string): ActionType => ({ type: Types.SWITCH_TO_LANGUAGE, language }),
  selectFromGallery: (): ActionType => ({ type: Types.SELECT_FROM_GALLERY }),
  selectFromCamera: (): ActionType => ({ type: Types.SELECT_FROM_CAMERA }),
  changeCurrency: (currency: FiatCurrencyType): ActionType => ({ type: Types.CHANGE_CURRENCY, currency }),
  switchNotifications: (): ActionType => ({ type: Types.SWITCH_NOTIFICATIONS }),
  // =================

  logOut: (): ActionType => ({ type: Types.LOG_OUT }),

  checkPinCode: (): ActionType => ({ type: Types.CHECK_PIN_CODE }),

  compareSignInPinCodes: (): ActionType => ({ type: Types.COMPARE_SIGN_IN_PIN_CODES }),
  submitSignInFields: (): ActionType => ({ type: Types.SUBMIT_SIGN_IN_FIELDS }),
  submitSignInTwelveWords: (): ActionType => ({ type: Types.SUBMIT_SIGN_IN_TWELVE_WORDS }),

  requestPasswordRecovery: (): ActionType => ({ type: Types.REQUEST_PASSWORD_RECOVERY }),
  checkRecoverySecret: (): ActionType => ({ type: Types.CHECK_RECOVERY_SECRET }),
  updatePassword: (): ActionType => ({ type: Types.UPDATE_PASSWORD }),

  submitSignUp: (): ActionType => ({ type: Types.SUBMIT_SIGN_UP }),
  checkSignUpSmsCode: (): ActionType => ({ type: Types.CHECK_SIGN_UP_SMS_CODE }),
  compareSignUpTwelveWords: (): ActionType => ({ type: Types.COMPARE_SIGN_UP_TWELVE_WORDS }),
  compareSignUpPinCodes: (): ActionType => ({ type: Types.COMPARE_SIGN_UP_PIN_CODES }),

  requestBalances: (): ActionType => ({ type: Types.REQUEST_BALANCES }),
  requestHistories: (): ActionType => ({ type: Types.REQUEST_HISTORIES }),
  updateHistory: (currency: CryptoCurrencyType, loadMore: boolean) => ({ type: Types.UPDATE_HISTORY, currency, loadMore }),
  pasteAddressFromClipBoard: (): ActionType => ({ type: Types.PASTE_ADDRESS_FROM_CLIPBOARD }),
  prepareToSend: (): ActionType => ({ type: Types.PREPARE_TO_SEND }),
  send: (toContact: boolean): ActionType => ({ type: Types.SEND, toContact }),

  editAccount: (): ActionType => ({ type: Types.EDIT_ACCOUNT }),

  checkTxHistory: (currency: CryptoCurrencyType): ActionType => ({ type: Types.CHECK_TX_HISTORY, currency }),

  startWs: (): ActionType => ({ type: Types.START_WS }),
  stopWs: (): ActionType => ({ type: Types.STOP_WS }),
  wsEvent: (payload: any): ActionType => ({ type: Types.WS_EVENT, payload }),
  updateBalanceHistory: (currency: CryptoCurrencyType): ActionType => ({ type: Types.UPDATE_BALANCE_HISTORY, currency }),
};
