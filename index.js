import { AppRegistry } from 'react-native';
// import process from 'process';
// import buffer from 'buffer';

import './shim';
// import crypto from 'crypto';

import App from './src/App';

AppRegistry.registerComponent('MyApp', () => App);
