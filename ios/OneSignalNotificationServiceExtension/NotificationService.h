//
//  NotificationService.h
//  OneSignalNotificationServiceExtension
//
//  Created by Andrey on 14.08.18.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
