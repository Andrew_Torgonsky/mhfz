package io.mahfazah;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.geektime.rnonesignalandroid.ReactNativeOneSignalPackage;
import com.levelasquez.androidopensettings.AndroidOpenSettingsPackage;
import org.reactnative.camera.RNCameraPackage;
import cl.json.RNSharePackage;
import io.mahfazah.BuildConfig;

import com.rt2zz.reactnativecontacts.ReactNativeContacts;
import com.BV.LinearGradient.LinearGradientPackage;
import com.surajit.rnrg.RNRadialGradientPackage;
import com.AlexanderZaytsev.RNI18n.RNI18nPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.bitgo.randombytes.RandomBytesPackage;
import com.oblador.keychain.KeychainPackage;
import com.horcrux.svg.SvgPackage;
import com.bugsnag.BugsnagReactNative;
import com.reactnative.ivpusic.imagepicker.PickerPackage;


import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNDeviceInfo(),
            new ReactNativeOneSignalPackage(),
            new AndroidOpenSettingsPackage(),
            new RNCameraPackage(),
            new RNSharePackage(),
            new ReactNativeContacts(),
            new LinearGradientPackage(),
            new RNRadialGradientPackage(),
            new RNI18nPackage(),
            new RandomBytesPackage(),
            new KeychainPackage(),
            new SvgPackage(),
            new PickerPackage(),
            BugsnagReactNative.getPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    BugsnagReactNative.start(this);
    SoLoader.init(this, /* native exopackage */ false);
  }
}
